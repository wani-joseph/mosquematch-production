<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::view('/{path?}', 'app');
Route::view('/admin/{path?}', 'app');
Route::view('/admin/mosques/{path?}', 'app');
Route::view('/admin/mosques/view-mosque/{path?}', 'app');
Route::view('/admin/mosques/edit-mosque/{path?}', 'app');
Route::view('/admin/members/{path?}', 'app');
Route::view('/admin/members/view-member/{path?}', 'app');
Route::view('/admin/members/edit-member/{path?}', 'app');
Route::view('/admin/users/{path?}', 'app');
Route::view('/setup-profile/{path?}', 'app');
Route::view('/member/{path?}', 'app');
Route::view('/profile/{path?}/about-me', 'app');
Route::view('/profile/{path?}/basic-information', 'app');
Route::view('/profile/{path?}/education-work', 'app');
Route::view('/profile/{path?}/personal-information', 'app');
Route::view('/profile/{path?}/religion', 'app');
Route::view('/profile/{path?}/photos', 'app');
Route::view('/messages/{path?}', 'app');
Route::view('/messages/chat/{path?}', 'app');
Route::view('/interest/{path?}', 'app');
Route::view('/accounts/password/{path?}', 'app');
Route::view('/accounts/password/edit/{path?}', 'app');
