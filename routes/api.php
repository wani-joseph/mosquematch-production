<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/**
 * User Routes
 */
Route::resource('users', 'User\UserController', ['except' => ['create', 'edit']]);
Route::resource('users.albums', 'User\UserAlbumController', ['only' => ['index', 'store', 'update', 'destroy']]);
Route::resource('users.filters', 'User\UserSearchController', ['only' => ['index']]);
Route::resource('users.blocked', 'User\UserBlockedController', ['only' => ['index', 'store', 'destroy']]);
Route::resource('users.blockers', 'User\UserBlockerController', ['only' => ['index']]);
Route::resource('users.followings', 'User\UserFollowingController', ['only' => ['index', 'store', 'destroy']]);
Route::resource('users.followers', 'User\UserFollowerController', ['only' => ['index']]);
Route::resource('users.messages', 'User\UserMessageController', ['only' => ['index', 'store']]);
Route::resource('users.messages-unread', 'User\UserMessageUnreadController', ['only' => ['index']]);
Route::resource('users.inbox', 'User\UserInboxController', ['only' => ['index']]);
Route::resource('users.outbox', 'User\UserOutboxController', ['only' => ['index']]);
Route::resource('users.offline', 'User\UserOfflineController', ['only' => ['index']]);
Route::resource('users.online', 'User\UserOnlineController', ['only' => ['index']]);
Route::resource('users.photo-requested', 'User\UserPhotoRequestedController', ['only' => ['index', 'store', 'destroy']]);
Route::resource('users.photo-requesters', 'User\UserPhotoRequesterController', ['only' => ['index']]);
Route::resource('users.reported', 'User\UserReportedController', ['only' => ['index', 'store', 'destroy']]);
Route::resource('users.reporters', 'User\UserReporterController', ['only' => ['index']]);
Route::resource('users.saved-searches', 'User\UserSavedSearchController', ['only' => ['index', 'store', 'destroy']]);
Route::resource('users.templates', 'User\UserTemplateController', ['only' => ['index', 'store', 'destroy']]);
Route::resource('users.visited', 'User\UserVisitedController', ['only' => ['index', 'store', 'destroy']]);
Route::resource('users.visitors', 'User\UserVisitorController', ['only' => ['index']]);
Route::resource('users.wali-requested', 'User\UserWaliRequestedController', ['only' => ['index', 'store', 'destroy']]);
Route::resource('users.wali-requesters', 'User\UserWaliRequesterController', ['only' => ['index']]);


/**
 * Album Routes
 */
Route::resource('albums', 'Album\AlbumController', ['only' => ['index', 'show']]);
Route::resource('albums.images', 'Album\AlbumImageController', ['only' => ['index', 'store', 'destroy']]);

/**
 * Image Routes
 */
Route::resource('images', 'Image\ImageController', ['only' => ['index', 'show', 'update']]);

/**
 * Block Routes
 */
Route::resource('blocks', 'Block\BlockController', ['only' => ['index', 'show', 'update']]);

/**
 * Favorite Routes
 */
Route::resource('favorites', 'Favorite\FavoriteController', ['only' => ['index', 'show', 'update']]);

/**
 * Message Routes
 */
Route::resource('messages', 'Message\MessageController', ['only' => ['index', 'show', 'update']]);

/**
 * PrivatePhoto Routes
 */
Route::resource('private-photos', 'PrivatePhoto\PrivatePhotoController', ['only' => ['index', 'show', 'update']]);

/**
 * Report Routes
 */
Route::resource('reports', 'Report\ReportController', ['only' => ['index', 'show', 'update']]);

/**
 * SavedSearch Routes
 */
Route::resource('saved-searchs', 'SavedSearch\SavedSearchController', ['only' => ['index', 'show']]);

/**
 * Template Routes
 */
Route::resource('templates', 'Template\TemplateController', ['only' => ['index', 'show']]);

/**
 * Visit Routes
 */
Route::resource('visits', 'Visit\VisitController', ['only' => ['index', 'show', 'update']]);

/**
 * Wali Routes
 */
Route::resource('walis', 'Wali\WaliController', ['only' => ['index', 'show', 'update', 'destroy']]);

/**
 * Mosque Routes
 */
Route::resource('mosques', 'Mosque\MosqueController', ['except' => ['create', 'edit']]);
Route::resource('mosques.users', 'Mosque\MosqueUserController', ['only' => ['index', 'update', 'destroy']]);
Route::resource('mosques.users.confirm', 'Mosque\MosqueUserConfirmController', ['only' => 'index']);
Route::resource('mosques.users.reject', 'Mosque\MosqueUserRejectController', ['only' => 'index']);
Route::post('mosques-search','Mosque\MosqueController@getSearch')->name('mosques.search');
Route::post('mosques-search-by-location','Mosque\MosqueController@getMosquesByLocation')->name('mosques.search-by-location');

/**
 * PasswordReset Routes
 */
Route::group([      
  'prefix' => 'password'
], function () {    
  Route::post('create', 'PasswordReset\PasswordResetController@create');
  Route::get('find/{token}', 'PasswordReset\PasswordResetController@find');
  Route::post('reset', 'PasswordReset\PasswordResetController@reset');
});

/**
 * Auth Routes
 */
Route::post('/register', 'Auth\ApiAuthController@register')->name('register');
Route::post('/login', 'Auth\ApiAuthController@login')->name('login');
Route::get('/logout', 'Auth\ApiAuthController@logout')->name('logout');
Route::get('/auth-user', 'Auth\ApiAuthController@authUser')->name('auth-user');
