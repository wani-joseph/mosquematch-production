@component('mail::message')
# Hi {{$passwordReset->email}}!

Looks like you have forgotten your password! If so, click the link below to create a new password:

[Reset your password]({{ config('app.url') }}/accounts/password/edit/{{ $passwordReset->token }})

If you continue to have problems accessing your account please feel free to contact us at <a>support@mosquematch.com<a>

If you didn't request this, please ignore this email.

Thanks,<br>
{{ config('app.name') }}
@endcomponent