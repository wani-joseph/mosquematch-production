@component('mail::message')
# Hello {{$mosque->imam}}

Thank you for being a part of our application
Please confirm that {{$user->firstname}} {{$user->lastname}} is a member of your mosque using this links below:

@component('mail::button', ['url' => route('mosques.users.confirm.index', [$mosque->id, $user->id])])
Confirm
@endcomponent

@component('mail::button', ['url' => route('mosques.users.reject.index', [$mosque->id, $user->id])])
Reject
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent