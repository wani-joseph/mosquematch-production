@component('mail::message')
# Hello {{$user->firstname}} {{$user->lastname}}

Your membership to {{$mosque->name}} has been rejected by {{$mosque->imam}}
In order to solve this issue you have to:
1- Contact the Mosque's Imam {{$mosque->contact}}.
2- Visit the Mosque at {{$mosque->address}} to meet the Imam in person.
3- You can get verified by another Mosque

Thanks,<br>
{{ config('app.name') }}
@endcomponent