@component('mail::message')
# Hello {{$user->firstname}} {{$user->lastname}}

Your membership to {{$mosque->name}} has been approved by {{$mosque->imam}}
Next you need to verify your email using the button below:

@component('mail::button', ['url' => route('verify', $user->verification_token)])
Verify Account
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent