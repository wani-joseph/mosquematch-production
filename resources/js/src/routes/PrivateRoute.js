import React from 'react'
import { Redirect, Route } from 'react-router-dom'

const PrivateRoute = ({ component: RouteComponent, ...rest }) => {
    const token = window.localStorage.getItem('token')

    return (
        <Route
            {...rest}
            render={routeProps =>
                token != null ? (
                    <RouteComponent {...routeProps} />
                ) : (
                    <Redirect to={"/login"} />
                )
            }
        />
    )
}

export default PrivateRoute