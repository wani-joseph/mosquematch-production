import React, { Component } from 'react'
import { connect } from 'react-redux'
import Echo from 'laravel-echo'
import { appRoutes } from './routes'
import * as actions from './store/actions'

class App extends Component {
    componentDidMount () {
        const { onTryAutoSignup } = this.props
        onTryAutoSignup()

        window.Echo = new Echo({
            broadcaster: 'pusher',
            key: 'bf769a6003a2b83adc37',
            cluster: 'eu',
            forceTLS: true,
            auth: {
                headers: {
                    Authorization: "Bearer " + localStorage.getItem('token')
                }
            }
        });
    }

    render() {
        return (
            <React.Fragment>
                {appRoutes}
            </React.Fragment>
        );
    }
}

const mapDispatchToProps = dispatch => ({
    onTryAutoSignup: () => dispatch(actions.authCheckState())
})

export default connect(null, mapDispatchToProps)(App)