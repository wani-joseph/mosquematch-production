import { updateObject } from '../../shared/utility'
import {
  ADMIN_RESOURCE_START,
  ADMIN_RESOURCE_FAIL,
  FETCH_RESOURCE_SUCCESS,
  VIEW_RESOURCE_SUCCESS
} from '../actions/actionTypes'

const initialState = {
  loading: false,
  resourceList: [],
  resource: null,
  errors: null,
  per: 10,
  page: 1,
  total: null,
  totalPages: null,
}

const resourceStart = (state, action) => {
  return updateObject(state, { loading: true })
}

const resourceFail = (state, action) => {
  return updateObject(state, {
    loading: false,
    resourceList: [],
    resource: null,
    errors: action.errors,
    per: 10,
    page: 1,
    total: null,
    totalPages: null,
  })
}

const fetchResourceSuccess = (state, action) => {
  console.log(action.resource)
}

const viewResourceSuccess = (state, action) => {
  return updateObject(state, {
    resource: action.resource
  })
}

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case FETCH_RESOURCE_SUCCESS: return fetchResourceSuccess(state, action)
    case ADMIN_RESOURCE_FAIL: return resourceFail(state, action)
    case ADMIN_RESOURCE_START: return resourceStart(state, action)
    case VIEW_RESOURCE_SUCCESS: return viewResourceSuccess(state, action)
    default: return state
  }
}

export default reducer