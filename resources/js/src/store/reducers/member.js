import * as actionTypes from '../actions/actionTypes';
import { updateObject } from '../../shared/utility';
import WaliRequest from '../../components/WaliRequest/WaliRequest';

const initialState = {
    members: [],
    errors: null,
    loading: false,
    pagination: null,
    member: null,
    templates: [],
    favoriteList: [],
    waliRequests: [],
    messages: [],
    inboxMessages: [],
    conversation: [],
}

const membersStart = (state, action) => {
    return updateObject(state, { loading: true} );
}

const memberActionStart = (state, action) => {
    return updateObject(state, { loading: true} );
}

const memberActionFail = (state, action) => {
    return updateObject(state, {
        errors: action.errors
    })
}

const updateMemberSuccess = (state, action) => {
    return updateObject(state, { loading: false })
}

const membersSuccess = (state, action) => {
    return updateObject(state, {
        loading: false,
        errors: null,
        members: action.members
    })
}

const membersFail = (state, action) => {
    return updateObject(state, {
        loading: false,
        errors: action.error
    });
}

const viewMember = (state, action) => {
    return updateObject(state, {
        loading: false,
        member: action.member
    })
}

const memberTemplatesSuccess = (state, action) => {
    return updateObject(state, {
        templates: action.templates,
        errors: null
    })
}

const memberTemplatesFail = (state, action) => {
    return updateObject(state, {
        templates: [],
        errors: action.errors
    })
}

const memberFavListSuccess = (state, action) => {
    return updateObject(state, {
        favoriteList: action.favList,
        errors: null
    })
}

const memberFavListFail = (state, action) => {
    return updateObject(state, {
        favoriteList: [],
        errors: action.errors
    })
}

const removeFromFavoriteSuccess = (state, action) => {
    return updateObject(state, {
        favoriteList: state.favoriteList.filter(favorite => {
            return favorite.identifier !== action.favorite.identifier
        })
    })
}

const removeFromFavoriteFail = (state, action) => {
    return updateObject(state, {
        errors: action.errors
    })
}

const addToFavoriteSuccess = (state, action) => {
    return updateObject(state, {
        favoriteList: [...state.favoriteList, action.favorite]
    })
}

const addToFavoriteFail = (state, action) => {
    return updateObject(state, {
        errors: action.errors
    })
}

const addTemplateSuccess = (state, action) => {
    return updateObject(state, {
        templates: [...state.templates, action.template]
    })
}

const addTemplateFail = (state, action) => {
    return updateObject(state, {
        errors: action.errors
    })
}

const deleteTemplateSuccess = (state, action) => {
    return updateObject(state, {
        templates: state.templates.filter(template => {
            return template.identifier !== action.template.identifier
        })
    })
}

const deleteTemplateFail = (state, action) => {
    return updateObject(state, {
        errors: action.errors
    })
}

const memberWaliRequestSuccess = (state, action) => {
    return updateObject(state, {
        waliRequests: action.waliRequests,
    })
}

const memberWaliRequestFail = (state, action) => {
    return updateObject(state, {
        waliRequests: [],
        errors: action.errors
    })
}

const memberMessagesSuccess = (state, action) => {
    return updateObject(state, {
        loading: false,
        messages: action.messages
    })
}

const memberInboxSuccess = (state, action) => {
    return updateObject(state, {
        loading: false,
        inboxMessages: action.messages
    })
}

const memberConversationSuccess = (state, action) => {
    return updateObject(state, {
        loading: false,
        conversation: action.messages
    })
}

const newMessageSuccess = (state, action) => {
    return updateObject(state, {
        conversation: [...state.conversation, action.message]
    })
}

const newInboxSuccess = (state, action) => {
    return updateObject(state, {
        inboxMessages: [...state.inboxMessages, action.message]
    })
}

const confirmWaliRequest = (state, action) => {
    const updatedWaliRequests = state.waliRequests.filter(waliRequest => {
        return waliRequest.identifier !== action.waliRequest.identifier
    })

    return updateObject(state, {
        waliRequests: [...updatedWaliRequests, action.waliRequest]
    })
}

const denyWaliRequest = (state, action) => {
    return updateObject(state, {
        waliRequests: state.waliRequests.filter(waliRequest => {
            return waliRequest.identifier !== action.waliRequest.identifier
        })
    })
}

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.MEMBERS_START: return membersStart(state, action)
        case actionTypes.MEMBERS_SUCCESS: return membersSuccess(state, action)
        case actionTypes.MEMBERS_FAIL: return membersFail(state, action)
        case actionTypes.VIEW_MEMBER: return viewMember(state, action)
        case actionTypes.MEMBER_ACTION_START: return memberActionStart(state, action)
        case actionTypes.MEMBER_ACTION_FAIL: return memberActionFail(state, action)
        case actionTypes.UPDATE_MEMBER_SUCCESS: return updateMemberSuccess(state, action)
        case actionTypes.MEMBER_TEMPLATES_SUCCESS: return memberTemplatesSuccess(state, action)
        case actionTypes.MEMBER_TEMPLATES_FAIL: return memberTemplatesFail(state, action)
        case actionTypes.MEMBER_FAVLIST_SUCCESS: return memberFavListSuccess(state, action)
        case actionTypes.MEMBER_FAVLIST_FAIL: return memberFavListFail(state, action)
        case actionTypes.REMOVE_FROM_FAVLIST_SUCCESS: return removeFromFavoriteSuccess(state, action)
        case actionTypes.REMOVE_FROM_FAVLIST_FAIL: return removeFromFavoriteFail(state, action)
        case actionTypes.ADD_TO_FAVLIST_SUCCESS: return addToFavoriteSuccess(state, action)
        case actionTypes.ADD_TO_FAVLIST_FAIL: return addToFavoriteFail(state, action)
        case actionTypes.ADD_TEMPLATE_SUCCESS: return addTemplateSuccess(state, action)
        case actionTypes.ADD_TEMPLATE_FAIL: return addTemplateFail(state, action)
        case actionTypes.DELETE_TEMPLATE_SUCCESS: return deleteTemplateSuccess(state, action)
        case actionTypes.DELETE_TEMPLATE_FAIL: return deleteTemplateFail(state, action)
        case actionTypes.WALI_REQUESTS_SUCCESS: return memberWaliRequestSuccess(state, action)
        case actionTypes.WALI_REQUESTS_FAIL: return memberWaliRequestFail(state, action)
        case actionTypes.CONFIRM_WALI_REQUEST: return confirmWaliRequest(state, action)
        case actionTypes.DENY_WALI_REQUEST: return denyWaliRequest(state, action)
        case actionTypes.MEMBER_MESSAGES_SUCCESS: return memberMessagesSuccess(state, action)
        case actionTypes.MEMBER_INBOX_SUCCESS: return memberInboxSuccess(state, action)
        case actionTypes.MEMBER_CONVERSATION_SUCCESS: return memberConversationSuccess(state, action)
        case actionTypes.NEW_MESSAGE_SUCCESS: return newMessageSuccess(state, action)
        case actionTypes.NEW_INBOX_SUCCESS: return newInboxSuccess(state, action)
        default: return state
    }
}

export default reducer