import * as actionTypes from '../actions/actionTypes'
import { updateObject } from '../../shared/utility'

const initialState = {
    loading: false,
    errors: null,
    token: null,
    authUser: null,
    message: null,
    signupRedirectPath: '/setup-profile/gender',
    signinRedirectPath: '/search',
    favlist: [],
    templates: [],
    viewedList: [],
    inboxCount: 0,
    requestsCount: 0,
    viewedMeCount: 0,
    favoritedMeCount: 0,
    reportedMeCount: 0,
}

const authStart = (state, action) => {
    return updateObject(state, { loading: true })
}

const authSuccess = (state, action) => {
    return updateObject(state, {
        token: action.token,
        errors: null,
        loading: false,
    })
}

const authFail = (state, action) => {
    return updateObject(state, {
        loading: false,
        errors: action.errors,
        token: null,
        authUser: null,
    })
}

const invalidCredentials = (state, action) => {
    return updateObject(state, {
        loading: false,
        errors: null,
        token: null,
        message: action.message
    })
}

const authUser = (state, action) => {
    return updateObject(state, { authUser: action.user })
}

const updateUser = (state, action) => {
    return updateObject(state, { loading: false })
}

const authLogout = (state, action) => {
    return updateObject(state, {
        loading: false,
        errors: null,
        token: null,
        authUser: null,
    })
}

const setAuthRedirectPath = (state, action) => {
    return updateObject(state, { signinRedirectPath: action.path })
}

const forgetPasswordSuccess = (state, action) => {
    return updateObject(state, { loading: false })
}

const userFavList = (state, action) => {
    return updateObject(state, { favlist: action.favlist })
}

const userTemplates = (state, action) => {
    return updateObject(state, { templates: action.templates })
}

const userViewedList = (state, action) => {
    return updateObject(state, { viewedList: action.viewedList })
}

const inboxCount = (state, action) => {
    return updateObject(state, { inboxCount: action.count })
}
  
const requestsCount = (state, action) => {
    return updateObject(state, { requestsCount: action.count })
}
  
const viewedMeCount = (state, action) => {
    return updateObject(state, { viewedMeCount: action.count })
}

const favoritedMeCount = (state, action) => {
    return updateObject(state, { favoritedMeCount: action.count })
}

const reportedMeCount = (state, action) => {
    return updateObject(state, { reportedMeCount: action.count })
}

const confirmInbox = (state, action) => {
    return updateObject(state, { inboxCount: action.updatedCount })
}

const confirmRequests = (state, action) => {
    return updateObject(state, {
        requestsCount: state.requestsCount - 1
    })
}

const confirmViewedMe = (state, action) => {
    return updateObject(state, { viewedMeCount: 0 })
}

const confirmFavoritedMe = (state, action) => {
    return updateObject(state, { favoritedMeCount: 0 })
}

const confirmReportedMe = (state, action) => {
    return updateObject(state, { reportedMeCount: 0 })
}

const notificationFail = (state, action) => {
    return updateObject(state, {
        inboxCount: 0,
        requestsCount: 0,
        viewedMeCount: 0,
        favoritedMeCount: 0,
        reportedMeCount: 0,
        errors: action.errors
    })
}

const removeFromFavoriteSuccess = (state, action) => {
    return updateObject(state, {
        favlist: state.favlist.filter(favorite => {
            return favorite.identifier !== action.favorite.identifier
        })
    })
}

const removeFromFavoriteFail = (state, action) => {
    return updateObject(state, {
        errors: action.errors
    })
}

const addToFavoriteSuccess = (state, action) => {
    return updateObject(state, {
        favlist: [...state.favlist, action.favorite]
    })
}

const addToFavoriteFail = (state, action) => {
    return updateObject(state, {
        errors: action.errors
    })
}

const addTemplateSuccess = (state, action) => {
    return updateObject(state, {
        templates: [...state.templates, action.template]
    })
}

const addTemplateFail = (state, action) => {
    return updateObject(state, {
        errors: action.errors
    })
}

const deleteTemplateSuccess = (state, action) => {
    return updateObject(state, {
        templates: state.templates.filter(template => {
            return template.identifier !== action.template.identifier
        })
    })
}

const deleteTemplateFail = (state, action) => {
    return updateObject(state, {
        errors: action.errors
    })
}

const addToViewListSuccess = (state, action) => {
    if (state.viewedList != null) {
        return updateObject(state, {
            viewedList: [...state.viewedList, action.visit]
        })
    } else {
        return updateObject(state, {
            viewedList: action.visit
        })
    }
}

const addToViewListFail = (state, action) => {
    return updateObject(state, {
        errors: action.errors
    })
}

const removeFromVisitSuccess = (state, action) => {
    return updateObject(state, {
        viewedList: state.viewedList.filter(visit => {
            return visit.identifier !== action.visit.identifier
        })
    })
}

const removeFromVisitFail = (state, action) => {
    return updateObject(state, {
        errors: action.errors
    })
}

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.ADD_TEMPLATE_SUCCESS: return addTemplateSuccess(state, action)
        case actionTypes.ADD_TEMPLATE_FAIL: return addTemplateFail(state, action)
        case actionTypes.DELETE_TEMPLATE_SUCCESS: return deleteTemplateSuccess(state, action)
        case actionTypes.DELETE_TEMPLATE_FAIL: return deleteTemplateFail(state, action)
        case actionTypes.REMOVE_FROM_FAVLIST_SUCCESS: return removeFromFavoriteSuccess(state, action)
        case actionTypes.REMOVE_FROM_FAVLIST_FAIL: return removeFromFavoriteFail(state, action)
        case actionTypes.REMOVE_FROM_VIEWEDLIST_SUCCESS: return removeFromVisitSuccess(state, action)
        case actionTypes.REMOVE_FROM_VIEWEDLIST_FAIL: return removeFromVisitFail(state, action)
        case actionTypes.ADD_TO_VIEWEDLIST_SUCCESS: return addToViewListSuccess(state, action)
        case actionTypes.ADD_TO_VIEWEDLIST_FAIL: return addToViewListFail(state, action)
        case actionTypes.ADD_TO_FAVLIST_SUCCESS: return addToFavoriteSuccess(state, action)
        case actionTypes.ADD_TO_FAVLIST_FAIL: return addToFavoriteFail(state, action)
        case actionTypes.NOTIFICATION_FAIL: return notificationFail(state, action)
        case actionTypes.CONFIRM_INBOX: return confirmInbox(state, action)
        case actionTypes.CONFIRM_REQUESTS: return confirmRequests(state, action)
        case actionTypes.CONFIRM_REPORTEDME: return confirmReportedMe(state, action)
        case actionTypes.CONFIRM_FAVORITEDME: return confirmFavoritedMe(state, action)
        case actionTypes.CONFIRM_VIEWEDME: return confirmViewedMe(state, action)
        case actionTypes.REPORTEDME_COUNT: return reportedMeCount(state, action)
        case actionTypes.FAVORITEDME_COUNT: return favoritedMeCount(state, action)
        case actionTypes.VIEWEDME_COUNT: return viewedMeCount(state, action)
        case actionTypes.REQUESTS_COUNT: return requestsCount(state, action)
        case actionTypes.INBOX_COUNT: return inboxCount(state, action)
        case actionTypes.AUTH_START: return authStart(state, action)
        case actionTypes.AUTH_SUCCESS: return authSuccess(state, action)
        case actionTypes.INVALID_CREDENTIALS: return invalidCredentials(state, action)
        case actionTypes.UPDATE_USER: return updateUser(state, action)
        case actionTypes.AUTH_USER: return authUser(state, action)
        case actionTypes.AUTH_FAIL: return authFail(state, action)
        case actionTypes.AUTH_LOGOUT: return authLogout(state, action)
        case actionTypes.SET_AUTH_REDIRECT_PATH: return setAuthRedirectPath(state, action)
        case actionTypes.FORGET_PASSWORD_SUCCESS: return forgetPasswordSuccess(state, action)
        case actionTypes.USER_FAVORITE_LIST: return userFavList(state, action)
        case actionTypes.USER_TEMPLATES: return userTemplates(state, action)
        case actionTypes.USER_VIEWED_LIST: return userViewedList(state, action)
        default: return state
    }
}

export default reducer