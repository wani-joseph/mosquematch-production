import * as actionTypes from '../actions/actionTypes'
import { updateObject } from '../../shared/utility'

const initialState = {
    loading: false,
    errors: null,
    favoritedMeList: null,
    viewedMeList: null,
    viewedList: null,
    reportedMeList: null,
    reportedList: null,
    blockedList: null
}

const memberInterestStart = (state, action) => {
    return updateObject(state, { loading: true })
}

const memberInterestFail = (state, action) => {
    return updateObject(state, {
        loading: false,
        errors: action.errors
    })
}

const viewedMeInterestSuccess = (state, action) => {
    return updateObject(state, {
        loading: false,
        viewedMeList: action.viewedMeList
    })
}

const viewedInterestSuccess = (state, action) => {
    return updateObject(state, {
        loading: false,
        viewedList: action.viewedList
    })
}

const favoritedMeInterestSuccess = (state, action) => {
    return updateObject(state, {
        loading: false,
        favoritedMeList: action.favoritedMeList
    })
}

const reportedMeInterestSuccess = (state, action) => {
    return updateObject(state, {
        loading: false,
        reportedMeList: action.reportedMeList
    })
}

const reportedInterestSuccess = (state, action) => {
    return updateObject(state, {
        loading: false,
        reportedList: action.reportedList
    })
}

const blockedInterestSuccess = (state, action) => {
    return updateObject(state, {
        loading: false,
        blockedList: action.blockedList
    })
}

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.MEMBER_INTEREST_START: return memberInterestStart(state, action)
        case actionTypes.MEMBER_INTEREST_FAIL: return memberInterestFail(state, action)
        case actionTypes.VIEWEDME_INTEREST_SUCCESS: return viewedMeInterestSuccess(state, action)
        case actionTypes.VIEWED_INTEREST_SUCCESS: return viewedInterestSuccess(state, action)
        case actionTypes.FAVORITEDME_INTEREST_SUCCESS: return favoritedMeInterestSuccess(state, action)
        case actionTypes.REPORTEDME_INTEREST_SUCCESS: return reportedMeInterestSuccess(state, action)
        case actionTypes.REPORTED_INTEREST_SUCCESS: return reportedInterestSuccess(state, action)
        case actionTypes.BLOCKED_INTEREST_SUCCESS: return blockedInterestSuccess(state, action)
        default: return state
    }
}

export default reducer