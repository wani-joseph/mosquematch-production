import * as actionTypes from '../actions/actionTypes'
import { updateObject } from '../../shared/utility'

const initialState = {
    loading: false,
    errors: null,
    contact: null,
}

const contactDetailsStart = (state, action) => {
    return updateObject(state, { loading : true })
}

const contactDetailsSuccess = (state, action) => {
    return updateObject(state, {
        loading: false,
        errors: null,
        contact: action.contact,
    })
}

const contactDetailsFail = (state, action) => {
    return updateObject(state, {
        loading: false,
        errors: action.errors,
        contact: null,
    })
}

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.CONTACT_DETAILS_START: return contactDetailsStart(state, action)
        case actionTypes.CONTACT_DETAILS_SUCCESS: return contactDetailsSuccess(state, action)
        case actionTypes.CONTACT_DETAILS_FAIL: return contactDetailsFail(state, action)
        default: return state
    }
}

export default reducer