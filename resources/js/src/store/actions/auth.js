import * as actionTypes from './actionTypes'
import axios from 'axios'
import { checkFavorite, checkViewed } from '../../shared/utility'

export const authStart = () => ({
    type: actionTypes.AUTH_START
})

export const authSuccess = (token, user) => ({
    type: actionTypes.AUTH_SUCCESS,
    token,
    user
})

export const authUser = (user) => ({
    type: actionTypes.AUTH_USER,
    user,
})

export const updateUser = () => ({
    type: actionTypes.UPDATE_USER
})

export const favlist = favlist => ({
    type: actionTypes.USER_FAVORITE_LIST,
    favlist
})

export const templates = templates => ({
    type: actionTypes.USER_TEMPLATES,
    templates
})

export const viewedList = viewedList => ({
    type: actionTypes.USER_VIEWED_LIST,
    viewedList
})

export const authFail = errors => ({
    type: actionTypes.AUTH_FAIL,
    errors
})

export const authLogout = () => ({
    type: actionTypes.AUTH_LOGOUT
})

export const forgetPasswordSuccess = () => ({
    type: actionTypes.FORGET_PASSWORD_SUCCESS,
})

export const invalidCredentials = message => ({
    type: actionTypes.INVALID_CREDENTIALS,
    message
})

const inboxCount = count => ({
    type: actionTypes.INBOX_COUNT,
    count
})

const requestsCount = count => ({
    type: actionTypes.REQUESTS_COUNT,
    count
})

const viewMeCount = count => ({
    type: actionTypes.VIEWEDME_COUNT,
    count
})

const favoritedMeCount = count => ({
    type: actionTypes.FAVORITEDME_COUNT,
    count
})

const reportedMeCount = count => ({
    type: actionTypes.REPORTEDME_COUNT,
    count
})

const confirmInbox = updatedCount => ({
    type: actionTypes.CONFIRM_INBOX,
    updatedCount
})

const confirmRequests = () => ({
    type: actionTypes.CONFIRM_REQUESTS
})

const confirmViewedMe = () => ({
    type: actionTypes.CONFIRM_VIEWEDME
})

const confirmFavritedMe = () => ({
    type: actionTypes.CONFIRM_FAVORITEDME
})

const confirmReportedMe = () => ({
    type: actionTypes.CONFIRM_REPORTEDME
})

const notificationFail = errors => ({
    type: actionTypes.NOTIFICATION_FAIL,
    errors
})

const addToFavoriteSuccess = favorite => ({
    type: actionTypes.ADD_TO_FAVLIST_SUCCESS,
    favorite
})

const addToFavoriteFail = errors => ({
    type: actionTypes.ADD_TO_FAVLIST_FAIL,
    errors
})

const removeFromFavoriteSuccess = favorite => ({
    type: actionTypes.REMOVE_FROM_FAVLIST_SUCCESS,
    favorite
})

const removeFromFavoriteFail = errors => ({
    type: actionTypes.REMOVE_FROM_FAVLIST_FAIL,
    errors
})

const addToViewListSuccess = visit => ({
    type: actionTypes.ADD_TO_VIEWEDLIST_SUCCESS,
    visit
})

const addToViewListFail = errors => ({
    type: actionTypes.ADD_TO_VIEWEDLIST_FAIL,
    errors
})

const removeFromVisitSuccess = visit => ({
    type: actionTypes.REMOVE_FROM_VIEWEDLIST_SUCCESS,
    visit
})

const removeFromVisitFail = errors => ({
    type: actionTypes.REMOVE_FROM_VIEWEDLIST_FAIL,
    errors
})

const addTemplateSuccess = template => ({
    type: actionTypes.ADD_TEMPLATE_SUCCESS,
    template
})

const addTemplateFail = errors => ({
    type: actionTypes.ADD_TEMPLATE_FAIL,
    errors
})

const deleteTemplateSuccess = template => ({
    type: actionTypes.DELETE_TEMPLATE_SUCCESS,
    template
})

const deleteTemplateFail = errors => ({
    type: actionTypes.DELETE_TEMPLATE_FAIL,
    errors
})

export const register = data => {
    return async dispatch => {
        dispatch(authStart())

        try {
            const res = await axios.post('/api/register', data)

            localStorage.setItem('token', res.data.access_token)
            dispatch(authSuccess(res.data.access_token))
            dispatch(currentUser())

        } catch(err) {
            dispatch(authFail(err))
        }
    }
}

export const login = credentials => {
    return async dispatch => {
        dispatch(authStart())

        try {
            const res = await axios.post('/api/login', credentials)

            if (res.data.message) {
                dispatch(invalidCredentials(res.data.message))
            } else {
                localStorage.setItem('token', res.data.access_token)
                dispatch(authSuccess(res.data.access_token))
                dispatch(currentUser())
            }

        } catch (err) {
            dispatch(authFail(err))
        }
    }
}

export const logout = next => {
    return dispatch => {
        axios({
            method: 'get',
            url: '/api/logout',
            headers: {
                'Authorization': `Bearer ${localStorage.getItem('token')}`
            }
        })
        .then(() => {
            dispatch(authLogout())
            localStorage.removeItem('token')
            next()
        })
        .catch(err => dispatch(authFail(err)))
    }
}

const currentUser = () => {
    return async dispatch => {
        const token = localStorage.getItem('token')

        try {
            const res = await axios({
                method: 'get',
                url: '/api/auth-user',
                headers: {
                    'Authorization': `Bearer ${token}`
                }
            })

            dispatch(authUser(res.data.data))
            dispatch(currentUserFavList(res.data.data.identifier))
            dispatch(currentUserTemplates(res.data.data.identifier))
            dispatch(currentUserViewedList(res.data.data.identifier))
            dispatch(loadInboxCount(res.data.data.identifier))
            dispatch(loadRequestsCount(res.data.data.identifier))
            dispatch(loadViewedMeCount(res.data.data.identifier))
            dispatch(loadFavoritedMeCount(res.data.data.identifier))
            // dispatch(loadReportedMeCount(res.data.data.identifier))

        } catch (err) {
            localStorage.removeItem('token')
            dispatch(authFail(err))
        }
    }
}

const currentUserFavList = userId => {
    return async dispatch => {
        const token = localStorage.getItem('token')

        try {
            const res = await axios({
                method: 'get',
                url: `/api/users/${userId}/followings`,
                headers: {
                    'Authorization': `Bearer ${token}`
                }
            })

            if (res.data.data) {
                dispatch(favlist(res.data.data))
            }

        } catch(err) {
            dispatch(authFail(err))
        }
    }
}

const currentUserTemplates = userId => {
    return async dispatch => {
        const token = localStorage.getItem('token')

        try {
            const res = await axios({
                method: 'get',
                url: `/api/users/${userId}/templates`,
                headers: {
                    'Authorization': `Bearer ${token}`
                }
            })

            if (res.data.data) {
                dispatch(templates(res.data.data))
            }

        } catch(err) {
            dispatch(authFail(err))
        }
    }
}

const currentUserViewedList = (userId) => {
    return async dispatch => {
        const token = localStorage.getItem('token')

        try {
            const res = await axios({
                method: 'get',
                url: `/api/users/${userId}/visited`,
                headers: {
                    'Authorization': `Bearer ${token}`
                }
            })

            if (res.data.data) {
                dispatch(viewedList(res.data.data))
            }

        } catch(err) {
            dispatch(authFail(err))
        }
    }
}

const loadInboxCount = userId => {
    return async dispatch => {

        try {
            const res = await axios({
                method: 'get',
                url: `/api/users/${userId}/inbox?isRead=0`,
                headers: {
                    'Authorization': `Bearer ${localStorage.getItem('token')}`
                }
            })

            if (res.data.data) {

                if (res.data.meta.pagination.total > 0) {
                    dispatch(inboxCount(res.data.meta.pagination.total))
                }
            }

        } catch(err) {
            dispatch(authFail(err))
        }
    }
}

const loadRequestsCount = userId => {
    return async dispatch => {

        try {
            const res = await axios({
                method: 'get',
                url: `/api/users/${userId}/wali-requesters?status=pending`,
                headers: {
                    'Authorization': `Bearer ${localStorage.getItem('token')}`
                }
            })

            if (res.data.data) {

                if (res.data.meta.pagination.total > 0) {
                    dispatch(requestsCount(res.data.meta.pagination.total))
                }
            }

        } catch(err) {
            dispatch(authFail(err))
        }
    }
}

const loadViewedMeCount = userId => {
    return async dispatch => {

        try {
            const res = await axios({
                method: 'get',
                url: `/api/users/${userId}/visitors?status=pending`,
                headers: {
                    'Authorization': `Bearer ${localStorage.getItem('token')}`
                }
            })

            if (res.data.data) {

                if (res.data.meta.pagination.total > 0) {
                    dispatch(viewMeCount(res.data.meta.pagination.total))
                }
            }

        } catch(err) {
            dispatch(authFail(err))
        }
    }
}

const loadFavoritedMeCount = userId => {
    return async dispatch => {

        try {
            const res = await axios({
                method: 'get',
                url: `/api/users/${userId}/followers?status=pending`,
                headers: {
                    'Authorization': `Bearer ${localStorage.getItem('token')}`
                }
            })

            if (res.data.data) {

                if (res.data.meta.pagination.total > 0) {
                    dispatch(favoritedMeCount(res.data.meta.pagination.total))
                }
            }

        } catch(err) {
            dispatch(authFail(err))
        }
    }
}

const loadReportedMeCount = userId => {
    return async dispatch => {

        try {
            const res = await axios({
                method: 'get',
                url: `/api/users/${userId}/reporters?status=pending`,
                headers: {
                    'Authorization': `Bearer ${localStorage.getItem('token')}`
                }
            })

            if (res.data.data) {

                if (res.data.meta.pagination.total > 0) {
                    dispatch(reportedMeCount(res.data.meta.pagination.total))
                }
            }

        } catch(err) {
            dispatch(authFail(err))
        }
    }
}

export const authCheckState = () => {
    return dispatch => {
        const token = localStorage.getItem('token')

        if (token === null) {
            localStorage.removeItem('token')
        } else {
            dispatch(currentUser())
        }
    }
}

export const setAuthRedirectPath = (path) => ({
    type: actionTypes.SET_AUTH_REDIRECT_PATH,
    path
})

export const forgetPassword = (data, next) => {
    return dispatch => {
        dispatch(authStart())

        axios({
            method: 'post',
            url: `/api/password/create`,
            data: data,
            headers: {
                'X-Requested-With': 'XMLHttpRequest'
            }
        })
        .then(res => {
            if (res) {
                dispatch(forgetPasswordSuccess())
                next()
            }
        })
        .catch(err => dispatch(authFail(err)))
    }
}

export const userStatus = (userId, status) => {
    return dispatch => {
        axios({
            method: 'get',
            url: `/api/users/${userId}/${status}`,
            headers: {
                'Authorization': `Bearer ${localStorage.getItem('token')}`
            }
        })
        .catch(err => dispatch(authFail(err)))
    }
}

export const userBlockedList = (userId, next) => {
    return async () => {
        const res = await axios({
            method: 'get',
            url: `api/users/${userId}/blocked`,
            headers: {
                'Authorization': `Bearer ${localStorage.getItem('token')}`
            }
        })

        if (res) {
            next(res)
        }
    }
}

export const confirmInboxCount = (inboxCount, userId, contact) => {
    return dispatch => {
        
        if (inboxCount > 0) {
            axios({
                method: 'get',
                url: `/api/users/${userId}/messages-unread?contact=${contact}`,
                headers: {
                    'Authorization': `Bearer ${localStorage.getItem('token')}`
                }
            })
            .then(res => {

                if (res.data.data) {
                    let readCount = 0

                    for (let message in res.data.data) {
                        axios({
                            method: 'put',
                            url: `/api/messages/${res.data.data[message].identifier}`,
                            headers: {
                                'Authorization': `Bearer ${localStorage.getItem('token')}`
                            }
                        })

                        readCount++
                    }

                    const updatedInboxCount = inboxCount - readCount
                    dispatch(confirmInbox(updatedInboxCount))
                }
            })
            .catch(err => dispatch(notificationFail(err)))
        }
    }
}

export const confirmWaliRequest = (waliRequestId, update) => {
    return async dispatch => {

        try {
            const res = await axios({
                method: 'put',
                url: `/api/walis/${waliRequestId}`,
                headers: {
                    'Authorization': `Bearer ${localStorage.getItem('token')}`
                }
            })

            dispatch(confirmRequests())
            update(waliRequestId, res.data.data)

        } catch(err) {
            dispatch(notificationFail(err))
        }
    }
}

export const denyWaliRequest = (waliRequestId, filter) => {
    return async dispatch => {

        try {
            const res = await axios({
                method: 'delete',
                url: `/api/walis/${waliRequestId}`,
                headers: {
                'Authorization': `Bearer ${localStorage.getItem('token')}`
                }
            })

            dispatch(confirmRequests())
            filter(waliRequestId)

        } catch(err) {
            dispatch(notificationFail(err))
        }
    }
}

export const confirmViewedMeCount = (userId, viewMeCount) => {
    return dispatch => {

        if (viewMeCount > 0) {
            axios({
                method: 'get',
                url: `/api/users/${userId}/visitors?status=pending`,
                headers: {
                    'Authorization': `Bearer ${localStorage.getItem('token')}`
                }
            })
            .then(res => {
                if (res.data.data) {

                    for (let interest in res.data.data) {
                        dispatch(confirmCount(`/api/visits/${res.data.data[interest].identifier}`))
                    }

                    dispatch(confirmViewedMe())
                }
            })
            .catch(err => dispatch(notificationFail(err)))
        }
    }
}

export const confirmFavoritedMeCount = (userId, favoritedMeCount) => {
    return dispatch => {

        if (favoritedMeCount > 0) {
            axios({
                method: 'get',
                url: `/api/users/${userId}/followers?status=pending`,
                headers: {
                    'Authorization': `Bearer ${localStorage.getItem('token')}`
                }
            })
            .then(res => {
                if (res.data.data) {

                    for (let interest in res.data.data) {
                        dispatch(confirmCount(`/api/favorites/${res.data.data[interest].identifier}`))
                    }

                    dispatch(confirmFavritedMe())
                }
            })
            .catch(err => dispatch(notificationFail(err)))
        }
    }
}

export const confirmReportedMeCount = (userId, reportedMeCount) => {
    return dispatch => {
        if (reportedMeCount > 0) {
            axios({
                method: 'get',
                url: `/api/users/${userId}/reporters?status=pending`,
                headers: {
                    'Authorization': `Bearer ${localStorage.getItem('token')}`
                }
            })
            .then(res => {
                if (res.data.data) {

                    for (let interest in res.data.data) {
                        dispatch(confirmCount(`/api/reports/${res.data.data[interest].identifier}`))
                    }

                    dispatch(confirmReportedMe())
                }
            })
            .catch(err => dispatch(notificationFail(err)))
        }
    }
}

const confirmCount = url => {
    return async () => {
        const res = await axios({
            method: 'put',
            url: url,
            headers: {
                'Authorization': `Bearer ${localStorage.getItem('token')}`
            }
        })
    }
}

export const updateCurrentUser = (userId, data, next) => {
    return async dispatch => {
        dispatch(authStart())

        try {
            const res = await axios({
                method: 'put',
                url: `/api/users/${userId}`,
                data: data,
                headers: {
                    'Authorization': `Bearer ${localStorage.getItem('token')}`
                }
            })

            dispatch(updateUser())
            if (next) next()

        } catch(err) {
            dispatch(authFail())
        }
    }
}

export const addToFavorite = (userId, memberId, favoriteList) => {
    return dispatch => {
        if (favoriteList.length > 0) {
            if (checkFavorite(favoriteList, memberId)) {
                axios({
                    method: 'delete',
                    url: `/api/users/${userId}/followings/${memberId}`,
                    headers: {
                        'Authorization': `Bearer ${localStorage.getItem('token')}`
                    }
                })
                .then(res => dispatch(removeFromFavoriteSuccess(res.data.data)))
                .catch(err => dispatch(removeFromFavoriteFail(err)))
            } else {
                const data = {
                    following_id: memberId
                }
        
                axios({
                    method: 'post',
                    url: `/api/users/${userId}/followings`,
                    data: data,
                    headers: {
                        'Authorization': `Bearer ${localStorage.getItem('token')}`
                    }
                })
                .then(res => dispatch(addToFavoriteSuccess(res.data.data)))
                .catch(err => dispatch(addToFavoriteFail(err)))
            }
        } else {
            const data = {
                following_id: memberId
            }
    
            axios({
                method: 'post',
                url: `/api/users/${userId}/followings`,
                data: data,
                headers: {
                    'Authorization': `Bearer ${localStorage.getItem('token')}`
                }
            })
            .then(res => dispatch(addToFavoriteSuccess(res.data.data)))
            .catch(err => dispatch(addToFavoriteFail(err)))
        }
    }
}

export const addToViewList = (userId, memberId) => {
    return dispatch => {
        const data = { visited_id: memberId }
            
        axios({
            method: 'post',
            url: `/api/users/${userId}/visits`,
            data: data,
            headers: {
                'Authorization': `Bearer ${localStorage.getItem('token')}`
            }
        })
        .then(res => dispatch(addToViewListSuccess(res.data.data)))
        .catch(err => dispatch(addToViewListFail(err)))
    }
}

export const addNewTemplate = (userId, template, next) => {
    return async dispatch => {

        try {
            const res = await axios({
                method: 'post',
                url: `/api/users/${userId}/templates`,
                data: { text: template },
                headers: {
                    'Authorization': `Bearer ${localStorage.getItem('token')}`
                }
            })

            dispatch(addTemplateSuccess(res.data.data))

            if (next) next()

        } catch(err) {
            dispatch(addTemplateFail(err))
        }
    }
}

export const deleteTemplate = (userId, template) => {
    return dispatch => {
        axios({
            method: 'delete',
            url: `/api/users/${userId}/templates/${template}`,
            headers: {
                'Authorization': `Bearer ${localStorage.getItem('token')}`
            }
        })
        .then(res => dispatch(deleteTemplateSuccess(res.data.data)))
        .catch(err => dispatch(deleteTemplateFail(err)))
    }
}

const addImage  = (ablumId, data) => {
    return async dispatch => {
        try {
            const res = await axios({
                method: 'post',
                url: `/api/albums/${ablumId}/images`,
                data: data,
                headers: {
                    'Authorization': `Bearer ${localStorage.getItem('token')}`
                },
            })

            console.log(res)
        } catch (err) {
            dispatch(authFail(err))
        }
    }
}

export const moveImage = (userId, moveToAlbumId, moveToAlbumName, imageId) => {
    return async dispatch => {

        try {

            if (moveToAlbumId) {
                const res = await axios({
                    method: 'get',
                    url: `/api/albums/${moveToAlbumId}`,
                    headers: {
                        'Authorization': `Bearer ${localStorage.getItem('token')}`
                    },
                })
    
                const response = await axios({
                    method: 'put',
                    url: `/api/images/${imageId}`,
                    data: { album_id: res.data.data.identifier },
                    headers: {
                        'Authorization': `Bearer ${localStorage.getItem('token')}`
                    },
                })
    
                dispatch(currentUser())
            } else {
                const res = await axios({
                    method: 'post',
                    url: `/api/users/${userId}/albums`,
                    data: { name: moveToAlbumName },
                    headers: {
                        'Authorization': `Bearer ${localStorage.getItem('token')}`
                    },
                })

                const response = await axios({
                    method: 'put',
                    url: `/api/images/${imageId}`,
                    data: { album_id: res.data.data.identifier },
                    headers: {
                        'Authorization': `Bearer ${localStorage.getItem('token')}`
                    },
                })
    
                dispatch(currentUser())
            }
        } catch (err) {
            dispatch(authFail)
        }
    }
}

export const newAlbum = (userId, albumData, imageData) => {
    return async dispatch => {
        try {
            const res = await axios({
                method: 'post',
                url: `/api/users/${userId}/albums`,
                data: albumData,
                headers: {
                    'Authorization': `Bearer ${localStorage.getItem('token')}`
                },
            })

            dispatch(addImage(res.data.data.identifier, imageData))

        } catch (err) {
            dispatch(authFail)
        }
    }
}

export const updateAlbum = (userId, albumId, albumData, imageData) => {
    return async dispatch => {
        try {
            const res = await axios({
                method: 'post',
                url: `/api/users/${userId}/albums/${albumId}`,
                data: albumData,
                headers: {
                    'Authorization': `Bearer ${localStorage.getItem('token')}`
                },
            })

            dispatch(addImage(res.data.data.identifier, imageData))

        } catch (err) {
            dispatch(authFail)
        }
    }
}

export const deleteImage = (albumId, imageId) => {
    return async dispatch => {

        try {
            const res = await axios({
                method: 'delete',
                url: `/api/albums/${albumId}/images/${imageId}`,
                headers: {
                    'Authorization': `Bearer ${localStorage.getItem('token')}`
                }
            })

            dispatch(currentUser())

        } catch(err) {
            dispatch(authFail(err))
        }
    }
}