import * as actionTypes from './actionTypes'
import axios from 'axios'

export const contactDetailsStart = () => ({
    type: actionTypes.CONTACT_DETAILS_START
})

export const contactDetailsSuccess = contact => ({
    type: actionTypes.CONTACT_DETAILS_SUCCESS,
    contact
})

export const contactDetailsFail = errors => ({
    type: actionTypes.CONTACT_DETAILS_FAIL,
    errors
})

export const getContactDetails = (contact, next) => {
    return dispatch => {
        dispatch(contactDetailsStart())

        axios({
            method: 'get',
            url: `/api/users/${contact}`,
            headers: {
                'Authorization': `Bearer ${localStorage.getItem('token')}`
            }
        })
        .then(res => {
            dispatch(contactDetailsSuccess(res.data.data))

            if (next) {
                next(res)
            }
        })
        .catch(err => dispatch(contactDetailsFail(err)))
    }
}