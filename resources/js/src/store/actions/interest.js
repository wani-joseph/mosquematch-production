import * as actionTypes from './actionTypes'
import axios from 'axios'

export const memberInterestStart = () => ({
    type: actionTypes.MEMBER_INTEREST_START
})

export const memberInterestFail = errors => ({
    type: actionTypes.MEMBER_INTEREST_FAIL,
    errors
})

export const viewedMeInterestSuccess = viewedMeList => ({
    type: actionTypes.VIEWEDME_INTEREST_SUCCESS,
    viewedMeList
})

export const viewedInterestSuccess = viewedList => ({
    type: actionTypes.VIEWED_INTEREST_SUCCESS,
    viewedList
})

export const favoritedMeInterestSuccess = favoritedMeList => ({
    type: actionTypes.FAVORITEDME_INTEREST_SUCCESS,
    favoritedMeList
})

export const reportedMeInterestSuccess = reportedMeList => ({
    type: actionTypes.REPORTEDME_INTEREST_SUCCESS,
    reportedMeList
})

export const reportedInterestSuccess = reportedList => ({
    type: actionTypes.REPORTED_INTEREST_SUCCESS,
    reportedList
})

export const blockedInterestSuccess = blockedList => ({
    type: actionTypes.BLOCKED_INTEREST_SUCCESS,
    blockedList
})

export const getFavoritedMeList = (userId) => {
    return dispatch => {
        axios({
            method: 'get',
            url: `/api/users/${userId}/followers`,
            headers: {
                'Authorization': `Bearer ${localStorage.getItem('token')}`
            }
        })
        .then(res => {
            const favoritedMeList = []

            for (let member in res.data.data) {
                favoritedMeList.push({ ...res.data.data[member] })
            }

            dispatch(favoritedMeInterestSuccess(favoritedMeList))
        })
        .catch(err => dispatch(memberInterestFail(err)))
    }
}

export const confirmFavoritedMeList = (favoriteId) => {
    return async () => {
        await axios({
            method: 'put',
            url: `/api/favorites/${favoriteId}`,
            headers: {
                'Authorization': `Bearer ${localStorage.getItem('token')}`
            }
        })
    }
}

export const getViewedMeList = (userId) => {
    return dispatch => {
        axios({
            method: 'get',
            url: `/api/users/${userId}/visits`,
            headers: {
                'Authorization': `Bearer ${localStorage.getItem('token')}`
            }
        })
        .then(res => {
            const viewedMeList = []

            for (let member in res.data.data) {
                viewedMeList.push({ ...res.data.data[member] })
            }

            dispatch(viewedMeInterestSuccess(viewedMeList))
        })
        .catch(err => dispatch(memberInterestFail(err)))
    }
}

export const confirmViewedMeList = (viewId) => {
    return async () => {
        await axios({
            method: 'put',
            url: `/api/visits/${viewId}`,
            headers: {
                'Authorization': `Bearer ${localStorage.getItem('token')}`
            }
        })
    }
}

export const getViewedList = (userId) => {
    return dispatch => {
        axios({
            method: 'get',
            url: `/api/users/${userId}/visitors`,
            headers: {
                'Authorization': `Bearer ${localStorage.getItem('token')}`
            }
        })
        .then(res => {
            const viewedList = []

            for (let member in res.data.data) {
                viewedList.push({ ...res.data.data[member] })
            }

            dispatch(viewedInterestSuccess(viewedList))
        })
        .catch(err => dispatch(memberInterestFail(err)))
    }
}

export const getReportedMeList = (userId) => {
    return dispatch => {
        axios({
            method: 'get',
            url: `/api/users/${userId}/reporters`,
            headers: {
                'Authorization': `Bearer ${localStorage.getItem('token')}`
            }
        })
        .then(res => {
            const reportedMeList = []

            for (let member in res.data.data) {
                reportedMeList.push({ ...res.data.data[member] })
            }

            dispatch(reportedMeInterestSuccess(reportedMeList))
        })
        .catch(err => dispatch(memberInterestFail(err)))
    }
}

export const confirmReportedMeList = (reportId) => {
    return async () => {
        await axios({
            method: 'put',
            url: `/api/reports/${reportId}`,
            headers: {
                'Authorization': `Bearer ${localStorage.getItem('token')}`
            }
        })
    }
}

export const getReportedList = (userId) => {
    return dispatch => {
        axios({
            method: 'get',
            url: `/api/users/${userId}/reported`,
            headers: {
                'Authorization': `Bearer ${localStorage.getItem('token')}`
            }
        })
        .then(res => {
            const reportedList = []

            for (let member in res.data.data) {
                reportedList.push({ ...res.data.data[member] })
            }

            dispatch(reportedInterestSuccess(reportedList))
        })
        .catch(err => dispatch(memberInterestFail(err)))
    }
}

export const getBlockedList = (userId) => {
    return dispatch => {
        axios({
            method: 'get',
            url: `/api/users/${userId}/blocked`,
            headers: {
                'Authorization': `Bearer ${localStorage.getItem('token')}`
            }
        })
        .then(res => {
            const blockedList = []

            for (let member in res.data.data) {
                blockedList.push({ ...res.data.data[member] })
            }

            dispatch(blockedInterestSuccess(blockedList))
        })
        .catch(err => dispatch(memberInterestFail(err)))
    }
}

export const confirmBlockedList = (blockId) => {
    return async () => {
        await axios({
            method: 'put',
            url: `/api/blocks/${blockId}`,
            headers: {
                'Authorization': `Bearer ${localStorage.getItem('token')}`
            }
        })
    }
}