import * as actionTypes from './actionTypes';
import axios from 'axios'

export const getCountries = () => {
    return dispatch => {
        axios.get('https://restcountries.eu/rest/v2/all')
            .then(res => console.log(res))
            .catch(err => console.log(err))
    }
}