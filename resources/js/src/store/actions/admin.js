import {
  ADMIN_RESOURCE_START,
  ADMIN_RESOURCE_FAIL,
  FETCH_RESOURCE_SUCCESS,
  VIEW_RESOURCE_SUCCESS
} from './actionTypes'

const resourceStart = () => ({
  type: ADMIN_RESOURCE_START
})

const resourceFail = errors => ({
  type: ADMIN_RESOURCE_FAIL,
  errors
})

const fetchResourceSuccess = resourceList => ({
  type: FETCH_RESOURCE_SUCCESS,
  resourceList
})

const viewResourceSuccess = resource => ({
  type: VIEW_RESOURCE_SUCCESS,
  resource
})

export const fetchAdminResource = resource => {
  return async dispatch => {
    dispatch(resourceStart())

    try {
      const res = await axios({
        method: 'get',
        url: resource,
        headers: {
          'Authorization': `Bearer ${localStorage.getItem('token')}`
        }
      })

      console.log(res)

    } catch (err) {
      dispatch(resourceFail(err))
    }
  }
}

export const viewAdminResource = resource => {
  return async dispatch => {
    dispatch(resourceStart())

    try {
      const res = await axios({
        method: 'get',
        url: resource,
        headers: {
          'Authorization': `Bearer ${localStorage.getItem('token')}`
        }
      })

      dispatch(viewResourceSuccess(res.data.data))

    } catch (err) {
      dispatch(resourceFail(err))
    }
  }
}