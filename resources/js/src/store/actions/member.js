import * as actionTypes from './actionTypes'
import axios from 'axios'
import { checkFavorite } from '../../shared/utility'

export const memberActionStart = () => ({
    type: actionTypes.MEMBER_ACTION_START
})

export const memberActionFail = errors => ({
    type: actionTypes.MEMBER_ACTION_FAIL,
    errors
})

export const updateMemberSuccess = () => ({
    type: actionTypes.UPDATE_MEMBER_SUCCESS
})

export const memberTemplatesSuccess = (templates) => ({
    type: actionTypes.MEMBER_TEMPLATES_SUCCESS,
    templates
})

export const memberTemplatesFail = errors => ({
    type: actionTypes.MEMBER_TEMPLATES_FAIL,
    errors
})

export const memberFavListSuccess = favList => ({
    type: actionTypes.MEMBER_FAVLIST_SUCCESS,
    favList
})

export const memberFavListFail = errors => ({
    type: actionTypes.MEMBER_FAVLIST_FAIL,
    errors
})

export const removeFromFavoriteSuccess = favorite => ({
    type: actionTypes.REMOVE_FROM_FAVLIST_SUCCESS,
    favorite
})

export const removeFromFavoriteFail = errors => ({
    type: actionTypes.REMOVE_FROM_FAVLIST_FAIL,
    errors
})

export const addToFavoriteSuccess = favorite => ({
    type: actionTypes.ADD_TO_FAVLIST_SUCCESS,
    favorite
})

export const addToFavoriteFail = errors => ({
    type: actionTypes.ADD_TO_FAVLIST_FAIL,
    errors
})

export const addTemplateSuccess = template => ({
    type: actionTypes.ADD_TEMPLATE_SUCCESS,
    template
})

export const addTemplateFail = errors => ({
    type: actionTypes.ADD_TEMPLATE_FAIL,
    errors
})

export const deleteTemplateSuccess = template => ({
    type: actionTypes.DELETE_TEMPLATE_SUCCESS,
    template
})

export const deleteTemplateFail = errors => ({
    type: actionTypes.DELETE_TEMPLATE_FAIL,
    errors
})

export const memberWaliRequestSuccess = waliRequests => ({
    type: actionTypes.WALI_REQUESTS_SUCCESS,
    waliRequests
})

export const memberWaliRequestFail = errors => ({
    type: actionTypes.WALI_REQUESTS_FAIL,
    errors
})

export const memberMessagesSuccess = messages => ({
    type: actionTypes.MEMBER_MESSAGES_SUCCESS,
    messages
})

export const memberInboxSuccess = messages => ({
    type: actionTypes.MEMBER_INBOX_SUCCESS,
    messages
})

export const memberConversationSuccess = messages => ({
    type: actionTypes.MEMBER_CONVERSATION_SUCCESS,
    messages
})

export const newMessageSuccess = message => ({
    type: actionTypes.NEW_MESSAGE_SUCCESS,
    message
})

export const newInboxSuccess = message => ({
    type: actionTypes.NEW_INBOX_SUCCESS,
    message
})

export const viewMember = (member) => ({
    type: actionTypes.VIEW_MEMBER,
    member
})

export const confirmWaliRequest = waliRequest => ({
    type: actionTypes.CONFIRM_WALI_REQUEST,
    waliRequest
})

export const denyWaliRequest = (waliRequest) => ({
    type: actionTypes.DENY_WALI_REQUEST,
    waliRequest
})

export const fetchMember = memberId => {
    return async dispatch => {
        dispatch(memberActionStart())

        try {
            const res = await axios({
                method: 'get',
                url: `/api/users/${memberId}`,
                headers: {
                    'Authorization': `Bearer ${localStorage.getItem('token')}`
                }
            })

            dispatch(viewMember(res.data.data))

        } catch(err) {
            dispatch(memberActionFail(err))
        }
    }
}

export const updateMember =  (userId, data, next) => {
    return async (dispatch) => {
        dispatch(memberActionStart())

        let res = await axios({
            method: 'put',
            url: `/api/users/${userId}`,
            data: data,
            headers: {
                'Authorization': `Bearer ${localStorage.getItem('token')}`
            }
        })

        if (res) {
            dispatch(updateMemberSuccess())
            next()
        }
    }
}

export const userAction = (url, data, next) => {
    return async () => {
        let res = await axios({
            method: 'post',
            url: url,
            data: data,
            headers: {
                'Authorization': `Bearer ${localStorage.getItem('token')}`
            }
        })

        if (res) {
            next()
        }
    }
}

export const blockMember = (userId, data, next) => {
    return async () => {
        const res = await axios({
            method: 'post',
            url: `/api/users/${userId}/blocked`,
            data: data,
            headers: {
                'Authorization': `Bearer ${localStorage.getItem('token')}`
            }
        })

        if (res) {
            next(res)
        }
    }
}

export const userInterest = (interest, next) => {
    return async () => {
        const res = await axios({
            method: 'get',
            url: interest,
            headers: {
                'Authorization': `Bearer ${localStorage.getItem('token')}`
            }
        })

        if (res) {
            next(res)
        }
    }
}

export const getMemberTemplates = (userId) => {
    return dispatch => {
        axios({
            method: 'get',
            url: `/api/users/${userId}/templates`,
            headers: {
                'Authorization': `Bearer ${localStorage.getItem('token')}`
            }
        })
        .then(res => {
            const templates = []

            for (let template in res.data.data) {
                templates.push({ ...res.data.data[template] })
            }

            dispatch(memberTemplatesSuccess(templates))
        })
        .catch(err => dispatch(memberTemplatesFail(err)))
    }
}

export const addNewTemplate = (userId, template) => {
    return dispatch => {
        axios({
            method: 'post',
            url: `/api/users/${userId}/templates`,
            data: { template: template },
            headers: {
                'Authorization': `Bearer ${localStorage.getItem('token')}`
            }
        })
        .then(res => dispatch(addTemplateSuccess(res.data.data)))
        .catch(err => dispatch(addTemplateFail(err)))
    }
}

export const deleteTemplate = (userId, template) => {
    return dispatch => {
        axios({
            method: 'delete',
            url: `/api/users/${userId}/templates/${template}`,
            headers: {
                'Authorization': `Bearer ${localStorage.getItem('token')}`
            }
        })
        .then(res => dispatch(deleteTemplateSuccess(res.data.data)))
        .catch(err => dispatch(deleteTemplateFail(err)))
    }
}

export const getMemberFavoriteList = (userId) => {
    return dispatch => {
        axios({
            method: 'get',
            url: `/api/users/${userId}/followings`,
            headers: {
                'Authorization': `Bearer ${localStorage.getItem('token')}`
            }
        })
        .then(res => {
            const favoriteList = []

            for (let favorite in res.data.data) {
                favoriteList.push({ ...res.data.data[favorite] })
            }

            dispatch(memberFavListSuccess(favoriteList))
        })
        .catch(err => dispatch(memberFavListFail(err)))
    }
}

export const getMemberWaliRequests = (userId) => {
    return dispatch => {
        axios({
            method: 'get',
            url: `/api/users/${userId}/wali_requesters`,
            headers: {
                'Authorization': `Bearer ${localStorage.getItem('token')}`
            }
        })
        .then(res => {
            const waliRequests = []

            for (let request in res.data.data) {
                waliRequests.push({ ...res.data.data[request] })
            }

            dispatch(memberWaliRequestSuccess(waliRequests))
        })
        .catch(err => dispatch(memberWaliRequestFail(err)))
    }
}

export const memberConfirmWaliRequest = (waliRequestId, user_details) => {
    return dispatch => {
        axios({
            method: 'put',
            url: `/api/wali_requests/${waliRequestId}`,
            headers: {
                'Authorization': `Bearer ${localStorage.getItem('token')}`
            }
        })
        .then(res => {
            const waliRequest = {
                ...res.data.data,
                user_details: user_details
            }

            dispatch(confirmWaliRequest(waliRequest))
        })
        .catch(err => dispatch(memberActionFail(err)))
    }
}

export const memberDenyWaliRequest = (waliRequestId) => {
    return dispatch => {
        axios({
            method: 'delete',
            url: `/api/wali_requests/${waliRequestId}`,
            headers: {
                'Authorization': `Bearer ${localStorage.getItem('token')}`
            }
        })
        .then(res => dispatch(denyWaliRequest(res.data.data)))
        .catch(err => dispatch(memberActionFail(err)))
    }
}

export const getMemberInboxMessages = (userId) => {
    return dispatch => {
        dispatch(memberActionStart())
        
        axios({
            method: 'get',
            url: `/api/users/${userId}/inbox`,
            headers: {
                'Authorization': `Bearer ${localStorage.getItem('token')}`
            }
        })
        .then(res => {
            const inboxMessages = []

            for (let message in res.data.data) {
                inboxMessages.push({ ...res.data.data[message] })
            }

            dispatch(memberInboxSuccess(inboxMessages))
        })
        .catch(err => dispatch(memberActionFail(err)))
    }
}

export const getMemberSentMessages = (userId) => {
    return dispatch => {
        dispatch(memberActionStart())

        axios({
            method: 'get',
            url: `/api/users/${userId}/sent`,
            headers: {
                'Authorization': `Bearer ${localStorage.getItem('token')}`
            }
        })
        .then(res => {
            const sentMessages = []

            for (let message in res.data.data) {
                sentMessages.push({ ...res.data.data[message] })
            }

            dispatch(memberMessagesSuccess(sentMessages))
        })
        .catch(err => dispatch(memberActionFail(err)))
    }
}

export const getMemberConversation = (userId, contact) => {
    return dispatch => {
        dispatch(memberActionStart())

        axios({
            method: 'get',
            url: `/api/users/${userId}/messages?contact=${contact}`,
            headers: {
                'Authorization': `Bearer ${localStorage.getItem('token')}`
            }
        })
        .then(res => {
            const messages = []

            for (let message in res.data.data) {
                messages.push({ ...res.data.data[message] })
            }

            dispatch(memberConversationSuccess(messages))
        })
        .catch(err => dispatch(memberActionFail(err)))
    }
}

export const newConversationMessage = (userId, data, next) => {
    return async dispatch => {

        try {
            const res = await axios({
                method: 'post',
                url: `/api/users/${userId}/messages`,
                data: data,
                headers: {
                    'Authorization': `Bearer ${localStorage.getItem('token')}`
                }
            })

            dispatch(newMessageSuccess(res.data.data))
            if (next) next()

        } catch(err) {
            dispatch(memberActionFail(err))
        }
    }
}

export const incomimgConversationMessage = (message) => {
    return dispatch => {
        dispatch(newMessageSuccess(message))
    }
}

export const incomimgInboxMessage = (message) => {
    return dispatch => {
        dispatch(newInboxSuccess(message))
    }
}

export const addToFavorite = (userId, memberId, favoriteList) => {
    return dispatch => {
        if (favoriteList.length > 0) {
            if (checkFavorite(favoriteList, memberId)) {
                axios({
                    method: 'delete',
                    url: `/api/users/${userId}/followings/${memberId}`,
                    headers: {
                        'Authorization': `Bearer ${localStorage.getItem('token')}`
                    }
                })
                .then(res => dispatch(removeFromFavoriteSuccess(res.data.data)))
                .catch(err => dispatch(removeFromFavoriteFail(err)))
            } else {
                const data = {
                    following_id: memberId
                }
        
                axios({
                    method: 'post',
                    url: `/api/users/${userId}/followings`,
                    data: data,
                    headers: {
                        'Authorization': `Bearer ${localStorage.getItem('token')}`
                    }
                })
                .then(res => dispatch(addToFavoriteSuccess(res.data.data)))
                .catch(err => dispatch(addToFavoriteFail(err)))
            }
        } else {
            const data = {
                following_id: memberId
            }
    
            axios({
                method: 'post',
                url: `/api/users/${userId}/followings`,
                data: data,
                headers: {
                    'Authorization': `Bearer ${localStorage.getItem('token')}`
                }
            })
            .then(res => dispatch(addToFavoriteSuccess(res)))
            .catch(err => dispatch(addToFavoriteFail(err)))
        }
    }
}

export const linkMosques = (userId, mosques, next) => {
    return dispatch => {
        dispatch(memberActionStart())

        for (let mosque in mosques) {
            axios({
                method: 'put',
                url: `/api/mosques/${mosques[mosque].identifier}/users/${userId}`,
                headers: {
                    'Authorization': `Bearer ${localStorage.getItem('token')}`
                }
            })
            .then(res => Console.log(res))
            .catch(err => dispatch(memberActionFail(err)))
        }

        dispatch(updateMemberSuccess())

        if (next) next()
    }
}