import * as actionTypes from './actionTypes'
import axios from 'axios'

export const newMessageSuccess = message => ({
    type: actionTypes.NEW_MESSAGE_SUCCESS,
    message
})

export const newMessageFail = errors => ({
    type: actionTypes.NEW_MESSAGE_FAIL,
    errors
})



export const sendMessage = (userId, contact, message, next) => {
    return async dispatch => {
        try {
            const res = await axios({
                method: 'post',
                url: `/api/users/${userId}/conversation`,
                data: {
                    receiver: parseInt(contact),
                    content: message
                },
                headers: {
                    'Authorization': `Bearer ${localStorage.getItem('token')}`
                }
            })

            dispatch(newMessageSuccess(res.data.data))
            if (next) next()

        } catch {
            dispatch(newMessageFail(err))
        }
    }
}

export const markMessageAsRead = (message) => {
    return () => {
        axios({
            method: 'put',
            url: `/api/messages/${message}`,
            headers: {
                'Authorization': `Bearer ${localStorage.getItem('token')}`
            }
        })
    }
}