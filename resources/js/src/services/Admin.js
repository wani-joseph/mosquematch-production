import React, { Component } from 'react'
import axios from 'axios'
import Pagination from '../components/Pagination/Pagination'

class Admin extends Component {
  constructor(props) {
    super(props)
    this._isMounted = false
    this.state = {
      loading: false,
      list: [],
      resourceItem: null,
      per: 10,
      page: 1,
      total: null,
      totalPages: null,
      sortBy: 'status',
      orderBy: 'desc',
      resourceForm: false,
    }
    this.loadList = this.loadList.bind(this)
    this.viewResource = this.viewResource.bind(this)
    this.loadNextPage = this.loadNextPage.bind(this)
    this.loadPrevPage = this.loadPrevPage.bind(this)
    this.handlePerPage = this.handlePerPage.bind(this)
    this.handleSortBy = this.handleSortBy.bind(this)
    this.handleScrolling = this.handleScrolling.bind(this)
    this.addResource = this.addResource.bind(this)
    this.editResource = this.editResource.bind(this)
    this.deleteResource = this.deleteResource.bind(this)
    this.openResourceForm = this.openResourceForm.bind(this)
    this.closeResourceForm = this.closeResourceForm.bind(this)
  }

  componentDidMount() {
    this._isMounted = true

    const { page } = this.props

    if (page === 'resource listing') {
      this.loadList()
    } else {
      this.viewResource()
    }
  }

  componentWillUnmount() {
    this._isMounted = false
  }

  loadList() {
    if (this._isMounted) this.setState({ loading: true })
    
    const { resource } = this.props
    const { per, page, sortBy, orderBy } = this.state

    axios({
      method: 'get',
      url: `/api/${resource}?per_page=${per}&page=${page}&sort_by=${sortBy}&order_by=${orderBy}`,
      headers: {
        'Authorization': `Bearer ${localStorage.getItem('token')}`
      }
    })
    .then(res => {
      if (this._isMounted) {

        if (res.data.data) {
          const { per_page, current_page, total_pages, total } = res.data.meta.pagination

          this.setState({
            list: res.data.data,
            per: per_page,
            page: current_page,
            totalPages: total_pages,
            total: total,
            loading: false,
          })
        } else {
          this.setState({
            loading: false,
          })
        }
      }
    })
    .catch(err => console.log(err))
  }

  viewResource() {
    const { resourceId } = this.props

    if (this._isMounted) {
      this.setState({ loading: true })

      axios({
        method: 'get',
        url: `/api/${resourceId}`,
        headers: {
          'Authorization': `Bearer ${localStorage.getItem('token')}`
        }
      })
      .then(res => {
        if (res.data.data) {
          this.setState({
            resourceItem: res.data.data,
            loading: false,
          })
        } else {
          this.setState({
            loading: false,
          })
        }
      })
      .catch(err => console.log(err))
    }
  }

  addResource(data) {
    const { resource } = this.props

    if (this._isMounted) {
      this.setState({ loading: true })

      axios({
        method: 'post',
        url: `/api/${resource}`,
        data: data,
        headers: {
          'Authorization': `Bearer ${localStorage.getItem('token')}`
        }
      })
      .then(res => {
        const { list } = this.state

        if (res.data.data) {
          this.setState({
            list: [...list, res.data.data],
            loading: false,
            resourceForm: false
          })
        } else {
          this.setState({
            loading: false,
            resourceForm: false
          })
        }
      })
      .catch(err => console.log(err))
    }
  }

  editResource(data) {
    const { resourceId } = this.props

    if (this._isMounted) {
      this.setState({ loading: true })

      axios({
        method: 'put',
        url: `/api/${resourceId}`,
        data: data,
        headers: {
          'Authorization': `Bearer ${localStorage.getItem('token')}`
        }
      })
      .then(res => {

        if (res.data.data) {
          this.setState({
            resourceItem: res.data.data,
            loading: false,
          })
        } else {
          this.setState({ loading: false })
        }
      })
      .catch(err => console.log(err))
    }
  }

  deleteResource(resourceId) {
    if (this._isMounted) {
      this.setState({ loading: true })

      axios({
        method: 'delete',
        url: `/api/${resourceId}`,
        headers: {
          'Authorization': `Bearer ${localStorage.getItem('token')}`
        }
      })
      .then(res => {
        const { list } = this.state

        if (res.data.data) {
          this.setState({
            list: list.filter(resource => resource.identifier !== res.data.data.identifier),
            loading: false,
          })
        } else {
          this.setState({
            loading: false,
          })
        }
      })
      .catch(err => console.log(err))
    }
  }

  loadNextPage() {
    const { totalPages, page } = this.state

    if (totalPages <= page) return

    if (this._isMounted) {
      this.setState(prevState => ({
        page: prevState.page + 1,
      }), this.loadList)
    }

    this.handleScrolling()
  }

  loadPrevPage() {
    const { page } = this.state

    if (page == 1) return

    if (this._isMounted) {
      this.setState(prevState => ({
        page: prevState.page - 1,
      }), this.loadList)
    }

    this.handleScrolling()
  }

  handlePerPage(e) {
    if (this._isMounted) {
      this.setState({ per: e.target.value }, this.loadList)
    }
  }

  handleSortBy(e) {
    if (this._isMounted) {
      if (e.target.value == "desc" || e.target.value == "asc") {
        this.setState({ orderBy: e.target.value }, this.loadList)
      } else {
        this.setState({ sortBy: e.target.value }, this.loadList)
      }
    }
  }

  handleScrolling() {
    setTimeout(() => {
      window.scroll({
        top: 0, 
        left: 0, 
        behavior: 'smooth'
      })
    }, 50)
  }

  openResourceForm() {
    if (this._isMounted) {
      this.setState({ resourceForm: true })
    }
  }

  closeResourceForm() {
    if (this._isMounted) {
      this.setState({ resourceForm: false })
    }
  }
  
  render() {
    const { page, totalPages } = this.state
    const { customDividor } = this.props

    return (
      <>
        {this.props.children({
          ...this.state,
          handlePerPage: this.handlePerPage,
          handleSortBy: this.handlePerPage,
          addResource: this.addResource,
          editResource: this.editResource,
          deleteResource: this.deleteResource,
          openResourceForm: this.openResourceForm,
          closeResourceForm: this.closeResourceForm
        })}
        <Pagination
          customDividor={customDividor}
          currentPage={page}
          totalPages={totalPages}
          prev={this.loadPrevPage}
          next={this.loadNextPage}
        />
      </>
    )
  }
}

export default Admin