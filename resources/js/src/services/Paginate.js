import React, { Component } from 'react'
import { connect } from 'react-redux'
import axios from 'axios'
import Pagination from '../components/Pagination/Pagination'

class Paginate extends Component {
  constructor(props) {
    super(props)
    this._isMounted = false
    this.state = {
      loading: false,
      list: [],
      per: 10,
      page: 1,
      total: null,
      totalPages: null,
      sortBy: 'status',
      orderBy: 'desc',
    }
    this.loadList = this.loadList.bind(this)
    this.loadNextPage = this.loadNextPage.bind(this)
    this.loadPrevPage = this.loadPrevPage.bind(this)
    this.handlePerPage = this.handlePerPage.bind(this)
    this.handleSortBy = this.handleSortBy.bind(this)
    this.handleScrolling = this.handleScrolling.bind(this)
  }

  componentDidMount() {
    this._isMounted = true
    this.loadList()
  }

  componentWillUnmount() {
    this._isMounted = false
  }

  loadList() {
    if (this._isMounted) this.setState({ loading: true })
    
    const { user, resource } = this.props
    const { per, page, sortBy, orderBy } = this.state

    axios({
      method: 'get',
      url: `/api/users/${user.id}/${resource}?per_page=${per}&page=${page}&sort_by=${sortBy}&order_by=${orderBy}`,
      headers: {
        'Authorization': `Bearer ${localStorage.getItem('token')}`
      }
    })
    .then(res => {
      if (this._isMounted) {

        if (res.data.data) {
          const { per_page, current_page, total_pages, total } = res.data.meta.pagination

          this.setState({
            list: res.data.data,
            per: per_page,
            page: current_page,
            totalPages: total_pages,
            total: total,
            loading: false,
          })
        } else {
          this.setState({
            loading: false,
          })
        }
      }
    })
    .catch(err => console.log(err))
  }

  loadNextPage() {
    const { totalPages, page } = this.state

    if (totalPages <= page) return

    if (this._isMounted) {
      this.setState(prevState => ({
        page: prevState.page + 1,
      }), this.loadList)
    }

    this.handleScrolling()
  }

  loadPrevPage() {
    const { page } = this.state

    if (page == 1) return

    if (this._isMounted) {
      this.setState(prevState => ({
        page: prevState.page - 1,
      }), this.loadList)
    }

    this.handleScrolling()
  }

  handlePerPage(e) {
    if (this._isMounted) {
      this.setState({ per: e.target.value }, this.loadList)
    }
  }

  handleSortBy(e) {
    if (this._isMounted) {
      if (e.target.value == "desc" || e.target.value == "asc") {
        this.setState({ orderBy: e.target.value }, this.loadList)
      } else {
        this.setState({ sortBy: e.target.value }, this.loadList)
      }
    }
  }

  handleScrolling() {
    setTimeout(() => {
      window.scroll({
        top: 0, 
        left: 0, 
        behavior: 'smooth'
      })
    }, 50)
  }
  
  render() {
    const { page, totalPages } = this.state
    const { customDividor } = this.props

    return (
      <>
        {this.props.children({
          ...this.state,
          handlePerPage: this.handlePerPage,
          handleSortBy: this.handlePerPage
        })}
        <Pagination
          customDividor={customDividor}
          currentPage={page}
          totalPages={totalPages}
          prev={this.loadPrevPage}
          next={this.loadNextPage}
        />
      </>
    )
  }
}

const mapStateToProps = state => ({
  user: state.auth.authUser
})

export default connect(mapStateToProps)(Paginate)