import React, { Component } from 'react'

class Filter extends Component {
  constructor() {
    super()
    this.state = {
      history: null,
    }
    this.recover = this.recover.bind(this)
		this.saveSearchHandler = this.saveSearchHandler.bind(this)
    this.handleFilteration = this.handleFilteration.bind(this)
  }

  componentDidMount() {
		this.recover()
	}

	recover() {
		//parse the localstorage value
		let data = JSON.parse(localStorage.getItem('history'))

		this.setState({ history: data })
	}

	saveSearchHandler() {
    //local storage only takes in key value pair so you would have to serialize it.
    // let history = this.state.history ? this.state.history : {history: []};

    // history.history.push({text:this.state.input, link:'store linke here'});

    // localStorage.setItem('history', JSON.stringify(history));
    console.log('This is save search handler')
	}

  handleFilteration(values) {
    console.log(values)
  }

  render() {
    return this.props.children({
      handleFilteration: this.handleFilteration,
      saveSearchHandler: this.saveSearchHandler
    })
  }
}

export default Filter