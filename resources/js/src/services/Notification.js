import React, { Component } from 'react'
import { connect } from 'react-redux'
import axios from 'axios'

class Notification extends Component {
  constructor(props) {
    super(props)
    this.state = {
      viewedMeCount: 0,
      favoritedMeCount: 0,
      reportedMeCount: 0,
      inboxCount: 0,
      requestsCount: 0,
    }
    this.confirmInterest = this.confirmInterest.bind(this)
    this.checkNotifications = this.checkNotifications.bind(this)
    this.confirmInterestView = this.confirmInterestView.bind(this)
    this.loadViewedMeCount = this.loadViewedMeCount.bind(this)
    this.loadFavoritedMeCount = this.loadFavoritedMeCount.bind(this)
    this.loadReportedMeCount = this.loadReportedMeCount.bind(this)
    this.loadInboxCount = this.loadInboxCount.bind(this)
    this.loadRequestsCount = this.loadRequestsCount.bind(this)
    this.confirmWaliRequest = this.confirmWaliRequest.bind(this)
    this.denyWaliRequest = this.denyWaliRequest.bind(this)
    this.markMessagesAsRead = this.markMessagesAsRead.bind(this)
  }

  componentDidMount() {
    this._isMounted = true

    const { page } = this.props
    const viewedMe = localStorage.getItem('viewedMe')
    const favoritedMe = localStorage.getItem('favoritedMe')
    const reportedMe = localStorage.getItem('reportedMe')
    const inbox = localStorage.getItem('inbox')
    const requests = localStorage.getItem('requests')

    this.checkNotifications()

    if (!viewedMe) {
      this.loadViewedMeCount()
    }

    if (!favoritedMe) {
      this.loadFavoritedMeCount()
    }

    if (!reportedMe) {
      this.loadReportedMeCount()
    }

    if (!inbox) {
      this.loadInboxCount()
    }

    if (!requests) {
      this.loadRequestsCount()
    }

    if (page === "Views" || page === "Favorites" || page === "Reports") {
      this.confirmInterestView(page)
    }

    if (page === "Chat") {
      this.markMessagesAsRead()
    }
  }

  componentWillUnmount() {
    this._isMounted = false
  }

  confirmInterest(tab, model) {
    const { user } = this.props

    axios({
      method: 'get',
      url: `/api/users/${user.identifier}/${tab}?status=pending`,
      headers: {
        'Authorization': `Bearer ${localStorage.getItem('token')}`
      }
    })
    .then(res => {
      for (let interest in res.data.data) {
        axios({
          method: 'put',
          url: `/api/${model}/${res.data.data[interest].identifier}`,
          headers: {
              'Authorization': `Bearer ${localStorage.getItem('token')}`
          }
        })
      }
    })
    .catch(err => console.log(err))
  }

  confirmWaliRequest(identifier, user_details, update) {
    const { requestsCount } = this.state
    const updatedRequestsCount = requestsCount - 1

    axios({
      method: 'put',
      url: `/api/wali_requests/${identifier}`,
      headers: {
          'Authorization': `Bearer ${localStorage.getItem('token')}`
      }
    })
    .then(res => {
      this.setState({ requestsCount: updatedRequestsCount })
      localStorage.setItem('requests', updatedRequestsCount)
      
      const waliRequest = {
          ...res.data.data,
          user_details: user_details
      }

      update(identifier, waliRequest)
    })
    .catch(err => console.log(err))
  }

  denyWaliRequest(identifier, filter) {
    const { requestsCount } = this.state
    const updatedRequestsCount = requestsCount - 1

    axios({
      method: 'delete',
      url: `/api/wali_requests/${identifier}`,
      headers: {
          'Authorization': `Bearer ${localStorage.getItem('token')}`
      }
    })
    .then(res => {
      this.setState({ requestsCount: updatedRequestsCount })
      localStorage.setItem('requests', updatedRequestsCount)

      filter(identifier)
    })
    .catch(err => console.log(err))
  }

  markMessagesAsRead() {
    const { contact } = this.props

    if (contact) {
      axios({
        method: 'get',
        url: `/api/users/unread-conversation/${contact}`,
        headers: {
          'Authorization': `Bearer ${localStorage.getItem('token')}`
        }
      })
      .then(res => {
        const { inboxCount } = this.state
        let readCount = 0
        const updatedInboxCount = inboxCount - readCount

        if (res.data.data) {

          for (let message in res.data.data) {
            axios({
              method: 'put',
              url: `/api/messages/${res.data.data[message].identifier}`,
              headers: {
                  'Authorization': `Bearer ${localStorage.getItem('token')}`
              }
            })

            readCount++
          }

          this.setState({ inboxCount: updatedInboxCount })
          localStorage.setItem('inbox', updatedInboxCount)
        }
      })
      .catch(err => console.log(err))

      if (localStorage.getItem('inbox') == 0) {
        localStorage.removeItem('inbox')
      }
    } else {
      this.setState({ inboxCount: 0 })
      localStorage.removeItem('inbox')
    }
  }

  checkNotifications() {
    const viewedMe = localStorage.getItem('viewedMe')
    const favoritedMe = localStorage.getItem('favoritedMe')
    const reportedMe = localStorage.getItem('reportedMe')
    const inbox = localStorage.getItem('inbox')
    const requests = localStorage.getItem('requests')

    if (!viewedMe) {
      localStorage.removeItem('viewedMe')
    } else {
      if (this._isMounted) this.setState({ viewedMeCount: parseInt(viewedMe) })
    }

    if (!favoritedMe) {
      localStorage.removeItem('favoritedMe')
    } else {
      if (this._isMounted) this.setState({ favoritedMeCount: parseInt(favoritedMe) })
    }

    if (!reportedMe) {
      localStorage.removeItem('reportedMe')
    } else {
      if (this._isMounted) this.setState({ reportedMeCount: parseInt(reportedMe) })
    }

    if (!inbox) {
      localStorage.removeItem('inbox')
    } else {
      if (this._isMounted) this.setState({ inboxCount: parseInt(inbox) })
    }

    if (!requests) {
      localStorage.removeItem('requests')
    } else {
      if (this._isMounted) this.setState({ requestsCount: parseInt(requests) })
    }
  }

  loadViewedMeCount() {
    const { user } = this.props
    
    axios({
      method: 'get',
      url: `/api/users/${user.identifier}/visitors?status=pending`,
      headers: {
        'Authorization': `Bearer ${localStorage.getItem('token')}`
      }
    })
    .then(res => {
      if (res.data.data) {
        if (this._isMounted) {
          if (res.data.meta.pagination.total > 0) {
            this.setState({ viewedMeCount: res.data.meta.pagination.total })
            localStorage.setItem('viewedMe', res.data.meta.pagination.total)
          }
        }
      }
    })
    .catch(err => console.log(err))
  }

  loadFavoritedMeCount() {
    const { user } = this.props

    axios({
      method: 'get',
      url: `/api/users/${user.identifier}/followers?status=pending`,
      headers: {
        'Authorization': `Bearer ${localStorage.getItem('token')}`
      }
    })
    .then(res => {
      if (res.data.data) {
        if (this._isMounted) {
          if (res.data.meta.pagination.total > 0) {
            this.setState({ favoritedMeCount: res.data.meta.pagination.total })
            localStorage.setItem('favoritedMe', res.data.meta.pagination.total)
          }
        }
      }
    })
    .catch(err => console.log(err))
  }

  loadReportedMeCount() {
    const { user } = this.props

    axios({
      method: 'get',
      url: `/api/users/${user.identifier}/reporters?status=pending`,
      headers: {
        'Authorization': `Bearer ${localStorage.getItem('token')}`
      }
    })
    .then(res => {
      if (res.data.data) {
        if (this._isMounted) {
          if (res.data.meta.pagination.total > 0) {
            this.setState({ reportedMeCount: res.data.meta.pagination.total })
            localStorage.setItem('reportedMe', res.data.meta.pagination.total)
          }
        }
      }
    })
    .catch(err => console.log(err))
  }

  loadInboxCount() {
    const { user } = this.props

    axios({
      method: 'get',
      url: `/api/users/${user.identifier}/inbox?isRead=0`,
      headers: {
        'Authorization': `Bearer ${localStorage.getItem('token')}`
      }
    })
    .then(res => {
      if (res.data.data) {
        if (this._isMounted) {
          if (res.data.meta.pagination.total > 0) {
            this.setState({ inboxCount: res.data.meta.pagination.total })
            localStorage.setItem('inbox', res.data.meta.pagination.total)
          }
        }
      }
    })
    .catch(err => console.log(err))
  }

  loadRequestsCount() {
    const { user } = this.props

    axios({
      method: 'get',
      url: `/api/users/${user.identifier}/wali_requesters?status=pending`,
      headers: {
        'Authorization': `Bearer ${localStorage.getItem('token')}`
      }
    })
    .then(res => {
      if (res.data.data) {
        if (this._isMounted) {
          if (res.data.meta.pagination.total > 0) {
            this.setState({ requestsCount: res.data.meta.pagination.total })
            localStorage.setItem('requests', res.data.meta.pagination.total)
          }
        }
      }
    })
    .catch(err => console.log(err))
  }

  confirmInterestView(page) {
    const viewedMe = parseInt(localStorage.getItem('viewedMe'))
    const favoritedMe = parseInt(localStorage.getItem('favoritedMe'))
    const reportedMe = parseInt(localStorage.getItem('reportedMe'))

    if (page === "Views") {
      if (viewedMe > 0) {
        this.confirmInterest('visitors', 'visits')
        this.setState({ viewedMeCount: 0 })
        localStorage.removeItem('viewedMe')
      }
    }

    if (page === "Favorites") {
      if (favoritedMe > 0) {
        this.confirmInterest('followers', 'favorites')
        this.setState({ favoritedMeCount: 0 })
        localStorage.removeItem('favoritedMe')
      }
    }

    if (page === "Reports") {
      if (reportedMe > 0) {
        this.confirmInterest('reporters', 'reports')
        this.setState({ reportedMeCount: 0 })
        localStorage.removeItem('reportedMe')
      }
    }
  }

  render() {
    return this.props.children({
      ...this.state,
      confirmWaliRequest: this.confirmWaliRequest,
      denyWaliRequest: this.denyWaliRequest
    })
  }
}

const mapStateToProps = state => ({
  user: state.auth.authUser
})

export default connect(mapStateToProps)(Notification)