import React, { Component } from 'react'
import { connect } from 'react-redux'
import { withStyles } from '@material-ui/core'
import axios from 'axios'

const styles = theme => ({
  list: {
    listStyle: 'none',
    margin: 0,
    padding: theme.spacing(2),
  },
})

class InfiniteScroll extends Component {
  constructor(props) {
    super(props)
    this._isMounted = false
    this.state = {
      loading: false,
      list: [],
      per: 10,
      page: 1,
      totalPages: null,
      scrolling: false
    }
    this.loadList = this.loadList.bind(this)
    this.loadMore = this.loadMore.bind(this)
    this.handleScroll = this.handleScroll.bind(this)
    this.updateResource = this.updateResource.bind(this)
    this.filterResource = this.filterResource.bind(this)
  }

  componentDidMount() {
    this._isMounted = true

    this.loadList()
    this.scrollListenter  = window.addEventListener('scroll', (e) => {
      this.handleScroll(e)
    })
  }

  componentWillUnmount() {
    this._isMounted = false
  }

  loadList() {
    if (this._isMounted) this.setState({ loading: true })
    
    const { user, resource } = this.props
    const { per, page, list } = this.state

    axios({
      method: 'get',
      url: `/api/users/${user.identifier}/${resource}?per_page=${per}&page=${page}&order_by=desc`,
      headers: {
        'Authorization': `Bearer ${localStorage.getItem('token')}`
      }
    })
    .then(res => {
      if (this._isMounted) {

        if (res.data.data) {
          this.setState({
            list: [...list, ...res.data.data],
            scrolling: false,
            totalPages: res.data.meta.pagination.total_pages,
            loading: false,
          })
        } else {
          this.setState({
            scrolling: false,
            loading: false,
          })
        }
      }
    })
    .catch(err => console.log(err))
  }

  loadMore() {
    this.setState(prevState => ({
      page: prevState.page + 1,
      scrolling: true,
    }), this.loadList)
  }

  handleScroll() {
    if (this._isMounted) {
      const { scrolling, totalPages, page } = this.state
      const { resource } = this.props
      
      if (scrolling) return
      if (totalPages <= page) return
      const lastLi = document.querySelector(`ul#${resource} > li:last-child`)
      const lastLiOffset = lastLi.offsetTop + lastLi.clientHeight
      const pageOffset = window.pageYOffset + window.innerHeight
      let bottomOffset = 40
      if (pageOffset > lastLiOffset - bottomOffset) this.loadMore()
    }
  }

  updateResource(resourceId, resource) {
    const { list } = this.state
    const updatedList = list.filter(item => item.identifier !== resourceId)

    this.setState({ list: [...updatedList, resource] })
  }

  filterResource(resourceId) {
    const { list } = this.state
    const updatedList = list.filter(item => item.identifier !== resourceId)

    this.setState({ list: updatedList })
  }

  render() {
    const { classes, resource } = this.props

    return (
      <ul id={resource} className={classes.list}>
        {this.props.children({
          ...this.state,
          updateResource: this.updateResource,
          filterResource: this.filterResource,
        })}
      </ul>
    )
  }
}

const mapStateToProps = state => ({
  user: state.auth.authUser
})

export default connect(mapStateToProps)(withStyles(styles)(InfiniteScroll))