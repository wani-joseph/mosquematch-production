import React, { Component } from 'react'
import { connect } from 'react-redux'
import Layout from '../hoc/Layout/Layout'
import SearchTemplate from '../components/Search/Search'
import Loader from '../components/UI/Loader/Loader'
import CustomModal  from '../components/UI/CustomModal/CustomModal'
import CustomTextField from '../components/UI/CustomTextField/CustomTextField'
import CustomTypography from '../components/UI/CustomTypography/CustomTypography'
import * as actions from '../store/actions'
import axios from 'axios'

class Search extends Component {
    constructor() {
        super()
        this._isMounted = false
        this.state = {
            loading: false,
            members: [],
            per: 10,
            page: 1,
            total: null,
            totalPages: null,
            sortBy: 'status',
            orderBy: 'desc',
            showMap: false,
            hoveredCardId: 0,
            modal: false,
            filters: [],
            searches: [],
            searchName: '',
            viewSearches: false,
            mosques : [],
            search: '',
            distance: 1000,
            options: [
                {id: 1, value: "Viewed", isChecked: false},
                {id: 2, value: "Viewed Me", isChecked: false},
                {id: 3, value: "Favorited", isChecked: false},
                {id: 4, value: "Favorited Me", isChecked: false},
                {id: 5, value: "Pictures", isChecked: false},
                {id: 6, value: "Has a Wali", isChecked: false},
            ],
            'ethnicity': [
                {id: 1, value: "Asian", isChecked: false},
                {id: 2, value: "African", isChecked: false},
                {id: 3, value: "Latin", isChecked: false},
                {id: 4, value: "East Indian", isChecked: false},
                {id: 5, value: "Mixed", isChecked: false},
                {id: 6, value: "Native American", isChecked: false},
                {id: 7, value: "Pacific Islander", isChecked: false},
                {id: 8, value: "Caucasian", isChecked: false},
                {id: 9, value: "other", isChecked: false},
            ],
            'body type': [
                {id: 1, value: "Petite", isChecked: false},
                {id: 2, value: "Slim", isChecked: false},
                {id: 3, value: "Athletic", isChecked: false},
                {id: 4, value: "Medium", isChecked: false},
                {id: 5, value: "Muscular", isChecked: false},
                {id: 6, value: "Large", isChecked: false},
            ],
            'relationship status': [],
            'my income': [
                {id: 1, value: "Below Average", isChecked: false},
                {id: 2, value: "Average", isChecked: false},
                {id: 3, value: "Above Average", isChecked: false},
            ],
            'maritial status': [
                {id: 1, value: "Never married", isChecked: false},
                {id: 2, value: "Legally married", isChecked: false},
                {id: 3, value: "Divorced", isChecked: false},
                {id: 4, value: "Widowed", isChecked: false},
                {id: 5, value: "Analled", isChecked: false},
            ],
            'children': [],
            'smoking': [
                {id: 1, value: "No", isChecked: false},
                {id: 2, value: "Yes", isChecked: false},
                {id: 3, value: "Sometimes", isChecked: false},
                {id: 4, value: "Stopped", isChecked: false},
            ],
            'drinking': [
                {id: 1, value: "No", isChecked: false},
                {id: 2, value: "Yes", isChecked: false},
                {id: 3, value: "Sometimes", isChecked: false},
                {id: 4, value: "Stopped", isChecked: false},
            ],
            'willing to relocate': [],
            'looking to marry within': [],
            'education': [],
            'subjects studied': [],
            'time on phone daily': [
                {id: 1, value: "Attached to my phone", isChecked: false},
                {id: 2, value: "Regular use", isChecked: false},
                {id: 3, value: "Not much", isChecked: false},
            ],
            'religiousness': [
                {id: 1, value: "Very religious", isChecked: false},
                {id: 2, value: "Religious", isChecked: false},
                {id: 3, value: "Somewhat religious", isChecked: false},
                {id: 4, value: "Not religious", isChecked: false},
                {id: 5, value: "Prefer not say", isChecked: false},
            ],
            'sect': [
                {id: 1, value: "Just muslim", isChecked: false},
                {id: 2, value: "Sunni", isChecked: false},
                {id: 3, value: "Shia", isChecked: false},
                {id: 4, value: "Other", isChecked: false},
            ],
            'revert': [
                {id: 1, value: "Yes Niqab", isChecked: false},
                {id: 2, value: "No", isChecked: false},
            ],
            'hijab / niqab': [
                {id: 1, value: "Yes Hijab", isChecked: false},
                {id: 2, value: "Yes Niqab", isChecked: false},
                {id: 3, value: "No", isChecked: false},
                {id: 4, value: "Other", isChecked: false},
            ],
            'halal': [
                {id: 1, value: "Always keep halal", isChecked: false},
                {id: 2, value: "Usually keep halal", isChecked: false},
                {id: 3, value: "I keep halal at home", isChecked: false},
                {id: 4, value: "Don't keep halal", isChecked: false},
            ],
            'praying': [
                {id: 1, value: "Always", isChecked: false},
                {id: 2, value: "Usually", isChecked: false},
                {id: 3, value: "Sometimes", isChecked: false},
                {id: 4, value: "Never", isChecked: false},
            ],
            'reading quran': [
                {id: 1, value: "Always", isChecked: false},
                {id: 2, value: "Usually", isChecked: false},
                {id: 3, value: "Sometimes", isChecked: false},
                {id: 4, value: "Never", isChecked: false},
            ],
        }
        this.loadMembers = this.loadMembers.bind(this)
        this.loadNextPage = this.loadNextPage.bind(this)
        this.loadPrevPage = this.loadPrevPage.bind(this)
        this.handleScolling = this.handleScolling.bind(this)
        this.handlePerPage = this.handlePerPage.bind(this)
        this.handleSortBy = this.handleSortBy.bind(this)
        // this.handleSearch = this.handleSearch.bind(this)
        this.showConfirmationModal = this.openModal.bind(this)
        this.closeModal = this.closeModal.bind(this)
        this.loadSavedSearches = this.loadSavedSearches.bind(this)
        this.setSearchName = this.setSearchName.bind(this)
        this.handleFilteration = this.handleFilteration.bind(this)
        this.saveSearchHandler = this.saveSearchHandler.bind(this)
        this.viewSearchHandler = this.viewSearchHandler.bind(this)
        this.searchViewed = this.searchViewed.bind(this)
        this.runSearchHandler = this.runSearchHandler.bind(this)
        this.deleteSearchHandler = this.deleteSearchHandler.bind(this)
        this.handleShowMap = this.handleShowMap.bind(this)
        this.setCardMarkerHover = this.setCardMarkerHover.bind(this)
        this.resetCardMarkerHover = this.resetCardMarkerHover.bind(this)
        this.handleSearch = this.handleSearch.bind(this)
        // Attaching Lodash debounce to avoid frequent ajax requests
        this.getMosques = _.debounce(this.getMosques, 500)
        this.mosqueMembers = this.mosqueMembers.bind(this)
        this.handleAllChecked = this.handleAllChecked.bind(this)
        this.handleCheckbox = this.handleCheckbox.bind(this)
        this.handleOptions = this.handleOptions.bind(this)
    }

    componentDidMount() {
        this._isMounted = true

        const { userStatus } = this.props

        Echo.join('chat')
            .joining((user) => {
                userStatus(user.id, 'online')
            })
            .leaving((user) => {
                userStatus(user.id, 'offline')
            })
            .listen('UserOnline', (e) => {
                this.loadMembers()
            })
            .listen('UserOffline', (e) => {
                this.loadMembers()
            })

        document.getElementById('search').addEventListener('keydown', (e) => {
            // check whether up or down arrow keys are pressed
            if (e.keyCode === 38 || e.keyCode === 40) {
                // To prevent cursor from moving left or right in text input
                // You can only prevent it in keydown event
                // If you only use keyup then event already fired
                e.preventDefault()
            }
        })

        this.loadMembers("first loading")
        this.loadSavedSearches()
    }

    componentWillUnmount() {
        this._isMounted = false
    }

    openModal(values) {
        if (this._isMounted) {
            this.setState({
                modal: true,
                filters: values
            })
        }
    }

    closeModal() {
        if (this._isMounted) {
            this.setState({
                searchName: '',
                modal: false
            })
        }
    }

    setSearchName(event) {
        if (this._isMounted) {
            this.setState({ searchName: event.target.value })
        }
    }

    searchViewed() {
        if (this._isMounted) {
            this.setState({ viewSearches: false })
        }
    }

    loadMembers(load) {
        if (this._isMounted) {
            if (load == "first loading") this.setState({ loading: true })
        
            const { user } = this.props
            const {
                per,
                page,
                sortBy,
                orderBy
            } = this.state

            axios({
                method: 'get',
                url: `/api/users/${user.identifier}/filters?per_page=${per}&page=${page}&sort_by=${sortBy}&order_by=${orderBy}`,
                headers: {
                    'Authorization': `Bearer ${localStorage.getItem('token')}`
                }
            })
            .then(res => {
                
                if (res.data.data) {
                    const {
                        per_page,
                        current_page,
                        total_pages,
                        total
                    } = res.data.meta.pagination

                    this.setState({
                        members: res.data.data,
                        per: per_page,
                        page: current_page,
                        totalPages: total_pages,
                        total: total,
                        loading: false,
                    })
                } else {
                    this.setState({ loading: false })
                }
            })
            .catch(err => console.log(err))
        }
    }

    mosqueMembers(mosque) {
        if (this._isMounted) {

            axios({
                method: 'get',
                url: `/api/mosques/${mosque}/users`,
                headers: {
                    'Authorization': `Bearer ${localStorage.getItem('token')}`
                }
            })
            .then(res =>{
                if (res.data.data) {
                    const {
                        per_page,
                        current_page,
                        total_pages,
                        total
                    } = res.data.meta.pagination

                    const updatedMemerbs = [...this.state.members, ...res.data.data]

                    this.setState({
                        members: updatedMemerbs,
                        per: per_page,
                        page: current_page,
                        totalPages: total_pages,
                        total: total,
                        loading: false,
                    })
                } else {
                    this.setState({ loading: false })
                }
            })
            .catch(err => console.log(err))
        }
    }

    loadSavedSearches() {
        if (this._isMounted) {
            const { user } = this.props

            axios({
                method: 'get',
                url: `/api/users/${user.identifier}/saved-searches`,
                headers: {
                    'Authorization': `Bearer ${localStorage.getItem('token')}`
                }
            })
            .then(res => {
                if (res.data.data) {
                    this.setState({
                        searches: res.data.data,
                        loading: false,
                    })
                } else {
                    this.setState({
                        loading: false,
                    })
                }
            })
            .catch(err => console.log(err))
        }
    }

    handleSearch(e) {
        // check whether arrow keys are pressed using Loadash
        if(_.includes([37, 38, 39, 40, 13], e.keyCode) ) {
            if (e.keyCode === 38 || e.keyCode === 40) {
                // To prevent cursor from moving left or right in text input
                e.preventDefault()
            }

            if (e.keyCode === 40 && this.state.mosques == "") {
                // If mosque list is cleared and search input is not empty 
                // then call ajax again on down arrow key press 
                this.getMosques()
                return
            }

        } else {
            this.getMosques()
        }
    }

    getMosques() {
        this.setState(() => ({ 
            mosques: [],
            members: [],
            search: document.getElementById('search').value 
        }))

        if (this.state.search.trim() != '') {
            axios({
                method: 'post',
                url: '/api/mosques-search-by-location',
                data: { search : this.state.search },
                headers: {
                    'Authorization': `Bearer ${localStorage.getItem('token')}`
                }
            })
            .then( (response) => {
                if (response.data.data)
                    this.setState(() => ({ mosques : response.data.data }))
            })
            .catch( (error) => {
                console.log(error)
            })  
        }
    }

    loadNextPage() {
        const { totalPages, page } = this.state

        if (totalPages <= page) return

        if (this._isMounted) {
            this.setState(prevState => ({
                page: prevState.page + 1,
            }), this.loadMembers)
        }

        this.handleScolling()
    }

    loadPrevPage() {
        const { page } = this.state

        if (page == 1) return

        if (this._isMounted) {
            this.setState(prevState => ({
                page: prevState.page - 1,
            }), this.loadMembers)
        }

        this.handleScolling()
    }

    handleScolling() {
        setTimeout(() => {
            window.scroll({
                top: 0, 
                left: 0, 
                behavior: 'smooth'
            })
        }, 50)
    }

    handlePerPage(e) {
        if (this._isMounted) {
            this.setState({ per: e.target.value }, this.loadMembers)
        }
    }

    handleSortBy(e) {
        if (this._isMounted) {
            if (e.target.value == "desc" || e.target.value == "asc") {
                this.setState({ orderBy: e.target.value }, this.loadMembers)
            } else {
                this.setState({ sortBy: e.target.value }, this.loadMembers)
            }
        }
    }

    handleAllChecked(event, summary) {
        let filters = this.state[summary]
        filters.forEach(filter => filter.isChecked = event.target.checked)
        this.setState({ [summary]: filters })
    }

    handleCheckbox(event, summary) {
        let filters = this.state[summary]
        let filter = summary
        
        if (summary == 'body type') filter = 'build'
        if (summary == 'my income') filter = 'income'
        if (summary == 'maritial status') filter = 'martialStatus'
        if (summary == 'smoking') filter = 'smoke'
        if (summary == 'drinking') filter = 'drink'
        if (summary == 'time on phone daily') filter = 'phonetime'
        if (summary == 'religiousness') filter = 'religious'
        if (summary == 'sect') filter = 'sector'
        if (summary == 'hijab / niqab') filter = 'hijab'
        if (summary == 'halal') filter = 'keepHalal'
        if (summary == 'praying') filter = 'prayer'
        if (summary == 'reading quran') filter = 'readQuran'
        
        filters.forEach(filter => {
            if (filter.value === event.target.value)
                filter.isChecked =  event.target.checked
        })
		this.setState({ [summary]: filters }, this.handleFilteration(filter, event.target.value, event.target.checked))
    }

    handleOptions(value, checked) {

        if (checked) {

            if (this._isMounted) {
                this.setState({ loading: true })
            
                const { user } = this.props
                const {
                    per,
                    page,
                    sortBy,
                    orderBy
                } = this.state
    
                axios({
                    method: 'get',
                    url: `/api/users/${user.identifier}/filters?per_page=${per}&page=${page}&sort_by=${sortBy}&order_by=${orderBy}&filters=${value}`,
                    headers: {
                        'Authorization': `Bearer ${localStorage.getItem('token')}`
                    }
                })
                .then(res => {
                    
                    if (res.data.data) {
                        const {
                            per_page,
                            current_page,
                            total_pages,
                            total
                        } = res.data.meta.pagination
    
                        this.setState({
                            members: res.data.data,
                            per: per_page,
                            page: current_page,
                            totalPages: total_pages,
                            total: total,
                            loading: false,
                        })
                    } else {
                        this.setState({ loading: false })
                    }
                })
                .catch(err => console.log(err))
            }
        }
    }

    // handleSearch(param) {
    //     if (this._isMounted) {
    //         this.setState({ loading: true })

    //         axios({
    //             method: 'get',
    //             url: `/api/search-users?s=${param}`,
    //             headers: {
    //                 'Authorization': `Bearer ${localStorage.getItem('token')}`
    //             }
    //         })
    //         .then(res => {

    //             if (res.data.data) {
    //                 const { per_page, current_page, total_pages, total } = res.data.meta.pagination

    //                 this.setState({
    //                     members: res.data.data,
    //                     per: per_page,
    //                     page: current_page,
    //                     totalPages: total_pages,
    //                     total: total,
    //                     loading: false
    //                 })

    //             } else {
    //                 this.setState({ loading: false })
    //             }
    //         })
    //         .catch(err => console.log(err))
    //     }
    // }

    handleFilteration(filter, value, checked) {
        if (this._isMounted) {
            if (filter == 'options') return this.handleOptions(value, checked)

            let updatedFilters = this.state.filters.filter(item => item.filter !== filter)
            updatedFilters.push({ filter, value })
            
            this.setState({ loading: true, filters: updatedFilters }, () => {
                const { user } = this.props
                const {
                    per,
                    page,
                    sortBy,
                    orderBy,
                    filters
                } = this.state
    
                let url = `/api/users/${user.identifier}/filters?per_page=${per}&page=${page}&sort_by=${sortBy}&order_by=${orderBy}`
    
                if (checked) {
                    filters.map(item => url = url + `&${item.filter}=${item.value}`)
                }
                
                axios({
                    method: 'get',
                    url: url,
                    headers: {
                        'Authorization': `Bearer ${localStorage.getItem('token')}`
                    }
                })
                .then(res => {
                    
                    if (res.data.data) {
                        const {
                            per_page,
                            current_page,
                            total_pages,
                            total
                        } = res.data.meta.pagination
    
                        this.setState({
                            members: res.data.data,
                            per: per_page,
                            page: current_page,
                            totalPages: total_pages,
                            total: total,
                            loading: false,
                        })
                    } else {
                        this.setState({ loading: false })
                    }
                })
                .catch(err => console.log(err))
            })
        }
    }

    saveSearchHandler() {
        if (this._isMounted) {
            const { user } = this.props
            const { per, page, sortBy, orderBy, searches, searchName, filters } = this.state

            if (searchName != "") {

                let gender = user.gender
                let url = `/api/users/${user.identifier}/females?per_page=${per}&page=${page}&sort_by=${sortBy}&order_by=${orderBy}`
        
                if (gender === 'female') {
                    url = `/api/users/${user.identifier}/males?per_page=${per}&page=${page}&sort_by=${sortBy}&order_by=${orderBy}`
                }
        
                for (let filter in filters) {
                    if (filters[filter] != "") {
                        url = url + `&${filter}=${filters[filter]}`
                    }
                }

                const data = {
                    searchName: searchName,
                    searchUrl: url,
                    member: user.identifier
                }
                
                axios({
                    method: 'post',
                    url: `/api/users/${user.identifier}/saved-searches`,
                    data: data,
                    headers: {
                        'Authorization': `Bearer ${localStorage.getItem('token')}`
                    }
                })
                .then(res => {
                    if (res.data.data) {
                        this.setState({
                            searches: [...searches, res.data.data],
                            searchName: '',
                            modal: false
                        })
                    } else {
                        this.setState({
                            searchName: '',
                            modal: false
                        })
                    }
                })
                .catch(err => console.log(err))
                
            } else {
                document.querySelector('#confirm').innerHTML = 'Search name is a required field.'
            }
        }
    }

    viewSearchHandler() {
        if (this._isMounted) {
            this.setState({ viewSearches: true })
        }
    }

    runSearchHandler(savedSearch) {
        if (this._isMounted) {
            this.setState({ loading: true })

            axios({
                method: 'get',
                url: savedSearch.searchUrl,
                headers: {
                    'Authorization': `Bearer ${localStorage.getItem('token')}`
                }
            })
            .then(res => {
                if (res.data.data) {
                    if (res.data.data) {
                        const { per_page, current_page, total_pages, total } = res.data.meta.pagination
    
                        this.setState({
                            members: res.data.data,
                            per: per_page,
                            page: current_page,
                            totalPages: total_pages,
                            total: total,
                            loading: false,
                            viewSearches: false
                        })
                    } else {
                        this.setState({
                            loading: false,
                            viewSearches: false
                        })
                    }

                } else {
                    this.setState({ loading: false })
                }
            })
            .catch(err => console.log(err))
        }
    }

    deleteSearchHandler(savedSearch) {
        if (this._isMounted) {
            const { user } = this.props
            const { searches } = this.state

            axios({
                method: 'delete',
                url: `/api/users/${user.identifier}/saved-searches/${savedSearch.identifier}`,
                headers: {
                    'Authorization': `Bearer ${localStorage.getItem('token')}`
                }
            })
            .then(res => {
                if (res.data.data) {
                    this.setState({
                        searches: searches.filter(search => search.identifier !== res.data.data.identifier)
                    })
                }
            })
            .catch(err => console.log(err))
        }
    }

    handleShowMap() {
        if (this._isMounted) {
            this.setState(prevState => ({ showMap: !prevState.showMap}))
        }
    }
    
    setCardMarkerHover(member) {
        if (this.state.showMap) {
            if (this._isMounted) {
                this.setState({
                    hoveredCardId: member.identifier
                })
            }
        }
    }

    resetCardMarkerHover() {
        if (this.state.showMap) {
            if (this._isMounted) {
                this.setState({
                    hoveredCardId: 0
                })
            }
        }
    }

    render() {
        const { user } = this.props
        const {
            total,
            totalPages,
            page,
            members,
            loading,
            per,
            sortBy,
            showMap,
            hoveredCardId,
            modal,
            searches,
            searchName,
            viewSearches,
            search,
            mosques
        } = this.state

        let searchTemplate = <Loader />

        if (!loading) searchTemplate = (
            <SearchTemplate
                handleFilteration={this.handleFilteration}
                saveSearchHandler={this.showConfirmationModal}
                searches={searches}
                viewSearches={viewSearches}
                searchViewed={this.searchViewed}
                viewSearchHandler={this.viewSearchHandler}
                runSearchHandler={this.runSearchHandler}
                deleteSearchHandler={this.deleteSearchHandler}
                loadMembers={this.loadMembers}
                loading={loading}
                currentPage={page}
                totalPages={totalPages}
                total={total}
                results={members}
                perPage={per}
                handlePerPage={this.handlePerPage}
                sortBy={sortBy}
                handleSortBy={this.handleSortBy}
                prev={this.loadPrevPage}
                next={this.loadNextPage}
                showMap={showMap}
                handleShowMap={this.handleShowMap}
                hoveredCardId={hoveredCardId}
                setCardMarkerHover={this.setCardMarkerHover}
                resetCardMarkerHover={this.resetCardMarkerHover}
                search={search}
                handleSearch={this.handleSearch}
                getMosques={this.getMosques}
                mosques={mosques}
                mosqueMembers={this.mosqueMembers}
                handleAllChecked={this.handleAllChecked}
                handleCheckbox={this.handleCheckbox}
                readingQuran={this.state['reading quran']}
                praying={this.state.praying}
                options={this.state.options}
                religiousness={this.state.religiousness}
                sect={this.state.sect}
                revert={this.state.revert}
                hijabNiqab={this.state['hijab / niqab']}
                halal={this.state.halal}
                ethnicity={this.state.ethnicity}
                bodyType={this.state['body type']}
                income={this.state['my income']}
                maritialStatus={this.state['maritial status']}
                smoking={this.state.smoking}
                drinking={this.state.drinking}
                phoneTime={this.state['time on phone daily']}
                distance={this.state.distance}
            />
        )
        
        return (
            <Layout user={user}>
                <CustomModal
                    show={modal}
                    handleClose={this.closeModal}
                    title={'Save Search'}
                    content={
                        <>
                            <CustomTypography bold="true" bottommargin="true">
                                Save your search by name
                            </CustomTypography>
                            <CustomTextField
                                type="text"
                                value={searchName}
                                placeholder="Type your search name here..."
                                onChange={this.setSearchName}
                                autoFocus={true}
                                fullWidth
                            />
                            <CustomTypography id="confirm" invalid="true" />
                        </>
                    }
                    confirmation={this.saveSearchHandler}
                />
               {searchTemplate}
            </Layout>
        )
    }
}

const mapStateToProps = state => ({
    user: state.auth.authUser
})

const mapDispatchToprops = dispatch => ({
    userStatus: (userId, status) => dispatch(actions.userStatus(userId, status))
})

export default connect(mapStateToProps, mapDispatchToprops)(Search)