import React, { Component } from 'react'
import { connect } from 'react-redux'
import Layout from '../hoc/Layout/Layout'
import {
    Container,
    Grid
} from '@material-ui/core'
import GridItem from '../components/UI/GridItem/GridItem'
import Sidebar from '../components/Sidebar/Sidebar'
import Heading from '../components/Heading/Heading'
import MessagesTemplate from '../components/Messages/Messages'
import * as actions from '../store/actions'

class Messages extends Component {
    componentDidMount() {
        const { userStatus } = this.props

        Echo.join('chat')
            .joining((user) => {
                userStatus(user.id, 'online')
            })
            .leaving((user) => {
                userStatus(user.id, 'offline')
            })
            .listen('UserOnline', (e) => {
                //
            })
            .listen('UserOffline', (e) => {
                //
            });
    }

    render() {
        const { page, contact } = this.props

        return (
            <Layout>
                <Container maxWidth="lg">
                    <Grid container>
                        <GridItem xs={12} sm={12} md={3}>
                            <Heading text={page} />
                            <Sidebar
                                page={page}
                                items={[
                                    { link: '/messages/inbox', text: 'Inbox', badge: "inboxCount" },
                                    { link: '/messages/sent', text: 'Sent' },
                                    { link: '/messages/requests', text: 'Requests', badge: "requestsCount" },
                                    { link: '/messages/templates', text: 'Templates' },
                                ]}
                            />
                        </GridItem>
                        <GridItem xs={12} sm={12} md={9}>
                            <Heading text={page} />
                            <MessagesTemplate
                                page={page}
                                contact={contact}
                            />
                        </GridItem>
                    </Grid>      
                </Container>
            </Layout>
        )
    }
}

const mapDispatchToProps = dispatch => ({
    userStatus: (userId, status) => dispatch(actions.userStatus(userId, status)),
})

export default connect(null, mapDispatchToProps)(Messages)