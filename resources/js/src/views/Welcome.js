import React from 'react'
import Header from '../components/Header/Header'
import Logo from '../components/Logo/Logo'
import Account from '../components/Account/Acount'
import HeroSection from '../components/HomeSections/Hero/Hero'
import AboutSection from '../components/HomeSections/About/About'
import HowItworks from '../components/HomeSections/HowItWorks/HowItWorks'
import Testimonial from '../components/HomeSections/Testimonial/Testimonial'
import Footer from '../components/Footer/Footer'

const Welcome = () => {
    return (
        <React.Fragment>
            <Header
                spacing="true"
                siteLogo={<Logo />}
                account={<Account />}
            />
            <HeroSection />
            {/* <AboutSection /> */}
            <HowItworks />
            <Testimonial />
            <Footer />
        </React.Fragment>
    )
}

export default Welcome