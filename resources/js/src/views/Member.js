import React, { Component } from 'react'
import { connect } from 'react-redux'
import Layout from '../hoc/Layout/Layout'
import Loader from '../components/UI/Loader/Loader'
import SingleMemder from '../components/SingleMember/SingleMember'
import * as actions from '../store/actions'
import { getMemberIdentifier } from '../shared/utility'

class Member extends Component {
    componentDidMount() {
        const { member, onViewMember, user, addToViewList } = this.props
        const memberId = getMemberIdentifier(member)

        onViewMember(memberId)
        addToViewList(user.identifier, parseInt(memberId))
    }

    render() {
        const { memberProfile } = this.props

        let pageContent = <Loader />

        if (memberProfile) pageContent = (
            <SingleMemder {...memberProfile} />
        )

        return (
            <Layout>
               {pageContent}
            </Layout>
        )
    }
}

const mapStateToProps = state => ({
    memberProfile: state.member.member,
    user: state.auth.authUser,
})

const mapDispatchToProps = dispatch => ({
    onViewMember: (memberId) => dispatch(actions.fetchMember(memberId)),
    addToViewList: (userId, memberId) => dispatch(actions.addToViewList(userId, memberId))
})

export default connect(mapStateToProps, mapDispatchToProps)(Member)