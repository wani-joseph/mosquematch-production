import React, { Component } from 'react'
import { connect } from 'react-redux'
import Layout from '../hoc/Layout/Layout'
import { Container, Grid } from '@material-ui/core'
import GridItem from '../components/UI/GridItem/GridItem'
import Sidebar from '../components/Sidebar/Sidebar'
import Heading from '../components/Heading/Heading'
import UpdateProfileTemplate from '../components/UpdateProfile/UpdateProfile'
import * as actions from '../store/actions'

class UpdateProfile extends Component {
    constructor() {
        super()
        this.handleMemberUpdate = this.handleSubmit.bind(this)
    }

    handleSubmit(values, actions) {
        const { user, onMemberUpdate } = this.props

        onMemberUpdate(user.identifier, values, () => {
            console.log("Show popup that displays your data has been updated.")
        })

        actions.setSubmitting(true)
    }

    render() {
        const { link, fields } = this.props

        return (
            <Layout>
                <Container maxWidth="lg">
                    <Grid container>
                        <GridItem xs={12} sm={12} md={3}>
                            <Heading text={fields} />
                            <Sidebar
                                items={[
                                    { link: `/profile/${link}/basic-information`, text: 'Basic Information' },
                                    { link: `/profile/${link}/about-me`, text: 'About Me' },
                                    { link: `/profile/${link}/education-work`, text: 'Education & Work' },
                                    { link: `/profile/${link}/personal-information`, text: 'Personal Inormation' },
                                    { link: `/profile/${link}/religion`, text: 'Religion' },
                                    { link: `/profile/${link}/photos`, text: 'Photos' },
                                ]}
                            />
                        </GridItem>
                        <GridItem xs={12} sm={12} md={9}>
                            <Heading text={fields} />
                            <UpdateProfileTemplate
                                page={fields}
                                handleMemberUpdate={this.handleMemberUpdate}
                            />
                        </GridItem>
                    </Grid>
                </Container>
            </Layout>
        )
    }
}

const mapStateToProps = state => ({
    user: state.auth.authUser
})

const mapDispatchToprops = dispatch => ({
    onMemberUpdate: (userId, data, next) => dispatch(actions.updateCurrentUser(userId, data, next))
})

export default connect(mapStateToProps, mapDispatchToprops)(UpdateProfile)