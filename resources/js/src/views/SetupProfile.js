import React, { Component } from 'react'
import withStyles from "@material-ui/core/styles/withStyles"
import Header from '../components/Header/Header'
import Logo from '../components/Logo/Logo'
import SetupProfileTemplate from '../components/SetupProfile/SetupProfile'

const styles = theme => ({
    header: {
        paddingTop: theme.spacing(1),
        paddingBottom: theme.spacing(1)
    },
    pageContent: {
        marginTop: theme.spacing(10)
    }
})

class SetupProfile extends Component {

    render() {
        const { fields, nextFields, classes } = this.props

        return (
            <React.Fragment>
                <Header
                    classes={{ root: classes.header }}
                    siteLogo={<Logo />}
                />
                <main className={classes.pageContent}>
                    <SetupProfileTemplate
                        page={fields}
                        nextFields={nextFields}
                    />
                </main>
            </React.Fragment>
        )
    }
}

export default withStyles(styles)(SetupProfile)