import React, { Component } from 'react'
import { connect } from 'react-redux'
import Layout from '../hoc/Layout/Layout'
import InterestTemplate from '../components/Interest/Interest'
import * as actions from '../store/actions'
import {
    confirmViewedMeCount,
    confirmFavoritedMeCount,
    confirmReportedMeCount
} from '../store/actions/auth'

class Interest extends Component {
    componentDidMount() {
        const {
            tab,
            user,
            viewedMeCount,
            favoritedMeCount,
            reportedMeCount,
            userStatus,
            confirmViewedMeCount,
            confirmFavoritedMeCount,
            confirmReportedMeCount
        } = this.props

        Echo.join('chat')
            .joining((user) => {
                userStatus(user.id, 'online')
            })
            .leaving((user) => {
                userStatus(user.id, 'offline')
            })
            .listen('UserOnline', (e) => {
                //
            })
            .listen('UserOffline', (e) => {
                //
            });

        if (tab === "Views") {
            confirmViewedMeCount(user.identifier, viewedMeCount)
        }

        if (tab === "Favorites") {
            confirmFavoritedMeCount(user.identifier, favoritedMeCount)
        }

        // if (tab === "Reports") {
        //     confirmReportedMeCount(user.identifier, reportedMeCount)
        // }
    }
    
    render() {
        const { tab } = this.props

        return (
            <Layout>
                <InterestTemplate tab={tab} />
            </Layout>
        )
    }
}

const mapStateToProps = state => ({
    user: state.auth.authUser,
	viewedMeCount: state.auth.viewedMeCount,
	favoritedMeCount: state.auth.favoritedMeCount,
	reportedMeCount: state.auth.reportedMeCount
})

const mapDispatchToProps = dispatch => ({
    userStatus: (userId, status) => dispatch(actions.userStatus(userId, status)),
    confirmViewedMeCount: (userId, viewedMeCount) => dispatch(confirmViewedMeCount(userId, viewedMeCount)),
	confirmFavoritedMeCount: (userId, favoritedMeCount) => dispatch(confirmFavoritedMeCount(userId, favoritedMeCount)),
	confirmReportedMeCount: (userId, reportedMeCount) => dispatch(confirmReportedMeCount(userId, reportedMeCount))
})

export default connect(mapStateToProps, mapDispatchToProps)(Interest)