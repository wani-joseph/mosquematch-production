export const updateObject = (oldObject, updatedProps) => {
    return {
        ...oldObject,
        ...updatedProps
    }
}

export const getInitials = (firstname, lastname) => {
    return firstname.charAt(0) + lastname.charAt(0)
}

export const setProfileLink = (firstname, lastname, identifier) => {
    return firstname.split(' ').join('+') +'-'+ lastname.split(' ').join('+') +'-'+ identifier
}

export const getMemberIdentifier = (url) => {
    return url.split('-')[2]
}

export const checkFavorite = (favoriteList, memberId) => {
    let found = false

    for(let i = 0; i < favoriteList.length; i++) {
        if (favoriteList[i].following.data.identifier === memberId) {
            found = true
            break
        }
    }

    return found
}

export const checkViewed = (viewedList, memberId) => {
    let found = false

    for (let i = 0; i < viewedList.length; i++) {
        if (viewedList[i].visited.data.identifier === memberId) {
            
            found = true
            break
        }
    }

    return found
}

export const checkAlbum = (albums, albumTitle) => {
    let found = false

    for (let i = 0; i < albums.length; i++) {

        if (albums[i].title === albumTitle) {

            found = true
            break
        }
    }

    return found
}

export const calculateAge = (dob) => {
    var diff_ms = Date.now() - dob.getTime();
    var age_dt = new Date(diff_ms); 
  
    return Math.abs(age_dt.getUTCFullYear() - 1970);
}

export const getDistanceFromLatLonInKm = (lat1,lon1,lat2,lon2) => {
    const R = 6371 // Radius of the earth in km
    const dLat = deg2rad(lat2-lat1)  // deg2rad below
    const dLon = deg2rad(lon2-lon1) 
    const a = 
    Math.sin(dLat/2) * Math.sin(dLat/2) +
    Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) * 
    Math.sin(dLon/2) * Math.sin(dLon/2)
     
    const c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a)) 
    const d = R * c // Distance in km
    return parseInt(d)
}

function deg2rad(deg) {
    return deg * (Math.PI/180)
}

/** @private is the given object an Object? */
export const isObject = obj => obj !== null && typeof obj === 'object';

export function setNestedObjectValues(
    object,
    value,
    visited = new WeakMap(),
    response = {}
    ) {
    for (let k of Object.keys(object)) {
        const val = object[k];
        if (isObject(val)) {
            if (!visited.get(val)) {
                visited.set(val, true);
                // In order to keep array values consistent for both dot path  and
                // bracket syntax, we need to check if this is an array so that
                // this will output  { friends: [true] } and not { friends: { "0": true } }
                response[k] = Array.isArray(val) ? [] : {};
                setNestedObjectValues(val, value, visited, response[k]);
            }
        } else {
            response[k] = value;
        }
    }

    return response;
}