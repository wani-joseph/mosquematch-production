import React from 'react'
import { connect } from 'react-redux'
import { Link, withRouter } from 'react-router-dom'
import { Container, Typography, CircularProgress } from '@material-ui/core'
import AuthHeader from '../../components/AuthHeader/AuthHeader'
import Logo from '../../components/Logo/Logo'
import CustomButton from '../../components/UI/CustomButton/CustomButton'
import CustomPaper from '../../components/UI/CustomPaper/CustomPaper'
import ForgetPasswordForm from '../../components/UI/CustomForm/CustomForm'
import { ForgetPasswordValidation } from '../../shared/validation'
import withStyles from "@material-ui/core/styles/withStyles"
import * as actions from '../../store/actions'

const styles = theme => ({
    paper: {
        marginBottom: theme.spacing(6)
    },
    link: {
        textDecoration: 'none',
        margin: theme.spacing(2),
        display: 'inline-block',
        fontWeight: 500
    },
    title: {
        padding: theme.spacing(2),
        paddingBottom: 0
    }
})

const ForgetPassword = props => {
    const { loading, onForgetPassword, history, classes } = props

    const fields = [
        {label: 'Email', type: 'email', name: 'email', value: ''},
    ]

    const handleSubmit = (values)=> {
        onForgetPassword(values, () => {
            history.push({
                pathname: '/login',
                state: {
                    reset: 'reset password request has been send.'
                }
            })
        })
    }

    return (
        <Container maxWidth="sm">
            <AuthHeader>
                <Logo />
            </AuthHeader>
            <CustomPaper classes={{ root: classes.paper }}>
                <div className={classes.title}>
                    <Typography
                        variant="h5"
                        component="h1"
                    >
                        Forgotten your password?
                    </Typography>
                    <Typography
                        variant="body1"
                        color="textSecondary"
                    >
                        We've got you covered
                    </Typography>
                </div>
                <ForgetPasswordForm
                    fields={fields}
                    // validation={ForgetPasswordValidation}
                    submitHandler={handleSubmit}
                    submitButton={
                        <CustomButton
                            type="submit"
                            fullWidth
                            topmargin="true"
                            uppercase="true"
                        >
                            {loading === true
                                    ? <CircularProgress color="inherit" size={24} />
                                    : "Submit"}
                        </CustomButton>
                    }
                />
                <Typography variant="body2" align="center">
                    <Link to="/login" className={classes.link}>Remembered your password? Login.</Link>
                </Typography>
            </CustomPaper>
        </Container>
    )
}

const mapStateToProps = state => ({
    loading: state.auth.loading,
    errors: state.auth.error
})

const mapDispatchToProps = dispatch => ({
    onForgetPassword: (data, next) => dispatch(actions.forgetPassword(data, next))
})

export default connect(mapStateToProps, mapDispatchToProps)(withStyles(styles)(withRouter(ForgetPassword)))