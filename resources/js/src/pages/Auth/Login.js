import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Redirect, Link, withRouter } from 'react-router-dom'
import {
    Checkbox,
    Container,
    FormControlLabel,
    Typography,
    CircularProgress
} from '@material-ui/core'
import withErrorHandler from '../../hoc/withErrorHandler/withErrorHandler'
import AuthHeader from '../../components/AuthHeader/AuthHeader'
import Logo from '../../components/Logo/Logo'
import CustomButton from '../../components/UI/CustomButton/CustomButton'
import CustomPaper from '../../components/UI/CustomPaper/CustomPaper'
import LoginForm from '../../components/UI/CustomForm/CustomForm'
import { LoginValidation } from '../../shared/validation'
import withStyles from "@material-ui/core/styles/withStyles"
import InfoIcon from '@material-ui/icons/Info'
import * as actions from '../../store/actions'
import axios from 'axios'

const styles = theme => ({
    paper: {
        marginBottom: theme.spacing(6)
    },
    link: {
        textDecoration: 'none',
        margin: theme.spacing(2),
        display: 'inline-block',
        fontWeight: 500
    },
    forgetPassword: {
        margin: theme.spacing(2),
        marginBottom: 0,
        border: '1px solid rgba(0, 0, 0, .08)',
        display: 'flex',
        justifyContent: 'space-between',
        alignItems: 'center',
    },
    infoIcon: {
        background: theme.palette.primary.main,
        color: theme.palette.common.white,
        display: 'inline-flex',
        justifyContent: 'center',
        alignItems: 'center',
        height: 100,
        width: 60,
    },
    invalidInfo: {
        border: `1px solid ${theme.palette.primary.main}`,
    },
    invalidInfoIcon: {
        background: theme.palette.primary.main,
    },
    invalidTitle: {
        color: theme.palette.primary.main,
    },
    title: {
        flex: 1,
        padding: theme.spacing(2)
    },
    accounts: {
        display: 'flex',
        justifyContent: 'space-between',
        alignItems: 'center',
    },
    rememberme: {
        color: '#919191',
        fontSize: 15,
    },
    forgetPasswordLink: {
        textDecoration: 'none',
        color: '#919191',
        fontSize: 15,
    }
})

class Login extends Component {
    constructor() {
        super()
        this.state = {
            checked: false
        }
        this.handleChange = this.handleChange.bind(this)
        this.handleSubmit = this.handleSubmit.bind(this)
    }

    componentDidMount() {
        const { onSetAuthRedirectPath } = this.props
        onSetAuthRedirectPath('/search')
    }

    handleChange(event) {
        this.setState({ checked: event.target.checked })
    }

    handleSubmit(values) {
        const { onSignin } = this.props
        const { checked } = this.state
        const credentials = {
            email: values.email,
            password: values.password,
            remember_me: checked,
        }

        onSignin(credentials)
    }


    render() {
        const { loading, signinRedirectPath, errors, classes, history, message } = this.props
        const { checked } = this.state
        const token = localStorage.getItem('token')

        if (token != null) {
            return <Redirect to={signinRedirectPath} />
        }

        const fields = [
            {label: 'Email', type: 'email', name: 'email', value: ''},
            {label: 'Password', type: 'password', name: 'password', value: ''}
        ]

        let infoClasses = [classes.forgetPassword]
        let infoIconClasses = [classes.infoIcon]
        let textClasses = [classes.title]

        if (errors) {
            infoClasses.push(classes.invalidInfo)
            infoIconClasses.push(classes.invalidInfoIcon)
            textClasses.push(classes.invalidTitle)
        }

        return (
            <Container maxWidth="sm">
                <AuthHeader>
                    <Logo />
                </AuthHeader>
                <CustomPaper classes={{ root: classes.paper }}>
                    {history.location.state ? (
                        <div className={infoClasses.join(' ')}>
                            <div className={infoIconClasses.join(' ')}>
                                <InfoIcon />
                            </div>
                            <Typography
                                className={textClasses.join(' ')}
                                variant="body1"
                                color="textSecondary"
                            >
                                {history.location.state == "reset password request has been send."
                                    ? "If your email is valid, you'll receive an email with instructions on how to reset your password"
                                    : "Your password have been updated successfully."}
                            </Typography>
                        </div>
                    ) : null}
                    {message ? (
                        <div className={infoClasses.join(' ')}>
                            <div className={infoIconClasses.join(' ')}>
                                <InfoIcon />
                            </div>
                            <Typography
                                className={textClasses.join(' ')}
                                variant="body1"
                                color="textSecondary"
                            >
                                Invalid credentials, check your email and password
                            </Typography>
                        </div>
                    ) : null}
                    <LoginForm
                        fields={fields}
                        // validation={LoginValidation}
                        submitHandler={this.handleSubmit}
                        submitButton={
                            <React.Fragment>
                                <div className={classes.accounts}>
                                    <FormControlLabel
                                        classes={{ label: classes.rememberme }}
                                        control={
                                            <Checkbox checked={checked} onChange={this.handleChange} />
                                        }
                                        label="Remember me"
                                    />
                                    <Link
                                        className={classes.forgetPasswordLink}
                                        to="/accounts/password/new"
                                    >
                                        Forgot your password?
                                    </Link>
                                </div>
                                <CustomButton
                                    type="submit"
                                    fullWidth
                                    topmargin="true"
                                    uppercase="true"
                                >
                                    {loading === true
                                        ? <CircularProgress color="inherit" size={24} />
                                        : "Login"}
                                </CustomButton>
                            </React.Fragment>
                        }
                    />
                    <Typography variant="body2" align="center">
                        <Link to="/register" className={classes.link}>Don't have an account? Register.</Link>
                    </Typography>
                </CustomPaper>
            </Container>
        )
    }
}

const mapStateToProps = state => ({
    signinRedirectPath: state.auth.signinRedirectPath,
    loading: state.auth.loading,
    errors: state.auth.errors,
    message: state.auth.message
})

const mapDispatchToProps = dispatch => ({
    onSetAuthRedirectPath: (path) => dispatch(actions.setAuthRedirectPath(path)),
    onSignin: (credentials) => dispatch(actions.login(credentials)),
})

export default connect(mapStateToProps, mapDispatchToProps)(withStyles(styles)(withRouter(withErrorHandler(Login, axios))))