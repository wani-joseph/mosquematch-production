import React from 'react'
import { connect } from 'react-redux'
import { withRouter } from 'react-router-dom'
import { Container, Typography, CircularProgress } from '@material-ui/core'
import AuthHeader from '../../components/AuthHeader/AuthHeader'
import Logo from '../../components/Logo/Logo'
import CustomButton from '../../components/UI/CustomButton/CustomButton'
import CustomPaper from '../../components/UI/CustomPaper/CustomPaper'
import ResetPasswordForm from '../../components/UI/CustomForm/CustomForm'
import { ResetPasswordValidation } from '../../shared/validation'
import withStyles from "@material-ui/core/styles/withStyles"
import axios from 'axios'

const styles = theme => ({
    paper: {
        marginBottom: theme.spacing(6)
    },
    link: {
        textDecoration: 'none',
        margin: theme.spacing(2),
        display: 'inline-block',
        fontWeight: 500
    },
    title: {
        padding: theme.spacing(2),
        paddingBottom: 0
    }
})

const ResetPassword = ({ loading, match, history, classes}) => {
    const resetPasswordToken = match.params.reset_token
    const fields = [
        {label: 'Password', type: 'password', name: 'password', value: ''},
        {label: 'Password Confirmation', type: 'password', name: 'confirmPassword', value: ''}
    ]

    const handleSubmit = (values)=> {
        axios.get(`/api/password/find/${resetPasswordToken}`)
            .then(res => {
                const data = {
                    email: res.data.email,
                    password: values.password,
                    password_confirmation: values.confirmPassword,
                    token: resetPasswordToken
                }
                axios.post('/api/password/reset', data)
                    .then(res => {
                        history.push({
                            pathname: '/login',
                            state: {
                                reset: 'password updated successfully.'
                            }
                        })
                    })
            })
            .catch(err => console.log(err))
    }

    let pageContent = <p>Your password reset token is expired, you need to send another request.</p>

    if (resetPasswordToken) pageContent = (
        <ResetPasswordForm
            fields={fields}
            // validation={ResetPasswordValidation}
            submitHandler={handleSubmit}
            submitButton={
                <CustomButton
                    type="submit"
                    fullWidth
                    topmargin="true"
                    uppercase="true"
                >
                    {loading === true
                            ? <CircularProgress color="inherit" size={24} />
                            : "Change my password"}
                </CustomButton>
            }
        />
    )

    return (
        <Container maxWidth="sm">
            <AuthHeader>
                <Logo />
            </AuthHeader>
            <CustomPaper classes={{ root: classes.paper }}>
                <div className={classes.title}>
                    <Typography
                        variant="h5"
                        component="h1"
                    >
                        Forgotten your password?
                    </Typography>
                    <Typography
                        variant="body1"
                        color="textSecondary"
                    >
                        We've got you covered
                    </Typography>
                </div>
                {pageContent}
            </CustomPaper>
        </Container>
    )
}

const mapStateToProps = state => ({
    loading: state.auth.loading,
    errors: state.auth.error,
})

export default connect(mapStateToProps)(withStyles(styles)(withRouter(ResetPassword)))