import React from 'react';
import { connect } from 'react-redux'
import { Redirect, Link } from 'react-router-dom'
import { Container, Typography, CircularProgress } from '@material-ui/core'
import AuthHeader from '../../components/AuthHeader/AuthHeader'
import Logo from '../../components/Logo/Logo'
import CustomButton from '../../components/UI/CustomButton/CustomButton'
import CustomModal from '../../components/UI/CustomModal/CustomModal'
import CustomPaper from '../../components/UI/CustomPaper/CustomPaper'
import LoginForm from '../../components/UI/CustomForm/CustomForm'
import ToggleModal from '../../components/ToggleModal/ToggleModal'
import { RegisterValidation } from '../../shared/validation'
import withStyles from "@material-ui/core/styles/withStyles"
import * as actions from '../../store/actions'

const styles = theme => ({
    paper: {
        marginBottom: theme.spacing(6)
    },
    link: {
        textDecoration: 'none',
        margin: theme.spacing(2),
        display: 'inline-block',
        fontWeight: 500
    }
})

const Register = (props) => {
    const { loading, isAuthenticated, signupRedirectPath, onRegister, classes, errors } = props

    if (isAuthenticated) {
        return <Redirect to={signupRedirectPath} />
    }

    const fields = [
        {label: 'First Name', type: 'text', name: 'firstname', value: ''},
        {label: 'Last Name', type: 'text', name: 'lastname', value: ''},
        {label: 'Email', type: 'email', name: 'email', value: ''},
        {label: 'Password', type: 'password', name: 'password', value: ''},
        {label: 'Password Confirmation', type: 'password', name: 'confirmPassword', value: ''}
    ]

    const handleSubmit = (values)=> {
        const data = {
            firstname: values.firstname,
            lastname: values.lastname,
            email: values.email,
            password: values.password,
            password_confirmation: values.confirmPassword
        }

        onRegister(data)
    }

    return (
        <Container maxWidth="sm">
            <AuthHeader>
                <Logo />
            </AuthHeader>
            <CustomPaper classes={{ root: classes.paper }}>
                <LoginForm
                    fields={fields}
                    // validation={RegisterValidation}
                    submitHandler={handleSubmit}
                    submitButton={
                        <React.Fragment>
                            <Typography variant="body2">
                                By continuing you are agree to MosqueMatch
                                <ToggleModal
                                    label="TERMS"
                                    highlights="true"
                                />
                                {/* {<CustomModal
                                    label="TERMS"
                                    highlights="true"
                                />} */}
                                &
                                <ToggleModal
                                    label="TERMS"
                                    highlights="true"
                                />
                                {/* {<CustomModal
                                    label="PRIVACY"
                                    highlights="true"
                                />} */}
                                Policy
                            </Typography>
                            <CustomButton
                                type="submit"
                                fullWidth
                                topmargin="true"
                                uppercase="true"
                            >
                                {loading === true
                                    ? <CircularProgress color="inherit" size={24} />
                                    : "Register"}
                            </CustomButton>
                        </React.Fragment>
                    }
                />
                <Typography variant="body2" align="center">
                    <Link to="/login" className={classes.link}>Already have an account? Login.</Link>
                </Typography>
            </CustomPaper>
        </Container>
    )
}

const mapStateToProps = state => ({
    isAuthenticated: state.auth.token != null,
    signupRedirectPath: state.auth.signupRedirectPath,
    loading: state.auth.loading,
    errors: state.auth.error
})

const mapDispatchToProps = dispatch => ({
    onRegister: (data) => dispatch(actions.register(data)),
})

export default connect(mapStateToProps, mapDispatchToProps)(withStyles(styles)(Register))