import React, { Component } from 'react'
import { connect } from 'react-redux'
import SetupProfile from '../../views/SetupProfile'
import Spinner from '../../components/UI/Spinner/Spinner'
import * as actions from '../../store/actions'

class AboutMe extends Component {
    componentDidMount() {
        const { onSetAuthRedirectPath } = this.props
        onSetAuthRedirectPath('/setup-profile/about-me')
    }

    render() {
        const { profile } = this.props

        let pageContent = <Spinner />

        if (profile) pageContent = (
            <SetupProfile
                fields="About Me"
                nextFields="/setup-profile/education-and-work"
            />
        )

        return (
            <React.Fragment>
                {pageContent}
            </React.Fragment>
        )
    }
}

const mapStateToProps = state => ({
    profile: state.auth.authUser
})

const mapDispatchToprops = dispatch => ({
    onSetAuthRedirectPath: (path) => dispatch(actions.setAuthRedirectPath(path))
})

export default connect(mapStateToProps, mapDispatchToprops)(AboutMe)