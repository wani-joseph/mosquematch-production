import React, { Component } from 'react'
import { connect } from 'react-redux'
import SetupProfile from '../../views/SetupProfile'
import Spinner from '../../components/UI/Spinner/Spinner'
import * as actions from '../../store/actions'

class EducationWork extends Component {
    componentDidMount() {
        const { onSetAuthRedirectPath } = this.props
        onSetAuthRedirectPath('/setup-profile/education-and-work')
    }

    render() {
        const { profile } = this.props

        let pageContent = <Spinner />

        if (profile) pageContent = (
            <SetupProfile
                fields="Education and Work"
                nextFields="/setup-profile/personal-information"
            />
        )

        return (
            <React.Fragment>
                {pageContent}
            </React.Fragment>
        )
    }
}

const mapStateToProps = state => ({
    profile: state.auth.authUser
})

const mapDispatchToprops = dispatch => ({
    onSetAuthRedirectPath: (path) => dispatch(actions.setAuthRedirectPath(path))
})

export default connect(mapStateToProps, mapDispatchToprops)(EducationWork)