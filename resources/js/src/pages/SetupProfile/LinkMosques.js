import React, { Component } from 'react'
import { connect } from 'react-redux'
import SetupProfile from '../../views/SetupProfile'
import Spinner from '../../components/UI/Spinner/Spinner'
import * as actions from '../../store/actions'

class LinkMosques extends Component {
    componentDidMount() {
        const { onSetAuthRedirectPath } = this.props
        onSetAuthRedirectPath('/setup-profile/link-mosques')
    }

    render() {
        const { loading, profile } = this.props

        let pageContent = <Spinner />

        if (!loading) {

            if (profile) pageContent = (
                <SetupProfile
                    fields="Link to a Mosque"
                    nextFields="/search"
                />
            )
        }

        return (
            <React.Fragment>
                {pageContent}
            </React.Fragment>
        )
    }
}

const mapStateToProps = state => ({
    loading: state.auth.loading,
    profile: state.auth.authUser
})

const mapDispatchToprops = dispatch => ({
    onSetAuthRedirectPath: (path) => dispatch(actions.setAuthRedirectPath(path))
})

export default connect(mapStateToProps, mapDispatchToprops)(LinkMosques)