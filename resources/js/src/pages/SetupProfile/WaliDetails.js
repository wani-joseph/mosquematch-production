import React, { Component } from 'react'
import { connect } from 'react-redux'
import SetupProfile from '../../views/SetupProfile'
import Spinner from '../../components/UI/Spinner/Spinner'
import * as actions from '../../store/actions'

class WaliDetails extends Component {
    componentDidMount() {
        const { onSetAuthRedirectPath } = this.props
        onSetAuthRedirectPath('/setup-profile/wali-details')
    }

    render() {
        const { profile } = this.props

        let pageContent = <Spinner />

        if (profile) pageContent = (
            <SetupProfile
                fields="Wali Details"
                nextFields="/setup-profile/link-mosques"
            />
        )

        return (
            <React.Fragment>
                {pageContent}
            </React.Fragment>
        )
    }
}

const mapStateToProps = state => ({
    profile: state.auth.authUser
})

const mapDispatchToprops = dispatch => ({
    onSetAuthRedirectPath: (path) => dispatch(actions.setAuthRedirectPath(path))
})

export default connect(mapStateToProps, mapDispatchToprops)(WaliDetails)