import React, { useEffect } from 'react'
import { connect } from 'react-redux'
import AdminPanel from '../../../views/Admin'
import Spinner from '../../../components/UI/Spinner/Spinner'
import * as actions from '../../../store/actions'

const Members = ({ profile, onSetAuthRedirectPath }) => {
    useEffect(() => {
        onSetAuthRedirectPath('/admin/members')
    }, [])

    let pageContent = <Spinner />

    if (profile) pageContent = <AdminPanel page="members" />


    return <>{pageContent}</>
}

const mapStateToProps = state => ({
    profile: state.auth.authUser
})

const mapDispatchToProps = dispatch => ({
    onSetAuthRedirectPath: (path) => dispatch(actions.setAuthRedirectPath(path))
})
export default connect(mapStateToProps, mapDispatchToProps)(Members)