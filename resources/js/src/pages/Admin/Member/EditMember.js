import React, { useEffect } from 'react'
import { connect } from 'react-redux'
import { withRouter } from 'react-router-dom'
import AdminPanel from '../../../views/Admin'
import Spinner from '../../../components/UI/Spinner/Spinner'
import * as actions from '../../../store/actions'

const EditMember = ({ match, profile, onSetAuthRedirectPath }) => {
  const memberId = match.params.memberId

  useEffect(() => {
    onSetAuthRedirectPath(`/admin/members/edit-member/${memberId}`)
  }, [])

  let pageContent = <Spinner />

  if (profile) pageContent = <AdminPanel page="edit member" />

  return <>{pageContent}</>
}

const mapStateToProps = state => ({
  profile: state.auth.authUser
})

const mapDispatchToProps = dispatch => ({
  onSetAuthRedirectPath: (path) => dispatch(actions.setAuthRedirectPath(path))
})

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(EditMember))