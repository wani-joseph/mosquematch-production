import React, { useEffect } from 'react'
import { connect } from 'react-redux'
import { withRouter } from 'react-router-dom'
import AdminPanel from '../../../views/Admin'
import Spinner from '../../../components/UI/Spinner/Spinner'
import * as actions from '../../../store/actions'

const SingleMosque = ({ match, profile, onSetAuthRedirectPath }) => {
  const mosqueId = match.params.mosqueId

  useEffect(() => {
    onSetAuthRedirectPath(`/admin/mosques/${mosqueId}`)
  }, [])

  let pageContent = <Spinner />

  if (profile) pageContent = <AdminPanel page="view mosque" />

  return <>{pageContent}</>
}

const mapStateToProps = state => ({
  profile: state.auth.authUser
})

const mapDispatchToProps = dispatch => ({
  onSetAuthRedirectPath: (path) => dispatch(actions.setAuthRedirectPath(path))
})

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(SingleMosque))