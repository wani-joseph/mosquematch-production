import React, { Component } from 'react'
import { connect } from 'react-redux'
import InterestView from '../../views/Interest'
import Spinner from '../../components/UI/Spinner/Spinner'
import * as actions from '../../store/actions'

class Favorites extends Component {
    componentDidMount() {
        const { onSetAuthRedirectPath } = this.props
        onSetAuthRedirectPath('/interest/favorites')
    }

    render() {
        const { profile } = this.props

        let pageContent = <Spinner />

        if (profile) pageContent = <InterestView tab="Favorites" />

        return (
            <React.Fragment>
                {pageContent}
            </React.Fragment>
        )
    }
}

const mapStateToProps = state => ({
    profile: state.auth.authUser
})

const mapDispatchToprops = dispatch => ({
    onSetAuthRedirectPath: (path) => dispatch(actions.setAuthRedirectPath(path)),
})

export default connect(mapStateToProps, mapDispatchToprops)(Favorites)