import React, { Component } from 'react'
import { withRouter } from 'react-router-dom'

class Interest extends Component {
    componentDidMount() {
        const { history } = this.props
        history.push('/interest/views')
    }

    render() {
        return <div></div>
    }
}

export default withRouter(Interest)