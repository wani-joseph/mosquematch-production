import React, { Component } from 'react'
import { connect } from 'react-redux'
import { withRouter } from 'react-router-dom'
import MemberView from '../views/Member'
import Spinner from '../components/UI/Spinner/Spinner'
import * as actions from '../store/actions'

class Member extends Component {
    componentDidMount() {
        const { onSetAuthRedirectPath } = this.props
        onSetAuthRedirectPath(`/member/${this.props.match.params.profile}`)
    }

    render() {
        const { profile } = this.props
        const member = this.props.match.params.profile

        let pageContent = <Spinner />

        if (profile) pageContent = <MemberView member={member} />

        return (
            <React.Fragment>
                {pageContent}
            </React.Fragment>
        )
    }
}

const mapStateToProps = state => ({
    profile: state.auth.authUser
})

const mapDispatchToprops = dispatch => ({
    onSetAuthRedirectPath: (path) => dispatch(actions.setAuthRedirectPath(path))
})

export default connect(mapStateToProps, mapDispatchToprops)(withRouter(Member))