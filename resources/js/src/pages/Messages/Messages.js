import React, { Component } from 'react'
import { withRouter } from 'react-router-dom'

class Messages extends Component {
    componentDidMount() {
        const { history } = this.props
        history.push('/messages/inbox')
    }

    render() {
        return <div></div>
    }
}

export default withRouter(Messages)