import React, { Component } from 'react'
import { connect } from 'react-redux'
import MessagesView from '../../views/Messages'
import Spinner from '../../components/UI/Spinner/Spinner'
import * as actions from '../../store/actions'

class Inbox extends Component {
    componentDidMount() {
        const { onSetAuthRedirectPath } = this.props     
        onSetAuthRedirectPath('/messages/inbox')
    }

    render() {
        const { profile } = this.props

        let pageContent = <Spinner />

        if (profile) pageContent = <MessagesView page="Inbox" />

        return (
            <React.Fragment>
                {pageContent}
            </React.Fragment>
        )
    }
}

const mapStateToProps = state => ({
    profile: state.auth.authUser
})

const mapDispatchToprops = dispatch => ({
    onSetAuthRedirectPath: (path) => dispatch(actions.setAuthRedirectPath(path))
})

export default connect(mapStateToProps, mapDispatchToprops)(Inbox)