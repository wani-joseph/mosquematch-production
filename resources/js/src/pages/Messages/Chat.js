import React, { Component } from 'react'
import { connect } from 'react-redux'
import { withRouter } from 'react-router-dom'
import MessagesView from '../../views/Messages'
import Spinner from '../../components/UI/Spinner/Spinner'
import * as actions from '../../store/actions'

class Chat extends Component {
    componentDidMount() {
        const { onSetAuthRedirectPath } = this.props
        onSetAuthRedirectPath(`/messages/chat/${this.props.match.params.contact}`)
    }

    render() {
        const { profile } = this.props
        const contact = this.props.match.params.contact

        let pageContent = <Spinner />

        if (profile) pageContent = (
            <MessagesView
                page="Chat"
                contact={contact}
            />
        )

        return (
            <React.Fragment>
                {pageContent}
            </React.Fragment>
        )
    }
}

const mapStateToProps = state => ({
    profile: state.auth.authUser
})

const mapDispatchToprops = dispatch => ({
    onSetAuthRedirectPath: (path) => dispatch(actions.setAuthRedirectPath(path))
})

export default connect(mapStateToProps, mapDispatchToprops)(withRouter(Chat))