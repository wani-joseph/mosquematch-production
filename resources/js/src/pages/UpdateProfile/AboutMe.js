import React, { Component } from 'react'
import { connect } from 'react-redux'
import { withRouter } from 'react-router-dom'
import AboutMeView from '../../views/UpdateProfile'
import Spinner from '../../components/UI/Spinner/Spinner'
import * as actions from '../../store/actions'

class AboutMe extends Component {
    componentDidMount() {
        const { onSetAuthRedirectPath } = this.props
        onSetAuthRedirectPath(`/profile/${this.props.match.params.user}/about-me`)
    }

    render() {
        const { profile } = this.props

        let pageContent = <Spinner />

        if (profile) pageContent = (
            <AboutMeView
                link={this.props.match.params.user}
                fields="About Me"
            />
        )

        return (
            <React.Fragment>
                {pageContent}
            </React.Fragment>
        )
    }
}

const mapStateToProps = state => ({
    profile: state.auth.authUser
})

const mapDispatchToprops = dispatch => ({
    onSetAuthRedirectPath: (path) => dispatch(actions.setAuthRedirectPath(path))
})

export default connect(mapStateToProps, mapDispatchToprops)(withRouter(AboutMe))