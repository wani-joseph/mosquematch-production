import React, { Component } from 'react'
import { connect } from 'react-redux'
import { withRouter } from 'react-router-dom'
import PhotosView from '../../views/UpdateProfile'
import Spinner from '../../components/UI/Spinner/Spinner'
import * as actions from '../../store/actions'

class Photos extends Component {
  componentDidMount() {
    const { onSetAuthRedirectPath } = this.props
    onSetAuthRedirectPath(`/profile/${this.props.match.params.user}/photos`)
  }

  render() {
    const { profile } = this.props

    let pageContent = <Spinner />

    if (profile) pageContent = (
      <PhotosView
        link={this.props.match.params.user}
        fields="Photos"
      />
    )

    return <>{pageContent}</>
  }
}

const mapStateToProps = state => ({
  profile: state.auth.authUser
})

const mapDispatchToprops = dispatch => ({
  onSetAuthRedirectPath: (path) => dispatch(actions.setAuthRedirectPath(path))
})

export default connect(mapStateToProps, mapDispatchToprops)(withRouter(Photos))