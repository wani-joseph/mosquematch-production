import React, { Component } from 'react'
import { connect } from 'react-redux'
import { withRouter } from 'react-router-dom'
import ReligionView from '../../views/UpdateProfile'
import Spinner from '../../components/UI/Spinner/Spinner'
import * as actions from '../../store/actions'

class Religion extends Component {
    componentDidMount() {
        const { onSetAuthRedirectPath } = this.props
        onSetAuthRedirectPath(`/profile/${this.props.match.params.user}/religion`)
    }

    render() {
        const { profile } = this.props

        let pageContent = <Spinner />

        if (profile) pageContent = (
            <ReligionView
                link={this.props.match.params.user}
                fields="Religion"
            />
        )

        return (
            <React.Fragment>
                {pageContent}
            </React.Fragment>
        )
    }
}

const mapStateToProps = state => ({
    profile: state.auth.authUser
})

const mapDispatchToprops = dispatch => ({
    onSetAuthRedirectPath: (path) => dispatch(actions.setAuthRedirectPath(path))
})

export default connect(mapStateToProps, mapDispatchToprops)(withRouter(Religion))