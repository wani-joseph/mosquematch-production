import React from 'react'
import ReactDOM from 'react-dom'
import { BrowserRouter } from 'react-router-dom'
import { 
    createStore, 
    applyMiddleware, 
    compose,
    combineReducers
} from 'redux'
import { Provider } from 'react-redux'
import thunk from 'redux-thunk'
import { 
    createMuiTheme, 
    responsiveFontSizes,
    withStyles
} from '@material-ui/core/styles'
import { ThemeProvider } from '@material-ui/styles'
import CssBaseline from '@material-ui/core/CssBaseline'

import App from './App'
import adminReducer from './store/reducers/admin'
import authReducer from './store/reducers/auth'
import interestReducer from './store/reducers/interest'
import memberReducer from './store/reducers/member'
import contactReducer from './store/reducers/contact'

const composeEnhancers =
    typeof window === 'object' &&
    window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ ?   
    window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__({
    }) : compose;

const reducer = combineReducers({
    admin: adminReducer,
    auth: authReducer,
    member: memberReducer,
    contact: contactReducer,
    interest: interestReducer,
})

const enhancer = composeEnhancers(
    applyMiddleware(thunk)
)

const store = createStore(reducer, enhancer);

let theme = createMuiTheme({
    palette: {
        primary: {
            light: '#d32f2f',
            main: '#B32024',
            dark: '#801313',
            contrastText: '#fff',
        },
        secondary: {
            light: '#009688',
            main: '#008489',
            dark: '#004d40',
            contrastText: '#fff',
        },
        background: {
            default: '#F9F9F9',
        }
    },
})

const GlobalCss = withStyles({
    '@global': {
        'label + .MuiInput-formControl': {
            marginTop: theme.spacing(2),
            [theme.breakpoints.up('md')]: {
                marginTop: theme.spacing(3),
            }
        },
        '.MuiInputBase-input': {
            padding: '.4rem',
            [theme.breakpoints.up('md')]: {
                padding: theme.spacing(1),
            }
        },
        '.MuiInput-underline:hover:not(.Mui-disabled):before': {
            borderBottom: 'none'
        },
        '.MuiInput-underline:before': {
            borderBottom: 'none'
        },
        '.MuiInput-underline:after': {
            borderBottom: 'none'
        },
        '.MuiButton-root': {
            minWidth: 50
        },
        '.slick-prev': {
            left: 10,
            zIndex: 100
        },
        '.slick-next': {
            right: 10
        },
        '.slick-dots': {
            bottom: 10
        },
        '.slick-dots li button:before': {
            color: 'white',
            opacity: '.8'
        },
        '.slick-dots li button': {
            color: 'white',
        },
        '.slick-dots li.slick-active button:before': {
            color: 'white',
            opacity: 1,
            transform: 'scale(1.5)'
        },
        '.slick-dots li': {
            width: 2
        }
    },
})(() => null)

theme = responsiveFontSizes(theme)

const app = (
    <Provider store={store}>
        <BrowserRouter>
            <ThemeProvider theme={theme}>
                <React.Fragment>
                    <CssBaseline />
                    <GlobalCss />
                    <App />
                </React.Fragment>
            </ThemeProvider>
        </BrowserRouter>
    </Provider>
);

ReactDOM.render(app, document.getElementById('app'))