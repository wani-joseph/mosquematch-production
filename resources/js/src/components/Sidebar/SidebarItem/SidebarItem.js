import React from 'react'
import { NavLink } from 'react-router-dom'
import { Badge } from '@material-ui/core'
import { useStyles } from './SidebarItemStyles'

const SidebarItem = ({ link, text, badge }) => {
    const classes = useStyles()

    return (
        <li className={classes.sidebarItem}>
            <NavLink
                className={classes.link}
                activeClassName={classes.active}
                to={link}
            >
                <span className={classes.linkText}>{text}</span>
                <Badge className={classes.linkBadge} badgeContent={badge} color="primary" />
            </NavLink>
        </li>
    )
}

export default SidebarItem