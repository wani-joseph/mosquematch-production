import { makeStyles } from '@material-ui/core/styles'

export const useStyles = makeStyles(theme => ({
    sidebarItem: {
        display: 'inline-flex',
        marginRight: theme.spacing(2),
        '&:last-of-type': {
            marginRight: 0,
        },
        [theme.breakpoints.up('md')]: {
            display: 'block',
            marginRight: 0,
            borderBottom: '1px solid rgba(0, 0, 0, .08)',
        }
    },
    link: {
        borderBottom: '4px solid transparent',
        borderTop: '4px solid transparent',
        color: theme.palette.common.black,
        display: 'block',
        fontWeight: 500,
        padding: theme.spacing(1),
        textDecoration: 'none',
        [theme.breakpoints.up('sm')]: {
            padding: theme.spacing(1, 2),
        },
        [theme.breakpoints.up('md')]: {
            borderBottom: 'none',
            borderTop: 'none',
            fontSize: 16,
            padding: theme.spacing(2),
            display: 'flex',
            justifyContent: 'space-between',
            alignItems: 'center',
        }
    },
    active: {
        borderBottom: `4px solid ${theme.palette.primary.main}`,
        [theme.breakpoints.up('md')]: {
            borderBottom: 'none',
            borderRight: `4px solid ${theme.palette.primary.main}`,
        }
    },
    linkText: {
        marginRight: theme.spacing(1),
        [theme.breakpoints.up('md')]: {
            marginRight: 0,
        }
    },
    linkBadge: {
        marginTop: '-16px',
        [theme.breakpoints.up('md')]: {
            marginTop: 0,
            marginRight: theme.spacing(1),
        }
    }
}))