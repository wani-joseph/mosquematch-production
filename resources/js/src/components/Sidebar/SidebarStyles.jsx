export const styles = theme => ({
    sidebar: {
        display: 'flex',
        padding: 0,
        margin: 0,
        listStyle: 'none',
        width: '100%',
        overflowX: 'auto',
        [theme.breakpoints.up('md')]: {
            flexDirection: 'column'
        }
    }
})