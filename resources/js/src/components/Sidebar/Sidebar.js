import React from 'react'
import { connect } from 'react-redux'
import { withStyles } from '@material-ui/core'
import SidebarItem from './SidebarItem/SidebarItem'
import { styles } from './SidebarStyles'

const Sidebar = ({ items, inboxCount, requestsCount, classes }) => {

    let sidebarItems = <p>Add your items as an array of objects with link, and text values</p>

    if (items.length > 0) {
        sidebarItems = items.map(sidebarItem => {
            let badge = 0

            if (sidebarItem.badge == "inboxCount") badge = inboxCount
            if (sidebarItem.badge == "requestsCount") badge = requestsCount

            return (
                <SidebarItem
                    key={sidebarItem.link}
                    link={sidebarItem.link}
                    text={sidebarItem.text}
                    badge={badge}
                />
            )
        })
    }

    return (
        <ul className={classes.sidebar}>
            {sidebarItems}
        </ul>
    )
}

const mapStateToProps = state => ({
    inboxCount: state.auth.inboxCount,
    requestsCount: state.auth.requestsCount
})

export default connect(mapStateToProps)(withStyles(styles)(Sidebar))