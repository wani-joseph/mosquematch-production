export const styles = theme =>({
    pageHeader: {
        display: 'flex',
        justifyContent: 'space-between',
        alignItems: 'center',
        marginBottom: theme.spacing(3),
        marginTop: theme.spacing(2),
    },
    paper: {
        marginBottom: theme.spacing(3),
    },
    spacingPaper: {
        marginBottom: theme.spacing(3),
        paddingTop: theme.spacing(3)
    },
    button: {
        display: 'flex',
        justifyContent: 'space-between',
        alignItems: 'center',
        textTransform: 'uppercase',
        outline: 'none',
        border: `2px solid ${theme.palette.secondary.main}`,
        color: theme.palette.secondary.main,
        cursor: 'pointer',
        background: theme.palette.common.white,
        boxShadow: theme.shadows[2],
        textDecoration: 'none',
        padding: '4px 16px'
    },
    form: {
        maxHeight: '80vh',
        overflowY: 'auto'
    },
    table: {
        marginBottom: theme.spacing(3),
    },
    tableHead: {
        fontSize: 16,
    },
    tableData: {
        padding: '12px 16px',
    },
    actions: {
        display: 'flex',
        justifyContent: 'space-between',
    },
    link: {
        color: theme.palette.grey[700]
    },
    danger: {
        color: theme.palette.primary.main,
        cursor: 'pointer'
    }
})