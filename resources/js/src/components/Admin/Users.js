import React, { Component } from 'react'
import axios from 'axios'

class Users extends Component {
    constructor() {
        super()
        this._isMounted = false
        this.state = {
            users: [],
            loading: false,
            per: 10,
            page: 1,
            total: null,
            totalPages: null,
            sortBy: 'age',
            orderBy: 'asc',
            
        }
        this.loadUsers = this.loadUsers.bind(this)
        this.handleClick = this.handleClick.bind(this)
        this.loadNextPage = this.loadNextPage.bind(this)
        this.loadPrevPage = this.loadPrevPage.bind(this)
        this.handleScolling = this.handleScolling.bind(this)
        this.handlePerPage = this.handlePerPage.bind(this)
        this.handleSortBy = this.handleSortBy.bind(this)
    }

    componentDidMount() {
        this._isMounted = true
        this.loadUsers()
    }

    componentWillUnmount() {
        this._isMounted = false
    }

    loadUsers() {
        if (this._isMounted) this.setState({ loading: true })

        const { per, page, sortBy, orderBy } = this.state

        axios({
            method: 'get',
            url: `/api/users?per_page=${per}&page=${page}&sort_by=${sortBy}&order_by=${orderBy}`,
            headers: {
                'Authorization': `Bearer ${localStorage.getItem('token')}`
            }
        })
        .then(res => {
            const { per_page, current_page, total_pages, total } = res.data.meta.pagination
            
            if (this._isMounted) {
                this.setState({
                    loading: false,
                    users: res.data.data,
                    per: per_page,
                    page: current_page,
                    totalPages: total_pages,
                    total: total
                })
            }
        })
        .catch(err => dispatch(authFail(err)))
    }

    loadNextPage() {
        const { totalPages, page } = this.state

        if (totalPages <= page) return

        if (this._isMounted) {
            this.setState(prevState => ({
                page: prevState.page + 1,
            }), this.loadUsers)
        }

        this.handleScolling()
    }

    loadPrevPage() {
        const { page } = this.state

        if (page == 1) return

        if (this._isMounted) {
            this.setState(prevState => ({
                page: prevState.page - 1,
            }), this.loadUsers)
        }

        this.handleScolling()
    }

    handleScolling() {
        setTimeout(() => {
            window.scroll({
                top: 0, 
                left: 0, 
                behavior: 'smooth'
            });
        }, 50)
    }

    handleClick() {
        console.log('This is working')
    }

    handlePerPage(e) {
        if (this._isMounted) {
            this.setState({ per: e.target.value }, this.loadUsers)
        }
    }

    handleSortBy(e) {
        if (this._isMounted) {
            if (e.target.value == "desc" || e.target.value == "asc") {
                this.setState({ orderBy: e.target.value }, this.loadUsers)
            } else {
                this.setState({ sortBy: e.target.value }, this.loadUsers)
            }
        }
    }

    render() {
        return this.props.children({
            ...this.state,
            loadUsers: this.loadUsers,
            handleClick: this.handleClick,
            handlePerPage: this.handlePerPage,
            handleSortBy: this.handleSortBy,
            prev: this.loadPrevPage,
            next: this.loadNextPage,
        })
    }
}

export default Users