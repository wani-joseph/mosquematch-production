import React from 'react'
import AdminResource from '../../../services/Admin'
import { Link } from 'react-router-dom'
import CustomButton  from '../../UI/CustomButton/CustomButton'
import CustomPaper from '../../UI/CustomPaper/CustomPaper'
import CustomModal from '../../UI/CustomModal/CustomModal'
import CustomForm  from '../../UI/CustomForm/CustomForm'
import CustomTypography from '../../UI/CustomTypography/CustomTypography'
import Loader from '../../UI/Loader/Loader'
import {
  Table,
  TableHead,
  TableBody,
  TableRow,
  TableCell,
  withStyles
} from '@material-ui/core'
import VisibilityIcon from '@material-ui/icons/Visibility'
import EditIcon from '@material-ui/icons/Edit'
import DeleteForeverIcon from '@material-ui/icons/DeleteForever'
import AddIcon from '@material-ui/icons/Add'
import { styles } from '../AdminStyles'

const Mosques = ({ classes }) => {
  const [modal, setModal] = React.useState(false)
  const [resourceItem, setResourceItem] = React.useState(null)

  const openModal = () => {
    setModal(true)
  }

  const closeModal = () => {
    setModal(false)
  }

  const fields = [
    {label: 'Mosque Name', type: 'text', name: 'mosqueName', value: ''},
    {label: 'Mosque Imam', type: 'text', name: 'mosqueImam', value: ''},
    {label: 'Region Covered', type: 'text', name: 'coveredRegion', value: ''},
    {label: 'City', type: 'text', name: 'city', value: ''},
    {label: 'Address', type: 'text', name: 'streetAddress', value: ''},
    {label: 'Post Code', type: 'text', name: 'postCode', value: ''},
    {label: 'Contact', type: 'text', name: 'mosqueNumber', value: ''},
    {label: 'Email', type: 'email', name: 'email', value: ''},
    {label: 'Website', type: 'url', name: 'website', value: ''},
    {label: 'Latitude', type: 'text', name: 'latitude', value: ''},
    {label: 'Longitude', type: 'text', name: 'longtitude', value: ''}
  ]
  
  const showConfirmationModal = (resource) => {
    openModal()
    setResourceItem(resource.identifier)
  }
  
  return (
    <AdminResource page="resource listing" resource="mosques">
      {resource => {

        const { loading, list, addResource, deleteResource, resourceForm, openResourceForm, closeResourceForm } = resource

        const handleAddResource = values => {
          addResource(values)
        }

        const handleDeleteResource = () => {
          deleteResource(`mosques/${resourceItem}`)
          closeModal()
        }

        let pageContent = (
          <TableRow>
            <TableCell><Loader /></TableCell>
            <TableCell></TableCell>
            <TableCell></TableCell>
            <TableCell></TableCell>
            <TableCell></TableCell>
          </TableRow>
        )

        if (!loading) {

          if (list.length > 0) {
            pageContent = list.map(mosque => (
              <TableRow key={mosque.identifier}>
                <TableCell className={classes.tableData}>{mosque.mosqueName}</TableCell>
                <TableCell className={classes.tableData}>{mosque.mosqueImam}</TableCell>
                <TableCell className={classes.tableData}>{mosque.email}</TableCell>
                <TableCell className={classes.tableData}>{mosque.mosqueNumber}</TableCell>
                <TableCell className={classes.tableData}>
                  <div className={classes.actions}>
                    <Link
                      to={`/admin/mosques/view-mosque/${mosque.identifier}`}
                      className={classes.link}
                    >
                      <VisibilityIcon />
                    </Link>
                    <Link
                      to={`/admin/mosques/edit-mosque/${mosque.identifier}`}
                      className={classes.link}
                    >
                      <EditIcon />
                    </Link>
                    <DeleteForeverIcon 
                      onClick={() => showConfirmationModal(mosque)}
                      className={classes.danger}
                    />
                  </div>
                </TableCell>
              </TableRow>
            ))
          } else {
            pageContent = (
              <TableRow>
                <TableCell>There is no data to display...</TableCell>
                <TableCell></TableCell>
                <TableCell></TableCell>
                <TableCell></TableCell>
                <TableCell></TableCell>
              </TableRow>
            )
          }
        }

        return (
          <>
            <div className={classes.pageHeader}>
                <CustomTypography bold="true" variant="h4" component="h1">
                  Mosques
                </CustomTypography>
                <button
                  type="button"
                  className={classes.button}
                  onClick={openResourceForm}
                >
                  <AddIcon />Add New
                </button>
            </div>
            <CustomPaper bottommargin="true">
              <Table className={classes.table} aria-label="simple table">
                <TableHead>
                  <TableRow>
                    <TableCell className={classes.tableHead}>Mosque Name</TableCell>
                    <TableCell className={classes.tableHead}>Mosque Imam</TableCell>
                    <TableCell className={classes.tableHead}>Email Address</TableCell>
                    <TableCell className={classes.tableHead}>Contact Number</TableCell>
                    <TableCell className={classes.tableHead} align="right">Actions</TableCell>
                  </TableRow>
                </TableHead>
                <TableBody>
                  {pageContent}
                </TableBody>
              </Table>
            </CustomPaper>
            <CustomModal
              show={modal}
              handleClose={closeModal}
              title={'Delete Mosque'}
              content={
                <CustomTypography variant="h6" component="p" bold="true">
                  Are you sure you want to delete this mosque
                </CustomTypography>
              }
              confirmation={handleDeleteResource}
            />
            <CustomModal
              show={resourceForm}
              handleClose={closeResourceForm}
              title={'Add New Mosque'}
              content={
                <div className={classes.form}>
                  <CustomForm
                    fields={fields}
                    // validation={validation}
                    submitHandler={handleAddResource}
                    submitButton={
                      <CustomButton
                        type="submit"
                      >
                        Add New
                      </CustomButton>
                    }
                  />
                </div>
              }
            />
          </>
        )
      }}
    </AdminResource>
  )
}

export default withStyles(styles)(Mosques)