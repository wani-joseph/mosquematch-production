import React from 'react'
import AdminResource from '../../../services/Admin'
import { Link, withRouter } from 'react-router-dom'
import CustomPaper from '../../UI/CustomPaper/CustomPaper'
import CustomTable from '../../UI/CustomTable/CustomTable'
import CustomTypography from '../../UI/CustomTypography/CustomTypography'
import Loader from '../../UI/Loader/Loader'
import { withStyles } from '@material-ui/core'
import { styles } from '../AdminStyles'
import KeyboardBackspaceIcon from '@material-ui/icons/KeyboardBackspace'

const SingleMosque = ({ classes, match }) => {
  const resourceId = match.params.mosqueId

  return (
    <AdminResource resourceId={`mosques/${resourceId}`}>
      {resource => {

        const { resourceItem } = resource

        let pageContent = <Loader />

        if (resourceItem) pageContent = (
          <>
            <div className={classes.pageHeader}>
              <CustomTypography bold="true" variant="h4" component="h1">
                View Mosque
              </CustomTypography>
              <Link
                className={classes.button}
                to="/admin/mosques"
              >
                <KeyboardBackspaceIcon />back
              </Link>
            </div>
            <CustomPaper spacing="true" classes={{ root: classes.spacingPaper }}>
              <CustomTable
                question="Mosque Name"
                answer={resourceItem.mosqueName}
              />
              <CustomTable
                question="Mosque Imam"
                answer={resourceItem.mosqueImam}
              />
              <CustomTable
                question="Region Covered"
                answer={resourceItem.coveredRegion}
              />
              <CustomTable
                question="City"
                answer={resourceItem.city}
              />
              <CustomTable
                question="Address"
                answer={resourceItem.streetAddress}
              />
              <CustomTable
                question="Post Code"
                answer={resourceItem.postCode}
              />
              <CustomTable
                question="Contact Number"
                answer={resourceItem.mosqueNumber}
              />
              <CustomTable
                question="Email"
                answer={resourceItem.email}
              />
              <CustomTable
                question="Website"
                answer={resourceItem.website}
              />
            </CustomPaper>
          </>
        )

        return <>{pageContent}</>
      }}
    </AdminResource>
  )
}

export default withStyles(styles)(withRouter(SingleMosque))