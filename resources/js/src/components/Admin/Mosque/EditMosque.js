import React from 'react'
import AdminResource from '../../../services/Admin'
import { Link, withRouter } from 'react-router-dom'
import CustomPaper from '../../UI/CustomPaper/CustomPaper'
import CustomButton from '../../UI/CustomButton/CustomButton'
import CustomForm from '../../UI/CustomForm/CustomForm'
import CustomTypography from '../../UI/CustomTypography/CustomTypography'
import Loader from '../../UI/Loader/Loader'
import { withStyles } from '@material-ui/core'
import { styles } from '../AdminStyles'
import KeyboardBackspaceIcon from '@material-ui/icons/KeyboardBackspace'

const EditMosque = ({ classes, match }) => {
  const resourceId = match.params.mosqueId

  return (
    <AdminResource resourceId={`mosques/${resourceId}`}>
      {resource => {

        const { loading, resourceItem, editResource } = resource

        const handleEditResource = values => {
          editResource(values)
        }

        let pageContent = <Loader />

        if (!loading && resourceItem) {

          const fields = [
            {label: 'Mosque Name', type: 'text', name: 'mosqueName', value: resourceItem.mosqueName},
            {label: 'Mosque Imam', type: 'text', name: 'mosqueImam', value: resourceItem.mosqueImam},
            {label: 'Region Covered', type: 'text', name: 'coveredRegion', value: resourceItem.coveredRegion},
            {label: 'City', type: 'text', name: 'city', value: resourceItem.city},
            {label: 'Address', type: 'text', name: 'streetAddress', value: resourceItem.streetAddress},
            {label: 'Post Code', type: 'text', name: 'postCode', value: resourceItem.postCode},
            {label: 'Contact', type: 'text', name: 'mosqueNumber', value: resourceItem.mosqueNumber},
            {label: 'Email', type: 'email', name: 'email', value: resourceItem.email},
            {label: 'Website', type: 'url', name: 'website', value: resourceItem.website},
            {label: 'Latitude', type: 'text', name: 'latitude', value: resourceItem.latitude},
            {label: 'Longitude', type: 'text', name: 'longtitude', value: resourceItem.longtitude}
          ]

          pageContent = (
            <>
              <div className={classes.pageHeader}>
                <CustomTypography bold="true" variant="h4" component="h1">
                  Edit Mosque
                </CustomTypography>
                <Link
                  className={classes.button}
                  to="/admin/mosques"
                >
                  <KeyboardBackspaceIcon />back
                </Link>
              </div>
              <CustomPaper classes={{ root: classes.paper }}>
                <CustomForm
                  fields={fields}
                  // validation={validation}
                  submitHandler={handleEditResource}
                  submitButton={
                    <CustomButton
                      type="submit"
                    >
                      Update Mosque
                    </CustomButton>
                  }
                />
              </CustomPaper>
            </>
          )
        }

        return <>{pageContent}</>
      }}
    </AdminResource>
  )
}

export default withStyles(styles)(withRouter(EditMosque))