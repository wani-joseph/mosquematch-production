import React, { Component } from 'react'
import axios from 'axios'

class Mosques extends Component {
    constructor() {
        super()
        this._isMounted = false
        this.state = {
            mosques: [],
            loading: false,
            per: 10,
            page: 1,
            total: null,
            totalPages: null,
        }
        this.loadMosques = this.loadMosques.bind(this)
        this.handleClick = this.handleClick.bind(this)
        this.loadNextPage = this.loadNextPage.bind(this)
        this.loadPrevPage = this.loadPrevPage.bind(this)
        this.handleScolling = this.handleScolling.bind(this)
    }

    componentDidMount() {
        this._isMounted = true
        this.loadMosques()
    }

    componentWillUnmount() {
        this._isMounted = false
    }
    
    loadMosques() {
        if (this._isMounted) this.setState({ loading: true })

        const { per, page } = this.state

        axios({
            method: 'get',
            url: `/api/mosques?per_page=${per}&page=${page}`,
            headers: {
                'Authorization': `Bearer ${localStorage.getItem('token')}`
            }
        })
        .then(res => {
            const mosques = []

            for (let mosque in res.data.data) {
                mosques.push({ ...res.data.data[mosque] })
            }

            if (this._isMounted) {
                this.setState({
                    mosques: mosques,
                    loading: false,
                })
            }
        })
        .catch(err => console.log(err))
    }

    loadNextPage() {
        const { totalPages, page } = this.state

        if (totalPages <= page) return

        if (this._isMounted) {
            this.setState(prevState => ({
                page: prevState.page + 1,
            }), this.loadMosques)
        }

        this.handleScolling()
    }

    loadPrevPage() {
        const { page } = this.state

        if (page == 1) return

        if (this._isMounted) {
            this.setState(prevState => ({
                page: prevState.page - 1,
            }), this.loadMosques)
        }

        this.handleScolling()
    }

    handleScolling() {
        setTimeout(() => {
            window.scroll({
                top: 0, 
                left: 0, 
                behavior: 'smooth'
            });
        }, 50)
    }

    handleClick(values, actions) {
        if (this._isMounted) this.setState({ loading: true })

        axios({
            method: 'post',
            url: '/api/mosques',
            data: values,
            headers: {
                'Authorization': `Bearer ${localStorage.getItem('token')}`
            }
        })
        .then(res => {
            const { mosques } = this.state
            if (this._isMounted) {
                this.setState({
                    loading: false,
                    mosques: [...mosques, res.data.data],
                })
            }
        })
        .catch(err => console.log(err))

        actions.setSubmitting(true)
        actions.resetForm()
    }

    render() {
        return this.props.children({
            ...this.state,
            loadMosques: this.loadMosques,
            handleClick: this.handleClick,
            loadNextPage: this.loadNextPage,
            loadPrevPage: this.loadPrevPage,
            handleScolling: this.handleScolling,
        })
    }
}

export default Mosques