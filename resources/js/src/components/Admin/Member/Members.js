import React from 'react'
import AdminResource from '../../../services/Admin'
import { Link } from 'react-router-dom'
import CustomButton  from '../../UI/CustomButton/CustomButton'
import CustomPaper from '../../UI/CustomPaper/CustomPaper'
import CustomModal from '../../UI/CustomModal/CustomModal'
import CustomForm  from '../../UI/CustomForm/CustomForm'
import CustomTypography from '../../UI/CustomTypography/CustomTypography'
import Loader from '../../UI/Loader/Loader'
import {
  Table,
  TableHead,
  TableBody,
  TableRow,
  TableCell,
  withStyles
} from '@material-ui/core'
import VisibilityIcon from '@material-ui/icons/Visibility'
import EditIcon from '@material-ui/icons/Edit'
import DeleteForeverIcon from '@material-ui/icons/DeleteForever'
import AddIcon from '@material-ui/icons/Add'
import { styles } from '../AdminStyles'
import { cities } from '../../../shared/cities'
import { countries } from '../../../shared/countries'
import {
  days,
  months,
  years,
  languages,
  profession,
  jobTitles,
  educationLevels,
  income,
  marriageWithin,
  martialStatus,
  likeChildren,
  arrangements,
  height,
  build,
  smoke,
  drink,
  disabilities,
  phonetime,
  religious,
  sector,
  hijab,
  keepHalal,
  prayerQuraan,
  yesNo
} from '../../../shared/selectOptions'

const Members = ({ classes }) => {
  const [modal, setModal] = React.useState(false)
  const [resourceItem, setResourceItem] = React.useState(null)

  const openModal = () => {
    setModal(true)
  }

  const closeModal = () => {
    setModal(false)
  }

  const fields = [
    {label: 'First Name', type: 'text', name: 'fname', value: ''},
    {label: 'Last Name', type: 'text', name: 'lname', value: ''},
    {label: 'Where you live', type: 'select', data: cities, name: 'location', value: ''},
    {label: 'Country of birth', type: 'select', data: countries, name: 'countryBirth', value: ''},
    {label: 'Date of birth', type: 'select', data: days, name: 'dayOfBirth', value: ''},
    {label: 'Month of birth', type: 'select', data: months, name: 'monthOfBirth', value: ''},
    {label: 'Year of birth', type: 'select', data: years, name: 'yearOfBirth', value: ''},
    {label: 'Tagline', type: 'text', name: 'tagline', value: ''},
    {label: 'A little about you', type: 'textarea', rows: '4', name: 'bio', value: ''},
    {label: "What I'm looking for", type: 'textarea', rows: '4', name: 'seeking', value: ''},
    {label: 'My education level', type: 'select', data: educationLevels, name: 'eduLevel', value: ''},
    {label: 'Subject I studied', type: 'text', name: 'subject', value: ''},
    {label: 'My profession', type: 'select', data: profession, name: 'profession', value: ''},
    {label: 'My job title', type: 'select', data: jobTitles, name: 'jobTitle', value: ''},
    {label: 'My first language', type: 'select', data: languages, name: 'fLang', value: ''},
    {label: 'My second language', type: 'select', data: languages, name: 'sLang', value: ''},
    {label: 'My citizenship', type: 'select', data: countries, name: 'citizenship', value: ''},
    {label: 'Country of Origin', type: 'select', data: countries, name: 'countryOrigin', value: ''},
    {label: 'Willing to relocate', type: 'select', data: countries, name: 'relocate', value: ''},
    {label: 'My income', type: 'select', data: income, name: 'income', value: ''},
    {label: "I'm looking to marriage within", type: 'select', data: marriageWithin, name: 'marriageWithin', value: ''},
    {label: 'Martial status', type: 'select', data: martialStatus, name: 'martialStatus', value: ''},
    {label: 'Would I like to have children', type: 'select', data: likeChildren, name: 'likeChildren', value: ''},
    {label: 'Do I have children?', type: 'select', data: yesNo, name: 'children', value: ''},
    {label: 'My living arrangements', type: 'select', data: arrangements, name: 'arrangements', value: ''},
    {label: 'My height', type: 'select', data: height, name: 'height', value: ''},
    {label: "My build", type: 'select', data: build, name: 'build', value: ''},
    {label: 'Do I smoke?', type: 'select', data: smoke, name: 'smoke', value: ''},
    {label: 'Do I drink?', type: 'select', data: drink, name: 'drink', value: ''},
    {label: "Do I have any disabilities?", type: 'select', data: disabilities, name: 'disabilities', value: ''},
    {label: 'How long do you spend on your phone daily?', type: 'select', data: phonetime, name: 'phonetime', value: ''},
    {label: 'Religiousness', type: 'select', data: religious, name: 'religious', value: ''},
    {label: 'My sector', type: 'select', data: sector, name: 'sector', value: ''},
    {label: 'Do you prefer Hijab / Niqab?', type: 'select', data: hijab, name: 'hijab', value: ''},
    {label: 'Do you prefer a beard?', type: 'select', data: yesNo, name: 'beard', value: ''},
    {label: "Are you a revert?", type: 'select', data: yesNo, name: 'revert', value: ''},
    {label: 'Do you keep halal?', type: 'select', data: keepHalal, name: 'keepHalal', value: ''},
    {label: 'Do you pray?', type: 'select', data: prayerQuraan, name: 'prayer', value: ''},
    {label: 'How often do you read Quran?', type: 'select', data: prayerQuraan, name: 'readQuran', value: ''},
    {label: 'Wali Name', type: 'text', name: 'waliName', value: ''},
    {label: 'Wali Contact Number', type: 'text', name: 'waliContact', value: ''},
    {label: 'Wali Email', type: 'email', name: 'waliEmail', value: ''},
  ]
  
  const showConfirmationModal = (resource) => {
    openModal()
    setResourceItem(resource.identifier)
  }
  
  return (
    <AdminResource page="resource listing" resource="users">
      {resource => {

        const { loading, list, addResource, deleteResource, resourceForm, openResourceForm, closeResourceForm } = resource

        const handleAddResource = values => {
          addResource(values)
        }

        const handleDeleteResource = () => {
          deleteResource(`users/${resourceItem}`)
          closeModal()
        }

        let pageContent = (
          <TableRow>
            <TableCell><Loader /></TableCell>
            <TableCell></TableCell>
            <TableCell></TableCell>
            <TableCell></TableCell>
            <TableCell></TableCell>
          </TableRow>
        )

        if (!loading) {

          if (list.length > 0) {
            pageContent = list.map(member => (
              <TableRow key={member.identifier}>
                <TableCell className={classes.tableData}>{member.fname}</TableCell>
                <TableCell className={classes.tableData}>{member.lname}</TableCell>
                <TableCell className={classes.tableData}>{member.email}</TableCell>
                <TableCell className={classes.tableData}>{member.mosques.data != "" ? member.mosques.data[0].mosqueName : 'London Central Mosque'}</TableCell>
                <TableCell className={classes.tableData}>
                  <div className={classes.actions}>
                    <Link
                      to={`/admin/members/view-member/${member.identifier}`}
                      className={classes.link}
                    >
                      <VisibilityIcon />
                    </Link>
                    <Link
                      to={`/admin/members/edit-member/${member.identifier}`}
                      className={classes.link}
                    >
                      <EditIcon />
                    </Link>
                    <DeleteForeverIcon 
                      onClick={() => showConfirmationModal(member)}
                      className={classes.danger}
                    />
                  </div>
                </TableCell>
              </TableRow>
            ))
          } else {
            pageContent = (
              <TableRow>
                <TableCell>There is no data to display...</TableCell>
                <TableCell></TableCell>
                <TableCell></TableCell>
                <TableCell></TableCell>
                <TableCell></TableCell>
              </TableRow>
            )
          }
        }

        return (
          <>
            <div className={classes.pageHeader}>
                <CustomTypography bold="true" variant="h4" component="h1">
                  Members
                </CustomTypography>
                <button
                  type="button"
                  className={classes.button}
                  onClick={openResourceForm}
                >
                  <AddIcon />Add New
                </button>
            </div>
            <CustomPaper bottommargin="true">
              <Table className={classes.table} aria-label="simple table">
                <TableHead>
                  <TableRow>
                    <TableCell className={classes.tableHead}>First Name</TableCell>
                    <TableCell className={classes.tableHead}>Last Name</TableCell>
                    <TableCell className={classes.tableHead}>Email Address</TableCell>
                    <TableCell className={classes.tableHead}>Mosque</TableCell>
                    <TableCell className={classes.tableHead} align="right">Actions</TableCell>
                  </TableRow>
                </TableHead>
                <TableBody>
                  {pageContent}
                </TableBody>
              </Table>
            </CustomPaper>
            <CustomModal
              show={modal}
              handleClose={closeModal}
              title={'Delete Member'}
              content={
                <CustomTypography variant="h6" component="p" bold="true">
                  Are you sure you want to delete this member
                </CustomTypography>
              }
              confirmation={handleDeleteResource}
            />
            <CustomModal
              show={resourceForm}
              handleClose={closeResourceForm}
              title={'Add New Member'}
              content={
                <div className={classes.form}>
                  <CustomForm
                    fields={fields}
                    // validation={validation}
                    submitHandler={handleAddResource}
                    submitButton={
                      <CustomButton
                        type="submit"
                      >
                        Add New
                      </CustomButton>
                    }
                  />
                </div>
              }
            />
          </>
        )
      }}
    </AdminResource>
  )
}

export default withStyles(styles)(Members)