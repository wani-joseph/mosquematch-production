import React from 'react'
import AdminResource from '../../../services/Admin'
import { Link, withRouter } from 'react-router-dom'
import CustomPaper from '../../UI/CustomPaper/CustomPaper'
import CustomButton from '../../UI/CustomButton/CustomButton'
import CustomForm from '../../UI/CustomForm/CustomForm'
import CustomTypography from '../../UI/CustomTypography/CustomTypography'
import Loader from '../../UI/Loader/Loader'
import { withStyles } from '@material-ui/core'
import { styles } from '../AdminStyles'
import KeyboardBackspaceIcon from '@material-ui/icons/KeyboardBackspace'
import { cities } from '../../../shared/cities'
import { countries } from '../../../shared/countries'
import {
  days,
  months,
  years,
  languages,
  profession,
  jobTitles,
  educationLevels,
  income,
  marriageWithin,
  martialStatus,
  likeChildren,
  arrangements,
  height,
  build,
  smoke,
  drink,
  disabilities,
  phonetime,
  religious,
  sector,
  hijab,
  keepHalal,
  prayerQuraan,
  yesNo
} from '../../../shared/selectOptions'

const EditMember = ({ classes, match }) => {
  const resourceId = match.params.memberId

  return (
    <AdminResource resourceId={`users/${resourceId}`}>
      {resource => {

        const { loading, resourceItem, editResource } = resource

        const handleEditResource = values => {
          editResource(values)
        }

        let pageContent = <Loader />

        if (!loading && resourceItem) {

          const fields = [
            {label: 'First Name', type: 'text', name: 'fname', value: resourceItem.fname},
            {label: 'Last Name', type: 'text', name: 'lname', value: resourceItem.lname},
            {label: 'Where you live', type: 'select', data: cities, name: 'location', value: resourceItem.location},
            {label: 'Country of birth', type: 'select', data: countries, name: 'countryBirth', value: resourceItem.countryBirth},
            {label: 'Date of birth', type: 'select', data: days, name: 'dayOfBirth', value: resourceItem.dayOfBirth},
            {label: 'Month of birth', type: 'select', data: months, name: 'monthOfBirth', value: resourceItem.monthOfBirth},
            {label: 'Year of birth', type: 'select', data: years, name: 'yearOfBirth', value: resourceItem.yearOfBirth},
            {label: 'Tagline', type: 'text', name: 'tagline', value: resourceItem.tagline},
            {label: 'A little about you', type: 'textarea', rows: '4', name: 'bio', value: resourceItem.bio},
            {label: "What I'm looking for", type: 'textarea', rows: '4', name: 'seeking', value: resourceItem.seeking},
            {label: 'My education level', type: 'select', data: educationLevels, name: 'eduLevel', value: resourceItem.eduLevel},
            {label: 'Subject I studied', type: 'text', name: 'subject', value: resourceItem.subject},
            {label: 'My profession', type: 'select', data: profession, name: 'profession', value: resourceItem.profession},
            {label: 'My job title', type: 'select', data: jobTitles, name: 'jobTitle', value: resourceItem.jobTitle},
            {label: 'My first language', type: 'select', data: languages, name: 'fLang', value: resourceItem.fLang},
            {label: 'My second language', type: 'select', data: languages, name: 'sLang', value: resourceItem.sLang},
            {label: 'My citizenship', type: 'select', data: countries, name: 'citizenship', value: resourceItem.citizenship},
            {label: 'Country of Origin', type: 'select', data: countries, name: 'countryOrigin', value: resourceItem.countryOrigin},
            {label: 'Willing to relocate', type: 'select', data: countries, name: 'relocate', value: resourceItem.relocate},
            {label: 'My income', type: 'select', data: income, name: 'income', value: resourceItem.income},
            {label: "I'm looking to marriage within", type: 'select', data: marriageWithin, name: 'marriageWithin', value: resourceItem.marriageWithin},
            {label: 'Martial status', type: 'select', data: martialStatus, name: 'martialStatus', value: resourceItem.martialStatus},
            {label: 'Would I like to have children', type: 'select', data: likeChildren, name: 'likeChildren', value: resourceItem.likeChildren},
            {label: 'Do I have children?', type: 'select', data: yesNo, name: 'children', value: resourceItem.children},
            {label: 'My living arrangements', type: 'select', data: arrangements, name: 'arrangements', value: resourceItem.arrangements},
            {label: 'My height', type: 'select', data: height, name: 'height', value: resourceItem.height},
            {label: "My build", type: 'select', data: build, name: 'build', value: resourceItem.build},
            {label: 'Do I smoke?', type: 'select', data: smoke, name: 'smoke', value: resourceItem.smoke},
            {label: 'Do I drink?', type: 'select', data: drink, name: 'drink', value: resourceItem.drink},
            {label: "Do I have any disabilities?", type: 'select', data: disabilities, name: 'disabilities', value: resourceItem.disabilities},
            {label: 'How long do you spend on your phone daily?', type: 'select', data: phonetime, name: 'phonetime', value: resourceItem.phonetime},
            {label: 'Religiousness', type: 'select', data: religious, name: 'religious', value: resourceItem.religious},
            {label: 'My sector', type: 'select', data: sector, name: 'sector', value: resourceItem.sector},
            {label: 'Do you prefer Hijab / Niqab?', type: 'select', data: hijab, name: 'hijab', value: resourceItem.hijab},
            {label: 'Do you prefer a beard?', type: 'select', data: yesNo, name: 'beard', value: resourceItem.beard},
            {label: "Are you a revert?", type: 'select', data: yesNo, name: 'revert', value: resourceItem.revert},
            {label: 'Do you keep halal?', type: 'select', data: keepHalal, name: 'keepHalal', value: resourceItem.keepHalal},
            {label: 'Do you pray?', type: 'select', data: prayerQuraan, name: 'prayer', value: resourceItem.prayer},
            {label: 'How often do you read Quran?', type: 'select', data: prayerQuraan, name: 'readQuran', value: resourceItem.readQuran},
            {label: 'Wali Name', type: 'text', name: 'waliName', value: resourceItem.waliName},
            {label: 'Wali Contact Number', type: 'text', name: 'waliContact', value: resourceItem.waliContact},
            {label: 'Wali Email', type: 'email', name: 'waliEmail', value: resourceItem.waliEmail},
          ]

          pageContent = (
            <>
              <div className={classes.pageHeader}>
                <CustomTypography bold="true" variant="h4" component="h1">
                  Edit Member
                </CustomTypography>
                <Link
                  className={classes.button}
                  to="/admin/members"
                >
                  <KeyboardBackspaceIcon />back
                </Link>
              </div>
              <CustomPaper classes={{ root: classes.paper }}>
                <CustomForm
                  fields={fields}
                  // validation={validation}
                  submitHandler={handleEditResource}
                  submitButton={
                    <CustomButton
                      type="submit"
                    >
                      Update Member
                    </CustomButton>
                  }
                />
              </CustomPaper>
            </>
          )
        }

        return <>{pageContent}</>
      }}
    </AdminResource>
  )
}

export default withStyles(styles)(withRouter(EditMember))