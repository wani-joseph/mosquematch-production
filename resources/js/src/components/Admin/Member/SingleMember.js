import React from 'react'
import AdminResource from '../../../services/Admin'
import { Link, withRouter } from 'react-router-dom'
import CustomPaper from '../../UI/CustomPaper/CustomPaper'
import CustomTable from '../../UI/CustomTable/CustomTable'
import CustomTypography from '../../UI/CustomTypography/CustomTypography'
import Loader from '../../UI/Loader/Loader'
import { withStyles } from '@material-ui/core'
import { styles } from '../AdminStyles'
import KeyboardBackspaceIcon from '@material-ui/icons/KeyboardBackspace'

const SingleMember = ({ classes, match }) => {
  const resourceId = match.params.memberId

  return (
    <AdminResource resourceId={`users/${resourceId}`}>
      {resource => {

        const { resourceItem } = resource

        let pageContent = <Loader />

        if (resourceItem) pageContent = (
          <>
            <div className={classes.pageHeader}>
              <CustomTypography bold="true" variant="h4" component="h1">
                View Member
              </CustomTypography>
              <Link
                className={classes.button}
                to="/admin/members"
              >
                <KeyboardBackspaceIcon />back
              </Link>
            </div>
            <CustomPaper spacing="true" classes={{ root: classes.spacingPaper }}>
              <CustomTable
                question="First Name"
                answer={resourceItem.fname}
              />
              <CustomTable
                question="Last Name"
                answer={resourceItem.lname}
              />
              <CustomTable
                question="Where you live"
                answer={resourceItem.location}
              />
              <CustomTable
                question="Country of birth"
                answer={resourceItem.countryBirth}
              />
              <CustomTable
                question="Day of birth"
                answer={resourceItem.dayOfBirth}
              />
              <CustomTable
                question="Month of birth"
                answer={resourceItem.monthOfBirth}
              />
              <CustomTable
                question="Year of birth"
                answer={resourceItem.yearOfBirth}
              />
              <CustomTable
                question="Tagline"
                answer={resourceItem.tagline}
              />
              <CustomTable
                question="A little about you"
                answer={resourceItem.bio}
              />
              <CustomTable
                question="What I'm looking for"
                answer={resourceItem.seeking}
              />
              <CustomTable
                question="My education level"
                answer={resourceItem.eduLevel}
              />
              <CustomTable
                question="Subject I studied"
                answer={resourceItem.subject}
              />
              <CustomTable
                question="My profession"
                answer={resourceItem.profession}
              />
              <CustomTable
                question="My job title"
                answer={resourceItem.jobTitle}
              />
              <CustomTable
                question="My first language"
                answer={resourceItem.fLang}
              />
              <CustomTable
                question="My second language"
                answer={resourceItem.sLang}
              />
              <CustomTable
                question="My citizenship"
                answer={resourceItem.citizenship}
              />
              <CustomTable
                question="Country of Origin"
                answer={resourceItem.countryOrigin}
              />
              <CustomTable
                question="Willing to relocate"
                answer={resourceItem.relocate}
              />
              <CustomTable
                question="I'm looking to marriage within"
                answer={resourceItem.marriageWithin}
              />
              <CustomTable
                question="Martial status"
                answer={resourceItem.martialStatus}
              />
              <CustomTable
                question="Would I like to have children"
                answer={resourceItem.likeChildren}
              />
              <CustomTable
                question="Do I have children"
                answer={resourceItem.children}
              />
              <CustomTable
                question="My living arrangements"
                answer={resourceItem.arrangements}
              />
              <CustomTable
                question="My height"
                answer={resourceItem.height}
              />
              <CustomTable
                question="My build"
                answer={resourceItem.build}
              />
              <CustomTable
                question="Do I smoke"
                answer={resourceItem.smoke}
              />
              <CustomTable
                question="Do I drink"
                answer={resourceItem.drink}
              />
              <CustomTable
                question="Do I have any disabilities"
                answer={resourceItem.disabilities}
              />
              <CustomTable
                question="How long do you spend on your phone daily"
                answer={resourceItem.phonetime}
              />
              <CustomTable
                question="Religiousness"
                answer={resourceItem.religious}
              />
              <CustomTable
                question="My sector"
                answer={resourceItem.sector}
              />
              <CustomTable
                question="Do you prefer Hijab / Niqab"
                answer={resourceItem.hijab}
              />
              <CustomTable
                question="Do you prefer a beard"
                answer={resourceItem.beard}
              />
              <CustomTable
                question="Are you a revert"
                answer={resourceItem.revert}
              />
              <CustomTable
                question="Do you keep halal"
                answer={resourceItem.keepHalal}
              />
            </CustomPaper>
          </>
        )

        return <>{pageContent}</>
      }}
    </AdminResource>
  )
}

export default withStyles(styles)(withRouter(SingleMember))