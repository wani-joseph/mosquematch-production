// import React from 'react'
// import Mosques from './Mosques'
// import Users from './Users'
// import {
//     Table,
//     TableHead,
//     TableBody,
//     TableRow,
//     TableCell
// } from '@material-ui/core'
// import AdminHeader from '../AdminHeader/AdminHeader'
// import Loader from '../UI/Loader/Loader'
// import CustomPaper from '../UI/CustomPaper/CustomPaper'
// import Pagination from '../Pagination/Pagination'
// import VisibilityIcon from '@material-ui/icons/Visibility'
// import EditIcon from '@material-ui/icons/Edit'
// import DeleteForeverIcon from '@material-ui/icons/DeleteForever'
// import { useStyles } from './AdminStyles'

// const Admin = props => {
//     const classes = useStyles()
    
//     if (props.page == "mosques") {
//         return (
//             <Mosques>
//                 {(props) => {
//                     const { loading, mosques, handleClick, page, totalPages, prev, next } = props

//                     const fields = [
//                         {label: 'Mosque Name', type: 'text', name: 'mosqueName', value: ''},
//                         {label: 'Mosque Imam', type: 'text', name: 'mosqueImam', value: ''},
//                         {label: 'Region Covered', type: 'text', name: 'coveredRegion', value: ''},
//                         {label: 'City', type: 'text', name: 'city', value: ''},
//                         {label: 'Address', type: 'text', name: 'streetAddress', value: ''},
//                         {label: 'Post Code', type: 'text', name: 'postCode', value: ''},
//                         {label: 'Contact', type: 'text', name: 'mosqueNumber', value: ''},
//                         {label: 'Email', type: 'email', name: 'email', value: ''},
//                         {label: 'Website', type: 'url', name: 'website', value: ''},
//                         {label: 'Latitude', type: 'text', name: 'latitude', value: ''},
//                         {label: 'Longtitude', type: 'text', name: 'longtitude', value: ''}
//                     ]

//                     let mosquesList = (
//                         <TableRow>
//                             <TableCell><Loader /></TableCell>
//                         </TableRow>
//                     )

//                     if (!loading) {
//                         mosquesList = mosques.map(mosque => (
//                             <TableRow key={mosque.identifier}>
//                                 <TableCell className={classes.tableData}>{mosque.mosqueName}</TableCell>
//                                 <TableCell className={classes.tableData}>{mosque.mosqueImam}</TableCell>
//                                 <TableCell className={classes.tableData}>{mosque.email}</TableCell>
//                                 <TableCell className={classes.tableData}>{mosque.mosqueNumber}</TableCell>
//                                 <TableCell className={classes.tableData}>
//                                     <div className={classes.actions}>
//                                         <VisibilityIcon />
//                                         <EditIcon />
//                                         <DeleteForeverIcon />
//                                     </div>
//                                 </TableCell>
//                             </TableRow>
//                         ))
//                     }


//                     return (
//                         <React.Fragment>
//                             <AdminHeader
//                                 pageHeader="Mosques"
//                                 handleClick={handleClick}
//                                 fields={fields}
//                             />
//                             <CustomPaper bottommargin="true">
//                                 <Table className={classes.table} aria-label="simple table">
//                                     <TableHead>
//                                         <TableRow>
//                                             <TableCell className={classes.tableHead}>Mosque Name</TableCell>
//                                             <TableCell className={classes.tableHead}>Mosque Imam</TableCell>
//                                             <TableCell className={classes.tableHead}>Email Address</TableCell>
//                                             <TableCell className={classes.tableHead}>Contact Number</TableCell>
//                                             <TableCell className={classes.tableHead} align="right">Actions</TableCell>
//                                         </TableRow>
//                                     </TableHead>
//                                     <TableBody>
//                                         {mosquesList}
//                                     </TableBody>
//                                 </Table>
//                                 <Pagination
//                                     currentPage={page}
//                                     totalPages={totalPages}
//                                     prev={prev}
//                                     next={next}
//                                 />
//                             </CustomPaper>
//                         </React.Fragment>
//                     )
//                 }}
//             </Mosques>
//         )
//     }
    
//     return (
//         <Users>
//             {(props) => {
//                 const { loading, users, handleClick, page, totalPages, prev, next } = props

//                 let usersList = (
//                     <TableRow>
//                         <TableCell><Loader /></TableCell>
//                     </TableRow>
//                 )

//                 if (!loading) {
//                     usersList = users.map(user => (
//                         <TableRow key={user.identifier}>
//                             <TableCell className={classes.tableData}>{user.fname}</TableCell>
//                             <TableCell className={classes.tableData}>{user.lname}</TableCell>
//                             <TableCell className={classes.tableData}>{user.email}</TableCell>
//                             <TableCell className={classes.tableData}>
//                                 <div className={classes.actions}>
//                                     <VisibilityIcon />
//                                     <EditIcon />
//                                     <DeleteForeverIcon />
//                                 </div>
//                             </TableCell>
//                         </TableRow>
//                     ))
//                 }


//                 return (
//                     <React.Fragment>
//                         <AdminHeader
//                             pageHeader="Users"
//                             handleClick={handleClick}
//                         />
//                         <CustomPaper bottommargin="true">
//                             <Table className={classes.table} aria-label="simple table">
//                                 <TableHead>
//                                     <TableRow>
//                                         <TableCell className={classes.tableHead}>First Name</TableCell>
//                                         <TableCell className={classes.tableHead}>Last Name</TableCell>
//                                         <TableCell className={classes.tableHead}>Email</TableCell>
//                                         <TableCell className={classes.tableHead} align="right">Actions</TableCell>
//                                     </TableRow>
//                                 </TableHead>
//                                 <TableBody>
//                                     {usersList}
//                                 </TableBody>
//                             </Table>
//                             <Pagination
//                                 currentPage={page}
//                                 totalPages={totalPages}
//                                 prev={prev}
//                                 next={next}
//                             />
//                         </CustomPaper>
//                     </React.Fragment>
//                 )
//             }}
//         </Users>
//     )
// }

// export default Admin

import React from 'react'
import Members from './Member/Members'
import SingleMember from './Member/SingleMember'
import EditMember from './Member/EditMember'
import Mosques from './Mosque/Mosques'
import SingleMosque from './Mosque/SingleMosque'
import EditMosque from './Mosque/EditMosque'

const Admin = ({ page }) => {

    switch(page) {
        case 'members': return <Members />
        case 'view member': return <SingleMember />
        case 'edit member': return <EditMember />
        case 'mosques': return <Mosques />
        case 'view mosque': return <SingleMosque />
        case 'edit mosque': return <EditMosque />
        default: return <p>This is dashboard page...</p>
    }
}

export default Admin