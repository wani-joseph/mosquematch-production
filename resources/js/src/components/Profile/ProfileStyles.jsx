export const styles = theme =>({
    avatar: {
        width: 32,
        height: 32,
        fontSize: 16,
        background: theme.palette.secondary.main,
        color: theme.palette.common.white
    },
    dropdownIcon: {
        position: 'absolute',
        bottom: '-7px',
        right: 0,
        padding: 0,
        color: theme.palette.primary.main,
        transform: 'scaleY(.8)'
    },
    account: {
        borderRadius: 0,
        textTransform: "capitalize",
        padding: 0,
        minWidth: 32,
    },
    label: {
        display: 'none',
        [theme.breakpoints.up('sm')]: {
            display: 'inline-flex',
            marginLeft: theme.spacing(1),
            textAlign: 'start',
        }
    },
    dropdownIcon: {
        display: 'none',
        [theme.breakpoints.up('sm')]: {
            display: 'inline-flex',
        }
    },
    paper: {
        position: 'absolute',
        top: 52,
        right: 0,
        left: 0,
        borderRadius: 0,
        textAlign: 'start',
        textTransform: 'capitalize'
    },
    purpleAvatar: {
        color: '#fff',
        backgroundColor: theme.palette.primary.main,
        textTransform: "uppercase",
        fontSize: 15
    },
    icon: {
        display: 'none',
        [theme.breakpoints.up('sm')]: {
            display: 'inline-flex',
            minWidth: 42,
        }
    },
    dashboardLabel: {
        display: 'none',
    },
    dashboardPaper: {
        left: 'auto'
    }
})