import React, { useState } from 'react'
import { connect } from 'react-redux'
import { Link, withRouter } from 'react-router-dom'
import {
    Avatar,
    ClickAwayListener,
    Button,
    Paper,
    ListItem,
    ListItemIcon,
    ListItemText,
    withStyles
} from '@material-ui/core'
import { getInitials, setProfileLink } from '../../shared/utility'
import ArrowDropDownIcon from '@material-ui/icons/ArrowDropDown'
import MeetingRoomIcon from '@material-ui/icons/MeetingRoom'
import AccountBoxIcon from '@material-ui/icons/AccountBox'
import DashboardIcon from '@material-ui/icons/Dashboard'
import HomeIcon from '@material-ui/icons/Home'
import * as actions from '../../store/actions'
import { styles } from './ProfileStyles'

const Profile = ({ currentUser, classes, dashboard, onLogout, history }) => {
    const [open, setOpen] = useState(false)

    const handleClick = () => {
        setOpen(prev => !prev)
    }

    const handleClickAway = () => {
        setOpen(false)
    }

    const handleLogout = () => {
        onLogout(() => {
            history.push('/login')
        })
    }

    let labelClass = [classes.label]
    let dropdownIconClass = [classes.dropdownIcon]
    let paperClass = [classes.paper]

    if (dashboard == "true") labelClass.push(classes.dashboardLabel)
    if (dashboard == "true") dropdownIconClass.push(classes.dashboardLabel)
    if (dashboard == "true") paperClass.push(classes.dashboardPaper)

    return (
        <ClickAwayListener onClickAway={handleClickAway}>
            <React.Fragment>
                <Button
                    className={classes.account}
                    onClick={handleClick}
                    disableFocusRipple={true}
                    disableRipple
                >
                    <Avatar className={classes.purpleAvatar}>
                        {getInitials(currentUser.fname, currentUser.lname)}
                    </Avatar>
                        <span className={labelClass.join(' ')}>{currentUser.fname} {currentUser.lname}</span>
                    <ArrowDropDownIcon className={dropdownIconClass.join(' ')} viewBox="0 0 10 24"/>
                </Button>
                {open ? (
                <Paper className={paperClass.join(' ')}>
                    {currentUser.isAdmin ? 
                        <ListItem
                            button 
                            component={Link}
                            to={dashboard == "true" ? "/search" : "/admin/dashboard"}
                        >
                            <ListItemIcon classes={{ root: classes.icon }} >
                                {dashboard == "true" ? <HomeIcon /> : <DashboardIcon />}
                            </ListItemIcon>
                            <ListItemText primary={dashboard == "true" ? "Visit site" : "Dashboard"} />
                        </ListItem> : null}
                    <ListItem
                        button 
                        component={Link}
                        to={`/profile/${setProfileLink(currentUser.fname, currentUser.lname, currentUser.identifier)}/basic-information`}
                    >
                        <ListItemIcon classes={{ root: classes.icon }} >
                            <AccountBoxIcon />
                        </ListItemIcon>
                        <ListItemText primary="Edit Profile" />
                    </ListItem>
                    <ListItem
                        button
                        onClick={handleLogout}
                    >
                        <ListItemIcon classes={{ root: classes.icon }}>
                            <MeetingRoomIcon />
                        </ListItemIcon>
                        <ListItemText primary="Logout" />
                    </ListItem>
                </Paper>
                ) : null}
            </React.Fragment>
        </ClickAwayListener>
    )
}

const mapStateToProps = state => ({
    currentUser: state.auth.authUser
})

const mapDispatchToprops = dispatch => ({
    onLogout: (next) => dispatch(actions.logout(next))
})

export default connect(mapStateToProps, mapDispatchToprops)(withStyles(styles)(withRouter(Profile)))