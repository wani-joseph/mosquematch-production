import { makeStyles } from '@material-ui/core/styles'

export const useStyles = makeStyles(theme =>({
    formLabel: {
        border: `1px solid rgba(0, 0, 0, .08)`,
        padding: '.4rem .8rem',
        width: '100%',
        margin: 0,
        justifyContent: 'center',
        [theme.breakpoints.up('md')]: {
            padding: theme.spacing(1, 2),
        }
    },
    radio: {
        display: 'none',
    },
    checked: {
        background: theme.palette.primary.main,
        color: theme.palette.common.white,
        borderColor: theme.palette.primary.main,
    }
}))