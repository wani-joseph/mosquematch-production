import React from 'react'
import {
    FormControlLabel,
    Radio
} from '@material-ui/core'
import { useStyles } from './GenderRadionButtonStyles'

const GenderRadioButton = props => {
    const classes = useStyles()
    let formLabelClasses = [classes.formLabel];

    if (props.gender === props.value) {
        formLabelClasses.push(classes.checked)
    }

    return (
        <FormControlLabel
            className={formLabelClasses.join(' ')}
            value={props.value}
            control={
                <Radio 
                    className={classes.radio}
                />
            }
            label={props.label}
            gender={props.gender}
        />
    )
}

export default GenderRadioButton