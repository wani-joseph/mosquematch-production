import React from 'react'
import { Link } from 'react-router-dom'
import { Container, Button } from '@material-ui/core'
import SectionTitle from '../../SectionTitle/SectionTitle'
import SectionSubTitle from '../../SectionSubTitle/SectionSubTitle'
import ExtendedParagraph from '../../Paragraph/ExtendedParagraph'
import Paragraph from '../../Paragraph/Paragraph'
import { useStyles } from './AboutStyles'

const About = () => {
    const classes = useStyles()

    return (
        <div className={classes.aboutSection}>
            <Container maxWidth="lg">
                <SectionTitle title="Your Perfect Local Match, Completely Free!" />
                <ExtendedParagraph align="center">
                    Your Perfect Local Match, Completely Free! 
                </ExtendedParagraph>
                <SectionSubTitle title="What is MosqueMatch?" />
                <Paragraph>
                    We know how diﬃcult it is meeting someone special to share life's journey with... you want to ﬁnd someone deeply compatible, but in a way that doesn’t compromise your values as a practising Muslim. 
                </Paragraph>
                <Paragraph>
                    Finding the right person shouldn't be so hard
                </Paragraph>
                <Paragraph>
                    That's why we created our unique 3 Step Pure Match System which is designed to help you connect with the right person in a halal way.
                </Paragraph>
                <SectionSubTitle title="Our Positives" />
                <Paragraph>
                    MosqueMatch is a Muslim community for marrying and ﬁnding the right partner.
                </Paragraph>
                <Paragraph>
                    No one can harass Muslim women since she has to approve people who wants to talk to her, and the Wali has to approve you to. 
                </Paragraph>
                <Paragraph>
                    Everyone is Linked to a mosque, where the mosque Imam will approve everyone who wants to link to that mosque.
                </Paragraph>
                <Paragraph>
                    You can view all the people near your mosque, and all our mosques and people linked to it.
                </Paragraph>
                <Paragraph>
                    Easy search to ﬁnd the right partner.
                </Paragraph>
                <Paragraph>
                    Private Photos.
                </Paragraph>
                <Paragraph>
                    Live Messages.
                </Paragraph>
                <Paragraph align="right">
                    <Button
                        className={classes.findMatchButton}
                        component={Link}
                        to="#"
                        variant="contained"
                        color="primary"
                        size="small"
                    >
                        Find your match now!
                    </Button>
                </Paragraph>
            </Container>
        </div>
    )
}

export default About