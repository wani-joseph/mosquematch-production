import { makeStyles } from '@material-ui/core/styles';

export const useStyles = makeStyles(theme =>({
    aboutSection: {
        background: theme.palette.common.white,
        paddingBottom: theme.spacing(4),
        paddingTop: theme.spacing(4),
        [theme.breakpoints.up('md')]: {
            paddingBottom: theme.spacing(6),
            paddingTop: theme.spacing(6),
        },
    },
    findMatchButton: {
        boxShadow: theme.shadows[0],
        textTransform: 'capitalize',
    }
}));