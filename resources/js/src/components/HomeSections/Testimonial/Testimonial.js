import React from 'react'
import { Link } from 'react-router-dom'
import {
    Button,
    Container,
    Grid
} from '@material-ui/core'
import SectionTitle from '../../SectionTitle/SectionTitle'
import ExtendedParagraph from '../../Paragraph/ExtendedParagraph'
import Story from '../../Testimonial/Testimonial'
import { useStyles } from './TestimonialStyles'

const Testimonial = () => {
    const classes = useStyles()

    return (
        <div className={classes.testimonials}>
            <Container maxWidth="lg">
                <SectionTitle title="Success Stories!" />
                <ExtendedParagraph align="center">
                    Your Perfect Local Match, Completely Free! 
                </ExtendedParagraph>
                <Grid container spacing={3}>
                    <Story />
                    <Grid item xs={12} className={classes.allStories}>
                        <Button
                            className={classes.allStoriesButton}
                            component={Link}
                            to="#"
                            variant="contained"
                            color="primary"
                            size="small"
                        >
                            See all success stories
                        </Button>
                    </Grid>
                </Grid>
            </Container>
        </div>
    )
}

export default Testimonial