import { makeStyles } from '@material-ui/core/styles';

export const useStyles = makeStyles(theme =>({
    testimonials: {
        background: theme.palette.common.white,
        paddingBottom: theme.spacing(4),
        paddingTop: theme.spacing(4),
        [theme.breakpoints.up('md')]: {
            paddingBottom: theme.spacing(6),
            paddingTop: theme.spacing(6),
        },
    },
    allStories: {
        display: 'flex',
        justifyContent: 'flex-end',
        [theme.breakpoints.up('md')]: {
            marginTop: theme.spacing(2),
        },
    },
    allStoriesButton: {
        boxShadow: theme.shadows[0],
        textTransform: 'capitalize',
    }
}));