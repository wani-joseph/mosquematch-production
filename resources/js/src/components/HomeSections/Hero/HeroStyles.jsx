import { makeStyles } from '@material-ui/core/styles';

export const useStyles = makeStyles(theme =>({
    hero: {
        background: "url('/images/mosquematch-banner-bg.png')",
        marginTop: theme.spacing(5),
        padding: theme.spacing(12, 0)
    },
    img: {
        display: 'flex',
        maxWidth: '100%',
        height: 'auto'
    },
    slides: {
        padding: theme.spacing(2)
    },
    slide: {
        color: 'white',
    },
    dots: {
        bottom: '-16px !important',
    },
}));