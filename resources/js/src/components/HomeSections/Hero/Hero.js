import React from 'react'
import Slider from 'react-slick'
import { Container, Grid } from '@material-ui/core'
import CustomTypography from '../../UI/CustomTypography/CustomTypography'
import bannerImage from '../../../assets/images/mosquematch-banner.png'
import { useStyles } from './HeroStyles'

const Slide = ({ children }) => {
    const classes = useStyles()

    return (
        <div className={classes.slide}>
            <CustomTypography
                bold="true"
                variant="h4"
                component="h1"
                align="center"
            >
                {children}
            </CustomTypography>
        </div>
    )
}

const Hero = () => {
    const classes = useStyles()
    const dotsClasses = ['slick-dots', classes.dots]

    const settings = {
        arrows: false,
        dots: true,
        infinite: true,
        speed: 500,
        slidesToShow: 1,
        slidesToScroll: 1,
        className: classes.slides,
        dotsClass: dotsClasses.join(' ')
    }
    
    return (
        <div className={classes.hero}>
            <Container maxWidth="lg">
                <Grid 
                    className={classes.banner}
                    container
                    justify='space-between'
                    alignItems='center'
                >
                    <Grid item lg={7}>
                        <img
                            className={classes.img}
                            src={bannerImage}
                            alt="Mosque Match Banner"
                        />
                    </Grid>
                    <Grid item lg={5}>
                        <Slider {...settings}>
                            <Slide>
                                Match within a certain Mosque or Organization
                            </Slide>
                            <Slide>
                                Common ground instantly is key with the wali
                            </Slide>
                            <Slide>
                                Any organization can have database
                            </Slide>
                            <Slide>
                                Meet people from your local area or mosque
                            </Slide>
                        </Slider>
                    </Grid>
                </Grid>
            </Container>
        </div>
    )
}

export default Hero