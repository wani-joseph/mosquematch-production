import React from 'react'
import {
    Container,
    Grid
} from '@material-ui/core'
import SectionTitle from '../../SectionTitle/SectionTitle'
import SectionSubTitle from '../../SectionSubTitle/SectionSubTitle'
import ExtendedParagraph from '../../Paragraph/ExtendedParagraph'
import Paragraph from '../../Paragraph/Paragraph'
import SvgIcon from '../../svgIcon/svgIcon'
import { useStyles } from './HowItWorksStyles'

const HowItWorks = () => {
    const classes = useStyles()

    return (
        <div className={classes.howItWorks}>
            <Container maxWidth="lg">
                <SectionTitle title="How It Works!" />
                <ExtendedParagraph align="center">
                    Your Perfect Local Match, Completely Free! 
                </ExtendedParagraph>
                <Grid container justify="space-between">
                    <Grid item xs={12} sm={true}>
                        <SectionSubTitle 
                            align="center" 
                            title="Register"
                            classes={{ root: classes.sectionTitle }}
                        />
                        <div className={classes.iconWrap}>
                            <SvgIcon width="25%" height="60" src="register" />
                        </div>
                        <Paragraph align="center">
                            Register your Info
                        </Paragraph>
                    </Grid>
                    <Grid item xs={12} sm={true}>
                        <SectionSubTitle 
                            align="center" 
                            title="Fill your Interests"
                            classes={{ root: classes.sectionTitle }} 
                        />
                        <div className={classes.iconWrap}>
                            <SvgIcon width="25%" height="60" src="interests" />
                        </div>
                        <Paragraph align="center">
                            Tell us what you are looking for
                        </Paragraph>
                    </Grid>
                    <Grid item xs={12} sm={true}>
                        <SectionSubTitle 
                            align="center" 
                            title="Link to a Mosque"
                            classes={{ root: classes.sectionTitle }}
                        />
                        <div className={classes.iconWrap}>
                            <SvgIcon width="25%" height="60" src="mosque" />
                        </div>
                        <Paragraph align="center">
                            Link to a mosque and get approved by the Imam
                        </Paragraph>
                    </Grid>
                    <Grid item xs={12} sm={true}>
                        <SectionSubTitle 
                            align="center" 
                            title="Connect"
                            classes={{ root: classes.sectionTitle }}
                        />
                        <div className={classes.iconWrap}>
                            <SvgIcon width="25%" height="60" src="connect" />
                        </div>
                        <Paragraph align="center">
                            Connect and ﬁnd the right partner
                        </Paragraph>
                    </Grid>
                    <Grid item xs={12} sm={true}>
                        <SectionSubTitle 
                            align="center" 
                            title="Wali"
                            classes={{ root: classes.sectionTitle }}
                        />
                        <div className={classes.iconWrap}>
                            <SvgIcon width="25%" height="60" src="thumb-up" />
                        </div>
                        <Paragraph align="center">
                            Talk to your partner's Wali to get his approval
                        </Paragraph>
                    </Grid>
                </Grid>
            </Container>
        </div>
    )
}

export default HowItWorks