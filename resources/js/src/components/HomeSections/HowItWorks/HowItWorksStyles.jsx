import { makeStyles } from '@material-ui/core/styles';

export const useStyles = makeStyles(theme =>({
    howItWorks: {
        borderTop: '1px solid rgba(0, 0, 0, .05)',
        paddingBottom: theme.spacing(4),
        paddingTop: theme.spacing(4),
        [theme.breakpoints.up('md')]: {
            paddingBottom: theme.spacing(6),
            paddingTop: theme.spacing(6),
        },
    },
    sectionTitle: {
        [theme.breakpoints.up('sm')]: {
            minHeight: 60
        }
    },
    iconWrap: {
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        padding: theme.spacing(1, 0),
        [theme.breakpoints.up('sm')]: {
            marginBottom: theme.spacing(1)
        }
    }
}));