import { makeStyles } from '@material-ui/core/styles'

export const useStyles = makeStyles(theme =>({
    infoWindow: {
        borderRadius: 28,
        boxShadow: theme.shadows[4],
        width: 280,
        height: 320,
        position: 'absolute',
        transform: 'translate(-50%, 0%)',
        bottom: 0,
        '&:before': {
            content: '""',
            height: 0,
            position: 'absolute',
            width: 0,
            border: '10px solid transparent', /* arrow size */
            borderTopColor: '#fff',  /* arrow color */
            position: 'absolute',
            bottom: '-19px',
            left: '50%',
            zIndex: 2,
        },
        '&:after': {
            content: '""',
            height: 0,
            position: 'absolute',
            width: 0,
            border: '1px solid transparent', /* arrow size */
            borderTopColor: '#333',  /* arrow color */
            position: 'absolute',
            bottom: '-24px',
            left: '50%',
            zIndex: 1,
        }
    },
    thumb: {
        width: '100%',
        height: 200,
        overflow: 'hidden',
        borderTopLeftRadius: 18,
        borderTopRightRadius: 18,
        position: 'relative',
    },
    img: {
        width: '100%',
        height: 'auto',
        display: 'block',
        transform: 'translateY(-10%)'
    },
    status: {
        position: 'absolute',
        bottom: theme.spacing(1),
        right: theme.spacing(1),
        textAlign: 'end',
        color: theme.palette.grey[400]
    },
    online: {
        color: '#39CE48'
    },
    info: {
        background: theme.palette.common.white,
        borderBottomLeftRadius: 18,
        borderBottomRightRadius: 18,
        padding: theme.spacing(2),
    }
}))