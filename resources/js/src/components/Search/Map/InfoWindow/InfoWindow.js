import React from 'react'
import { connect } from 'react-redux'
import { Typography } from '@material-ui/core'
import Tagline from '../../../Tagline/Tagline'
import { useStyles } from './InfoWindowStyles'
import { calculateAge, getDistanceFromLatLonInKm } from '../../../../shared/utility'
import FiberManualRecordIcon from '@material-ui/icons/FiberManualRecord'

const InfoWindow = props => {
    const classes = useStyles()
    const { member, user } = props
    const {
        avatar,
        fname,
        lname,
        gender,
        tagline,
        dayOfBirth,
        monthOfBirth,
        yearOfBirth,
        latitude,
        longtitude,
        mosques
    } = member

    let defaultImg = '/images/male-placeholder.jpg'
    let statusClasses = [classes.status]

    if (gender == 'female') defaultImg = '/images/female-placeholder.jpg'
    if (status == "online") statusClasses.push(classes.online)

    return (
        <div className={classes.infoWindow}>
            <div className={classes.thumb}>
                <img
                    className={classes.img}
                    src={avatar ? `/images/${avatar}` : defaultImg}
                    alt={`mosque-match-${fname}-${lname}`}
                />
                <div className={statusClasses.join(' ')}>
                    <FiberManualRecordIcon />
                </div>
            </div>
            <div className={classes.info}>
                <Typography variant="body1" noWrap>{tagline}</Typography>
                <Tagline text={`${fname}, ${calculateAge(new Date(yearOfBirth, monthOfBirth, dayOfBirth))}`} />
                <Typography variant="body2" noWrap>
                    {user.latitude && user.longtitude && latitude && longtitude
                        ? `${user.location}, ${getDistanceFromLatLonInKm(user.latitude, user.longtitude, latitude, longtitude)} km`
                        : 'London, 2km'}
                </Typography>
                <Typography variant="body2" noWrap>
                    {mosques.data != "" ? mosques.data[0].mosqueName : 'London Central Mosque'}
                </Typography>
            </div>
        </div>
    )
}

const mapStateToProps = state => ({
    user: state.auth.authUser
})

export default connect(mapStateToProps)(InfoWindow)