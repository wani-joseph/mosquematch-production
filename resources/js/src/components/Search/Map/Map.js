import React, { Component } from 'react'
import { connect } from 'react-redux'
import GoogleMapReact from 'google-map-react'
import Marker from './Marker/Marker'
import InfoWindow from './InfoWindow/InfoWindow'
import { withStyles } from '@material-ui/core/styles'
import { styles } from './MapStyles'
import { mapOptions } from '../../../shared/mapOptions'

class Map extends Component {
    constructor(props) {
        super(props)
        this.state = {
            center: {
                lat: parseFloat(props.user.latitude), // 71.596878,
                lng: parseFloat(props.user.longtitude) //-59.508342
            },
            zoom: 2,
            selectedMarker: null
        }
        this.handleMapClick = this.handleMapClick.bind(this)
    }

    handleMapClick(obj) {
        this.setState({ selectedMarker: null })
    }

    render() {
        const { center, zoom, selectedMarker } = this.state
        const { classes, members, hoveredCardId } = this.props

        return (
            <div className={classes.mapContainer}>
                <GoogleMapReact
                    onClick={this.handleMapClick}
                    bootstrapURLKeys={{
                        key: 'AIzaSyBQu11pb3jkL4i1sa8w75ASby7V4DBgciM',
                        language: 'en',
                    }}
                    defaultCenter={center}
                    defaultZoom={zoom}
                    options={mapOptions}
                >
                    {members.map(member => (
                        <Marker
                            key={member.identifier}
                            markerId={member.identifier}
                            hoveredCardId={hoveredCardId}
                            selected={selectedMarker ? selectedMarker.identifier: null}
                            gender={member.gender}
                            lat={member.latitude}
                            lng={member.longtitude}
                            onClick={() => {
                                this.setState({ selectedMarker: member })
                            }}
                        />
                    ))}

                    {selectedMarker && (
                        <InfoWindow
                            lat={selectedMarker.latitude}
                            lng={selectedMarker.longtitude}
                            member={selectedMarker}
                        />
                    )}
                </GoogleMapReact>
            </div>
        )
    }
}

const mapStateToProps = state => ({
    user: state.auth.authUser
})

export default connect(mapStateToProps)(withStyles(styles)(Map))