import { makeStyles } from '@material-ui/core/styles'

export const useStyles = makeStyles((theme) =>({
    marker: {
        position: 'absolute',
        transform: 'translate(-25%, -62%)',
        width: 40,
        background: theme.palette.common.white,
        alignItems: 'center',
        borderRadius: 28,
        boxShadow: theme.shadows[2],
        display: 'flex',
        height: 28,
        justifyContent: 'center',
    },
    hovered: {
        background: theme.palette.primary.main,
    },
    selected: {
        visibility: 'hidden',
    }
}))