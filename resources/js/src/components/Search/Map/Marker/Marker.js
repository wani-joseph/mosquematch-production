import React from 'react'
import SvgIcon from '../../../svgIcon/svgIcon'
import { useStyles } from './MarkerStyles'

const Marker = props => {
    const classes = useStyles()
    const {
        onClick,
        markerId,
        hoveredCardId,
        selected,
        gender
    } = props

    let markerClasses = [classes.marker]

    if (hoveredCardId === markerId) markerClasses.push(classes.hovered)
    if (selected === markerId) markerClasses.push(classes.selected)

    return (
        <div className={markerClasses.join(' ')} onClick={onClick}>
            <SvgIcon width="25" height="24" src={`${gender}-marker`} />
        </div>
    )
}

export default Marker