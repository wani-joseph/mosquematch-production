import { makeStyles } from '@material-ui/core/styles'

export const useStyles = makeStyles(theme =>({
    container: {
        padding: 0,
        margin: 0,
        maxWidth: '50%',
        [theme.breakpoints.up('xl')]: {
            maxWidth: '60%',
        }
    },
    gridItem: {
        marginBottom: 0,
    },
    hideFilters: {
        '&:first-of-type': {
            [theme.breakpoints.up('lg')]: {
                display: 'none',
            },
            [theme.breakpoints.up('xl')]: {
                display: 'flex',
            }
        }
    },
    searchMap: {
        position: 'fixed',
        top: theme.spacing(12),
        left: '50%',
        width: '50%',
        height: 'calc(100vh - 96px)',
        [theme.breakpoints.up('xl')]: {
            left: '60%',
            width: '40%',
        }
    },
    searches: {
        listStyle: 'none',
        padding: 0,
        margin: 0,
    },
}))