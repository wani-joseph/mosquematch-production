import React from 'react'
import {
    Checkbox,
    Grid,
    Typography,
    MenuItem,
    FormControlLabel,
    Switch,
} from '@material-ui/core'
import Loader from '../../UI/Loader/Loader'
import MemberCard from '../../Member/MemberCard/MemberCard'
import Pagination from '../../Pagination/Pagination'
import CustomSelect from '../../UI/CustomSeclect/CustomSeclect'
import { useStyles } from './ResultsStyles'
import CustomTypography from '../../UI/CustomTypography/CustomTypography'

const MosqueByLocation = ({ mosque, mosqueMembers }) => {
    const classes = useStyles()
    const [checked, setChecked] = React.useState(false)

    const handleChange = event => {
        setChecked(event.target.checked)
        if (event.target.checked == true)
            mosqueMembers(event.target.value)
    }

    return (
        <Grid container component='li' alignItems="center">
            <Grid item lg={4}>
                <div className={classes.mosqueName}>
                    <Checkbox
                        value={mosque.identifier}
                        checked={checked}
                        onChange={handleChange}
                    />
                    <Typography>
                        {mosque.mosqueName}
                    </Typography>
                </div>
            </Grid>
            <Grid item lg={4}>
                <Typography>
                    {mosque.mosqueImam}
                </Typography>
            </Grid>
            <Grid item lg={4}>
                <Typography>
                    {mosque.city}
                </Typography>
            </Grid>
        </Grid>
    )
}

const SearchResult = props => {
    const classes = useStyles();
    const {
        currentPage,
        totalPages,
        total,
        prev,
        next,
        loading,
        perPage,
        handlePerPage,
        sortBy,
        handleSortBy,
        showMap,
        handleShowMap,
        setCardMarkerHover,
        resetCardMarkerHover,
        search,
        mosques,
        mosqueMembers
    } = props

    let searchResults = <Loader />

    if (!loading) {
        if (props.results.length > 0) {
            searchResults = props.results.map(member => (
                <MemberCard
                    key={member.identifier}
                    member={member}
                    setCardMarkerHover={setCardMarkerHover}
                    resetCardMarkerHover={resetCardMarkerHover}
                />
            ))
        } else if (search != ''){
            searchResults = <Typography color="textSecondary">Loading...</Typography>
        } else {
            searchResults = <Typography color="textSecondary">There is no results...</Typography>
        }
    }

    const mosquesList = mosques.map(mosque => (
        <MosqueByLocation
            key={mosque.identifier}
            mosque={mosque}
            mosqueMembers={mosqueMembers}
        />
    ))

    return (
        <React.Fragment>
            <div className={classes.actions}>
                <div className={classes.meta}>
                    <Typography className={classes.title} variant="h4" component="h1">
                        Search Results
                    </Typography>
                    <Typography className={classes.showMap} variant="h5" component="h2" gutterBottom>
                        Show Map <FormControlLabel className={classes.switch} control={<Switch checked={showMap} onChange={handleShowMap} value={showMap} />} />
                    </Typography>
                    <Typography variant="body2" color="textSecondary">
                        About {total ? total : '...'} Results...
                    </Typography>
                </div>
                <div className={classes.sort}>
                    <CustomSelect
                        classes={{ root: classes.select }}
                        id="where-you-live"
                        name="sortby"
                        value={sortBy}
                        onChange={handleSortBy}
                    >
                        <MenuItem value="status">Sort by: Online</MenuItem>
                        <MenuItem value="age">Sort by: Age</MenuItem>
                        <MenuItem value="asc">Order: ASC</MenuItem>
                        <MenuItem value="desc">Order: DSC</MenuItem>
                    </CustomSelect>
                    <CustomSelect
                        id="where-you-live"
                        name="sortby"
                        value={perPage}
                        onChange={handlePerPage}
                    >
                        <MenuItem value="10">Per Page: 10</MenuItem>
                        <MenuItem value="15">Per Page: 15</MenuItem>
                        <MenuItem value="20">Per Page: 20</MenuItem>
                    </CustomSelect>
                </div>
            </div>
            {mosques.length > 0 && (
                <>
                    <CustomTypography
                        bold="true"
                        variant="h5"
                        component="p"
                        gutterBottom
                    >
                        Check to view members belong to each Mosque
                    </CustomTypography>
                    <ul className={classes.mosques}>
                        {mosquesList}
                    </ul>
                </>
            )}
            {searchResults}
            {props.results != ''
                ? (
                    <Pagination
                        customDividor="true"
                        currentPage={currentPage}
                        totalPages={totalPages}
                        prev={prev}
                        next={next}
                    />
                ) : null}
        </React.Fragment>
    )
}

export default SearchResult