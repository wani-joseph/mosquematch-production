import { makeStyles } from '@material-ui/core/styles'

export const useStyles = makeStyles(theme =>({
    actions: {
        background: theme.palette.common.white,
        padding: theme.spacing(2, 2, 0, 2),
        border: '1px solid rgba(0, 0, 0, .08)',
        borderBottom: 'none',
        [theme.breakpoints.up('sm')]: {
            display: 'flex',
            justifyContent: 'space-between',
            padding: theme.spacing(2),
        },
        [theme.breakpoints.up('md')]: {
            border: 'none',
            marginBottom: theme.spacing(3),
            padding: 0,
        }
    },
    title: {
        fontWeight: '500',
    },
    showMap: {
        display: 'none',
        [theme.breakpoints.up('lg')]: {
            fontWeight: '500',
            display: 'flex'
        }
    },
    sort: {
        display: 'flex',
        justifyContent: 'space-between',
        marginTop: theme.spacing(2),
        [theme.breakpoints.up('sm')]: {
            display: 'block',
            maxWidth: 120,
            marginTop: 0,
        },
        [theme.breakpoints.up('md')]: {
            maxWidth: 140,
        },
        [theme.breakpoints.up('lg')]: {
            maxWidth: 160,
        }
    },
    select: {
        marginRight: theme.spacing(2),
        marginBottom: theme.spacing(0),
        '&:last-of-type': {
            marginLeft: 0,
        },
        [theme.breakpoints.up('sm')]: {
            marginBottom: theme.spacing(1),
        }
    },
    switch: {
        marginLeft: 0,
    },
    mosques: {
        listStyle: 'none',
        padding: 0,
        margin: 0,
    },
    mosqueName: {
        display: 'flex',
        alignItems: 'center'
    }
}));