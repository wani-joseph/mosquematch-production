import React from 'react'
import {
    Container,
    Grid
} from '@material-ui/core'
import GridItem from '../UI/GridItem/GridItem'
import CustomModal from '../UI/CustomModal/CustomModal'
import SearchFilters from './Filters/Filters'
import SearchResult from './Results/Results'
import SearchMap from './Map/Map'
import SavedSearch from '../SavedSearch/SavedSearch'
import { useStyles } from './SearchStyles'

const Search = props => {
    const classes = useStyles()
    const {
        handleFilteration,
        saveSearchHandler,
        searches,
        viewSearches,
        searchViewed,
        viewSearchHandler,
        runSearchHandler,
        deleteSearchHandler,
        loadMembers,
        loading,
        currentPage,
        totalPages,
        total,
        results,
        perPage,
        handlePerPage,
        sortBy,
        handleSortBy,
        prev,
        next,
        showMap,
        handleShowMap,
        hoveredCardId,
        setCardMarkerHover,
        resetCardMarkerHover,
        search,
        handleSearch,
        getMosques,
        mosques,
        mosqueMembers,
        handleAllChecked,
        handleCheckbox,
        readingQuran,
        praying,
        options,
        religiousness,
        sect,
        revert,
        hijabNiqab,
        halal,
        ethnicity,
        bodyType,
        income,
        maritialStatus,
        smoking,
        drinking,
        phoneTime,
        distance
    } = props

    let containerClasses = []
    let gridItemClasses = [classes.gridItem]
    let mapClasses = [classes.searchMap]

    if (showMap) {
        containerClasses.push(classes.container)
        gridItemClasses.push(classes.hideFilters)
        mapClasses.push(classes.hideMap)
    } else {
        containerClasses.pop()
        gridItemClasses.pop()
        mapClasses.pop()
    }

    let savedSearches = <p>Your seved searches will apear here...</p>

    if (searches.length > 0) {
        savedSearches = searches.map(savedSearch => (
            <SavedSearch
                key={savedSearch.identifier}
                savedSearch={savedSearch}
                runSearchHandler={runSearchHandler}
                deleteHandler={deleteSearchHandler}
            />
        ))
    } else {
        savedSearches = <p>You didn't save any search yet.</p>
    }
        
    return (
        <React.Fragment>
            <CustomModal
                show={viewSearches}
                handleClose={searchViewed}
                title={'Saved Searches'}
                content={
                    <ul className={classes.searches}>
                        {savedSearches}
                    </ul>
                }
            />
            <Container className={containerClasses.join(' ')} maxWidth={!showMap ? 'lg' : false}>
                <Grid container>
                    <GridItem
                        xs={12} md={3} lg={showMap ? 12 : 3} xl={3}
                        noborder="true"
                        padding="true"
                        classes={{ root: gridItemClasses.join(' ') }}
                    >
                        <SearchFilters
                            showMap={showMap}
                            handleFilteration={handleFilteration}
                            saveSearchHandler={saveSearchHandler}
                            viewSearchHandler={viewSearchHandler}
                            loadMembers={loadMembers}
                            search={search}
                            handleSearch={handleSearch}
                            getMosques={getMosques}
                            readingQuran={readingQuran}
                            praying={praying}
                            handleAllChecked={handleAllChecked}
                            handleCheckbox={handleCheckbox}
                            options={options}
                            religiousness={religiousness}
                            sect={sect}
                            revert={revert}
                            hijabNiqab={hijabNiqab}
                            halal={halal}
                            ethnicity={ethnicity}
                            bodyType={bodyType}
                            income={income}
                            maritialStatus={maritialStatus}
                            smoking={smoking}
                            drinking={drinking}
                            phoneTime={phoneTime}
                            distance={distance}
                        />
                    </GridItem>
                    <GridItem
                        xs={12} md={9} lg={showMap ? 12 : 9} xl={9}
                        noborder="true" padding="true"
                        classes={{ root: gridItemClasses.join(' ') }}
                    >
                        <SearchResult
                            loading={loading}
                            currentPage={currentPage}
                            totalPages={totalPages}
                            total={total}
                            results={results}
                            perPage={perPage}
                            handlePerPage={handlePerPage}
                            sortBy={sortBy}
                            handleSortBy={handleSortBy}
                            prev={prev}
                            next={next}
                            showMap={showMap}
                            handleShowMap={handleShowMap}
                            setCardMarkerHover={setCardMarkerHover}
                            resetCardMarkerHover={resetCardMarkerHover}
                            search={search}
                            mosques={mosques}
                            mosqueMembers={mosqueMembers}
                        />
                    </GridItem>
                </Grid>
            </Container>
            <div className={mapClasses.join(' ')}>
                {showMap ? (
                    <SearchMap
                        members={results}
                        hoveredCardId={hoveredCardId}
                    />
                ) : null}
            </div>
        </React.Fragment>
    )
}

export default Search