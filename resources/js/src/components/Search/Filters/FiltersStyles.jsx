export const styles = theme => ({
	meta: {
		display: 'none',
		[theme.breakpoints.up('md')]: {
			display: 'flex',
			justifyContent: 'space-between',
			alignItems: 'center',
			marginBottom: theme.spacing(2)
		}
	},
	viewSearch: {
		color: theme.palette.grey[700]
	},
	actions: {
		display: 'flex',
		justifyContent: 'space-between',
		alignItems: 'center',
		marginTop: theme.spacing(2),
		borderBottom: '1px solid rgba(0, 0, 0, .2)',
		[theme.breakpoints.up('md')]: {
			paddingBottom: theme.spacing(2),
		}
	},
	filter: {
		border: 'none',
		outline: 'none',
		padding: theme.spacing(2),
		background: theme.palette.primary.main,
		color: 'white',
		borderRadius: 3,
		textTransform: 'capitalize',
		cursor: 'pointer',
	},
	saveSearch: {
		border: 'none',
		outline: 'none',
		padding: theme.spacing(2),
		background: theme.palette.grey[700],
		color: 'white',
		borderRadius: 3,
		textTransform: 'capitalize',
		cursor: 'pointer',
	},
	reset: {
		border: 'none',
		outline: 'none',
		padding: 0,
		textTransform: 'capitalize',
		background: 'transparent',
		cursor: 'pointer',
		fontWeight: 500,
		color: theme.palette.grey[700],
	},
	inputControl: {
		borderBottom: '1px solid rgba(0, 0, 0, .2)',
		padding: theme.spacing(2, 0)
	},
	inputLabel: {
		fontSize: 15,
		color: theme.palette.common.black,
		display: 'block',
		marginBottom: theme.spacing(1),
		textTransform: 'uppercase',
		fontWeight: 500,
	},
	inputTextField: {
		outline: 'none',
		border: '1px solid rgba(0, 0, 0, .08)',
		borderRadius: 4,
		padding: theme.spacing(1, 2),
		width: '100%',
		font: 'inherit'
	},
	bottomSpacing: {
		marginBottom: theme.spacing(2),
	},
	filtersTitle: {
		fontSize: 15,
		color: theme.palette.common.black,
		display: 'block',
		marginTop: theme.spacing(4),
		marginBottom: theme.spacing(2),
		textTransform: 'uppercase',
		fontWeight: 500,
	},
	radioGroup: {
		marginBottom: theme.spacing(2),
	},
	invalid: {
		color: theme.palette.primary.main,
	},
	invalidBorder: {
		border: `1px solid ${theme.palette.primary.main}`
	},
	selectAll: {
		textTransform: 'capitalize'
	},
	root: {
		color: theme.palette.primary.main,
		height: 6,
	},
	thumb: {
		height: 18,
		width: 18,
		backgroundColor: '#fff',
		border: '1px solid rgba(0, 0, 0, .08)',
		marginTop: -6,
		marginLeft: -9,
		'&:focus,&:hover,&$active': {
			boxShadow: 'inherit',
		},
	},
	active: {},
		valueLabel: {
		left: 'calc(-50% + 3px)',
	},
	track: {
		height: 6,
		borderRadius: 4,
	},
	rail: {
		height: 6,
		borderRadius: 4,
		background: 'grey'
	},
})