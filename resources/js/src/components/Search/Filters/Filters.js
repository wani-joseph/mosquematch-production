import React from 'react'
import Filter from '../../Filter/Filter'
import CustomCheckbox from '../../UI/CustomCheckbox/CustomCheckbox'
import CustomTypography from '../../UI/CustomTypography/CustomTypography'
import {
	Slider,
	withStyles
} from '@material-ui/core'
import { styles } from './FiltersStyles'

const Filters = ({
	search,
	handleSearch,
	getMosques,
	loadMembers,
	options,
	readingQuran,
	praying,
	religiousness,
	sect,
	revert,
	hijabNiqab,
	halal,
	ethnicity,
	bodyType,
	income,
	maritialStatus,
	smoking,
	drinking,
	phoneTime,
	distance,
	handleAllChecked,
	handleCheckbox,
	classes
}) => {

	const renderCheckboxs = (summary, fullWidth) => {
		let filters = null

		if (summary == 'options') filters = options
		if (summary == 'ethnicity') filters = ethnicity
		if (summary == 'body type') filters = bodyType
		if (summary == 'my income') filters = income
		if (summary == 'maritial status') filters = maritialStatus
		if (summary == 'smoking') filters = smoking
		if (summary == 'drinking') filters = drinking
		if (summary == 'time on phone daily') filters = phoneTime
		if (summary == 'religiousness') filters = religiousness
		if (summary == 'sect') filters = sect
		if (summary == 'revert') filters = revert
		if (summary == 'hijab / niqab') filters = hijabNiqab
		if (summary == 'halal') filters = halal
		if (summary == 'praying') filters = praying
		if (summary == 'reading quran') filters = readingQuran
		
		 
		return (
			<>
				<div className={classes.selectAll}>
					<CustomCheckbox
						value={`Select All ${summary}`}
						fullWidth={'true'}
						onChange={(event) => handleAllChecked(event, summary)}
					/>
				</div>
				{filters.map(filter => (
					<CustomCheckbox
						key={filter.id}
						checked={filter.isChecked}
						value={filter.value}
						fullWidth={fullWidth}
						onChange={(event) => handleCheckbox(event, summary)}
					/>
				))}
			</>
		)
	}

	let locationClasses = [classes.inputTextField, classes.bottomSpacing]
	if (search === '') locationClasses.push(classes.invalidBorder)

	return (
		<>
			<div className={classes.meta}>
				<CustomTypography
					bold="true"
					variant="h4"
					component="h1"
				>
					Fitlers
				</CustomTypography>
				<button
					className={classes.reset}
					type="button"
					onClick={() => console.log('this not working')}
				>
					view saved search
				</button>
			</div>
			<div className={classes.inputControl}>
				<label
					className={classes.inputLabel}
					htmlFor={"location"}
				>
					Search by Location
				</label>
				<input
					className={locationClasses.join(' ')}
					type="text"
					autoComplete="off" 
					onKeyUp={handleSearch}
					id="search"
					placeholder="City, post code, region, area, etc"
				/>
					{search !== '' ? (
						<>
							<label
								className={classes.inputLabel}
								htmlFor={"distance"}
							>
								Distance
							</label>
							<CustomTypography bold="true" variant="body2" color="textSecondary">
								{`0 - ${distance} ${distance != 1000 ? '+': ''} km`}
							</CustomTypography>
							<Slider
								classes={{
									root: classes.root,
									thumb: classes.thumb,
									active: classes.active,
									track: classes.track,
									rail: classes.rail
								}}
								value={distance}
								max={1000}
								onChange={() => console.log('This is working')}
								aria-labelledby={'distance-range'}
								step={10}
							/>
						</>
					): (
						<CustomTypography
							classes={{ root: classes.invalid }}
							variant="body2"
						>
							Location is required
						</CustomTypography>
					)}
				</div>
			<div className={classes.actions}>
				<button
					className={classes.filter}
					type="submit"
				>
					search
				</button>
				<button
					className={classes.saveSearch}
					type="button"
					onClick={() => console.log('This is save handler')}
				>
					save search
				</button>
				<button
					className={classes.reset}
					type="reset"
					onClick={() => loadMembers('first loading')}
				>
					reset all
				</button>
			</div>
			<CustomTypography
				classes={{ root: classes.filtersTitle }}
				bold="true"
			>
				Options
			</CustomTypography>
			{renderCheckboxs('options', 'false')}
			<div className={classes.inputControl}>
				<CustomTypography
					classes={{ root: classes.filtersTitle }}
					bold="true"
				>
					Personal information
				</CustomTypography>
				<Filter summary='ethnicity'>
					{renderCheckboxs('ethnicity', 'true')}
				</Filter>
				<Filter summary='body type'>
					{renderCheckboxs('body type', 'false')}
				</Filter>
				<Filter summary='my income'>
					{renderCheckboxs('my income', 'true')}
				</Filter>
				<Filter summary='maritial status'>
					{renderCheckboxs('maritial status', 'true')}
				</Filter>
				<Filter summary='smoking'>
					{renderCheckboxs('smoking', 'false')}
				</Filter>
				<Filter summary='drinking'>
					{renderCheckboxs('drinking', 'false')}
				</Filter>
				<Filter summary='time on phone daily'>
					{renderCheckboxs('time on phone daily', 'true')}
				</Filter>
			</div>
			<div className={classes.inputControl}>
				<CustomTypography
					classes={{ root: classes.filtersTitle }}
					bold="true"
				>
					Religion
				</CustomTypography>
				<Filter summary='religiousness'>
					{renderCheckboxs('religiousness', 'true')}
				</Filter>
				<Filter summary='sect'>
					{renderCheckboxs('sect', 'false')}
				</Filter>
				<Filter summary='revert'>
					{renderCheckboxs('revert', 'false')}
				</Filter>
				<Filter summary='hijab / niqab'>
					{renderCheckboxs('hijab / niqab', 'false')}
				</Filter>
				<Filter summary='halal'>
					{renderCheckboxs('halal', 'true')}
				</Filter>
				<Filter summary='praying'>
					{renderCheckboxs('praying', 'false')}
				</Filter>
				<Filter summary='reading quran'>
					{renderCheckboxs('reading quran', 'false')}
				</Filter>
			</div>
			<div className={classes.actions}>
				<button
					className={classes.filter}
					type="submit"
				>
					search
				</button>
				<button
					className={classes.saveSearch}
					type="button"
					onClick={() => console.log('This is save handler')}
				>
					save search
				</button>
				<button
					className={classes.reset}
					type="reset"
					onClick={() => loadMembers('first loading')}
				>
					reset all
				</button>
			</div>
		</>
	)
}

export default withStyles(styles)(Filters)