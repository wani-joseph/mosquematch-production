import React from 'react'
import { connect } from 'react-redux'
import Form from '../UI/CustomForm/CustomForm'
import {
    BasicInfoValidation,
    AboutMeValidation,
    EducationValidation,
    PersonalInfoValidation,
    religionValidation,
    waliDetailsValidation
} from '../../shared/validation'
import { cities } from '../../shared/cities'
import { countries } from '../../shared/countries'
import {
    days,
    months,
    years,
    languages,
    profession,
    jobTitles,
    educationLevels,
    income,
    marriageWithin,
    martialStatus,
    likeChildren,
    arrangements,
    height,
    build,
    smoke,
    drink,
    disabilities,
    phonetime,
    religious,
    sector,
    hijab,
    keepHalal,
    prayerQuraan,
    yesNo
} from '../../shared/selectOptions'

const ProfileForm = ({ page, user, submitButton, handleSubmit }) => {

    let fields = []
    let validation = null

    if (page == "Basic Information") {
        fields = [
            {label: 'First Name', type: 'text', name: 'fname', value: user ? user.fname : ''},
            {label: 'Last Name', type: 'text', name: 'lname', value: user ? user.lname : ''},
        ]
        validation = BasicInfoValidation
    }

    if (page == "About Me") {
        fields = [
            {label: 'Where you live', type: 'select', data: cities, name: 'location', value: user ? user.location : ''},
            {label: 'Country of birth', type: 'select', data: countries, name: 'countryBirth', value: user ? user.countryBirth : ''},
            {label: 'Date of birth', type: 'select', data: days, name: 'dayOfBirth', value: user ? user.dayOfBirth : ''},
            {label: 'Month of birth', type: 'select', data: months, name: 'monthOfBirth', value: user ? user.monthOfBirth : ''},
            {label: 'Year of birth', type: 'select', data: years, name: 'yearOfBirth', value: user ? user.yearOfBirth : ''},
            {label: 'Tagline', type: 'text', name: 'tagline', value: user ? user.tagline : ''},
            {label: 'A little about you', type: 'textarea', rows: '4', name: 'bio', value: user ? user.bio : ''},
            {label: "What I'm looking for", type: 'textarea', rows: '4', name: 'seeking', value: user ? user.seeking : ''},
        ]
        validation = AboutMeValidation
    }

    if (page == "Education and Work") {
        fields = [
            {label: 'My education level', type: 'select', data: educationLevels, name: 'eduLevel', value: user ? user.eduLevel : ''},
            {label: 'Subject I studied', type: 'text', name: 'subject', value: user ? user.subject : ''},
            {label: 'My profession', type: 'select', data: profession, name: 'profession', value: user ? user.profession : ''},
            {label: 'My job title', type: 'select', data: jobTitles, name: 'jobTitle', value: user ? user.jobTitle : ''},
            {label: 'My first language', type: 'select', data: languages, name: 'fLang', value: user ? user.fLang : ''},
            {label: 'My second language', type: 'select', data: languages, name: 'sLang', value: user ? user.sLang : ''},
        ]
        validation = EducationValidation
    }

    if (page == "Personal Information") {
        fields = [
            {label: 'My citizenship', type: 'select', data: countries, name: 'citizenship', value: user ? user.citizenship : ''},
            {label: 'Country of Origin', type: 'select', data: countries, name: 'countryOrigin', value: user ? user.countryOrigin : ''},
            {label: 'Willing to relocate', type: 'select', data: countries, name: 'relocate', value: user ? user.relocate : ''},
            {label: 'My income', type: 'select', data: income, name: 'income', value: user ? user.income : ''},
            {label: "I'm looking to marriage within", type: 'select', data: marriageWithin, name: 'marriageWithin', value: user ? user.marriageWithin : ''},
            {label: 'Martial status', type: 'select', data: martialStatus, name: 'martialStatus', value: user ? user.martialStatus : ''},
            {label: 'Would I like to have children', type: 'select', data: likeChildren, name: 'likeChildren', value: user ? user.likeChildren : ''},
            {label: 'Do I have children?', type: 'select', data: yesNo, name: 'children', value: user ? user.children : ''},
            {label: 'My living arrangements', type: 'select', data: arrangements, name: 'arrangements', value: user ? user.arrangements : ''},
            {label: 'My height', type: 'select', data: height, name: 'height', value: user ? user.height : ''},
            {label: "My build", type: 'select', data: build, name: 'build', value: user ? user.build : ''},
            {label: 'Do I smoke?', type: 'select', data: smoke, name: 'smoke', value: user ? user.smoke : ''},
            {label: 'Do I drink?', type: 'select', data: drink, name: 'drink', value: user ? user.drink : ''},
            {label: "Do I have any disabilities?", type: 'select', data: disabilities, name: 'disabilities', value: user ? user.disabilities : ''},
            {label: 'How long do you spend on your phone daily?', type: 'select', data: phonetime, name: 'phonetime', value: user ? user.phonetime : ''}
        ]
        validation = PersonalInfoValidation
    }

    if (page == "Religion") {
        fields = [
            {label: 'Religiousness', type: 'select', data: religious, name: 'religious', value: user ? user.religious : ''},
            {label: 'My sector', type: 'select', data: sector, name: 'sector', value: user ? user.sector : ''},
            {label: 'Do you prefer Hijab / Niqab?', type: 'select', data: hijab, name: 'hijab', value: user ? user.hijab : ''},
            {label: 'Do you prefer a beard?', type: 'select', data: yesNo, name: 'beard', value: user ? user.beard : ''},
            {label: "Are you a revert?", type: 'select', data: yesNo, name: 'revert', value: user ? user.revert : ''},
            {label: 'Do you keep halal?', type: 'select', data: keepHalal, name: 'keepHalal', value: user ? user.keepHalal : ''},
            {label: 'Do you pray?', type: 'select', data: prayerQuraan, name: 'prayer', value: user ? user.prayer : ''},
            {label: 'How often do you read Quran?', type: 'select', data: prayerQuraan, name: 'readQuran', value: user ? user.readQuran : ''},
        ]
        validation = religionValidation
    }

    if (page == "Wali Details") {
        fields = [
            {label: 'Wali Name', type: 'text', name: 'waliName', value: user ? user.waliName : ''},
            {label: 'Wali Contact Number', type: 'text', name: 'waliContact', value: user ? user.waliContact : ''},
            {label: 'Wali Email', type: 'email', name: 'waliEmail', value: user ? user.waliEmail : ''},
        ]
        validation = waliDetailsValidation
    }

    return (
        <Form
            fields={fields}
            // validation={validation}
            submitHandler={handleSubmit}
            submitButton={submitButton}
        />
    )
}

const mapStateToProps = state => ({
    user: state.auth.authUser
})

export default connect(mapStateToProps)(ProfileForm)