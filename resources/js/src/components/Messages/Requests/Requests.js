import React, { Component } from 'react'
import { connect } from 'react-redux'
import InfiniteScroll from '../../../services/InfiniteScroll'
import WaliRequest from '../../WaliRequest/WaliRequest'
import Loader from '../../UI/Loader/Loader'
import { Typography } from '@material-ui/core'
import {
    confirmWaliRequest,
    denyWaliRequest
} from '../../../store/actions/auth'

class Requests extends Component {
    constructor() {
        super()
        this.handleConfirmation = this.handleConfirmation.bind(this)
        this.handleDenial = this.handleDenial.bind(this)
    }

    handleConfirmation(request) {
        const { confirmWaliRequest } = this.props

        confirmWaliRequest(request)
    }

    handleDenial(request) {
        const { denyWaliRequest } = this.props

        denyWaliRequest(request)
    }

    render() {
        const { confirmWaliRequest, denyWaliRequest } = this.props

        return (
            <InfiniteScroll resource='wali-requesters'>
                {resource => {
                    const { updateResource, filterResource } = resource

                    let memberWaliRequests = <Loader />

                    if (!resource.loading) {

                        if (resource.list.length > 0) {
                            memberWaliRequests = resource.list.map(waliRequest => (
                                <WaliRequest
                                    key={waliRequest.identifier}
                                    confirm={confirmWaliRequest}
                                    deny={denyWaliRequest}
                                    update={updateResource}
                                    filter={filterResource}
                                    waliRequest={waliRequest}
                                />
                            ))
                        } else {
                            memberWaliRequests = (
                                <Typography variant="body2">
                                    You don't have requests yet...
                                </Typography>
                            )
                        }
                    }

                    return <>{memberWaliRequests}</>
                }}
            </InfiniteScroll>
        )
    }
}

const mapDispatchToProps = dispatch => ({
    confirmWaliRequest: (waliRequestId, update) => dispatch(confirmWaliRequest(waliRequestId, update)),
    denyWaliRequest: (waliRequestId, filter) => dispatch(denyWaliRequest(waliRequestId, filter))
})

export default connect(null, mapDispatchToProps)(Requests)