import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Typography } from '@material-ui/core'
import Template from '../../Template/Template'
import Loader from '../../UI/Loader/Loader'
import CustomButton from '../../UI/CustomButton/CustomButton'
import CustomDividor from '../../UI/CustomDividor/CustomDividor'
import CustomTextField from '../../UI/CustomTextField/CustomTextField'
import { withStyles } from '@material-ui/core/styles'
import { styles } from './TemplatesStyles'
import * as actions from '../../../store/actions'

class Templates extends Component {
    constructor() {
        super()
        this.handleNewTemplate = this.handleSubmit.bind(this)
        this.handleDeleteTemplate = this.handleDeleteTemplate.bind(this)
    }

    handleSubmit() {
        const { user, addNewTemplate } = this.props
        const template = document.querySelector('#template').value

        addNewTemplate(user.identifier, template, () => {
            document.querySelector('#template').value = ""
        })
    }

    handleDeleteTemplate(template) {
        const { user, deleteTemplate } = this.props
        deleteTemplate(user.identifier, template)
    }

    render() {
        const { templates, classes } = this.props

        let memberTempaltes = <Loader />

        if (templates.length > 0) {
            memberTempaltes = templates.map(template => (
                <Template
                    key={template.identifier}
                    handleDeleteTemplate={this.handleDeleteTemplate}
                    userTemplate={template}
                />
            ))
        } else {
            memberTempaltes = <p>You don't have templates yet.</p>
        }

        return (
            <React.Fragment>
                <Typography className={classes.title} variant="h5" component="h2">
                    Templates
                </Typography>
                <ul className={classes.templatesList}>
                    {memberTempaltes}
                    <CustomDividor />
                </ul>
                <Typography className={classes.title} variant="h5" component="h2">
                    Template
                </Typography>
                <div className={classes.templateForm}>
                    <CustomTextField
                        id="template"
                        name="template"
                        placeholder="Type your template here..."
                        fullWidth
                        multiline
                        rows="3"
                    />
                    <div className={classes.submitbutton}>
                        <CustomButton
                            buttontype="secondary"
                            onClick={this.handleNewTemplate}
                            
                        >
                            Save as Template
                        </CustomButton>
                    </div>
                </div>
            </React.Fragment>
        )
    }
}

const mapStateToProps = state => ({
    user: state.auth.authUser,
    templates: state.auth.templates
})

const mapDispatchToProps = dispatch => ({
    addNewTemplate: (userId, template, next) => dispatch(actions.addNewTemplate(userId, template, next)),
    deleteTemplate: (userId, template) => dispatch(actions.deleteTemplate(userId, template))
})

export default connect(mapStateToProps, mapDispatchToProps)(withStyles(styles)(Templates))