export const styles = theme => ({
    title: {
        fontWeight: 500,
        padding: theme.spacing(2)
    },
    templatesList: {
        padding: theme.spacing(0, 2),
        margin: theme.spacing(2, 0),
        listStyle: 'none'
    },
    templateForm: {
        padding: theme.spacing(2),
    },
    submitbutton: {
        textAlign: 'end'
    }
})