import React from 'react'
import Chat from './Chat/Chat'
import Inbox from './Inbox/Inbox'
import Sent from './Sent/Sent'
import Requests from './Requests/Requests'
import Templates from './Templates/Templates'

const Messages = props => {
    const { page, contact } = props

    switch(page) {
        case "Inbox": return <Inbox />
        case "Sent": return <Sent />
        case "Chat": return <Chat page={page} contact={contact} />
        case "Requests": return <Requests />
        default: return <Templates />
    }
}

export default Messages