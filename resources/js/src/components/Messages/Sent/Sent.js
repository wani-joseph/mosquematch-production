import React from 'react'
import InfiniteScroll from '../../../services/InfiniteScroll'
import Message from '../Message/Message'
import Loader from '../../UI/Loader/Loader'

const Sent = () => {
    return (
        <InfiniteScroll resource="outbox">
            {resource => {
                let sentMessages = <Loader />

                if (!resource.loading) {

                    if (resource.list.length > 0) {
                        sentMessages = resource.list.map(message => (
                            <Message
                                key={message.identifier}
                                message={message}
                            />
                        ))
                    } else {
                        sentMessages = <p>You didn't sent any messages yet</p>
                    }
                }

                return <>{sentMessages}</>
            }}
        </InfiniteScroll>
    )
}

export default Sent