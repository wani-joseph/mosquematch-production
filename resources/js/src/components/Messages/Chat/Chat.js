import React, { Component } from 'react'
import { connect } from 'react-redux'
import ContactDetails from './ContactDetails/ContactDetails'
import Conversation from './Conversation/Conversation'
import MessageComposer from './MessageComposer/MessageComposer'
import Loader from '../../UI/Loader/Loader'
import { withStyles } from '@material-ui/core/styles'
import { styles } from './ChatStyles'
import * as actions from '../../../store/actions'
import { confirmInboxCount } from '../../../store/actions/auth'

class Chat extends Component {
    constructor() {
        super()
        this.handleIncomingMessage = this.handleIncomingMessage.bind(this)
        this.handleSubmit = this.handleSubmit.bind(this)
        this.scrollToBottom = this.scrollToBottom.bind(this)
    }

    componentDidMount() {
        const {
            user,
            contact,
            inboxCount,
            confirmInboxCount,
            loadConversation,
            loadContactDetails
        } = this.props
        const newMessage = Echo.private(`messages.${user.identifier}`)

        newMessage.listen('NewMessage', (e) => {
            this.handleIncomingMessage(e.message)
        })

        confirmInboxCount(inboxCount, user.identifier, contact)
        loadContactDetails(contact)
        loadConversation(user.identifier, contact)
    }

    componentDidUpdate() {
        this.scrollToBottom();
    }

    handleIncomingMessage(message) {
        const { incomimgMessage, markMessageAsRead } = this.props
        const newMessage = {
            identifier: message.id,
            sender: message.sender_id,
            receiver: message.receiver_id,
            content: message.message,
            isRead: message.is_read,
            creationDate: message.created_at,
            modifiedDate: message.updated_at
        }

        incomimgMessage(newMessage)
        markMessageAsRead(message.id)
    }

    scrollToBottom() {
        setTimeout(() => {
            this.messagesEnd.scrollTop = this.messagesEnd.scrollHeight - this.messagesEnd.clientHeight;
        }, 50)
    }

    handleSubmit() {
        const message = document.querySelector('#message').value
        const { user, contact, handleNewMessage } = this.props
        const data = {
            receiver_id: parseInt(contact),
            message: message
        }

        handleNewMessage(user.identifier, data, () => {
            document.querySelector('#message').value = ""
        })
    }

    render() {
        const {
            user,
            contact,
            loading,
            messages,
            loadingContact,
            contactDetails,
            markMessageAsRead,
            classes
        } = this.props

        let messagesFeed = <Loader />

        if (!loading) {
            if (messages.length > 0) {
                messagesFeed = messages.map(message => {
                    if (user.identifier == message.receiver) {
                        markMessageAsRead(message.identifier)
                    }

                    return (
                        <Conversation
                            key={message.identifier}
                            userId={user.identifier}
                            {...message}
                        />
                    )
                })
            }
        }

        return (
            <>
                <ContactDetails
                    loading={loadingContact}
                    contactDetails={contactDetails}
                />
                <ul className={classes.messagesFeed} ref={(el) => { this.messagesEnd = el }}>
                    {messagesFeed}
                </ul>
                <MessageComposer
                    userId={user.identifier}
                    contact={contact}
                    handleSubmit={this.handleSubmit}
                />
            </>
        )
    }
}

const mapStateToProps = state => ({
    user: state.auth.authUser,
    inboxCount: state.auth.inboxCount,
    loading: state.member.loading,
    messages: state.member.conversation,
    loadingContact: state.contact.loading,
    contactDetails: state.contact.contact
})

const mapDispatchToProps = dispatch => ({
    loadConversation: (userId, contact) => dispatch(actions.getMemberConversation(userId, contact)),
    loadContactDetails: (contact) => dispatch(actions.getContactDetails(contact)),
    handleNewMessage: (userId, data, next) => dispatch(actions.newConversationMessage(userId, data, next)),
    incomimgMessage: (message) => dispatch(actions.incomimgConversationMessage(message)),
    markMessageAsRead: (message) => dispatch(actions.markMessageAsRead(message)),
    confirmInboxCount: (inboxCount, userId, contact) => dispatch(confirmInboxCount(inboxCount, userId, contact))
})

export default connect(mapStateToProps, mapDispatchToProps)(withStyles(styles)(Chat))