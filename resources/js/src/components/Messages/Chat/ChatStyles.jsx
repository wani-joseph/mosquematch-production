export const styles = theme => ({
    messagesFeed: {
        listStyle: 'none',
        margin: 0,
        padding: theme.spacing(2),
        height: 300,
        overflowY: 'auto',
        [theme.breakpoints.up('sm')]: {
            height: 400,
        },
        [theme.breakpoints.up('md')]: {
            height: 500,
        },
    }
})