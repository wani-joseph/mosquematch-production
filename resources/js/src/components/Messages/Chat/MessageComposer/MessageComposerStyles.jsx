export const styles = theme =>({
    form: {
        background: theme.palette.grey[400],
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'space-between',
        alignItems: 'start',
        padding: theme.spacing(2),
        [theme.breakpoints.up('sm')]: {
            flexDirection: 'row',
        }
    },
    message: {
        background: theme.palette.common.white,
        [theme.breakpoints.up('sm')]: {
            marginBottom: 0,
        }
    },
    submitButton: {
        textAlign: 'end',
        width: '100%',
        [theme.breakpoints.up('sm')]: {
            marginLeft: theme.spacing(2),
            width: 'auto',
        }
    }
})