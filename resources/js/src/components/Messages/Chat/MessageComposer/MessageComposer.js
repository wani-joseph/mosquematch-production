import React from 'react'
import { withStyles } from '@material-ui/core'
import CustomTextField from '../../../UI/CustomTextField/CustomTextField'
import CustomButton from '../../../UI/CustomButton/CustomButton'
import { styles } from './MessageComposerStyles'

const MessageComposer = ({ handleSubmit, classes }) => (
	<div className={classes.form}>
		<CustomTextField
			id="message"
			name="message"
			placeholder="Type your message here..."
			fullWidth
			multiline
			rows="3"
			classes={{ root: classes.message }}
		/>
		<div className={classes.submitButton}>
			<CustomButton
				buttontype="primary"
				onClick={handleSubmit}
			>
				Send
			</CustomButton>
		</div>
	</div>
)

export default withStyles(styles)(MessageComposer)