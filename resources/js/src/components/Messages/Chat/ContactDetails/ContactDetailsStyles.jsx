import { makeStyles } from '@material-ui/core/styles'

export const useStyles = makeStyles(theme => ({
    contactDetails: {
        alignItems: 'center',
        borderBottom: '1px solid rgba(0, 0, 0, .08)',
        display: 'flex',
        padding: theme.spacing(2),
    },
    thumb: {
        width: 44,
        height: 44,
        borderRadius: '50%',
        marginRight: theme.spacing(2),
        overflow: 'hidden',
    },
    info: {
        flex: 1,
    },
    bold: {
        fontWeight: 500,
    },
    status: {
        color: theme.palette.grey[400],
        display: 'flex',
        alignItems: 'center',
    },
    statusIcon: {
        fontSize: 13,
        marginRight: theme.spacing(1),
    },
    onlineIcon: {
        fontSize: 13,
        marginRight: theme.spacing(1),
        color: '#39CE48'
    },
    statusText: {
        fontSize: 13,
    },
    online: {
        fontSize: 13,
        color: '#39CE48'
    }
}))