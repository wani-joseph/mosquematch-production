import React from 'react'
import Moment from 'react-moment'
import { Typography } from '@material-ui/core'
import Loader from '../../../UI/Loader/Loader'
import Image from '../../../UI/Image/Image'
import FiberManualRecordIcon from '@material-ui/icons/FiberManualRecord'
import { useStyles } from './ContactDetailsStyles'

const ContactDetails = props => {
    const classes = useStyles()
    const { contactDetails } = props

    let contact = <Loader />

    if (contactDetails){
        contact = (
            <React.Fragment>
                <div className={classes.thumb}>
                    <Image imgSrc={`/images/${contactDetails.avatar}`} imgAlt={'contact'} />
                </div>
                <div className={classes.info}>
                    <Typography className={classes.bold}>{contactDetails.fname}, {contactDetails.age}</Typography>
                    <div className={classes.status}>
                        {contactDetails.status == "online"
                            ? <FiberManualRecordIcon className={classes.onlineIcon} />
                            : <FiberManualRecordIcon className={classes.statusIcon} />}
                        {contactDetails.status == "online" ? (
                            <Typography className={classes.online}>
                                Online now
                            </Typography>
                        ) : (
                            <Typography className={classes.statusText}>
                                Active {<Moment fromNow>{contactDetails.modifiedDate}</Moment>}
                            </Typography>
                        )}
                    </div>
                </div>
            </React.Fragment>
        )
    }

    return (
        <div className={classes.contactDetails}>
            {contact}
        </div>
    )
}

export default ContactDetails