import React from 'react'
import Moment from 'react-moment'
import CheckIcon from '@material-ui/icons/Check'
import { useStyles } from './ConversationStyles'

const Conversation = props => {
    const classes = useStyles()
    const { userId, content, sender, isRead, creationDate } = props
    let messageClasses = [classes.message]
    let textClasses = [classes.text]
    let timeClasses = [classes.time]
    let isReadClasses = [classes.read]

    if (userId == sender) {
        messageClasses.push(classes.send)
        textClasses.push(classes.sendText)
        timeClasses.push(classes.sendColor)
    } else {
        messageClasses.push(classes.received)
        textClasses.push(classes.receivedText)
        timeClasses.push(classes.receivedColor)
    }

    if (isRead) isReadClasses.push(classes.isRead)

    return (
        <li className={messageClasses.join(' ')}>
            <div className={textClasses.join(' ')}>
                <span>{content}</span>
                <div className={timeClasses.join(' ')}>
                    <Moment fromNow>{creationDate}</Moment>
                    <CheckIcon  className={isReadClasses.join(' ')}/>
                </div>
            </div>
        </li>
    )
}

export default Conversation