import { makeStyles } from '@material-ui/core/styles'

export const useStyles = makeStyles(theme => ({
    message: {
        display: 'flex',
        marginBottom: theme.spacing(1),
    },
    text: {
        // display: 'inline-flex',
        maxWidth: 200,
        padding: theme.spacing(1),
        borderRadius: theme.shape.borderRadius,
        boxShadow: theme.shadows[1],
        [theme.breakpoints.up('sm')]: {
            maxWidth: 400,
        }
    },
    send: {
        justifyContent: 'flex-end',
    },
    received: {
        justifyContent: 'flex-start',
    },
    sendText: {
        background: '#9dbd9d', //#cdeacd
    },
    receivedText: {
        background: '#f8bbbb',
    },
    time: {
        alignItems: 'center',
        display: 'flex',
        justifyContent: 'flex-end',
        fontSize: 13,
        textAlign: 'end',
    },
    sendColor: {
        color: 'white',
    },
    receivedColor: {
        color: 'white',
    },
    read: {
        fontSize: 13,
        marginLeft: 4
    },
    isRead: {
        color: theme.palette.secondary.main
    }
}))