import { makeStyles } from '@material-ui/core/styles'

export const useStyles = makeStyles(theme => ({
    listItem: {
        marginBottom: theme.spacing(1),
        '&:last-of-type': {
            marginBottom: 0,
        }
    },
    message: {
        display: 'flex',
        justifyContent: 'flex-start',
        alignItems: 'center',
        borderRadius: theme.shape.borderRadius,
        background: 'rgba(0, 132, 137, 0.1)',
        padding: theme.spacing(1),
        textDecoration: 'none',
    },
    thumb: {
        width: 42,
        height: 42,
        borderRadius: '50%',
        marginRight: theme.spacing(2),
        overflow: 'hidden',
    },
    messageInfo: {
        flex: '0.70%'
    },
    user: {
        display: 'flex',
        justifyContent: 'space-between',
    },
    name: {
        color: theme.palette.common.black,
        fontWeight: 500,
        fontSize: 16,
    },
    time: {
        fontSize: 13
    },
    messageText: {
        display: 'flex',
        justifyContent: 'space-between',
        alignItems: 'center',
    },
    text: {
        fontSize: 14,
    },
    read: {
        color: theme.palette.grey[400],
    },
    isRead: {
        color: theme.palette.secondary.main,
    }
}))