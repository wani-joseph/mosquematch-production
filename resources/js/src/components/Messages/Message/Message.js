import React from 'react'
import { Link } from 'react-router-dom'
import Moment from 'react-moment'
import { Typography } from '@material-ui/core'
import CheckIcon from '@material-ui/icons/Check'
import Image from '../../UI/Image/Image'
import { useStyles } from './MessageStyles'

const Message = props => {
    const classes = useStyles()
    const { message } = props

    let isReadClasses = [classes.read]

    if (message.isRead) isReadClasses.push(classes.isRead)

    return (
        <li className={classes.listItem}>
            <Link
                to={`/messages/chat/${message.sender.data.identifier}`}
                className={classes.message}
            >
                <div className={classes.thumb}>
                    <Image
                        imgSrc={`/images/${message.sender.data.avatar}`}
                        imgAlt={message.sender.data.fname}
                    />
                </div>
                <div className={classes.messageInfo}>
                    <div className={classes.user}>
                        <Typography
                            className={classes.name}
                            variant='body2'
                        >
                            {message.sender.data.fname}
                        </Typography>
                        <Typography
                            className={classes.time}
                            color="textSecondary"
                        >
                            {<Moment fromNow>{message.creationDate}</Moment>}
                        </Typography>
                    </div>
                    <div className={classes.messageText}>
                        <Typography
                            color="textPrimary"
                            className={classes.text}
                        >
                            {message.content}
                        </Typography>
                        <CheckIcon className={isReadClasses.join(' ')} />
                    </div>
                </div>
            </Link>
        </li>
    )
}

export default Message