import React, { Component } from 'react'
import { connect } from 'react-redux'
import InfiniteScroll from '../../../services/InfiniteScroll'
import Message from '../Message/Message'
import Loader from '../../UI/Loader/Loader'
import * as actions from '../../../store/actions'

class Inbox extends Component {
    constructor() {
        super()
        this.handleIncomingMessage = this.handleIncomingMessage.bind(this)
        this.scrollToBottom = this.scrollToBottom.bind(this)
    }

    componentDidMount() {
        const { userId } = this.props
        const newMessage = Echo.private(`messages.${userId}`)

        newMessage.listen('NewMessage', (e) => {
            this.handleIncomingMessage(e.message)
        })
    }

    scrollToBottom() {
        setTimeout(() => {
            this.messagesEnd.scrollTop = this.messagesEnd.scrollHeight - this.messagesEnd.clientHeight;
        }, 50)
    }

    handleIncomingMessage(message) {
        const { loadContactDetails, incomimgInboxMessage } = this.props
        loadContactDetails(message.sender_id, res => {
            const newMessage = {
                identifier: message.id,
                sender: message.sender_id,
                receiver: message.receiver_id,
                content: message.message,
                isRead: message.is_read,
                contactDetails: {
                    id: res.data.data.identifier,
                    firstname: res.data.data.fname
                },
                creationDate: message.created_at,
                modifiedDate: message.updated_at
            }

            incomimgInboxMessage(newMessage)
        })
    }

    render() {
        
        return (
            <InfiniteScroll resource="inbox">
                {resource => {
                    let inboxMessages = <Loader />

                    if (!resource.loading) {
                        if (resource.list.length > 0) {
                            inboxMessages = resource.list.map(message => (
                                <Message
                                    key={message.identifier}
                                    message={message}
                                />
                            ))
                        } else {
                            inboxMessages = <p>No one send you messages yet...</p>
                        }
                    }

                    return <>{inboxMessages}</>
                }}
            </InfiniteScroll>
        )
    }
}

const mapStateToProps = state => ({
    userId: state.auth.authUser.id,
    loading: state.member.loading,
    messages: state.member.inboxMessages,
})

const mapDispatchToProps = dispatch => ({
    loadContactDetails: (contact, next) => dispatch(actions.getContactDetails(contact, next)),
    incomimgInboxMessage: (message) => dispatch(actions.incomimgInboxMessage(message))
})

export default connect(mapStateToProps, mapDispatchToProps)(Inbox)