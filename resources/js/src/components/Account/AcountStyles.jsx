import { makeStyles } from '@material-ui/core/styles';

export const useStyles = makeStyles(theme =>({
    root: {
        position: 'relative',
        width: 'fit-content',
    },
    loginRegister: {
        padding: '.3rem .6rem',
        marginRight: '.4rem',
        minWidth: 1,
        borderRadius: 2,
        boxShadow: theme.shadows[0],
        fontSize: 13,
        textTransform: 'capitalize',
        '&:last-of-type': {
            marginRight: 0,
        },
        [theme.breakpoints.up('sm')]: {
            marginRight: theme.spacing(1),
            padding: theme.spacing(1, 2),
            fontSize: 15,
            marginTop: theme.spacing(1),
            marginBottom: theme.spacing(1),
        }
    }
}));