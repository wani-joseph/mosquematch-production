import React from 'react'
import { Link } from 'react-router-dom'
import { Button } from '@material-ui/core'
import { useStyles } from './AcountStyles'

const Acount = () => {
    const classes = useStyles()

    return (
        <React.Fragment>
            <Button
                className={classes.loginRegister}
                component={Link}
                to="/login"
                variant="outlined"
                color="primary"
            >
                Log in
            </Button>
            <Button
                className={classes.loginRegister}
                component={Link}
                to="/register"
                variant="contained"
                color="primary"
            >
                Register
            </Button>
        </React.Fragment>
    )
}

export default Acount