import { makeStyles } from '@material-ui/core/styles'

export const useStyles = makeStyles(theme =>({
    authHeader: {
        textAlign: 'center',
        marginTop: theme.spacing(2),
        marginBottom: theme.spacing(3),
        [theme.breakpoints.up('md')]: {
            marginTop: theme.spacing(4),
            marginBottom: theme.spacing(5),
        },
        [theme.breakpoints.up('lg')]: {
            marginTop: theme.spacing(6),
            marginBottom: theme.spacing(7),
        }
    }
}))