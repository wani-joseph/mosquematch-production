import React from 'react'
import { useStyles } from './AuthHeaderStyles'

const AuthHeader = props => {
    const classes = useStyles()

    return (
        <div className={classes.authHeader}>
            {props.children}
        </div>
    )
}

export default AuthHeader