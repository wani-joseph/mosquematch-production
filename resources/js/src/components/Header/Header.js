import React from 'react'
import { 
    AppBar,
    Container,
    Grid
} from '@material-ui/core'
import { useStyles } from './HeaderStyles'

const Header = props => {
    const classes = useStyles()

    let headerClasses = [classes.appHeader]

    if (props.spacing) 
        headerClasses.push(classes.spacing)

    return (
        <AppBar className={headerClasses.join(' ')} classes={props.classes}>
            <Container maxWidth="lg">
                <Grid container justify="space-between" alignItems="center">
                    <Grid item xs={6} sm={3}>{props.siteLogo}</Grid>
                    {props.mainNav
                        ? <Grid item sm={6} lg={7}>{props.mainNav}</Grid>
                        : null}
                    <Grid item xs={6} sm={3} lg={2} className={classes.lastItem}>{props.account}</Grid>
                </Grid>
            </Container>
            {props.sideDrawer}
        </AppBar>
    )
}

export default Header