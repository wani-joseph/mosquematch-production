import { makeStyles } from '@material-ui/core/styles'

export const useStyles = makeStyles(theme =>({
    appHeader: {
        background: theme.palette.common.white,
        boxShadow: theme.shadows[1]
    },
    lastItem: {
        display: 'flex',
        justifyContent: 'flex-end',
        position: 'relative',
    },
    spacing: {
        paddingBottom: theme.spacing(1),
        paddingTop: theme.spacing(1),
        [theme.breakpoints.up('sm')]: {
            paddingBottom: 0,
            paddingTop: 0,
        }
    }
}))