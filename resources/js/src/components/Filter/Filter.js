import React from 'react'
import {
	ExpansionPanel,
	ExpansionPanelSummary,
	ExpansionPanelDetails,
	Typography,
	withStyles
} from '@material-ui/core'
import ExpandMoreIcon from '@material-ui/icons/ExpandMore'
import CustomIconButton from '../UI/CustomIconButton/CustomIconButton'
import { styles } from './FilterStyles'

const Filter = ({ summary, children, classes}) => {
	const [expanded, setExpanded] = React.useState(false)

	const handleChange = () => {
		setExpanded(prev => !prev)
	}

	return (
		<ExpansionPanel
			classes={{root: classes.panel}}
			expanded={expanded}
			onChange={handleChange}
		>
			<ExpansionPanelSummary
				className={classes.heading}
				expandIcon={<CustomIconButton><ExpandMoreIcon /></CustomIconButton>}
				aria-controls={`${summary}-content`}
				id={`${summary}-header`}
			>
				<Typography variant="body2" className={classes.title}>{summary}</Typography>
			</ExpansionPanelSummary>
			<ExpansionPanelDetails className={classes.details}>
				{children}
			</ExpansionPanelDetails>
		</ExpansionPanel>
	)
}

export default withStyles(styles)(Filter)