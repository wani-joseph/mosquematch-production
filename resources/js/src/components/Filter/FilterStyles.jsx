export const styles = theme =>({
    panel: {
        padding: 0,
        boxShadow: theme.shadows[0],
    },
    heading: {
        padding: theme.spacing(0, 1),
    },
    title: {
        textTransform: 'uppercase',
        color: '#919191',
        fontWeight: 500
    },
    details: {
        display: 'block',
        padding: theme.spacing(0, 1),
    }
})