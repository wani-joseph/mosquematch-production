import { makeStyles } from '@material-ui/core/styles'

export const useStyles = makeStyles(theme =>({
    singleMember: {
        position: 'relative',
    },
    cover: {
        background: theme.palette.secondary.main,
        marginBottom: theme.spacing(10),
        paddingTop: theme.spacing(12)
    },
    paper: {
        maxWidth: 180,
        borderRadius: '50%',
        overflow: 'hidden',
        marginLeft: 'auto',
        marginRight: 'auto',
        marginBottom: -40,
        [theme.breakpoints.up('sm')]: {
            borderRadius: theme.spacing(2),
            marginLeft: 0,
            maxWidth: '90%',
        },
        [theme.breakpoints.up('md')]: {
            marginBottom: -60,
        },
        [theme.breakpoints.up('lg')]: {
            maxWidth: '80%',
        },
    },
    heading: {
        display: 'none',
        [theme.breakpoints.up('sm')]: {
            display: 'block',
            paddingTop: theme.spacing(1)
        },
        [theme.breakpoints.up('md')]: {
            display: 'block',
            paddingTop: theme.spacing(3)
        }
    },
    coverTitle: {
        color: theme.palette.common.black,
        fontSize: 20,
        fontWeight: 500,
        [theme.breakpoints.up('sm')]: {
            color: theme.palette.common.white,
        },
        [theme.breakpoints.up('md')]: {
            fontSize: theme.spacing(3),
        },
        [theme.breakpoints.up('lg')]: {
            fontSize: theme.spacing(4),
        }
    },
    img: {
        width: '100%',
        height: 'auto',
        display: 'block'
    },
    meta: {
        [theme.breakpoints.up('sm')]: {
            maxWidth: '90%'
        },
        [theme.breakpoints.up('lg')]: {
            maxWidth: '80%',
        },
    },
    tags: {
        background: 'white',
        padding: theme.spacing(2),
        border: '1px solid rgba(0, 0, 0, .08)',
        marginTop: theme.spacing(2),
        [theme.breakpoints.up('sm')]: {
            display: 'none',
        }
    },
    description: {
        background: 'white',
        padding: theme.spacing(2),
        border: '1px solid rgba(0, 0, 0, .08)',
        marginBottom: theme.spacing(6),
        [theme.breakpoints.up('sm')]: {
            marginTop: -60,
        }
    },
    photosWrap: {
        background: 'white',
        padding: theme.spacing(2),
        border: '1px solid rgba(0, 0, 0, .08)',
        marginBottom: theme.spacing(2),
        [theme.breakpoints.up('sm')]: {
            background: 'transparent',
            padding: 0,
            border: 'none',
        }
    },
    status: {
        display: 'flex',
        justifyContent: 'space-between',
        alignItems: 'center',
        color: theme.palette.common.black
    },
    online: {
        color: '#39CE48'
    },
    card: {
        background: 'white',
        padding: theme.spacing(2),
        border: '1px solid rgba(0, 0, 0, .08)',
        marginTop: theme.spacing(2),
        '&:last-of-type': {
            marginBottom: theme.spacing(4)
        }
    },
    title: {
        fontWeight: '500',
    },
    actions: {
        display: 'flex',
        justifyContent: 'space-between',
        alignItems: 'center',
        marginTop: theme.spacing(2),
        [theme.breakpoints.up('sm')]: {
            maxWidth: 320,
        },
    },
    photos: {
        display: 'flex',
        justifyContent: 'space-between',
        alignItems: 'center',
    },
    photo: {
        width: 64,
        height: 64,
        background: theme.palette.grey[700],
        borderRadius: 4,
        marginTop: theme.spacing(1),
        marginBottom: theme.spacing(1),
    },
    loadMore: {
        textAlign: 'end',
        fontWeight: 500,
        marginTop: theme.spacing(1),
        marginBottom: theme.spacing(1),
    },
    info: {
        marginBottom: theme.spacing(3),
    },
}))