import React from 'react'
import Moment from 'react-moment'
import {
    Container,
    Grid,
    Paper,
    Typography
} from '@material-ui/core'
import Tagline from '../Tagline/Tagline'
import MemberActions from '../Member/MemberActions/MemberActions'
import CustomDividor from '../UI/CustomDividor/CustomDividor'
import CustomTable from '../UI/CustomTable/CustomTable'
import { useStyles } from './SingleMemberStyles'

const SingleMember = props => {
    const classes = useStyles()
    const {
        identifier,
        fname,
        lname,
        email,
        isVerified,
        gender,
        location,
        dayOfBirth,
        monthOfBirth,
        yearOfBirth,
        age,
        countryBirth,
        status,
        tagline,
        bio,
        seeking,
        eduLevel,
        subject,
        jobTitle,
        profession,
        fLang,
        sLang,
        citizenship,
        countryOrigin,
        relocate,
        income,
        marriageWithin,
        martialStatus,
        children,
        likeChildren,
        arrangements,
        height,
        build,
        smoke,
        drink,
        disabilities,
        phonetime,
        religious,
        sector,
        hijab,
        beard,
        revert,
        keepHalal,
        prayer,
        readQuran,
        avatar,
        modifiedDate,
        mosques,
    } = props

    return (
        <div className={classes.singleMember}>
            <div className={classes.cover}>
                <Container>
                    <Grid container>
                        <Grid item xs={12} sm={4}>
                            <Paper className={classes.paper}>
                                <img
                                    className={classes.img}
                                    src={`/images/${avatar}`}
                                    alt={`Mosque Match ${fname} ${lname}`}
                                />
                            </Paper>
                        </Grid>
                        <Grid item xs={12} sm={8}>
                            <div className={classes.heading}>
                                <Typography
                                    className={classes.coverTitle}
                                    variant="body1"
                                    gutterBottom
                                    noWrap
                                >
                                    {tagline}
                                </Typography>
                                <Typography
                                    className={classes.coverTitle}
                                    variant="body1"
                                    gutterBottom
                                    noWrap
                                >
                                    {`${fname} ${lname}`}
                                </Typography>
                                <MemberActions identifier={identifier} />
                            </div>
                        </Grid>
                    </Grid>
                </Container>
            </div>
            <Container>
                <Grid container>
                    <Grid item xs={12} sm={4}>
                        <div className={classes.meta}>
                            <div className={classes.status}>
                                <Typography variant="body1">
                                    {`@${fname}${identifier}`}
                                </Typography>
                                {status == "online" ? (
                                    <Typography
                                        variant="body1"
                                        className={classes.online}
                                    >
                                        Online Now
                                    </Typography>
                                ) : (
                                    <Typography variant="body1">
                                        active {<Moment fromNow>{modifiedDate}</Moment>}
                                    </Typography>
                                )}
                            </div>
                            <CustomDividor />
                            <Typography
                                className={classes.title}
                                variant="h4"
                                component="h1"
                                gutterBottom
                            >
                                {mosques.data != "" ? mosques.data[0].mosqueName : 'London Central Mosque'}
                            </Typography>
                            <Tagline noWrap bottommargin="true" text="London, 2km" />
                            <CustomDividor />
                            <div className={classes.tags}>
                                <Typography
                                    className={classes.coverTitle}
                                    variant="body1"
                                    gutterBottom
                                    noWrap
                                >
                                    {tagline}
                                </Typography>
                                <Typography
                                    className={classes.coverTitle}
                                    variant="body1"
                                    gutterBottom
                                    noWrap
                                >
                                    {`${fname} ${lname}`}
                                </Typography>
                                <MemberActions identifier={identifier} />
                            </div>
                            <div className={classes.photosWrap}>
                                <Typography className={classes.title} variant="h5" component="h2" gutterBottom>
                                    Photos
                                </Typography>
                                <div className={classes.photos}>
                                    <div className={classes.photo}></div>
                                    <div className={classes.photo}></div>
                                    <div className={classes.photo}></div>
                                </div>
                                <div className={classes.photos}>
                                    <div className={classes.photo}></div>
                                    <div className={classes.photo}></div>
                                    <div className={classes.photo}></div>
                                </div>
                                <div className={classes.loadMore}>Load more...</div>
                            </div>
                        </div>
                    </Grid>
                    <Grid item xs={12} sm={8}>
                        <div className={classes.description}>
                        <Typography className={classes.title} variant="h4" component="h1" gutterBottom>
                            About Me
                        </Typography>
                        <CustomDividor />
                        <Typography variant="body1" className={classes.info}>{bio}</Typography>
                        <Typography className={classes.title} variant="h4" component="h1" gutterBottom>
                            Looking For
                        </Typography>
                        <CustomDividor />
                        <Typography variant="body1" className={classes.info}>{seeking}</Typography>
                        <Typography className={classes.title} variant="h4" component="h1" gutterBottom>
                            Personal Information
                        </Typography>
                        <CustomDividor />
                        <CustomTable
                            question="Name"
                            answer={`${fname} ${lname}`}
                        />
                        <CustomTable
                            question="Citizenship"
                            answer={citizenship}
                        />
                        <CustomTable
                            question="Country of Origin"
                            answer={countryOrigin}
                        />
                        <CustomTable
                            question="Willing to relocate"
                            answer={relocate}
                        />
                        <CustomTable
                            question="Income"
                            answer={income}
                        />
                        <CustomTable
                            question="Looking to marriage within"
                            answer={marriageWithin}
                        />
                        <CustomTable
                            question="Martial status"
                            answer={martialStatus}
                        />
                        <CustomTable
                            question="Would I like to have children?"
                            answer={likeChildren}
                        />
                        <CustomTable
                            question="Do I have children?"
                            answer={children}
                        />
                        <CustomTable
                            question="My living arrangements"
                            answer={arrangements}
                        />
                        <CustomTable
                            question="My height"
                            answer={height}
                        />
                        <CustomTable
                            question="My build"
                            answer={build}
                        />
                        <CustomTable
                            question="Do I smoke?"
                            answer={smoke}
                        />
                        <CustomTable
                            question="Do I drink?"
                            answer={drink}
                        />
                        <CustomTable
                            question="Do I have any disabilities?"
                            answer={disabilities}
                        />
                        <CustomTable
                            question="How long do you spend on your phone daily?"
                            answer={phonetime}
                        />
                        <Typography className={classes.title} variant="h4" component="h1" gutterBottom>
                            Education & Work
                        </Typography>
                        <CustomDividor />
                        <CustomTable
                            question="My education level"
                            answer={eduLevel}
                        />
                        <CustomTable
                            question="Subject I studied"
                            answer={subject}
                        />
                        <CustomTable
                            question="My profession"
                            answer={profession}
                        />
                        <CustomTable
                            question="My job title"
                            answer={jobTitle}
                        />
                        <CustomTable
                            question="My first language"
                            answer={fLang}
                        />
                        <CustomTable
                            question="My second language"
                            answer={sLang}
                        />
                        <Typography className={classes.title} variant="h4" component="h1" gutterBottom>
                            Religion
                        </Typography>
                        <CustomDividor />
                        <CustomTable
                            question="Religiousness"
                            answer={religious}
                        />
                        <CustomTable
                            question="My sector"
                            answer={sector}
                        />
                        <CustomTable
                            question="Do I prefer Hijab / Niqab?"
                            answer={hijab}
                        />
                        <CustomTable
                            question="Do I prefer a beard?"
                            answer={beard}
                        />
                        <CustomTable
                            question="Am I a revert?"
                            answer={revert}
                        />
                        <CustomTable
                            question="Do I keep halal?"
                            answer={keepHalal}
                        />
                        <CustomTable
                            question="Do I pray?"
                            answer={prayer}
                        />
                        <CustomTable
                            question="How often do I read Quran?"
                            answer={readQuran}
                        />
                        </div>
                    </Grid>
                </Grid>
            </Container>
        </div>
    )
}

export default SingleMember