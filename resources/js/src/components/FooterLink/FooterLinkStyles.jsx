import { makeStyles } from '@material-ui/core/styles';

export const useStyles = makeStyles(theme =>({
    listItem: {
        padding: 0,
        marginBottom: theme.spacing(1),
    },
    footerLink: {
        color: theme.palette.common.black,
        fontSize: '1rem',
        textDecoration: 'none',
    }
}));