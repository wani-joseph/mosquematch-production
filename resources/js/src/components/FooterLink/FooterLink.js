import React from 'react'
import { Link } from 'react-router-dom'
import { ListItem } from '@material-ui/core'
import { useStyles } from './FooterLinkStyles'

const FooterLink = (props) => {
    const classes = useStyles()

    return (
        <ListItem
            {...props}
            className={classes.listItem}
        >
            <Link
                className={classes.footerLink}
                to={props.link}
            >
                {props.text}
            </Link>
        </ListItem>
    )
}

export default FooterLink