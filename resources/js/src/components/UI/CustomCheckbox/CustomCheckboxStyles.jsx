export const styles = theme => ({
  customCheckbox: {
    display: 'inline-block',
    width: '50%',
    marginBottom: theme.spacing(1)
  },
  fullWidth: {
    width: '100%',
  },
  formLabel: {
    margin: 0,
  },
  root: {
    padding: 4,
    marginRight: theme.spacing(1) - 2,
    transform: 'scale(.9)',
    '&:hover': {
      backgroundColor: 'transparent',
    },
  },
  label: {
    fontSize: 14,
  },
  icon: {
    borderRadius: 3,
    border: 'solid 1px grey',
    width: 16,
    height: 16,
    boxShadow: 'inset 0 0 0 1px rgba(16,22,26,.2), inset 0 -1px 0 rgba(16,22,26,.1)',
    backgroundColor: '#ffffff',
    backgroundImage: 'linear-gradient(180deg,hsla(0,0%,100%,.8),hsla(0,0%,100%,0))',
    '$root.Mui-focusVisible &': {
      outline: '2px auto rgba(19,124,189,.6)',
      outlineOffset: 2,
    },
    'input:hover ~ &': {
      backgroundColor: '#ebf1f5',
    },
    'input:disabled ~ &': {
      boxShadow: 'none',
      background: 'rgba(206,217,224,.5)',
    },
  },
  checkedIcon: {
    backgroundColor: theme.palette.secondary.main,
    backgroundImage: 'linear-gradient(180deg,hsla(0,0%,100%,.1),hsla(0,0%,100%,0))',
    'input:hover ~ &': {
      backgroundColor: theme.palette.secondary.dark,
    },
  },
})