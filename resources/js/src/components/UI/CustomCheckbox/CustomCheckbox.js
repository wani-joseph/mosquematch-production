import React from 'react'
import clsx from 'clsx'
import {
  FormControlLabel,
  Checkbox,
  withStyles
} from "@material-ui/core"
import { styles } from './CustomCheckboxStyles'

const CustomCheckbox = ({ checked, value, onChange, fullWidth, classes }) => {
  let customCheckboxClasses = [classes.customCheckbox]

  if (fullWidth === 'true') customCheckboxClasses.push(classes.fullWidth)

  return (
    <div className={customCheckboxClasses.join(' ')}>
      <FormControlLabel
        className={classes.formLabel}
        classes={{ label: classes.label }}
        label={value}
        control={
          <Checkbox
            className={classes.root}
            value={value}
            onChange={onChange}
            checked={checked}
            checkedIcon={<span className={clsx(classes.icon, classes.checkedIcon)} />}
            icon={<span className={classes.icon} />}
          />
        }
      />
    </div>
  )
}

export default withStyles(styles)(CustomCheckbox)