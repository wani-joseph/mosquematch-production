import React from 'react'
import {
	RadioGroup,
	withStyles
} from '@material-ui/core'
import { styles } from './CustomRadioGroupStyles'

const CustomRadioGroup = ({ name, value, onChange, onBlur, children, classes }) => (
	<RadioGroup
		className={classes.radioGroup}
		aria-label={name}
		name={name}
		value={value}
		onChange={onChange}
		onBlur={onBlur}
	>
		{children}
	</RadioGroup>
)

export default withStyles(styles)(CustomRadioGroup)