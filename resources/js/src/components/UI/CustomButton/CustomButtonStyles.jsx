import { makeStyles } from '@material-ui/core/styles'

export const useStyles = makeStyles(theme =>({
    primary: {
        boxShadow: theme.shadows[0],
        borderBottom: `3px solid ${theme.palette.primary.dark}`,
        borderRadius: 2,
        padding: theme.spacing(1, 3),
        textTransform: 'capitalize',
    },
    disabled: {
        background: 'rgba(179, 32, 36, .5) !important',
        borderBottom: 'none',
        color: 'white !important',
        cursor: 'not-allowed !important',
        pointerEvents: 'auto !important',
    },
    secondary: {
        background: theme.palette.grey[700],
        borderColor: theme.palette.grey.A400,
        '&:hover': {
            background: theme.palette.grey.A400,
        }
    },
    topmargin: {
        marginTop: theme.spacing(2),
    },
    uppercase: {
        textTransform: 'uppercase'
    }
}))