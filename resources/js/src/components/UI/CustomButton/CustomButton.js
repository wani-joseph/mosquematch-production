import React from 'react'
import { Button } from '@material-ui/core'
import { useStyles } from './CustomButtonStyles'

const CustomButton = props => {
    const classes = useStyles();
    const { buttontype, color, children, disabled, topmargin, uppercase } = props

    let CustomButtonClasses = [classes.primary];

    if (disabled) {
        CustomButtonClasses.push(classes.disabled)
    }

    if (buttontype == "secondary")
        CustomButtonClasses.push(classes.secondary)

    if (topmargin == "true")
        CustomButtonClasses.push(classes.topmargin)

    if (uppercase == "true")
        CustomButtonClasses.push(classes.uppercase)

    return (
        <Button
            {...props}
            className={CustomButtonClasses.join(' ')}
            variant="contained"
            color={color ? color : "primary"}
        >
            {children}
        </Button>
    )
}

export default CustomButton