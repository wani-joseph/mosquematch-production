import React from 'react'
import { Typography } from '@material-ui/core'
import { useStyles } from './CustomTypographyStyles'
 
const CustomTypography = props => {
    const classes = useStyles()
    const { children, bold, bottommargin, invalid } = props

    let typographyClasses = []

    if (bold == "true") typographyClasses.push(classes.bold)

    if (bottommargin == "true") typographyClasses.push(classes.bottomMargin)

    if (invalid == "true") typographyClasses.push(classes.invalid)
    
    return (
        <Typography
            className={typographyClasses.join(' ')}
            {...props}
        >
            {children}
        </Typography>
    )
}

export default CustomTypography