import { makeStyles } from '@material-ui/core/styles'

export const useStyles = makeStyles(theme =>({
    bold: {
        fontWeight: 500,
    },
    bottomMargin: {
        marginBottom: theme.spacing(2),
    },
    invalid: {
        color: theme.palette.primary.main
    }
}))