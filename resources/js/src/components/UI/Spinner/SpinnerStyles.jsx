import { makeStyles } from '@material-ui/core/styles'

export const useStyles = makeStyles(theme => ({
    spinner: {
        width: '100vw',
        height: '100vh',
        textAlign: 'center',
    },
    logo: {
        marginTop: theme.spacing(10),
        marginBottom: theme.spacing(4),
    },
    linearProgress: {
        maxWidth: 200,
        position: 'relative',
        left: '50%',
        transform: 'translateX(-50%)'
    },
    loader: {
        display: 'inline-block',
        position: 'relative',
        width: 80,
        height: 80,
        transform: 'rotate(45deg)',
        transformOrigin: '40px 40px',
    },
    loaderDiv: {
        top: 32,
        left: 32,
        position: 'absolute',
        width: 32,
        height: 32,
        background: theme.palette.primary.main,
        animation: '$lds-heart 1.2s infinite cubic-bezier(0.215, 0.61, 0.355, 1)',
        '&:before': {
            content: '""',
            position: 'absolute',
            display: 'block',
            width: 32,
            height: 32,
            background: theme.palette.primary.main,
            left: '-24px',
            borderRadius: '50% 0 0 50%',
        },
        '&:after': {
            content: '""',
            position: 'absolute',
            display: 'block',
            width: 32,
            height: 32,
            background: theme.palette.primary.main,
            top: '-24px',
            borderRadius: '50% 50% 0 0',
        },
    },
    '@keyframes lds-heart': {
        '0%': {
            transform: 'scale(0.95)',
        },
        '5%': {
            transform: 'scale(1.1)',
        },
        '39%': {
            transform: 'scale(0.85)',
        },
        '45%': {
            transform: 'scale(1)',
        },
        '60%': {
            transform: 'scale(0.95)',
        },
        '100%': {
            transform: 'scale(0.9)',
        }
    }
}))