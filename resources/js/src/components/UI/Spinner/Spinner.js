import React from 'react'
import Logo from '../../Logo/Logo'
import { useStyles } from './SpinnerStyles'

const Spinner = () => {
    const classes = useStyles()

    return (
        <div className={classes.spinner}>
            <div className={classes.logo}>
                <Logo />
            </div>
            <div className={classes.linearProgress}>
                <div className={classes.loader}>
                    <div className={classes.loaderDiv}></div>
                </div>
            </div>
        </div>
    )
}

export default Spinner