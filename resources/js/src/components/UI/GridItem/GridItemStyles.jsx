import { makeStyles } from '@material-ui/core/styles'

export const useStyles = makeStyles(theme =>({
    gridItem: {
        background: theme.palette.common.white,
        border: '1px solid rgba(0, 0, 0, .08)',
        marginBottom: theme.spacing(2),
        position: 'relative',
        [theme.breakpoints.up('md')]: {
            marginBottom: theme.spacing(6),
        }
    },
    noborder: {
        background: 'transparent',
        border: 'none',
        [theme.breakpoints.up('md')]: {
            background: theme.palette.common.white,
            border: '1px solid rgba(0, 0, 0, .08)',
        }
    },
    padding: {
        [theme.breakpoints.up('md')]: {
            padding: theme.spacing(2),
        }
    },
}))