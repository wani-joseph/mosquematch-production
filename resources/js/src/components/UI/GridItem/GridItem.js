import React from 'react'
import { Grid } from '@material-ui/core'
import { useStyles } from './GridItemStyles'

const GridItem = props => {
    const classes = useStyles();

    let gridItemClasses = [classes.gridItem]

    if (props.noborder === "true")
        gridItemClasses.push(classes.noborder)

    if (props.padding === "true")
        gridItemClasses.push(classes.padding)

    return (
        <Grid
            className={gridItemClasses.join(' ')}
            item
            {...props} 
        >
            {props.children}
        </Grid>
    );
};

export default GridItem