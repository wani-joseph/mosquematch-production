export const styles = theme => ({
    arrow: {
        cursor: 'pointer',
        position: 'absolute',
        top: 0,
        width: 40,
        height: 40,
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 4,
        zIndex: 110,
    },
    next: {
        right: 15,
        background: theme.palette.primary.main,
        color: theme.palette.common.white,
    },
    prev: {
        right: 65,
        background: '#919191',
        color: theme.palette.common.black,
    }
})