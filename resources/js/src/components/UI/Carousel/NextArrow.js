import React from 'react'
import { withStyles } from '@material-ui/core'
import NavigateNextIcon from '@material-ui/icons/NavigateNext'
import { styles } from './CarouselStyles'

const NextArrow = ({ onClick, classes }) => {
    const nextArrowClasses = [classes.arrow, classes.next]
    
    return (
        <div className={nextArrowClasses.join(' ')} onClick={onClick}>
            <NavigateNextIcon />
        </div>
    )
}

export default withStyles(styles)(NextArrow)