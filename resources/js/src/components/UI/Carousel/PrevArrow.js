import React from 'react'
import { withStyles } from '@material-ui/core'
import NavigateBeforeIcon from '@material-ui/icons/NavigateBefore'
import { styles } from './CarouselStyles'

const PrevArrow = ({ onClick, classes }) => {
    const prevArrowClasses = [classes.arrow, classes.prev]
    return (
        <div className={prevArrowClasses.join(' ')} onClick={onClick}>
            <NavigateBeforeIcon />
        </div>
    )
}

export default withStyles(styles)(PrevArrow)