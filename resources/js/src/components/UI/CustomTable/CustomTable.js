import React from 'react'
import { Typography } from '@material-ui/core'
import CustomDividor from '../CustomDividor/CustomDividor'
import { useStyles } from './CustomTableStyles'

const CustomTable = props => {
    const classes = useStyles()
    const { question, answer } = props

    return (
        <div className={classes.table}>
            <div className={classes.questionAsnwerWrap}>
                <Typography variant="body1" className={classes.question}>
                    {question}
                </Typography>
                <Typography variant="body1" className={classes.answer}>
                    {answer}
                </Typography>
            </div>
            <CustomDividor />
        </div>
    )
}

export default CustomTable