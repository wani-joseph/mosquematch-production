import { makeStyles } from '@material-ui/core/styles';

export const useStyles = makeStyles(theme =>({
    table:{
        padding: theme.spacing(0, 2),
        marginBottom: theme.spacing(3),
    },
    questionAsnwerWrap: {
        [theme.breakpoints.up('sm')]: {
            display: 'flex',
            flex: 1,
        }
    },
    question: {
        fontWeight: 500,
        [theme.breakpoints.up('sm')]: {
            display: 'flex',
            flex: '0 50%',
            width: '50%'
        }
    },
    answer: {
        textAlign: 'end',
        [theme.breakpoints.up('sm')]: {
            display: 'flex',
            flex: '0 50%',
            width: '50%',
            textAlign: 'start',
        }
    }
}))