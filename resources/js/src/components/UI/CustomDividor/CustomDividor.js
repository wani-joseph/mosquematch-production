import React from 'react'
import { Divider } from '@material-ui/core'
import { useStyles } from './CustomDividorStyles'

const CustomDividor = (props) => {
    const classes = useStyles();

    let dividorClasses = classes.horizontal

    if (props.orientation == "vertical") {
        dividorClasses = classes.vertical
    }

    return (
        <Divider {...props} className={dividorClasses}/>
    )
}

export default CustomDividor