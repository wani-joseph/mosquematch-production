import { makeStyles } from '@material-ui/core/styles'

export const useStyles = makeStyles(theme =>({
    horizontal: {
        marginBottom: theme.spacing(1),
        marginTop: theme.spacing(1),
    },
    vertical: {
        marginLeft: theme.spacing(1),
        marginRight: theme.spacing(1),
        width: 1,
        height: 16,
    },
}))