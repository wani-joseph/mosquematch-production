export const styles = theme => ({
  root: {
    color: theme.palette.primary.main,
    height: 6,
  },
  thumb: {
    height: 18,
    width: 18,
    backgroundColor: '#fff',
    border: '1px solid rgba(0, 0, 0, .08)',
    marginTop: -6,
    marginLeft: -9,
    '&:focus,&:hover,&$active': {
      boxShadow: 'inherit',
    },
  },
  active: {},
  valueLabel: {
    left: 'calc(-50% + 3px)',
  },
  track: {
    height: 6,
    borderRadius: 4,
  },
  rail: {
    height: 6,
    borderRadius: 4,
    background: 'grey'
  },
})