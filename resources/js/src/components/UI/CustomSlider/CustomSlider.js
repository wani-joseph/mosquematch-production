import React, { useState } from 'react'
import {
  Slider,
  withStyles
} from '@material-ui/core'
import CustomTypography from '../CustomTypography/CustomTypography'
import { styles } from './CustomSliderStyles'

const CustomSlider = ({ ariaLabel, defaultValues, min, max, getValues, step, classes }) => {
  const [value, setValue] = useState(defaultValues)

  const handleChange = (event, newValue) => {
    setValue(newValue)
    getValues(value)
  }
  
  return (
    <>
      <CustomTypography bold="true" variant="body2" color="textSecondary">
        {`${value[0] != min ? '-': ''} ${value[0]} - ${value[1]} ${value[1] != max ? '+': ''}`}
      </CustomTypography>
      <Slider
        classes={{
          root: classes.root,
          thumb: classes.thumb,
          active: classes.active,
          track: classes.track,
          rail: classes.rail
        }}
        value={value}
        min={min}
        max={max}
        onChange={handleChange}
        aria-labelledby={ariaLabel}
        step={step}
      />
    </>
  )
}

export default withStyles(styles)(CustomSlider)