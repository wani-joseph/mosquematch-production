import React from 'react'
import { useStyles } from './LoaderStyles'

const Loader = () => {
    const classes = useStyles()

    return (
        <div className={classes.loader}>
            <div className={classes.spinner}>
                <div className={classes.spinnerDiv}></div>
                <div className={classes.spinnerDiv}></div>
                <div className={classes.spinnerDiv}></div>
                <div className={classes.spinnerDiv}></div>
                <div className={classes.spinnerDiv}></div>
                <div className={classes.spinnerDiv}></div>
                <div className={classes.spinnerDiv}></div>
                <div className={classes.spinnerDiv}></div>
                <div className={classes.spinnerDiv}></div>
                <div className={classes.spinnerDiv}></div>
                <div className={classes.spinnerDiv}></div>
                <div className={classes.spinnerDiv}></div>
            </div>
        </div>
    )
}

export default Loader