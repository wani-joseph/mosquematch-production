import { makeStyles } from '@material-ui/core/styles'

export const useStyles = makeStyles(theme => ({
    loader: {
        background: 'rgba(255, 255, 255, .6)',
        position: 'fixed',
        top: 0,
        left: 0,
        width: '100vw',
        height: '100vh',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'flex-start',
        paddingTop: '20%',
        zIndex: 1400
    },
    spinner: {
        display: 'inline-block',
        position: 'relative',
        width: 80,
        height: 80,
    },
    spinnerDiv: {
        transformOrigin: '40px 40px',
        animation: `$lds-spinner 1.2s linear infinite`,
        '&:after': {
            content: '""',
            display: 'block',
            position: 'absolute',
            top: 3,
            left: 37,
            width: 6,
            height: 18,
            borderRadius: '20%',
            background: theme.palette.secondary.main,
        },
        '&:nth-child(1)': {
            transform: 'rotate(0deg)',
            animationDelay: '-1.1s',
        },
        '&:nth-child(2)': {
            transform: 'rotate(30deg)',
            animationDelay: '-1s',
        },
        '&:nth-child(3)': {
            transform: 'rotate(60deg)',
            animationDelay: '-0.9s',
        },
        '&:nth-child(4)': {
            transform: 'rotate(0deg)',
            animationDelay: '-1.1s',
        },
        '&:nth-child(5)': {
            transform: 'rotate(120deg)',
            animationDelay: '-0.7s',
        },
        '&:nth-child(6)': {
            transform: 'rotate(150deg)',
            animationDelay: '-0.6s',
        },
        '&:nth-child(7)': {
            transform: 'rotate(180deg)',
            animationDelay: '-0.5s',
        },
        '&:nth-child(8)': {
            transform: 'rotate(210deg)',
            animationDelay: '-0.4s',
        },
        '&:nth-child(9)': {
            transform: 'rotate(240deg)',
            animationDelay: '-0.3s',
        },
        '&:nth-child(10)': {
            transform: 'rotate(270deg)',
            animationDelay: '-0.2s',
        },
        '&:nth-child(11)': {
            transform: 'rotate(300deg)',
            animationDelay: '-0.1s',
        },
        '&:nth-child(12)': {
            transform: 'rotate(330deg)',
            animationDelay: '0s',
        },
    },
    '@keyframes lds-spinner': {
        from: {opacity: 1},
        to: {opacity: 0}
    }
}))