import React from 'react'
import { Paper } from '@material-ui/core'
import { useStyles } from './CustomPaperStyles'

const CustomPaper = (props) => {
    const classes = useStyles();

    let CustomPaperClasses = [classes.paper]

    if (props.spacing) {
        CustomPaperClasses.push(classes.spacing)
    }

    if (props.bottommargin) {
        CustomPaperClasses.push(classes.bottomMargin)
    }

    if (props.noborder == "true") {
        CustomPaperClasses.push(classes.noborder)
    }

    return (
        <Paper
            {...props}
            className={CustomPaperClasses.join(' ')}
            square
            elevation={0}
        >
            {props.children}
        </Paper>
    )
}

export default CustomPaper