import { makeStyles } from '@material-ui/core/styles'

export const useStyles = makeStyles(theme =>({
    paper: {
        border: '1px solid rgba(0, 0, 0, .08)',
    },
    spacing: {
        padding: theme.spacing(2),
    },
    bottomMargin: {
        marginBottom: theme.spacing(3),
    },
    noborder: {
        border: 'none'
    }
}))