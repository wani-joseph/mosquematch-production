import React from 'react'
import { TextField } from '@material-ui/core'
import { useStyles } from './CustomTextFieldStyles'

const CustomTextField = props => {
    const classes = useStyles()

    return (
        <TextField
            {...props}
            className={classes.root}
            InputProps={{ className: classes.formControl }}
            InputLabelProps={{
                shrink: true,
                className: classes.label
            }}
        />
    );
};

export default CustomTextField