export const styles = theme =>({
    formControl: {
        margin: 0,
    },
    label: {
        fontSize: 14,
    },
    radio: {
        padding: 0,
        transform: 'scale(.7    )',
        marginRight: theme.spacing(1) - 2,
        marginLeft: -4,
    }
})