import React from 'react'
import {
	FormControlLabel,
	Radio,
	withStyles
} from '@material-ui/core'
import { styles } from './CustomRadioButtonStyles'

const CustomRadioButton = ({label, value, classes}) => (
	<FormControlLabel
		className={classes.formControl}
		classes={{ label: classes.label }}
		control={<Radio className={classes.radio} />} 
		label={label}
		value={value}
	/>
)

export default withStyles(styles)(CustomRadioButton)