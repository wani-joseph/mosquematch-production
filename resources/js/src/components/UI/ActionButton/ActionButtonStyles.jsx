import { makeStyles } from '@material-ui/core/styles'

export const useStyles = makeStyles(theme =>({
    button: {
        color: 'inherit',
        border: 'none',
        display: 'inline-block',
        width: 36,
        height: 22,
        background: 'transparent',
        cursor: 'pointer',
        outline: 'none'
    }
}))