import React from 'react'
import { useStyles } from './ActionButtonStyles'

const ActionButton = props => {
    const classes = useStyles()

    let actionButtonClasses = [classes.button]

    if (props.small)
        actionButtonClasses.push(classes.small)

    return (
        <button
            className={actionButtonClasses.join(' ')}
            onClick={props.onClick}
            title={props.title}
        >
            {props.label}
        </button>
    )
}

export default ActionButton