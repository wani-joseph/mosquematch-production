import React from 'react'
import { TextField } from '@material-ui/core'
import { useStyles } from './CustomSelectStyles'

const CustomSelect = (props) => {
    const classes = useStyles()
    
    return (
        <TextField
            {...props}
            select
            className={classes.root}
            InputProps={{ className: classes.formControl }}
            InputLabelProps={{
                shrink: true,
                className: classes.label
            }}
            SelectProps={{
                MenuProps: {
                    classes: {paper: classes.paper}
                },
            }}
        >
            {props.children}
        </TextField>
    )
}

export default CustomSelect