import { makeStyles } from '@material-ui/core/styles'

export const useStyles = makeStyles(theme =>({
    root: {
        marginBottom: theme.spacing(2),
        width: '100%',
        [theme.breakpoints.up('md')]: {
            marginBottom: theme.spacing(3),
        },
    },
    formControl: {
        border: '1px solid rgba(0, 0, 0, .08)',
        color: theme.palette.grey[700],
        fontSize: 16,
    },
    label: {
        color: theme.palette.common.black,
        transform: 'scale(1)',
        fontSize: 13,
        [theme.breakpoints.up('md')]: {
            fontSize: 15,
        }
    },
    paper: {
        boxShadow: theme.shadows[3],
        borderRadius: 0,
    }
}))