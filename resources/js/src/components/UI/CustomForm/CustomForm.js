import React from 'react'
import { useFormik } from '../../../hooks/useFormik'
import { MenuItem, withStyles } from '@material-ui/core'
import CustomTextField from '../CustomTextField/CustomTextField'
import CustomSelect from '../CustomSeclect/CustomSeclect'
import { styles } from './CustomFormStyles'

const CustomForm = ({ fields, submitHandler, submitButton, validation, classes, customClasses }) => {

    let textareaClasses = [classes.muiInput]

    if (customClasses) textareaClasses.push(customClasses.message)

    const getInitialValues = inputs => {
        const initialValues = {}

        inputs.forEach(field => {
            if (!initialValues[field.name]) {
                initialValues[field.name] = field.value
            }
        })

        return initialValues
    }

    const renderTextField = (input) => (
        <div
            key={input.name}
            className={classes.formControl}
        >
            {input.label ? (
                    <label
                        htmlFor={input.name} 
                        className={classes.formLabel}
                    >
                        {input.label}
                    </label> 
                ) : null}
            <CustomTextField
                className={classes.formInput}
                type={input.type}
                name={input.name}
                placeholder={input.placeholder}
                {...getFieldProps(input.name)}
                helperText={touched.name ? errors.name : ""}
                error={touched.name && Boolean(errors.name)}
                classes={{ root: classes.muiInput }}
                fullWidth
            />
        </div>
    )

    const renderSelectField = input => {
        let selectClasses = [classes.formControl]
        let labelClasses = [classes.formLabel]

        if (input.name == "dayOfBirth") selectClasses.push(classes.daySelect)

        if (input.name == "monthOfBirth") {
            selectClasses.push(classes.monthSelect)
            labelClasses.push(classes.hideLabel)
        }

        if (input.name == "yearOfBirth") {
            selectClasses.push(classes.yearSelect)
            labelClasses.push(classes.hideLabel)
        }

        const options = input.data.map(option => (
            <MenuItem
                key={option.code ? option.code : option}
                value={option.code ? option.code : option}
            >
                {option.name ? option.name : option}
            </MenuItem>
        ))

        const selectOptions = [...options]

        return (
            <div
                key={input.name}
                className={selectClasses.join(' ')}
            >
                <label
                    htmlFor={input.name}
                    className={labelClasses.join(' ')}
                >
                    {input.name == "days" ? "Date of birth" : input.label}
                </label>
                <CustomSelect
                    className={classes.formInput}
                    type={input.type}
                    name={input.name}
                    {...getFieldProps(input.name)}
                    helperText={touched.name ? errors.name : ""}
                    error={touched.name && Boolean(errors.name)}
                    classes={{ root: classes.muiInput }}
                    fullWidth
                >
                    {selectOptions}
                </CustomSelect>
            </div>
        )
    }

    const renderTextareaFeild = input => (
        <div
            key={input.name}
            className={customClasses ? customClasses.formControl : classes.formControl}
        >
            {input.label ? (
                <label
                    htmlFor={input.name} 
                    className={classes.formLabel}
                >
                    {input.label}
                </label>
            ) : null}
            <CustomTextField
                className={classes.formInput}
                type={input.type}
                name={input.name}
                placeholder={input.placeholder}
                {...getFieldProps(input.name)}
                rows={input.rows}
                helperText={touched.name ? errors.name : ""}
                error={touched.name && Boolean(errors.name)}
                classes={{ root: classes.muiInput }}
                fullWidth
                multiline
            />
        </div>
    )

    const renderFields = inputs => {
        return inputs.map(input => {
            switch(input.type) {
                case 'textarea': return renderTextareaFeild(input)
                case 'select': return renderSelectField(input)
                default: return renderTextField(input)
            }
        })
    }

    const initialValues = getInitialValues(fields)

    const formik = useFormik({
        initialValues: initialValues,
        onSubmit: submitHandler,
        validate: validation ? values => validation(values) : null
    })

    const { handleSubmit, getFieldProps, touched, errors, submitError } = formik

    return (
        <form
            className={customClasses ? customClasses.form : classes.form}
            onSubmit={handleSubmit}
        >
            {renderFields(fields)}
            {submitButton}
        </form>
    )
}

export default withStyles(styles)(CustomForm)