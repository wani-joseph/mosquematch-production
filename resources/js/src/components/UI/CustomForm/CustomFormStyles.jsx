export const styles = theme => ({
    form: {
        padding: theme.spacing(2)
    },
    formControl: {
        marginBottom: theme.spacing(2)
    },
    muiInput: {
        marginBottom: 0,
    },
    formLabel: {
        color: theme.palette.common.black,
        fontWeight: 500,
        marginBottom: 4,
        display: 'inline-flex'
    },
    formInput: {
        width: '100%',
        padding: '6px 8px',
        display: 'inline-block',
        font: 'inherit',
        color: 'inherit',
        borderRadius: 2,
        border: '1px solid rgba(0, 0, 0, .08)',
        outline: 'none',
        fontSize: 16
    },
    daySelect: {
        display: 'inline-flex',
        flexDirection: 'column',
        marginRight: '5%',
        width: '30%'
    },
    monthSelect: {
        display: 'inline-flex',
        flexDirection: 'column',
        marginRight: '5%',
        width: '30%'
    },
    yearSelect: {
        display: 'inline-flex',
        flexDirection: 'column',
        width: '30%'
    },
    hideLabel: {
        visibility: 'hidden'
    },
    error: {
        color: 'red',
        marginTop: 4
    }
})