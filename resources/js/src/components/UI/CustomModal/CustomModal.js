// import React, { Component } from 'react'
// import {
//     Modal,
//     Typography,
//     Paper
// } from '@material-ui/core'
// import CloseIcon from '@material-ui/icons/Close'
// import ToggleModal from '../../ToggleModal/ToggleModal'
// import CustomDividor from '../CustomDividor/CustomDividor'
// import withStyles from '@material-ui/core/styles/withStyles'
// import { styles } from './CustomModalStyles'

// class CustomModal extends Component {
//     constructor() {
//         super()
//         this.state = {
//             open: false
//         }
//         this.handleOpen = this.handleOpen.bind(this)
//         this.handleClose = this.handleClose.bind(this)
//     }

//     handleOpen() {
//         this.setState({ open: true });
//     }

//     handleClose() {
//         this.setState({ open: false });
//     }

//     render() {
//         const { classes, title, content, label, highlights, action, addNew } = this.props

//         return (
//             <React.Fragment>
//                 <ToggleModal
//                     onClick={this.handleOpen}
//                     label={label}
//                     highlights={highlights}
//                     action={action}
//                     addNew={addNew}
//                 />
//                 <Modal
//                     aria-labelledby="simple-modal-title"
//                     aria-describedby="simple-modal-description"
//                     open={this.state.open}
//                     onClose={this.handleClose}
//                     disableEnforceFocus
//                     disableAutoFocus
//                     disableRestoreFocus
//                 >
//                     <div className={classes.modal}>
//                         <div className={classes.modalClose}>
//                             <CloseIcon onClick={this.handleClose} />
//                         </div>
//                         <Paper className={classes.paper}>
//                             <Typography
//                                 className={classes.modalTitle}
//                                 variant="h4"
//                                 component="h2"
//                             >
//                                 {title}
//                             </Typography>
//                             <CustomDividor />
//                             <div className={classes.modalContent}>
//                                 {content}
//                             </div>
//                         </Paper>
//                     </div>
//                 </Modal>
//             </React.Fragment>
//         )
//     }
// }

// export default withStyles(styles)(CustomModal)

import React from 'react'
import {
    Modal,
    Typography,
    Paper
} from '@material-ui/core'
import CloseIcon from '@material-ui/icons/Close'
import CustomButton from '../CustomButton/CustomButton'
import CustomDividor from '../CustomDividor/CustomDividor'
import withStyles from '@material-ui/core/styles/withStyles'
import { styles } from './CustomModalStyles'

const CustomModal = ({
    ariaLabelBy,
    ariaDescBy,
    show,
    handleClose,
    title,
    content,
    confirmation,
    classes
}) => (
	<Modal
        aria-labelledby={ariaLabelBy}
        aria-describedby={ariaDescBy}
		open={show}
		onClose={handleClose}
		disableEnforceFocus
		disableAutoFocus
		disableRestoreFocus
	>
		<div className={classes.modal}>
            <div className={classes.modalClose}>
                <CloseIcon onClick={handleClose} />
            </div>
            <Paper className={classes.paper}>
                <Typography
                    className={classes.modalTitle}
                    variant="h4"
                    component="h2"
                >
                    {title}
                </Typography>
                <CustomDividor />
                <div className={classes.modalContent}>
                    {content}
                    {confirmation ? (
                        <div className={classes.actions}>
                            <CustomButton
                                classes={{ root: classes.button }}
                                onClick={handleClose}
                            >
                                No
                            </CustomButton>
                            <CustomButton
                                classes={{ root: classes.button }}
                                onClick={confirmation}
                            >
                                Yes
                            </CustomButton>
                        </div>
                    ) : null}
                </div>
            </Paper>
        </div>
	</Modal>
)

export default withStyles(styles)(CustomModal)