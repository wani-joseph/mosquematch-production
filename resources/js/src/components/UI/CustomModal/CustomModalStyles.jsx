export const styles = theme =>({
    modal: {
        position: 'absolute',
        top: '50%',
        left: '50%',
        width: '90%',
        maxWidth: 800,
        transform: 'translate(-50%, -50%)',
    },
    modalClose: {
        position: 'absolute',
        top: theme.spacing(3),
        right: theme.spacing(3),
        cursor: 'pointer',
    },
    paper: {
        padding: theme.spacing(3),
        borderRadius: 0,
    },
    modalTitle: {
        fontWeight: 500,
        marginBottom: theme.spacing(2),
    },
    modalContent: {
        marginTop: theme.spacing(2)
    },
    actions: {
        display: 'flex',
        justifyContent: 'flex-end',
        marginTop: theme.spacing(2)
    },
    button: {
        textTransform: 'capitalize',
        marginLeft: theme.spacing(2),
        marginTop: 0,
        padding: theme.spacing(1, 3),
        '&:first-of-type': {
            background: theme.palette.grey[700],
            borderColor: theme.palette.grey.A400,
            '&:hover': {
                background: theme.palette.grey.A400,
            }
        }
    },
})