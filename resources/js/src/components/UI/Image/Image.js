import React from 'react'
import { useStyles } from './ImageStyles'

const Image = props => {
    const classes = useStyles()
    const { imgSrc, imgAlt } = props

    return (
        <img 
            className={classes.img}
            src={imgSrc} 
            alt={`mosque-match-${imgAlt}`}
        />
    )
}

export default Image