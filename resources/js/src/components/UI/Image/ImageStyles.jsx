import { makeStyles } from '@material-ui/core/styles'

export const useStyles = makeStyles(() => ({
    img: {
        maxWidth: '100%',
        height: 'auto',
        display: 'block'
    }
}))