import React from 'react'
import { IconButton } from '@material-ui/core'

const CustomIconButton = (props) => {
    const { onClick } = props

    return (
        <IconButton
            {...props}
            onClick={onClick}
            size="small"
        >
            {props.children}
        </IconButton>
    )
}

export default CustomIconButton