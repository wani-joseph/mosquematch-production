import React from 'react'
import { Modal, withStyles } from '@material-ui/core'
import { styles } from './ModalStyles'

const CustomModal = ({ show, handleClose, children, classes }) => (
	<Modal
		open={show}
		onClose={handleClose}
		disableEnforceFocus
		disableAutoFocus
		disableRestoreFocus
	>
		<div className={classes.modal}>
			{children}
		</div>
	</Modal>
)

export default withStyles(styles)(CustomModal)