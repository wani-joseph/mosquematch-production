export const styles = theme => ({
    modal: {
        boxShadow: theme.shadows[4],
        width: '90%',
        maxWidth: 600,
        background: theme.palette.common.white,
        borderRadius: theme.shape.borderRadius,
        padding: theme.spacing(2),
        transform: 'translate(-50%, -50%)',
        left: '50%',
        top: '50%',
        position: 'relative',
    }
})