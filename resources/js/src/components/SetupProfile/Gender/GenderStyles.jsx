export const styles = theme =>({
    paper: {
        [theme.breakpoints.up('sm')]: {
            border: '1px solid rgba(0, 0, 0, .08)',
        }
    },
    title: {
        fontWeight: 500,
    },
    label: {
        color: theme.palette.common.black,
        display: 'flex',
        fontWeight: 500,
        marginBottom: theme.spacing(2),
    },
    formLabel: {
        border: `1px solid rgba(0, 0, 0, .08)`,
        padding: '.4rem .8rem',
        width: '100%',
        margin: 0,
        justifyContent: 'center',
        [theme.breakpoints.up('md')]: {
            padding: theme.spacing(1, 2),
        }
    },
    radio: {
        display: 'none',
    },
    checked: {
        background: theme.palette.primary.main,
        color: theme.palette.common.white,
        borderColor: theme.palette.primary.main,
    }
})