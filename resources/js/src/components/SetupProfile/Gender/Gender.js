import React, { Component } from 'react'
import { connect } from 'react-redux'
import { withRouter } from 'react-router-dom'
import {
    Container,
    RadioGroup,
    FormLabel,
    Grid,
    Typography,
    withStyles
} from '@material-ui/core'
import CustomButton from '../../UI/CustomButton/CustomButton'
import CustomPaper from '../../UI/CustomPaper/CustomPaper'
import GenderRadionButton from '../../GenderRadioButton/GenderRadioButton'
import Loader from '../../UI/Loader/Loader'
import * as actions from '../../../store/actions'
import { styles } from './GenderStyles'

class Gender extends Component {
    constructor() {
        super()
        this._isMounted = false
        this.state = {
            gender: 'male'
        }
        this.handleChange = this.handleChange.bind(this)
        this.handleGenderUpdate = this.handleGenderUpdate.bind(this)
    }

    componentDidMount() {
        this._isMounted = true
    }

    componentWillUnmount() {
        this._isMounted = false
    }

    handleGenderUpdate() {
        const { user, nextFields, onMemberUpdate } = this.props
        const { gender } = this.state
        onMemberUpdate(user.identifier, { gender }, () => {
            this.props.history.push(nextFields)
        })
    }

    handleChange(event) {
        if (this._isMounted) {
            this.setState({ gender: event.target.value })
        }
    }

    render() {
        const { loading, classes } = this.props
        const { gender } = this.state

        let pageContent = <Loader />

        if (!loading) pageContent = (
            <Container maxWidth="sm">
                <Typography
                    className={classes.title}
                    variant="h3"
                    component="h1"
                    align="center"
                    gutterBottom
                >
                    Choose your Gender
                </Typography>
                <CustomPaper spacing="true" bottommargin="true" classes={{ root: classes.paper }}>
                    <FormLabel className={classes.label}>I am ...</FormLabel>
                    <RadioGroup
                        defaultValue={gender}
                        aria-label="gender" 
                        name="customized-radios"
                        onChange={this.handleChange}
                    >
                        <Grid container spacing={2}>
                            <Grid item xs={6}>
                                <GenderRadionButton
                                    gender={gender}
                                    value="male"
                                    label="Male"
                                />
                            </Grid>
                            <Grid item xs={6}>
                                <GenderRadionButton
                                    gender={gender}
                                    value="female"
                                    label="Female"
                                />
                            </Grid>
                            <Grid item xs={12}>
                                <CustomButton
                                    onClick={this.handleGenderUpdate}
                                    fullWidth
                                    topmargin="true"
                                >
                                    Next
                                </CustomButton>
                            </Grid>
                        </Grid>
                    </RadioGroup>
                </CustomPaper>
            </Container>
        )

        return (
            <React.Fragment>
                {pageContent}
            </React.Fragment>
        )
    }
}

const mapStateToProps = state => ({
    loading: state.auth.loading,
    user: state.auth.authUser,
})

const mapDispatchToProps = dispatch => ({
    onMemberUpdate: (userId, data, next) => dispatch(actions.updateCurrentUser(userId, data, next))
})

export default connect(mapStateToProps, mapDispatchToProps)(withStyles(styles)(withRouter(Gender)))