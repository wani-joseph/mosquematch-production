import React, { Component } from 'react'
import { connect } from 'react-redux'
import { withRouter } from 'react-router-dom'
import {
    Container,
    withStyles
} from '@material-ui/core'
import _ from 'lodash'
import axios from 'axios'
import Mosque from '../../Mosque/Mosque'
import CustomButton from '../../UI/CustomButton/CustomButton'
import CustomPaper from '../../UI/CustomPaper/CustomPaper'
import CustomTypography from '../../UI/CustomTypography/CustomTypography'
import { styles } from './LinkMosquesStyles'
import * as actions from '../../../store/actions'

class LinkMosques extends Component {
    constructor() {
        super()
        this._isMounted = false
        this.state = {
            loading: false,
            mosques : [],
            linkedMosques: [],
            search : '',
            lat: null,
            lng: null,
        }
        this.getUserLocation = this.getUserLocation.bind(this)
        this.handleSearch = this.handleSearch.bind(this)
        // Attaching Lodash debounce to avoid frequent ajax requests
        this.getMosques = _.debounce(this.getMosques, 500)
        this.linkMosque = this.linkMosque.bind(this)
        this.attachMosques = this.attachMosques.bind(this)
    }

    componentDidMount() {
        this._isMounted = true
        document.getElementById('search').addEventListener('keydown', (e) => {
            // check whether up or down arrow keys are pressed
            if (e.keyCode === 38 || e.keyCode === 40) {
                // To prevent cursor from moving left or right in text input
                // You can only prevent it in keydown event
                // If you only use keyup then event already fired
                e.preventDefault()
            }
        })
        this.getUserLocation()
    }

    componentWillUnmount() {
        this._isMounted = false
    }

    getUserLocation() {
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(position => {
                this.setState({
                    lat: position.coords.latitude,
                    lng: position.coords.longitude
                })
            })
        }
    }

    handleSearch(e) {
        // check whether arrow keys are pressed using Loadash
        if(_.includes([37, 38, 39, 40, 13], e.keyCode) ) {
            if (e.keyCode === 38 || e.keyCode === 40) {
                // To prevent cursor from moving left or right in text input
                e.preventDefault()
            }

            if (e.keyCode === 40 && this.state.mosques == "") {
                // If mosque list is cleared and search input is not empty 
                // then call ajax again on down arrow key press 
                this.getMosques()
                return
            }

            this.selectMosque(e.keyCode)
        } else {
            this.getMosques()
        }
    }

    getMosques() {
        this.setState(() => ({ 
            mosques: [],
            search: this.refs.newSearch.value 
        }))

        if (this.state.search.trim() != '') {
            axios({
                method: 'post',
                url: '/api/mosques-search',
                data: { search : this.state.search },
                headers: {
                    'Authorization': `Bearer ${localStorage.getItem('token')}`
                }
            })
            .then( (response) => {
                if (response.data.data)
                    this.setState(() => ({ mosques : response.data.data }))
            })
            .catch( (error) => {
                console.log(error)
            })  
        }
    }

    linkMosque(checked, mosque) {
        if (this._isMounted) {
            const { linkedMosques } = this.state

            if (!checked) {
                this.setState({ linkedMosques: [ ...linkedMosques, mosque ]})
            } else {
                this.setState({ linkedMosques: linkedMosques.filter(linkedMosque => linkedMosque !== mosque) })
            }
        }
    }

    attachMosques() {
        const { linkedMosques, lat, lng } = this.state
        const { user, onMemberUpdate, onLinkMosques, nextFields } = this.props

        if (linkedMosques.length > 0) {

            if (user.latitude === '' && user.longitude === '') {
                onLinkMosques(user.identifier, linkedMosques, () => {
                    onMemberUpdate(user.identifier, {
                        lat: lat,
                        lng: lng
                    }, () => {
                        this.props.history.push(nextFields)
                    })
                })
            } else {
                onLinkMosques(user.identifier, linkedMosques, () => {
                    onMemberUpdate(user.identifier, {
                        lat: linkedMosques[0].latitude,
                        lng: linkedMosques[0].longtitude
                    }, () => {
                        this.props.history.push(nextFields)
                    })
                })
            }
            
        } else {
            alert('You need to select at least one mosque')
        }
    }

    render() {
        const { classes } = this.props
        const mosques = this.state.mosques.map(mosque => (
            <Mosque
                key={mosque.identifier}
                linkMosque={this.linkMosque}
                mosque={mosque}
            />
        ))

        return (
            <Container maxWidth="sm">
                <CustomTypography
                    bold="true"
                    variant="h3"
                    component="h1"
                    align="center"
                    gutterBottom
                >
                    Link to a Mosque
                </CustomTypography>
                <CustomPaper bottommargin="true" classes={{ root: classes.paper }}>
                    <div className={classes.search}>
                        <input
                            className={classes.searchInput}
                            type="text"
                            autoComplete="off" 
                            onKeyUp={this.handleSearch}
                            id="search"
                            ref="newSearch"
                            placeholder="Enter your mosque name here" 
                        />
                    </div>
                    {this.state.mosques.length > 0 && 
                        <ul className={classes.mosques}>
                            { mosques }
                        </ul>
                    }
                    <div className={classes.nextButton}>
                        <CustomButton
                            fullWidth
                            topmargin="true"
                            onClick={this.attachMosques}
                        >
                            Next
                        </CustomButton>
                    </div>
                </CustomPaper>
            </Container>
        )
    }
}

const mapStateToProps = state => ({
    loading: state.member.loading,
    user: state.auth.authUser,
})

const mapDispatchToProps = dispatch => ({
    onMemberUpdate: (userId, data, next) => dispatch(actions.updateCurrentUser(userId, data, next)),
    onLinkMosques: (userId, mosques, next) => dispatch(actions.linkMosques(userId, mosques, next))
})

export default connect(mapStateToProps, mapDispatchToProps)(withStyles(styles)(withRouter(LinkMosques)))