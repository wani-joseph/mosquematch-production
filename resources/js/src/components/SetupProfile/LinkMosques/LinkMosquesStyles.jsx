export const styles = theme =>({
    paper: {
        [theme.breakpoints.up('sm')]: {
            border: '1px solid rgba(0, 0, 0, .08)',
        }
    },
    mosques: {
        listStyle: 'none',
        padding: 0,
        margin: 0,
        marginBottom: theme.spacing(2)
    },
    search: {
        borderBottom: '1px solid rgba(0, 0, 0, .08)',
        padding: theme.spacing(2)
    },
    searchInput: {
        width: '100%',
        padding: theme.spacing(1),
        outline: 'none',
        border: '1px solid rgba(0, 0, 0, .08)',
        borderRadius: 4,
        font: 'inherit'
    },
    nextButton: {
        padding: theme.spacing(2)
    }
})