import React from 'react'
import { connect } from 'react-redux'
import { withRouter } from 'react-router-dom'
import { Container } from '@material-ui/core'
import ProfileForm from '../ProfileForm/ProfileForm'
import CustomButton from '../UI/CustomButton/CustomButton'
import CustomPaper from '../UI/CustomPaper/CustomPaper'
import CustomTypography from '../UI/CustomTypography/CustomTypography'
import Gender from './Gender/Gender'
import LinkMosques from './LinkMosques/LinkMosques'
import Loader from '../UI/Loader/Loader'
import * as actions from '../../store/actions'

const SetupProfile = ({ page, nextFields, loading, user, onMemberUpdate, history }) => {

    const handleSubmit = (values) => {
        onMemberUpdate(user.identifier, values, () => {
            history.push(nextFields)
        })
    }

    switch(page) {
        case "Gender": return <Gender nextFields={nextFields} />
        case "Link to a Mosque": return <LinkMosques nextFields={nextFields} />
        default: return (
            <Container maxWidth="sm">
                <CustomTypography
                    bold="true"
                    variant="h3"
                    component="h1"
                    align="center"
                    gutterBottom
                >
                    {page}
                </CustomTypography>
                <CustomPaper bottommargin="true">
                    {!loading ? (
                        <ProfileForm
                            page={page}
                            handleSubmit={handleSubmit}
                            submitButton={
                                <CustomButton
                                    type="submit"
                                    fullWidth
                                    topmargin="true"
                                >
                                    Next
                                </CustomButton>
                            }
                        />
                    ) : <Loader />}
                </CustomPaper>
            </Container>
        )
    }
}

const mapStateToProps = state => ({
    loading: state.auth.loading,
    user: state.auth.authUser,
})

const mapDispatchToProps = dispatch => ({
    onMemberUpdate: (userId, data, next) => dispatch(actions.updateCurrentUser(userId, data, next))
})

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(SetupProfile))