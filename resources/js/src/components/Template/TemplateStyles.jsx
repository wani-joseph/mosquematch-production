export const styles = theme =>({
    listItem: {
        display: 'inline-flex',
        justifyContent: 'space-between',
        alignItems: 'center',
        position: 'relative',
        background: theme.palette.grey[400],
        padding: theme.spacing(1, 3),
        borderRadius: 20,
        marginRight: theme.spacing(2),
        marginBottom: theme.spacing(2),
        maxWidth: 260
    },
    delete: {
        display: 'inline-flex',
        marginLeft: theme.spacing(1),
        cursor: 'pointer',
    },
})