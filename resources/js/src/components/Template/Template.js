import React from 'react'
import {
	Typography,
	withStyles
} from '@material-ui/core'
import CloseIcon from '@material-ui/icons/Close'
import { styles } from './TemplateStyles'

const Template = ({ userTemplate, handleDeleteTemplate, handleSetTemplate, classes }) => (
	<li className={classes.listItem}>
		<Typography
			onClick={handleSetTemplate? () => handleSetTemplate(userTemplate.template) : null}
			variant="body2"
			noWrap
		>
			{userTemplate.template}
		</Typography>
		{handleDeleteTemplate ? (
			<span className={classes.delete}>
				<CloseIcon onClick={() => handleDeleteTemplate(userTemplate.identifier)} />
			</span>
		): null}
	</li>
)

export default withStyles(styles)(Template)