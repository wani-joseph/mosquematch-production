import { makeStyles } from '@material-ui/core/styles';

export const useStyles = makeStyles(theme =>({
    slides: {
        width: '100%',
        marginTop: theme.spacing(3),
        paddingTop: theme.spacing(3)
    },
    slide: {
        padding: theme.spacing(2),
    },
    fullStory: {
        display: 'block',
        fontSize: '1rem',
        fontWeight: 500,
        marginTop: theme.spacing(2),
        color: theme.palette.primary.main,
        textDecoration: 'none',
    }
}));