import React from 'react'
import Slider from 'react-slick'
import { Link } from 'react-router-dom'
import SectionSubTitle from '../SectionSubTitle/SectionSubTitle'
import Paragraph from '../Paragraph/Paragraph'
import { useStyles } from './TestimonialStyles'
import NextArrow from '../UI/Carousel/NextArrow'
import PrevArrow from '../UI/Carousel/PrevArrow'

const Testimonial = () => {
    const classes = useStyles()

    const settings = {
        dots: false,
        infinite: true,
        speed: 500,
        slidesToShow: 4,
        slidesToScroll: 1,
        className: classes.slides,
        nextArrow: <NextArrow />,
        prevArrow: <PrevArrow />
    }

    return (
        <Slider {...settings}>
            <div className={classes.slide}>
                <SectionSubTitle title="Omar"/>
                <Paragraph>
                Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                </Paragraph>
                <Link to="#" className={classes.fullStory}>Read full story</Link>
            </div>
            <div className={classes.slide}>
                <SectionSubTitle title="Omar"/>
                <Paragraph>
                Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                </Paragraph>
                <Link to="#" className={classes.fullStory}>Read full story</Link>
            </div>
            <div className={classes.slide}>
                <SectionSubTitle title="Omar"/>
                <Paragraph>
                Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                </Paragraph>
                <Link to="#" className={classes.fullStory}>Read full story</Link>
            </div>
            <div className={classes.slide}>
                <SectionSubTitle title="Omar"/>
                <Paragraph>
                Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                </Paragraph>
                <Link to="#" className={classes.fullStory}>Read full story</Link>
            </div>
            <div className={classes.slide}>
                <SectionSubTitle title="Omar"/>
                <Paragraph>
                Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                </Paragraph>
                <Link to="#" className={classes.fullStory}>Read full story</Link>
            </div>
        </Slider>
    )
}

export default Testimonial