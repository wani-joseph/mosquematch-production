import { makeStyles } from '@material-ui/core/styles'

export const useStyles = makeStyles(theme =>({
    openModel: {
        color: 'inherit',
        border: 'none',
        outline: 'none',
    },
    highlight: {
        color: theme.palette.common.black,
        textTransform: 'capitalize',
        margin: theme.spacing(0, 1),
        padding: 0,
        background: 'transparent',
        font: 'inherit',
        fontWeight: 500,
        cursor: 'pointer',
    },
    actionButton: {
        display: 'inline-flex',
        width: 38,
        height: 38,
        justifyContent: 'center',
        alignItems: 'center',
        background: theme.palette.grey[400],
        borderRadius: 2,
        cursor: 'pointer'
    },
    addNewButton: {
        minWidth: 80,
        textTransform: 'uppercase',
        padding: '.2rem .6rem',
        fontWeight: 500,
        border: `2px solid ${theme.palette.secondary.main}`,
        color: theme.palette.secondary.main,
        borderRadius: theme.shape.borderRadius,
        cursor: 'pointer',
        '&:hover': {
            color: theme.palette.common.white,
            background: theme.palette.secondary.main,
        }
    }
}))