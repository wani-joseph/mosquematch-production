import React from 'react'
import { useStyles } from './ToggleModalStyles'

const ToggleModal = props => {
    const classes = useStyles()

    let toggleModalClasses = [classes.openModel]

    if (props.highlights === "true")
        toggleModalClasses.push(classes.highlight)

    if (props.viewBio === "true")
        toggleModalClasses.push(classes.viewBio)

    if (props.action === "true") {
        toggleModalClasses.push(classes.actionButton)
    }

    if (props.addNew === "true") {
        toggleModalClasses.push(classes.addNewButton)
    }

    return (
        <button
            className={toggleModalClasses.join(' ')}
            onClick={props.onClick}
        >
            {props.label}
        </button>
    )
}

export default ToggleModal