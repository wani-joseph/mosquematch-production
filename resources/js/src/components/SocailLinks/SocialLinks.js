import React from 'react'
import { List } from '@material-ui/core'
import FooterLink from '../FooterLink/FooterLink'
import { useStyles } from './SocialLinksStyles'

const SocialLinks = () => {
    const classes = useStyles()

    return (
        <List>
            <FooterLink
                classes={{ root: classes.root }}
                link="/"
                text="Facebook"
            />
            <FooterLink
                classes={{ root: classes.root }}
                link="/"
                text="Twitter"
            />
            <FooterLink
                classes={{ root: classes.root }}
                link="/"
                text="YouTube"
            />
        </List>
    )
}

export default SocialLinks