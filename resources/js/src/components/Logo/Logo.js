import React from 'react'
import { Link } from 'react-router-dom'
import SideDrawer from '../SideDrawer/SideDrawer'
import { useStyles } from './LogoStyles'

const Logo = props => {
    const classes = useStyles()
    const { dashboard } = props

    let logoClasses = [classes.logo]

    if (dashboard == "true")
        logoClasses.push(classes.dashboard)

    return (
        <React.Fragment>
            {props.sideDrawer
                ?<div className={classes.siteLogo}>
                    <SideDrawer />
                    <Link to="/" className={logoClasses.join(' ')}><span className={classes.logoText}>Mosque</span><span className={classes.logoText}>Match</span></Link>
                </div>
                : <Link to="/" className={logoClasses.join(' ')}><span className={classes.logoText}>Mosque</span><span className={classes.logoText}>Match</span></Link>}
        </React.Fragment>
    )
}

export default Logo