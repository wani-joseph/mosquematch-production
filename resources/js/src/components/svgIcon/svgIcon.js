import React from 'react'

const svgIcon = props => {
    const { width, height, src } = props
    return (
        <svg
            width={width}
            height={height}
        >
            <use xlinkHref={`/sprite-sheet.svg#${src}`} />
        </svg>
    )
}

export default svgIcon