import React from 'react'
import { connect } from 'react-redux'
import Moment from 'react-moment'
import { Typography } from '@material-ui/core'
import { useStyles } from './WaliRequestStyles'

const WaliRequest = ({ user, waliRequest, confirm, deny, update, filter }) => {
    const classes = useStyles()

    return (
        <li className={classes.waliRequest}>
            <div className={classes.info}>
                <div className={classes.thumb}>
                    <img
                        className={classes.img}
                        src={`/images/${waliRequest.requester.data.avatar}`}
                        alt={`mosque-match-${waliRequest.requester.data.fname}`}
                    />
                </div>
                <div className={classes.description}>
                    <div className={classes.heading}>
                        <Typography className={classes.name}>
                            {waliRequest.requester.data.fname}, 24
                        </Typography>
                        <Typography className={classes.time} color="textSecondary">
                            <Moment fromNow>{waliRequest.creationDate}</Moment>
                        </Typography>
                    </div>
                    
                    <Typography className={classes.text}>
                        {user.gender == "femlae"
                            ? "Requested your wali details"
                            : "Invites you to speak to her wali"}
                    </Typography>
                </div>
            </div>
            {waliRequest.status == "pending" ? (
                <div className={classes.actions}>
                    <button
                        className={classes.actionButton}
                        onClick={() => {confirm(waliRequest.identifier, update)}}
                    >
                        Confirm
                    </button>
                    <button
                        className={classes.actionButton}
                        onClick={() => {deny(waliRequest.identifier, filter)}}
                    >
                        Deny
                    </button>
                </div>
            ): null}
        </li>
    )
}

const mapStateToProps = state => ({
    user: state.auth.authUser
})

export default connect(mapStateToProps)(WaliRequest)