import { makeStyles } from '@material-ui/core/styles'

export const useStyles = makeStyles(theme =>({
    waliRequest: {
        display: 'flex',
        flexDirection: 'column',
        marginBottom: theme.spacing(2),
        '&:last-of-type': {
            marginBottom: 0
        },
        [theme.breakpoints.up('sm')]: {
            flexDirection: 'row',
            alignItems: 'center',
        },
    },
    info: {
        display: 'flex',
        alignItems: 'center',
        [theme.breakpoints.up('sm')]: {
            marginRight: theme.spacing(2),
        },
    },
    thumb: {
        borderRadius: theme.shape.borderRadius,
        width: 64,
        height: 64,
        overflow: 'hidden',
        background: theme.palette.grey[700],
        marginRight: theme.spacing(2),
    },
    img: {
        maxWidth: '100%',
        height: 'auto',
        display: 'block',
    },
    description: {
        flex: 1,
        [theme.breakpoints.up('sm')]: {
            minWidth: 260,
        },
    },
    heading: {
        display: 'flex',
        justifyContent: 'space-between',
        alignItems: 'center',
    },
    name: {
        fontWeight: 500,
    },
    time: {
        fontSize: 13,
        fontWeight: 500,
        textTransform: 'capitalize',
    },
    text: {
        fontSize: 14,
    },
    actions: {
        display: 'flex',
        justifyContent: 'flex-end'
    },
    actionButton: {
        background: theme.palette.secondary.main,
        color: theme.palette.common.white,
        outline: 'none',
        border: 'none',
        marginRight: theme.spacing(1),
        padding: '4px 16px',
        borderRadius: 2,
        cursor: 'pointer',
        '&:last-of-type': {
            background: theme.palette.primary.main,
            marginRight: 0,
        },
    }
}))