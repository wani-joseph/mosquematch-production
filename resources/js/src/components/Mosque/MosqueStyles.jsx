export const styles = theme => ({
    mosque: {
        borderBottom: '1px solid rgba(0, 0, 0, .08)',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'space-between',
        padding: theme.spacing(2),
    },
})