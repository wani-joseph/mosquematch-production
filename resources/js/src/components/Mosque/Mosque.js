import React from 'react'
import { Checkbox, withStyles } from '@material-ui/core'
import CustomTypography from '../UI/CustomTypography/CustomTypography'
import { styles } from './MosqueStyles'

const Mosque = ({ mosque, linkMosque, classes }) => {
    const [checked, setChecked] = React.useState(false)
    const { mosqueName, streetAddress } = mosque

    const handleChange = event => {
        setChecked(event.target.checked, linkMosque(checked, mosque))
    }

    return (
        <li className={classes.mosque}>
            <div>
                <CustomTypography
                    bold="true"
                    variant="h5"
                    component="p"
                >
                    {mosqueName}
                </CustomTypography>
                <CustomTypography
                    variant="body2"
                    color="textSecondary"
                >
                    {streetAddress}
                </CustomTypography>
            </div>
            <Checkbox checked={checked} onChange={handleChange} />
        </li>
    )
}

export default withStyles(styles)(Mosque)