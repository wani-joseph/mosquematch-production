import React from 'react';
import { useStyles } from './PageContentStyles';

const PageContent = props => {
    const classes = useStyles()
    const { nomargin } = props
    let pageContentClasses = [classes.pageContent]

    if (nomargin == "true")
        pageContentClasses.push(classes.nomargin)
    
    return (
        <main className={pageContentClasses.join(' ')}>
            {props.children}
        </main>
    );
};

export default PageContent