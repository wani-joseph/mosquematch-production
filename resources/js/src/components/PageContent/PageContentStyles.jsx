import { makeStyles } from '@material-ui/core/styles'

export const useStyles = makeStyles(theme =>({
    pageContent: {
        marginTop: theme.spacing(9),
        [theme.breakpoints.up('md')]: {
            marginTop: theme.spacing(12),
        },
    },
    nomargin: {
        marginTop: 0,
    },
}))