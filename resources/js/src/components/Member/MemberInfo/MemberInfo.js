import React from 'react'
import { connect } from 'react-redux'
import Moment from 'react-moment'
import { Typography } from '@material-ui/core'
import { Grid, withStyles } from '@material-ui/core'
import MemberActions from '../MemberActions/MemberActions'
import CustomDividor from '../../UI/CustomDividor/CustomDividor'
import CustomTypography from '../../UI/CustomTypography/CustomTypography'
import {
    calculateAge,
    getDistanceFromLatLonInKm
} from '../../../shared/utility'
import { useStyles } from './MemberInfoStyles'

const MemberInfo = props => {
    const classes = useStyles()
    const {
        user,
        identifier,
        fname,
        tagline,
        status,
        modifiedDate,
        dayOfBirth,
        monthOfBirth,
        yearOfBirth,
        lat,
        lng,
        mosques,
        eduLevel,
        profession,
        sector,
        height,
        prayer,
        keepHalal,
        smoke,
        drink,
        bio,
        seeking,
        citizenship,
        countryOrigin
    } = props

    return (
        <React.Fragment>
            <Grid container justify="space-between" alignItems="center">
                <Grid item lg={9}>
                    <CustomTypography bold="true" gutterBottom noWrap>
                        {tagline}
                    </CustomTypography>
                </Grid>
                <Grid item lg={3}>
                    {status == "online" ? (
                        <CustomTypography
                            bold="true"
                            color="textSecondary"
                            variant="body2"
                            align="right"
                            gutterBottom
                        >
                            online now
                        </CustomTypography>
                    ) : (
                        <CustomTypography
                            bold="true"
                            color="textSecondary"
                            variant="body2"
                            align="right"
                            gutterBottom
                        >
                            active {<Moment fromNow>{modifiedDate}</Moment>}
                        </CustomTypography>
                    )} 
                </Grid>
            </Grid>
            <MemberActions
                identifier={identifier}
                fname={fname}
                bio={bio}
                seeking={seeking}
                age={calculateAge(new Date(yearOfBirth, monthOfBirth, dayOfBirth))}
            />
            <div className={classes.location}>
                <img
                    className={classes.flag}
                    src={`https://www.countryflags.io/${citizenship}/flat/24.png`}
                    alt={`mosque-match-${fname}`}
                />
                <img
                    className={classes.flag}
                    src={`https://www.countryflags.io/${countryOrigin}/flat/24.png`}
                    alt={`mosque-match-${fname}`}
                />
                <CustomDividor orientation="vertical" />
                <Typography variant="body2" color="textPrimary" noWrap>
                    {user.latitude && user.longtitude && lat && lng
                        ? `${user.location}, ${getDistanceFromLatLonInKm(user.latitude, user.longtitude, lat, lng)} km`
                        : 'London, 2km'}
                </Typography>
                <CustomDividor orientation="vertical" />
                <Typography className={classes.mosqueName} variant="body2" noWrap>
                    {mosques.data != "" ? mosques.data[0].mosqueName : 'London Central Mosque'}
                </Typography>
            </div>
            <Typography variant="body2" color="textSecondary">
                {`${eduLevel} - ${sector} - ${profession} - ${height} - ${prayer}`}
            </Typography>
            <Typography variant="body2" color="textSecondary">
                {`${keepHalal} - ${smoke} - ${drink}`}
            </Typography>
            <Typography variant="body2" color="textSecondary">
                {`seeking ${seeking} - will move abroad`}
            </Typography>
            <Typography variant="body2" color="textSecondary">
                Annual Income: £600,000
            </Typography>
        </React.Fragment>
    )
}

const mapDispatchToProps = state => ({
    user: state.auth.authUser
})

export default connect(mapDispatchToProps)(MemberInfo)