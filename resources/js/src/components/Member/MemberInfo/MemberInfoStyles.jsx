import { makeStyles } from '@material-ui/core/styles'

export const useStyles = makeStyles(theme => ({
    heading: {
        display: 'flex',
        justifyContent: 'space-between',
        alignItems: 'flex-start',
        maxWidth: 620,
    },
    tagline: {
        flex: '50%'
    },
    location: {
        display: 'flex',
        justifyContent: 'flex-start',
        alignItems: 'center',
        marginBottom: theme.spacing(1),
    },
    flag: {
        display: 'block',
        marginRight: theme.spacing(1),
        '&:last-of-type': {
            marginRight: 0
        }
    },
    mosqueName: {
        color: 'blue'
    },
    actions: {
        display: 'flex',
    }
}))