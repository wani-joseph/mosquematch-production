import React from 'react'
import Slider from 'react-slick'
import FiberManualRecordIcon from '@material-ui/icons/FiberManualRecord'
import { useStyles } from './MemberPhotosStyles'

import privateMale from '../../../assets/images/mosquematch-male-private.png'
import privateFemale from '../../../assets/images/mosquematch-female-private.png'

const MemberPhotos = props => {
    const classes = useStyles()
    const {
        avatar,
        fname,
        lname,
        gender,
        status,
        albums
    } = props

    let defaultImg = '/images/male-placeholder.jpg'
    let defaultPrivateImage = privateMale
    let statusClasses = [classes.status]
    const settings = {
        dots: true,
        infinite: true,
        speed: 500,
        slidesToShow: 1,
        slidesToScroll: 1
    }

    if (gender == 'female') defaultImg = '/images/female-placeholder.jpg'
    if (gender == 'female') defaultPrivateImage = privateFemale
    if (status == "online") statusClasses.push(classes.online)

    const publicAlbum = albums.data.filter(album => album.title === 'public')

    if (publicAlbum.length > 0) {
        
        if (publicAlbum[0].images.data.length > 0) {
            return (
                <div className={classes.thumb}>
                    <Slider {...settings}>
                        {publicAlbum[0].images.data.map(slide => (
                            <img
                                key={slide.identifier}
                                className={classes.slide}
                                src={`/images/${slide.title}`}
                                alt={`mosque-match-${fname}-${lname}`}
                            />
                        ))}
                    </Slider>
                    <div className={statusClasses.join(' ')}>
                        <FiberManualRecordIcon />
                    </div>
                </div>
            )
        }
    } else {
        const privateAlbum = albums.data.filter(album => album.title === 'private')

        if (privateAlbum.length > 0) {

            if (privateAlbum[0].images.data.length > 0) {
                return (
                    <div className={classes.thumb}>
                        <Slider {...settings}>
                            {privateAlbum[0].images.data.map(slide => (
                                <img
                                    key={slide.identifier}
                                    className={classes.slide}
                                    src={defaultPrivateImage}
                                    alt={`mosque-match-${fname}-${lname}`}
                                />
                            ))}
                        </Slider>
                        <div className={statusClasses.join(' ')}>
                            <FiberManualRecordIcon />
                        </div>
                    </div>
                )
            }
        }
    }

    return (
        <div className={classes.thumb}>
            <img
                className={classes.img}
                src={avatar ? `/images/${avatar}` : defaultImg}
                alt={`mosque-match-${fname}-${lname}`}
            />
            <div className={statusClasses.join(' ')}>
                <FiberManualRecordIcon />
            </div>
        </div>
    )
}

export default MemberPhotos