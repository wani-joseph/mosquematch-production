import { makeStyles } from '@material-ui/core/styles'

export const useStyles = makeStyles(theme => ({
    thumb: {
        display: 'block',
        position: 'relative',
        height: 200,
        overflow: 'hidden',
        borderRadius: 10,
        // marginRight: theme.spacing(2),
        [theme.breakpoints.up('sm')]: {
            width: 184,
            height: 'auto',
        },
        [theme.breakpoints.up('lg')]: {
            minWidth: 206,
            height: 'auto',
        },
    },
    img: {
        display: 'block',
        width: '100%',
        height: 'auto',
    },
    status: {
        position: 'absolute',
        bottom: theme.spacing(1),
        right: theme.spacing(1),
        textAlign: 'end',
        color: theme.palette.grey[400]
    },
    online: {
        color: '#39CE48'
    },
    slide: {
        borderRadius: 10,
        display: 'block',
        position: 'relative'
    },
    privateImg: {
        position: 'absolute',
        width: '100%',
        height: '100%'
    }
}))