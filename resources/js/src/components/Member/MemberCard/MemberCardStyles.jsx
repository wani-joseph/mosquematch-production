export const styles = theme => ({
    member: {
        background: theme.palette.common.white,
        border: '1px solid rgba(0, 0, 0, .08)',
        padding: theme.spacing(2),
        marginTop: -1,
    },
    memberInfo: {
        paddingLeft: theme.spacing(2)
    },
    root: {
        flexGrow: 1,
    },
    paper: {
        padding: theme.spacing(2),
        margin: 'auto',
    },
    img: {
        margin: 'auto',
        display: 'block',
        maxWidth: '100%',
        maxHeight: '100%',
    },
})