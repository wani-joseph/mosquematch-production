import React from 'react'
import { Grid, withStyles } from '@material-ui/core'
import MemberPhotos from '../MemberPhotos/MemberPhotos'
import MemberInfo from '../MemberInfo/MemberInfo'
import MemberTags from '../MemberTags/MemberTags'
import { styles } from './MemberCardStyles'

const MemberCard = ({ member, setCardMarkerHover, resetCardMarkerHover, classes }) => (
	<Grid
		container alignItems="center"
		className={classes.member}
		onMouseEnter={setCardMarkerHover ? e => setCardMarkerHover(member) : null}
		onMouseLeave={resetCardMarkerHover ? e => resetCardMarkerHover() : null}
	>
		<Grid item lg={3}>
			<div className={classes.image}>
				<MemberPhotos
					identifier={member.identifier}
					avatar={member.avatar}
					fname={member.fname}
					lname={member.lname}
					gender={member.gender}
					status={member.status}
					albums={member.albums}
				/>
			</div>
		</Grid>
		<Grid item lg={9}>
			<div className={classes.memberInfo}>
				<MemberInfo
					identifier={member.identifier}
					fname={member.fname}
					lname={member.lname}
					tagline={member.tagline}
					dayOfBirth={member.dayOfBirth}
					monthOfBirth={member.monthOfBirth}
					yearOfBirth={member.yearOfBirth}
					lat={member.latitude}
					lng={member.longtitude}
					mosques={member.mosques}
					eduLevel={member.eduLevel}
					profession={member.profession}
					sector={member.sector}
					height={member.height}
					prayer={member.prayer}
					keepHalal={member.keepHalal}
					smoke={member.smoke}
					drink={member.drink}
					bio={member.bio}
					seeking={member.seeking}
					status={member.status}
					modifiedDate={member.modifiedDate}
					citizenship={member.citizenship}
					countryOrigin={member.countryOrigin}
				/>
				<MemberTags />
			</div>
		</Grid>
	</Grid>
)

export default withStyles(styles)(MemberCard)