import React from 'react'
import { connect } from 'react-redux'
import { checkFavorite } from '../../../../shared/utility'
import SvgIcon from '../../../svgIcon/svgIcon'
import ActionButton from '../../../UI/ActionButton/ActionButton'
import * as actions from '../../../../store/actions'

const Favorite = ({ memberId, user, favoriteList, handleFavorites }) => (
  <ActionButton
    onClick={() => handleFavorites(user.identifier, memberId, favoriteList)}
    title="Favorite"
    label={
      checkFavorite(favoriteList, memberId)
        ? <SvgIcon width="100%" height="100%" src="favorite" />
        : <SvgIcon width="100%" height="100%" src="heart" />
    }
  />
)

const mapStateToProps = state => ({
  user: state.auth.authUser,
  favoriteList: state.auth.favlist
})

const mapDispatchToProps = dispatch => ({
  handleFavorites: (userId, memberId, favoriteList) => dispatch(actions.addToFavorite(userId, memberId, favoriteList))
})

export default connect(mapStateToProps, mapDispatchToProps)(Favorite)