import React from 'react'
import { connect } from 'react-redux'
import SvgIcon from '../../../svgIcon/svgIcon'
import ActionButton from '../../../UI/ActionButton/ActionButton'
import CustomModal from '../../../UI/CustomModal/CustomModal'
import CustomTextField from '../../../UI/CustomTextField/CustomTextField'
import CustomTypography from '../../../UI/CustomTypography/CustomTypography'
import * as actions from '../../../../store/actions'

const Question = ({ question, action }) => (
  <>
    <CustomTypography
      bold="true"
      variant="h6"
      component="p"
      gutterBottom
    >
      {question}
    </CustomTypography>
    {action ? (
      <CustomTextField
        id="report"
        name="report"
        placeholder="Type your report here..."
        fullWidth
        multiline
        rows="3"
      />
    ) : null}
    <CustomTypography id="confirm" variant="body1" color="textSecondary" />
  </>
)

const RequestAction = ({ action, memberId, user, onUserAction }) => {
  const [modal, setModal] = React.useState(false)

  const openModal = () => {
    setModal(true)
  }

  const closeModal = () => {
    setModal(false)
  }

  const setActionButtonLabel = () => {
    switch(action) {
      case 'Photo': return <SvgIcon width="100%" height="100%" src="photos" />
      case 'Report': return <SvgIcon width="100%" height="100%" src="report" />
      case 'Block': return <SvgIcon width="100%" height="100%" src="block" />
      default: return <SvgIcon width="100%" height="100%" src="wali" />
    }
  }

  const setModalTitle = () => {
    switch(action) {
      case 'Photo': return 'Request Private Photos'
      case 'Report': return 'Report Member'
      case 'Block': return 'Block Member'
      default: return 'Request Wali Details'
    }
  }

  const setRequestConfirmation = () => {
    switch(action) {
      case 'Photo': return <Question question="Are you sure you want to request private photos?"/>
      case 'Report': return (
        <Question
          question="Are you sure you want to report this member?"
          action={action}
        />
      )
      case 'Block': return <Question question="Are you sure you want to block this member?" />
      default: return <Question question="Are you sure you want to request wali details of this member?" />
    }
  }

  const blockMember = () => {
    const url = `/api/users/${user.identifier}/blocked`
    const data = { blocked_id: parseInt(memberId) }

    onUserAction(url, data, () => {
      document.querySelector('#confirm').innerHTML = 'Your successfully blocked this member'
      setTimeout(() => {
        setModal(false)
      }, 1000)
    })
  }

  const requestPrivatePhoto = () => {
    const url = `/api/users/${user.identifier}/photo-requested`
    const data = { requested_id: parseInt(memberId) }

    onUserAction(url, data, () => {
      document.querySelector('#confirm').innerHTML = 'Your request for wali details has been sent'
      setTimeout(() => {
        setModal(false)
      }, 1000)
    })
  }

  const reportMember = () => {
    const url = `/api/users/${user.identifier}/reported`
    const data = {
      reported_id: memberId,
      message: document.querySelector('#report').value
    }

    onUserAction(url, data, () => {
      document.querySelector('#report').value = ""
      document.querySelector('#confirm').innerHTML = 'Your report has been successfully send to mosquematch team, and it will consider seriously'
      setTimeout(() => {
        setModal(false)
      }, 1000)
    })
  }

  const requestWaliDetails = () => {
    const url = `/api/users/${user.identifier}/wali-requested`
    const data = { requested_id: parseInt(memberId) }

    onUserAction(url, data, () => {
      document.querySelector('#confirm').innerHTML = 'Your request for wali details has been sent'
      setTimeout(() => {
        setModal(false)
      }, 1000)
    })
  }

  const confirmRequest = () => {
    switch(action) {
      case 'Photo': return requestPrivatePhoto()
      case 'Report': return reportMember()
      case 'Block': return blockMember()
      default: return requestWaliDetails()
    }
  }

  return (
    <>
      <ActionButton
        onClick={openModal}
        title={setModalTitle()}
        label={setActionButtonLabel()}
      />
      <CustomModal
        show={modal}
        handleClose={closeModal}
        title={setModalTitle()}
        content={setRequestConfirmation()}
        confirmation={confirmRequest}
      />
    </>
  )
}

const mapStateToProps = state => ({
  user: state.auth.authUser,
})

const mapDispatchToProps = dispatch => ({
  onUserAction: (url, data, next) => dispatch(actions.userAction(url, data, next))
})

export default connect(mapStateToProps, mapDispatchToProps)(RequestAction)