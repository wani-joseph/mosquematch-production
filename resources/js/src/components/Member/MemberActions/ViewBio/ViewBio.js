import React from 'react'
import { connect } from 'react-redux'
import { withStyles } from '@material-ui/core'
import { checkViewed } from '../../../../shared/utility'
import CustomModal from '../../../UI/CustomModal/CustomModal'
import CustomTypography from '../../../UI/CustomTypography/CustomTypography'
import { styles } from '../MemberActionsStyles'
import * as actions from '../../../../store/actions'

const ViewBio = ({ memberId, bio, seeking, user, viewedList, addToViewList, classes }) => {
  const [modal, setModal] = React.useState(false)

  const openModal = () => {
    setModal(true)
  }

  const closeModal = () => {
    setModal(false)
  }

  const addToViewedListHandler = () => {
    openModal()
    const isViewed = checkViewed(viewedList, memberId)

    if (!isViewed) {
      addToViewList(user.identifier, memberId);
    }
  }

  return (
    <>
      <CustomModal
        show={modal}
        handleClose={closeModal}
        title="Biography"
        content={
          <>
            <CustomTypography
              bold="true"
              variant="h6"
              component="p"
              gutterBottom
            >
              A little bit about me
            </CustomTypography>
            <CustomTypography
              variant="body2"
              gutterBottom
              color="textSecondary"
              classes={{ root: classes.bio }}
            >
              {bio}
            </CustomTypography>
            <CustomTypography
              bold="true"
              variant="h6"
              component="p"
              gutterBottom
            >
              What I'm looking for
            </CustomTypography>
            <CustomTypography
              variant="body2"
              gutterBottom
              color="textSecondary"
            >
              {seeking}
            </CustomTypography>
          </>
        }
      />
      <button
        className={classes.viewBio}
        onClick={addToViewedListHandler}
        title="View Biography"
      >
        View Bio
      </button>
    </>
  )
}

const mapStateToProps = state => ({
  user: state.auth.authUser,
  viewedList: state.auth.viewedList
})

const mapDispatchToProps = dispatch => ({
  addToViewList: (userId, memberId) => dispatch(actions.addToViewList(userId, memberId))
})

export default connect(mapStateToProps, mapDispatchToProps)(withStyles(styles)(ViewBio))