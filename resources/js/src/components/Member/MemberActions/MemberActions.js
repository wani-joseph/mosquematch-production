import React from 'react'
import { Grid, withStyles } from '@material-ui/core'
import Favorite from './Favorite/Favorite'
import Message from './Message/Message'
import RequestAction from './RequestAction/RequestAction'
import ViewBio from './ViewBio/ViewBio'
import SvgIcon from '../../svgIcon/svgIcon'
import CustomTypography from '../../UI/CustomTypography/CustomTypography'
import { styles } from './MemberActionsStyles'

const MemberActions = ({ identifier, fname, age, bio, seeking, classes }) => (
	<Grid container>
		<Grid item lg={9}>
			<CustomTypography bold="true" gutterBottom classes={{ root: classes.actions }}>
				{fname}, {age}
				<span className={classes.dividor}>
				<SvgIcon width="100%" height="100%" src="line" />
				</span>
				<Favorite memberId={identifier} />
				<Message memberId={identifier} />
				<RequestAction memberId={identifier} action="Photo" />
				<RequestAction memberId={identifier} action="Block" />
				<RequestAction memberId={identifier} action="Report" />
				<RequestAction memberId={identifier} />
				<span className={classes.dividor}>
				<SvgIcon width="100%" height="100%" src="line" />
				</span>
				<ViewBio memberId={identifier} bio={bio} seeking={seeking} />
			</CustomTypography>
		</Grid>
		<Grid item lg={3}>
			<CustomTypography
				classes={{ root: classes.replies }}
				variant="body2"
				align="right"
				gutterBottom
				color="textSecondary"
			>
				replies within 3 days
			</CustomTypography>
		</Grid>
	</Grid>
)

export default withStyles(styles)(MemberActions)