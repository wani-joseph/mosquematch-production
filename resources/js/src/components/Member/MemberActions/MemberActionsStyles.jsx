export const styles = theme => ({
    memberActions: {
        display: 'flex',
        justifyContent: 'space-between',
        alignItems: 'flex-start',
        marginBottom: theme.spacing(1),
    },
    actions: {
        display: 'inline-flex',
        justifyContent: 'flex-start',
        alignItems: 'center',
    },
    replies: {
        fontStyle: 'italic'
    },
    templates: {
        borderBottom: '1px solid rgba(0, 0, 0, .08)',
        margin: 0,
        marginBottom: theme.spacing(2),
        padding: 0
    },
    actionButtons: {
        display: 'flex',
        justifyContent: 'flex-end',
        marginTop: theme.spacing(2)
    },
    button: {
        textTransform: 'capitalize',
        marginLeft: theme.spacing(2),
        marginTop: 0,
        padding: theme.spacing(1, 3),
        '&:first-of-type': {
            background: theme.palette.grey[700],
            borderColor: theme.palette.grey.A400,
            '&:hover': {
                background: theme.palette.grey.A400,
            }
        }
    },
    viewBio: {
        outline: 'none',
        border: 'none',
        background: theme.palette.primary.main,
        color: theme.palette.common.white,
        borderRadius: 2,
        padding: `4px 16px`,
        cursor: 'pointer',
        fontSize: 12
    },
    bio: {
        marginBottom: theme.spacing(6)
    },
    dividor: {
        color: 'inherit',
        border: 'none',
        display: 'inline-block',
        width: 1,
        height: 22,
        background: 'transparent',
        cursor: 'pointer',
        outline: 'none',
        marginLeft: 12,
        '&:last-of-type': {
            marginLeft: 0,
            marginRight: 12
        }
    }
})