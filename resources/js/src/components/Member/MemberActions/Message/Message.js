import React from 'react'
import { connect } from 'react-redux'
import { withStyles } from '@material-ui/core'
import SvgIcon from '../../../svgIcon/svgIcon'
import Template from '../../../Template/Template'
import ActionButton from '../../../UI/ActionButton/ActionButton'
import CustomButton from '../../../UI/CustomButton/CustomButton'
import CustomModal from '../../../UI/CustomModal/CustomModal'
import CustomTextField from '../../../UI/CustomTextField/CustomTextField'
import CustomTypography from '../../../UI/CustomTypography/CustomTypography'
import { styles } from '../MemberActionsStyles'
import * as actions from '../../../../store/actions'

const Message = ({ memberId, user, templates, addNewTemplate, deleteTemplate, onSendMessage, classes }) => {
  const [modal, setModal] = React.useState(false)

  const openModal = () => {
    setModal(true)
  }

  const closeModal = () => {
    setModal(false)
  }

  const handleNewTemplate = () => {
    const template = document.querySelector('#message').value

    addNewTemplate(user.identifier, template)
  }

  const handleDeleteTemplate = (template) => {
    deleteTemplate(user.identifier, template)
  }

  const handleSetTemplate = (template) => {
    document.querySelector('#message').value = template
  }

  const handleNewMessage = () => {
    const message = document.querySelector('#message').value

    onSendMessage(user.identifier, memberId, message, () => {
      document.querySelector('#message').value = ""
      document.querySelector('#confirm').innerHTML = 'Your message have been send successfully, press X button to close the popup, or send another one.'
    })
  }

  let messagesTemplates = (
    <CustomTypography variant="body1">
      Your saved templates will apear here...
    </CustomTypography>
  )

  if (templates.length > 0) {
    messagesTemplates = templates.map(template => (
      <Template
        key={template.identifier}
        handleNewTemplate={handleNewTemplate}
        handleDeleteTemplate={handleDeleteTemplate}
        handleSetTemplate={handleSetTemplate}
        userTemplate={template}
      />
    ))
  }

  return (
    <>
      <ActionButton
        onClick={openModal}
        title="Send message"
        label={<SvgIcon width="100%" height="100%" src="message" />}
      />
      <CustomModal
        show={modal}
        handleClose={closeModal}
        title="New Message"
        content={
          <>
            <CustomTypography
              bold="true"
              variant="h6"
              component="p"
              gutterBottom
            >
              Templates
            </CustomTypography>
            <ul className={classes.templates}>
              {messagesTemplates}
            </ul>
            <CustomTypography
              bold="true"
              variant="h6"
              component="p"
              gutterBottom
            >
              Message
            </CustomTypography>
            <CustomTextField
              id="message"
              name="message"
              placeholder="Type your message here..."
              fullWidth
              multiline
              rows="2"
            />
            <CustomTypography id="confirm" variant="body1" color="textSecondary" />
            <div className={classes.actionButtons}>
              <CustomButton
                classes={{ root: classes.button }}
                onClick={handleNewTemplate}
              >
                Save as Template
              </CustomButton>
              <CustomButton
                classes={{ root: classes.button }}
                onClick={handleNewMessage}
              >
                Send Message
              </CustomButton>
            </div>
          </>
        }
      />
    </>
  )
}

const mapStateToProps = state => ({
  user: state.auth.authUser,
  templates: state.auth.templates
})

const mapDispatchToProps = dispatch => ({
  addNewTemplate: (userId, template) => dispatch(actions.addNewTemplate(userId, template)),
  deleteTemplate: (userId, template) => dispatch(actions.deleteTemplate(userId, template)),
  onSendMessage: (userId, contact, message, next) => dispatch(actions.sendMessage(userId, contact, message, next))
})

export default connect(mapStateToProps, mapDispatchToProps)(withStyles(styles)(Message))