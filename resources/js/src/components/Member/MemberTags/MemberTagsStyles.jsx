export const styles = theme => ({
  memberTags: {
    display: 'flex',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginTop: theme.spacing(1),
  },
  tags: {
    listStyle: 'none',
    padding: 0,
    margin: 0,
  },
  tag: {
    display: 'inline-block',
    padding: '4px 16px',
    background: '#919191',
    marginRight: theme.spacing(1),
    borderRadius: theme.spacing(2),
    color: 'white',
    fontSize: 10,
    textTransform: 'capitalize',
  },
  moreTags: {
    outline: 'none',
    border: 'none',
    color: 'white',
    background: '#919191',
    borderRadius: 16,
    fontSize: 10,
    padding: '0px 16px',
    cursor: 'pointer'
  }
})