import React from 'react'
import MemberTag from './MemberTag/MemberTag'
import MoreHorizIcon from '@material-ui/icons/MoreHoriz'
import { withStyles } from '@material-ui/core'
import { styles } from './MemberTagsStyles'

const MemberTags = ({ classes }) => {
  const tags = [
    'marriage', 'cooking', 'sports', 'poligamy', 'move abroad', 'housewife'
  ]

  return (
    <div className={classes.memberTags}>
      <ul className={classes.tags}>
        {tags.map(tag => (
          <MemberTag key={tag} text={tag} />
        ))}
      </ul>
      <button
        className={classes.moreTags}
        type="button"
        onClick={() => console.log('View more tags')}
      >
        <MoreHorizIcon />
      </button>
    </div>
  )
}

export default withStyles(styles)(MemberTags)