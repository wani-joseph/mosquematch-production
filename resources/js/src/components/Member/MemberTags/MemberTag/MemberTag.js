import React from 'react'
import { withStyles } from '@material-ui/core'
import { styles } from '../MemberTagsStyles'

const MemberTag = ({ text, classes }) => {
  return (
    <li className={classes.tag}>
      {text}
    </li>
  )
}

export default withStyles(styles)(MemberTag)