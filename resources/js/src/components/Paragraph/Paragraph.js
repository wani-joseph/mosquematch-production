import React from 'react'
import { Typography } from '@material-ui/core'
import { useStyles } from './PragraphStyles'

const Paragraph = (props) => {
    const classes = useStyles()
    
    return (
        <Typography
            {...props}
            className={classes.paragraph}
            variant="body1"
        >
            {props.children}
        </Typography>
    )
}

export default Paragraph