import { makeStyles } from '@material-ui/core/styles';

export const useStyles = makeStyles(theme =>({
    paragraph: {
        color: '#707070',
        [theme.breakpoints.up('md')]: {
            fontSize: '1.2rem',
        }
    }
}));