import React from 'react'
import {
    IconButton,
    SwipeableDrawer
} from '@material-ui/core'
import MenuIcon from '@material-ui/icons/Menu'
import SearchIcon from '@material-ui/icons/Search'
import MessageIcon from '@material-ui/icons/Message'
import FavoriteIcon from '@material-ui/icons/Favorite'
import NotificationsIcon from '@material-ui/icons/Notifications'
import Logo from '../Logo/Logo'
import SideDrawerItem from './SideDrawerItem/SideDrawerItem'
import { useStyles } from './SideDrawerStyles'

const SideDrawer = () => {
    const classes = useStyles()
    const [state, setState] = React.useState({
        top: false,
        left: false,
        bottom: false,
        right: false,
    })

    const toggleDrawer = (side, open) => event => {
        if (event && event.type === 'keydown' && (event.key === 'Tab' || event.key === 'Shift')) {
            return
        }

        setState({ ...state, [side]: open })
    }

    const sideList = side => (
        <React.Fragment>
            <div className={classes.logoContainer}>
                <Logo />
            </div>
            <ul
                className={classes.list}
                role="presentation"
                onClick={toggleDrawer(side, false)}
                onKeyDown={toggleDrawer(side, false)}
            >
                <SideDrawerItem
                    link="/search"
                    icon={<SearchIcon />}
                    text="Search"
                />
                <SideDrawerItem
                    link="/messages/inbox"
                    icon={<MessageIcon />}
                    text="Messages"
                />
                <SideDrawerItem
                    link="/interest/views"
                    icon={<FavoriteIcon />}
                    text="Interest"
                />
                {/* <SideDrawerItem
                    link="/notifications"
                    icon={<NotificationsIcon />}
                    text="Notifications"
                /> */}
            </ul>
        </React.Fragment>
    )

    return (
        <React.Fragment>
            <IconButton
                className={classes.menuIcon}
                color="primary"
                size="small"
                aria-label="menu"
                onClick={toggleDrawer('left', true)}
            >
                <MenuIcon />
            </IconButton>
            <SwipeableDrawer
                open={state.left}
                onClose={toggleDrawer('left', false)}
                onOpen={toggleDrawer('left', true)}
            >
                {sideList('left')}
            </SwipeableDrawer>
        </React.Fragment>
    )
}

export default SideDrawer