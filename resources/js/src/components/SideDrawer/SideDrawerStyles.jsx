import { makeStyles } from '@material-ui/core/styles'

export const useStyles = makeStyles(theme => ({
    logoContainer: {
        display: 'flex',
        justifyContent: 'center',
        padding: 14,
        boxShadow: theme.shadows[1]
    },
    list: {
        width: 250,
        listStyle: 'none',
        padding: theme.spacing(1, 0),
        margin: 0,
    },
    menuIcon: {
        marginRight: theme.spacing(1),
        padding: 0,
        [theme.breakpoints.up('sm')]: {
            display: 'none',
        }
    },
}))