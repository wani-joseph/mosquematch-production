import React from 'react'
import { NavLink } from 'react-router-dom'
import { useStyles } from './SideDrawerItemStyles'

const SideDrawerItem = props => {
    const classes = useStyles()
    
    return (
        <li>
            <NavLink
                className={classes.navLink}
                activeClassName={classes.active}
                to={props.link}
            >
                <span className={classes.icon}>
                    {props.icon}
                </span>
                <span className={classes.text}>
                    {props.text}
                </span>
            </NavLink>
        </li>
    )
}

export default SideDrawerItem