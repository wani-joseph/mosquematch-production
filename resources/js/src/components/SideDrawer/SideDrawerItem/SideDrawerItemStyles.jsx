import { makeStyles } from '@material-ui/core/styles'

export const useStyles = makeStyles(theme => ({
    navLink: {
        alignItems: 'center',
        color: theme.palette.common.black,
        display: 'flex',
        textDecoration: 'none',
        borderBottom: '1px solid rgba(0, 0, 0, .08)',
        padding: theme.spacing(0, 1),
    },
    icon: {
        alignItems: 'center',
        display: 'inline-flex',
        justifyContent: 'center',
        width: 48,
        height: 48,
    },
    text: {
        fontSize: 16,
    },
    active: {
        background: theme.palette.background.default,
        borderBottom: `2px solid ${theme.palette.primary.main}`,
    }
}))