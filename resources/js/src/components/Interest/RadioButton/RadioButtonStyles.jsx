import { makeStyles } from '@material-ui/core/styles'

export const useStyles = makeStyles(theme =>({
    formLabel: {
        background: 'transparent',
        borderRadius: 20,
        color: theme.palette.common.white,
        margin: 0,
    },
    label: {
        display: 'block',
        fontSize: 14,
        padding: '.2rem .8rem',
    },
    radio: {
        display: 'none',
    },
    checked: {
        background: theme.palette.common.white,
        boxShadow: theme.shadows[1],
        color: theme.palette.secondary.main,
    },
    checkedLabel: {
        fontWeight: 500,
    }
}))