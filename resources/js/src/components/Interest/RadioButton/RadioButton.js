import React from 'react'
import {
    FormControlLabel,
    Radio
} from '@material-ui/core'
import { useStyles } from './RadioButtonStyles'

const RadioButton = props => {
    const classes = useStyles()
    const { interest, label, value } = props

    let formLabelClasses = [classes.formLabel]
    let labelClasses = [classes.label]

    if (interest === value) {
        formLabelClasses.push(classes.checked)
        labelClasses.push(classes.checkedLabel)
    }

    return (
        <FormControlLabel
            className={formLabelClasses.join(' ')}
            classes={{ label: labelClasses.join(' ') }}
            value={value}
            control={
                <Radio 
                    className={classes.radio}
                />
            }
            label={label}
            interest={interest}
        />
    )
}

export default RadioButton