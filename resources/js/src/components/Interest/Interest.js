import React, { Component } from 'react'
import { connect } from 'react-redux'
import {
    Container,
    Grid,
    RadioGroup,
    Typography,
    withStyles
} from '@material-ui/core'
import InterestTabs from './Tabs/Tabs'
import InterestRadioButton from './RadioButton/RadioButton'
import InfiniteScroll from '../../services/InfiniteScroll'
import MemberCard from '../Member/MemberCard/MemberCard'
import Loader from '../UI/Loader/Loader'
import { styles } from './InterestStyles'

class Interest extends Component {
	constructor() {
		super()
		this.state = {
			interest: 'interested in me'
		}
		this.handleInterest = this.handleInterest.bind(this)
	}

	handleInterest() {
		this.setState({ interest: event.target.value })
	}

	render() {
		const { user, tab, classes } = this.props
		const { interest } = this.state

		let interestedInMeResource = 'visitors'
		let myInterestResource = 'visited'
		let interestedInMeLabel = 'Viewed Me'
		let myInterestLabel = 'Viewed'
		let interestedInMeNotFound = 'No one view your profile yet.'
		let myInterestNotFound = "You didn't view any member profile yet."
		let containerClasses = [classes.container]
		let interestedInMeClasses = [classes.interested]
		let myInterestClasses = [classes.myInterest]

		if (interest === 'interested in me') {
			interestedInMeClasses.push(classes.show)
		} else {
			myInterestClasses.push(classes.show)
		}

		if (tab === "Favorites") {
			interestedInMeResource = 'followers'
			myInterestResource = 'followings'
			interestedInMeLabel = 'Favorited Me'
			myInterestLabel = 'My Favorites'
			interestedInMeNotFound = 'No one favorited you yet.'
			myInterestNotFound = "You didn't favorite any member yet."
		}

		if (tab === "Reports") {
			interestedInMeResource = 'reported'
			interestedInMeLabel = 'Reported'
			interestedInMeNotFound = "You didn't report any member yet."
			interestedInMeClasses.push(classes.blocked)
			containerClasses.pop()
		}

		if (tab === "Blocked") {
			interestedInMeResource = 'blocked'
			interestedInMeLabel = 'Blocked Members'
			interestedInMeNotFound = "You didn't block any member yet."
			interestedInMeClasses.push(classes.blocked)
			containerClasses.pop()
		}
					
		return (
			<>
				<InterestTabs userId={user.identifier} page={tab} />
					<div className={classes.interestControl}>
						<RadioGroup
							classes={{ root: classes.interetsRadioGroup }}
							defaultValue={interest}
							aria-label="interests" 
							name="interests"
							onChange={this.handleInterest}
						>
							<InterestRadioButton
								interest={interest}
								value="interested in me"
								label={interestedInMeLabel}
							/>
							{tab != 'Reports' && tab != 'Blocked' ? (
								<InterestRadioButton
									interest={interest}
									value="my interests in others"
									label={myInterestLabel}
								/>
							) : null}
						</RadioGroup>
					</div>
					<Container className={containerClasses.join(' ')} maxWidth={tab != "Reports" && tab != "Blocked" ? false : 'lg'}>
						<Grid container>
								<Grid className={interestedInMeClasses.join(' ')} item xs={12} lg={tab != "Reports" && tab != "Blocked" ? 6 : 12}>
									<Typography className={classes.title} variant="h4" component="h2">
										{interestedInMeLabel}
									</Typography>
									<InfiniteScroll resource={interestedInMeResource}>
										{resource => {
											let interests = <Loader />

											if (!resource.loading) {

												if (resource.list.length > 0) {
													interests = resource.list.map(interest => {

													if (tab == 'Views') {
														return (
															<MemberCard
																key={interest.identifier}
																userId={user.identifier}
																member={interest.visitor.data}
															/>
														)
													}

													if (tab == 'Favorites') {
														return (
															<MemberCard
																key={interest.identifier}
																userId={user.identifier}
																member={interest.follower.data}
															/>
														)
													}

													if (tab == 'Reports') {
														return (
															<MemberCard
																key={interest.identifier}
																userId={user.identifier}
																member={interest.reporter.data}
															/>
														)
													}

													if (tab == 'Blocked') {
														return (
															<MemberCard
																key={interest.identifier}
																userId={user.identifier}
																member={interest.blocked.data}
															/>
														)
													}

												})
											} else {
													interests = (
														<Typography variant="body2">
															{interestedInMeNotFound}
														</Typography>
													)
												}
											}

											return <>{interests}</>
										}}
									</InfiniteScroll>
							</Grid>
							{tab != "Reports" && tab != "Blocked" ? (
								<Grid className={myInterestClasses.join(' ')} item xs={12} lg={6}>
									<Typography className={classes.title} variant="h4" component="h2">
											{myInterestLabel}
									</Typography>
									<InfiniteScroll resource={myInterestResource}>
											{resource => {
												let interests = <Loader />

												if (!resource.loading) {

													if (resource.list.length > 0) {
														interests = resource.list.map(interest => {
															if (tab == 'Views') {
																return (
																	<MemberCard
																		key={interest.identifier}
																		userId={user.identifier}
																		member={interest.visited.data}
																	/>
																)
															}

															if (tab == 'Favorites') {
																return (
																	<MemberCard
																		key={interest.identifier}
																		userId={user.identifier}
																		member={interest.following.data}
																	/>
																)
															}

															// if (tab == 'Reports') {
															// 	return (
															// 		<MemberCard
															// 			key={interest.identifier}
															// 			userId={user.identifier}
															// 			member={interest.reported.data}
															// 		/>
															// 	)
															// }		
														})
													} else {
														interests = (
															<Typography variant="body1">
																{myInterestNotFound}
															</Typography>
														)
													}
												}

												return <>{interests}</>
											}}
									</InfiniteScroll>
								</Grid>
							) : null}
							</Grid>
					</Container> 
			</>
		)
	}
}

const mapStateToProps = state => ({
	user: state.auth.authUser,
})

export default connect(mapStateToProps)(withStyles(styles)(Interest))