export const styles = theme => ({
    container: {
        padding: 0,
        margin: 0,
    },
    interestControl: {
        marginBottom: theme.spacing(3),
        padding: theme.spacing(0, 2),
    },
    interetsRadioGroup: {
        flexDirection: 'row',
        background: '#c5e3e4',
        width: 'fit-content',
        borderRadius: 20,
        [theme.breakpoints.up('lg')]: {
            display: 'none'
        } 
    },
    title: {
        fontWeight: 500,
        padding: theme.spacing(0, 2),
        [theme.breakpoints.up('lg')]: {
            padding: theme.spacing(2),
        }
    },
    interested: {
        display: 'none',
        marginBottom: theme.spacing(6),
        [theme.breakpoints.up('lg')]: {
            background: theme.palette.common.white,
            border: '1px solid rgba(0, 0, 0, .08)',
            borderLeft: 'none',
            display: 'block'
        }
    },
    myInterest: {
        display: 'none',
        marginBottom: theme.spacing(6),
        [theme.breakpoints.up('lg')]: {
            background: theme.palette.common.white,
            border: '1px solid rgba(0, 0, 0, .08)',
            borderRight: 'none',
            display: 'block'
        }
    },
    show: {
        display: 'block'
    },
    blocked: {
        border: '1px solid rgba(0, 0, 0, .08)',
    }
})