import { makeStyles } from '@material-ui/core/styles'

export const useStyles = makeStyles(theme => ({
    tabsContainer: {
        background: theme.palette.common.white,
        borderBottom: '1px solid rgba(0, 0, 0, .08)',
        borderTop: '1px solid rgba(0, 0, 0, .08)',
        marginBottom: theme.spacing(3),
    },
    tabs: {
        display: 'flex',
        padding: 0,
        margin: 0,
        listStyle: 'none',
        width: '100%',
        overflowX: 'auto'
    }
}))