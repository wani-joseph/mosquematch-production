import { makeStyles } from '@material-ui/core/styles'

export const useStyles = makeStyles(theme => ({
    tabItem: {
        display: 'inline-flex',
        marginRight: theme.spacing(2),
        '&:last-of-type': {
            marginRight: 0,
        }
    },
    link: {
        borderBottom: '4px solid transparent',
        borderTop: '4px solid transparent',
        color: theme.palette.common.black,
        display: 'block',
        fontWeight: 500,
        padding: theme.spacing(1),
        textDecoration: 'none',
        textTransform: 'uppercase',
        [theme.breakpoints.up('sm')]: {
            padding: theme.spacing(1, 2),
        }
    },
    active: {
        borderBottom: `4px solid ${theme.palette.primary.main}`
    },
    linkText: {
        marginRight: theme.spacing(1),
    },
    linkBadge: {
        marginTop: '-16px',
    }
}))