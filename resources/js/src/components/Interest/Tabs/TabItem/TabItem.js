import React from 'react'
import { NavLink } from 'react-router-dom'
import { Badge } from '@material-ui/core'
import { useStyles } from './TabItemStyles'

const TabItem = props => {
    const classes = useStyles()
    const { link, text, badge } = props

    return (
        <li className={classes.tabItem}>
            <NavLink
                className={classes.link}
                activeClassName={classes.active}
                to={link}
            >
                <span className={classes.linkText}>{text}</span>
                <Badge className={classes.linkBadge} badgeContent={badge} color="primary" />
            </NavLink>
        </li>
    )
}

export default TabItem