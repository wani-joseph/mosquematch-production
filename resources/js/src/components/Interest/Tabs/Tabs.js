import React from 'react'
import { connect } from 'react-redux'
import { Container } from '@material-ui/core'
import TabItem from './TabItem/TabItem'
import { useStyles } from './TabsStyles'

const Tabs = ({ viewedMeCount, favoritedMeCount, reportedMeCount }) => {
    const classes = useStyles()

    return (
        <div className={classes.tabsContainer}>
            <Container maxWidth="lg">
                <ul className={classes.tabs}>
                    <TabItem
                        link="/interest/views"
                        text="Views"
                        badge={viewedMeCount}
                    />
                    <TabItem
                        link="/interest/favorites"
                        text="Favorites"
                        badge={favoritedMeCount}
                    />
                    <TabItem
                        link="/interest/reports"
                        text="Reports"
                        badge={reportedMeCount}
                    />
                    <TabItem
                        link="/interest/blocked"
                        text="Blocked"
                    />
                </ul>
            </Container>
        </div>
    )
}

const mapStateToProps = state => ({
    viewedMeCount: state.auth.viewedMeCount,
    favoritedMeCount: state.auth.favoritedMeCount,
    reportedMeCount: state.auth.reportedMeCount
})

export default connect(mapStateToProps)(Tabs)