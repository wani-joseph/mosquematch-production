import React from 'react'
import { Typography } from '@material-ui/core'
import { useStyles } from './TaglineStyles'

const Tagline = props => {
    const classes = useStyles()

    let taglineClasses = [classes.tagline]

    if (props.bottommargin === "true")
        taglineClasses.push(classes.bottommargin)

    if (props.rightmargin === "true")
        taglineClasses.push(classes.rightmargin)

    return (
        <Typography
            {...props}
            className={taglineClasses.join(' ')}
            color="textPrimary"
            noWrap
        >
            {props.text}
        </Typography>
    )
}

export default Tagline