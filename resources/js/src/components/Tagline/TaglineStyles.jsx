import { makeStyles } from '@material-ui/core/styles'

export const useStyles = makeStyles(theme =>({
    tagline: {
        fontWeight: 500,
    },
    bottommargin: {
        marginBottom: theme.spacing(1),
    },
    rightmargin: {
        [theme.breakpoints.up('md')]: {
            marginRight: theme.spacing(2),
        }
    }
}))