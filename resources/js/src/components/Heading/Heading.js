import React from 'react'
import { Typography } from '@material-ui/core'
import { useStyles } from './HeadingStyles'

const Heading = (props) => {
    const classes = useStyles()

    return (
        <Typography
            className={classes.heading}
            variant="h4"
            component="h2"
            noWrap
            {...props}
        >
            {props.text}
        </Typography>
    )
}

export default Heading