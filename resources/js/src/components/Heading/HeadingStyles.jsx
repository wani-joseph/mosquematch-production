import { makeStyles } from '@material-ui/core/styles'

export const useStyles = makeStyles(theme =>({
    heading: {
        fontWeight: 500,
        padding: theme.spacing(2),
        borderBottom: '1px solid rgba(0, 0, 0, .08)',
        textTransform: 'capitalize',
    },
}))