import { makeStyles } from '@material-ui/core/styles';

export const useStyles = makeStyles(theme =>({
    imageIcon: {
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        padding: theme.spacing(1, 0),
        [theme.breakpoints.up('sm')]: {
            marginBottom: theme.spacing(1)
        }
    },
    icon: {
        maxHeight: 60
    }
}));