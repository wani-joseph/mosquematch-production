import React from 'react';
import { useStyles } from './ImageIconStyles';

const ImageIcon = (props) => {
    const classes = useStyles();

    return (
        <div className={classes.imageIcon}>
            <img
                {...props}
                className={classes.icon}
                src={props.src} 
                alt={props.alt}
            />
        </div>
    );
};

export default ImageIcon;