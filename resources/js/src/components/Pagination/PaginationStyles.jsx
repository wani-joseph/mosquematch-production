import { makeStyles } from '@material-ui/core/styles'

export const useStyles = makeStyles(theme => ({
    pagination: {
        marginBottom: theme.spacing(3)
    },
    item: {
        color: theme.palette.primary.main,
        display: 'flex',
        alignItems: 'center',
        fontWeight: 500,
        '&:nth-child(2)': {
            justifyContent: 'center',
            visibility: 'hidden',
            [theme.breakpoints.up('sm')]: {
                visibility: 'visible',
            }
        },
        '&:last-child': {
            justifyContent: 'flex-end',
        }
    },
    link: {
        alignItems: 'center',
        color: theme.palette.primary.main,
        display: 'flex',
        textDecoration: 'none',
    },
    number: {
        color: '#919191',
        padding: theme.spacing(0, 1)
    }
}))