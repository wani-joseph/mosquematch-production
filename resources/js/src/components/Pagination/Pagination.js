import React from 'react'
import { Link } from 'react-router-dom'
import { Grid } from '@material-ui/core'
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft'
import ChevronRightIcon from '@material-ui/icons/ChevronRight'
import CustomDividor from '../UI/CustomDividor/CustomDividor'
import CustomIconButton from '../UI/CustomIconButton/CustomIconButton'
import { useStyles } from './PaginationStyles'

const Pagination = (props) => {
    const classes = useStyles()
    const { currentPage, totalPages, customDividor } = props

    return (
        <React.Fragment>
            {totalPages > 1
                ? (
                    <div className={classes.pagination}>
                        {customDividor == "true" ? <CustomDividor /> : null}
                        <Grid container>
                            <Grid item xs={3} sm={2} className={classes.item}>
                                <Link to={`?page=${currentPage - 1}`} onClick={props.prev} className={classes.link}>
                                    {props.currentPage > 1 ? <React.Fragment><CustomIconButton><ChevronLeftIcon/></CustomIconButton> Previous</React.Fragment> : null}
                                </Link>
                            </Grid>
                            <Grid item xs={6} sm={8} className={classes.item}>
                                Page<span className={classes.number}>{props.currentPage}</span>of<span className={classes.number}>{props.totalPages}</span>Pages
                            </Grid>
                            <Grid item xs={3} sm={2} className={classes.item}>
                                <Link to={`?page=${props.currentPage + 1}`} onClick={props.next} className={classes.link}>
                                    {props.currentPage === props.totalPages ? null : <React.Fragment>Next <CustomIconButton><ChevronRightIcon/></CustomIconButton></React.Fragment>}
                                </Link>
                            </Grid>
                        </Grid>
                    </div>
                ) : null}
        </React.Fragment>
    )
}

export default Pagination