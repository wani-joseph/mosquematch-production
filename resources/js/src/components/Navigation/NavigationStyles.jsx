export const styles = theme => ({
    navigation: {
        display: 'none',
        [theme.breakpoints.up('sm')]: {
            display: 'flex',
            justifyContent: 'flex-start',
            padding: 0,
        }
    }
})