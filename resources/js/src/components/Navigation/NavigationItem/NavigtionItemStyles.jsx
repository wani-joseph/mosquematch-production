import { makeStyles } from '@material-ui/core/styles'

export const useStyles = makeStyles(theme => ({
    navItem: {
        justifyContent: 'center',
        textAlign: 'center',
        padding: 0,
        width: 'auto',
    },
    navLink: {
        textDecoration: 'none',
        color: theme.palette.common.black,
        borderBottom: '3px solid transparent',
        [theme.breakpoints.up('sm')]: {
            padding: '.4rem 0 .2rem 0',
        },
        [theme.breakpoints.up('md')]: {
            padding: '.4rem .8rem .2rem .8rem',
        },
    },
    icon: {
        justifyContent: 'center',
        color: theme.palette.common.black,
        marginTop: theme.spacing(1),
    },
    text: {
        display: 'none',
        [theme.breakpoints.up('md')]: {
            display: 'flex',
            justifyContent: 'center',
            margin: 0,
            marginTop: '-8px',
            width: '100%',
        }
        
    },
    typography: {
        fontSize: '0.875rem',
    },
    active: {
        borderColor: theme.palette.primary.main,
        background: theme.palette.background.default
    }
}))