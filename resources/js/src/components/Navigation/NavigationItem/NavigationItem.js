import React from 'react'
import { NavLink } from 'react-router-dom'
import {
    ListItem,
    ListItemIcon,
    ListItemText
} from '@material-ui/core';
import { useStyles } from './NavigtionItemStyles'

const NavigationItem = (props) => {
    const classes = useStyles();

    return (
        <ListItem 
            className={classes.navItem}
            alignItems="center"
        >
            <NavLink
                className={classes.navLink}
                activeClassName={classes.active}
                to={props.link}
            >
                <ListItemIcon className={classes.icon}>
                    {props.icon}
                </ListItemIcon>
                <ListItemText
                    className={classes.text}
                    primary={props.text}
                    classes={{ primary:classes.typography }}
                />
            </NavLink>
        </ListItem>
    )
}

export default NavigationItem