import React from 'react'
import { connect } from 'react-redux'
import { List, Badge, withStyles } from '@material-ui/core'
import SearchIcon from '@material-ui/icons/Search'
import MessageIcon from '@material-ui/icons/Message'
import FavoriteIcon from '@material-ui/icons/Favorite'
import NavigationItem from './NavigationItem/NavigationItem'
import { styles } from './NavigationStyles'

const Navigation  = ({
    inboxCount,
    requestsCount,
    viewedMeCount,
    favoritedMeCount,
    reportedMeCount,
    classes
}) => {
    let messagesCount = inboxCount + requestsCount
    let interestCount = viewedMeCount + favoritedMeCount + reportedMeCount

    return (
        <List className={classes.navigation}>
            <NavigationItem
                link="/search"
                icon={
                    <SearchIcon />
                }
                text="search"
            />
            <NavigationItem
                link="/messages"
                icon={
                    <Badge badgeContent={messagesCount} color="primary">
                        <MessageIcon />
                    </Badge>
                }
                text="messages"
            />
            <NavigationItem
                link="/interest"
                icon={
                    <Badge badgeContent={interestCount} color="primary">
                        <FavoriteIcon />
                    </Badge>
                }
                text="interest"
            />
        </List>
    )
}

const mapStateToProps = state => ({
    inboxCount: state.auth.inboxCount,
    requestsCount: state.auth.requestsCount,
    viewedMeCount: state.auth.viewedMeCount,
    favoritedMeCount: state.auth.favoritedMeCount,
    reportedMeCount: state.auth.reportedMeCount
})

export default connect(mapStateToProps)(withStyles(styles)(Navigation))