import React from 'react'
import { Typography } from '@material-ui/core'
import { useStyles } from './CopyRightStyles'

const CopyRight = () => {
    const classes = useStyles()

    let year = new Date()
    year = year.getFullYear()
    
    return (
        <Typography
            className={classes.copyRight}
            variant="body2"
        >
            Copyright &copy; {year} MosqueMatch Co. All rights reserved.
        </Typography>
    )
}

export default CopyRight