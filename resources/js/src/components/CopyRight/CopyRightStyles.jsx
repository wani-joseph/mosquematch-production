import { makeStyles } from '@material-ui/core/styles';

export const useStyles = makeStyles(theme =>({
    copyRight: {
        color: theme.palette.common.black,
    }
}));