import React from 'react'
import AddIcon from '@material-ui/icons/Add'
import CustomButton from '../UI/CustomButton/CustomButton'
import CustomModal from '../UI/CustomModal/CustomModal'
import CustomForm from '../UI/CustomForm/CustomForm'
import Typography from '../UI/CustomTypography/CustomTypography'
import { useStyles } from './AdminHeaderStyles'

const AdminHeader = props => {
    const classes = useStyles()
    const {
        pageHeader,
        fields,
        validation,
        resourceForm,
        openResourceForm,
        closeResourceForm,
        addNewResource,
        page
    } = props

    let modalTitle = ""

    if (pageHeader == "Members") modalTitle = "Add New Member"
    if (pageHeader == "Mosques") modalTitle = "Add New Mosque"

    return (
        <>
            <div className={classes.pageHeader}>
                <Typography bold="true" variant="h4" component="h1">
                    {pageHeader}
                </Typography>
                {page === "resource listing" ? (
                    <button
                        type="button"
                        className={classes.button}
                        onClick={openResourceForm}
                    >
                        <AddIcon />Add New
                    </button>
                ) : null}
            </div>
            {page === "resource listing" ? (
                <CustomModal
                show={resourceForm}
                handleClose={closeResourceForm}
                title={modalTitle}
                content={
                    <div className={classes.form}>
                        <CustomForm
                            fields={fields}
                            // validation={validation}
                            submitHandler={addNewResource}
                            submitButton={
                                <CustomButton
                                    type="submit"
                                >
                                    Add New
                                </CustomButton>
                            }
                        />
                    </div>
                }
            />
            ) : null}
        </>
    )
}

export default AdminHeader