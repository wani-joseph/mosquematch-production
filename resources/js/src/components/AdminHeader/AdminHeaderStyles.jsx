import { makeStyles } from '@material-ui/core/styles';

export const useStyles = makeStyles(theme =>({
    pageHeader: {
        display: 'flex',
        justifyContent: 'space-between',
        alignItems: 'center',
        marginBottom: theme.spacing(3),
        marginTop: theme.spacing(2),
    },
    button: {
        display: 'flex',
        justifyContent: 'space-between',
        alignItems: 'center',
        textTransform: 'uppercase',
        outline: 'none',
        border: `2px solid ${theme.palette.secondary.main}`,
        color: theme.palette.secondary.main,
        cursor: 'pointer',
        background: theme.palette.common.white,
        boxShadow: theme.shadows[2]
    },
    form: {
        maxHeight: '80vh',
        overflowY: 'auto'
    }
}))