import React from 'react';
import { Link } from 'react-router-dom';
import CustomDividor from '../UI/CustomDividor/CustomDividor';
import { useStyles } from './PrivacyTermsStyles';

const PrivacyTerms = () => {
    const classes = useStyles();

    return (
        <ul className={classes.privacyTerms}>
            <li><Link className={classes.link} to="/">Privacy Policy</Link></li>
            <CustomDividor classes={{root: classes.dividor}} orientation="vertical" />
            <li><Link className={classes.link} to="/">Terms of Use</Link></li>
        </ul>
    );
};

export default PrivacyTerms;