import { makeStyles } from '@material-ui/core/styles';

export const useStyles = makeStyles(theme =>({
    privacyTerms: {
        listStyle: 'none',
        display: 'flex',
        alignItems: 'center',
        padding: 0,
        margin: 0,
        [theme.breakpoints.up('sm')]: {
            justifyContent: 'flex-end',
        },
    },
    link: {
        color: theme.palette.common.black,
        textDecoration: 'none',
    },
    dividor: {
        height: 16,
        margin: theme.spacing(0, 1),
    }
}));