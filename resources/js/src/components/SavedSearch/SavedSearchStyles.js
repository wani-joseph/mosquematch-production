export const styles = theme => ({
  searchesItem: {
    border: '1px solid rgba(0, 0, 0, .08)',
    display: 'flex',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginBottom: theme.spacing(1),
    padding: theme.spacing(1, 3),
  },
  button: {
    border: 'none',
    outline: 'none',
    padding: 0,
    background: 'transparent',
    marginLeft: theme.spacing(2),
    cursor: 'pointer'
  }
})