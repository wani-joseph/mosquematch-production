import React from 'react'
import CustomTypography from '../UI/CustomTypography/CustomTypography'
import { withStyles } from '@material-ui/core'
import { styles } from './SavedSearchStyles'

const SavedSearch = ({ savedSearch, runSearchHandler, deleteHandler, classes }) => (
  <li className={classes.searchesItem}>
    <CustomTypography bold="true">
      {savedSearch.searchName}
    </CustomTypography>
    <div>
      <button
        type="button"
        className={classes.button}
        onClick={() => runSearchHandler(savedSearch)}
      >
        <CustomTypography bold="true">
          Run Search
        </CustomTypography>
      </button>
      <button 
        type="button" 
        className={classes.button}
        onClick={() => deleteHandler(savedSearch)}
      >
        <CustomTypography bold="true" invalid="true">
          Delete
        </CustomTypography>
      </button>
    </div>
  </li>
)

export default withStyles(styles)(SavedSearch)