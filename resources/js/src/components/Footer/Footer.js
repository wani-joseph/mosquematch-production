import React from 'react'
import {
    Container,
    Grid
} from '@material-ui/core'
import SiteMap from '../SiteMap/SiteMap'
import SocialLinks from '../SocailLinks/SocialLinks'
import SectionSubTitle from '../SectionSubTitle/SectionSubTitle'
import Paragraph from '../Paragraph/Paragraph'
import CustomDividor from '../UI/CustomDividor/CustomDividor'
import CopyRight from '../CopyRight/CopyRight'
import PrivacyTerms from '../PrivacyTerms/PrivacyTerms'
import { useStyles } from './FooterStyles'



const Footer = () => {
    const classes = useStyles()

    return (
        <div className={classes.siteFooter}>
            <Container maxWidth="lg">
                <Grid container spacing={4}>
                    <Grid item xs={12} sm={6} md={3}>
                        <SectionSubTitle
                            classes={{ root: classes.widgetTitle }}
                            title="Sitemap"
                        />
                        <SiteMap />
                    </Grid>
                    <Grid item xs={12} sm={6} md={3}>
                        <SectionSubTitle
                            classes={{ root: classes.widgetTitle }}
                            title="Our Social Media"
                        />
                        <SocialLinks />
                    </Grid>
                    <Grid item xs={12} sm={12} md={6}>
                        <SectionSubTitle
                            classes={{ root: classes.widgetTitle }}
                            title="About Us"
                        />
                        <Paragraph
                            classes={{ root: classes.aboutText }}
                        >
                            Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.
                        </Paragraph>
                    </Grid>
                </Grid>
                <CustomDividor />
                <Grid container spacing={2} alignItems="flex-start">
                    <Grid item xs={12} sm={8}>
                        <CopyRight />
                    </Grid>
                    <Grid item xs={12} sm={4}>
                        <PrivacyTerms />
                    </Grid>
                </Grid>
            </Container>
        </div>
    )
}

export default Footer