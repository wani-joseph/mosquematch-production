import { makeStyles } from '@material-ui/core/styles';

export const useStyles = makeStyles(theme =>({
    siteFooter: {
        background: theme.palette.grey[300],
        paddingTop: theme.spacing(4),
        [theme.breakpoints.up('md')]: {
            paddingTop: theme.spacing(6),
        },
    },
    widgetTitle: {
        marginTop: 0,
    },
    aboutText: {
        fontWeight: 300,
        color: theme.palette.common.black
    }
}));