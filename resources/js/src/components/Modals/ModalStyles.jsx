import { makeStyles } from '@material-ui/core/styles'

export const useStyles = makeStyles(theme =>({
    title: {
        fontWeight: 500,
        color: theme.palette.common.black,
        marginBottom: theme.spacing(2),
    },
    actions: {
        display: 'flex',
        justifyContent: 'flex-end',
    },
    button: {
        textTransform: 'capitalize',
        marginLeft: theme.spacing(2),
        marginTop: 0,
        padding: theme.spacing(1, 3),
        '&:first-of-type': {
            background: theme.palette.grey[700],
            borderColor: theme.palette.grey.A400,
            '&:hover': {
                background: theme.palette.grey.A400,
            }
        }
    },
    templates: {
        borderBottom: '1px solid rgba(0, 0, 0, .08)',
        margin: 0,
        marginBottom: theme.spacing(2),
        padding: 0
    }
}))