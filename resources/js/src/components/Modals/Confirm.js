import React from 'react'
import { connect } from 'react-redux'
import { Typography } from '@material-ui/core'
import CustomButton from '../UI/CustomButton/CustomButton'
import { useStyles } from './ModalStyles'
import * as actions from '../../store/actions'

const Confirm = props => {
    const classes = useStyles()
    const {
        message,
        action,
        userId,
        memberId,
        onUserAction,
        handleBlockMember
    } = props

    const handleAction = () => {

        let url = null
        let data = {}

        if (action == "request private photos") {
            document.querySelector('#confirm').innerHTML = "Your request for private photos has been sent."
        }

        if (action == "request wali details") {
            url = `/api/users/${userId}/wali_requested`
            data = {
                wali_requested_id: parseInt(memberId)
            }

            onUserAction(url, data, () => {
                document.querySelector('#confirm').innerHTML = 'Your request for wali details has beed sent'
            });
        }
    }

    return (
        <React.Fragment>
            <Typography
                className={classes.title}
                variant="body1">
                {message}
            </Typography>
            <p id="confirm"></p>
            <div className={classes.actions}>
                <CustomButton
                    classes={{ root: classes.button }}
                >
                    No
                </CustomButton>
                <CustomButton
                    classes={{ root: classes.button }}
                    onClick={action == "block" ? () => {handleBlockMember(memberId)} : handleAction}
                >
                    Yes
                </CustomButton>
            </div>
        </React.Fragment>
    )
}

const mapDispatchToProps = dispatch => ({
    onUserAction: (url, data, next) => dispatch(actions.userAction(url, data, next))
})

export default connect(null, mapDispatchToProps)(Confirm)