import React from 'react'
import { connect } from 'react-redux'
import { Typography } from '@material-ui/core'
import Template from '../Template/Template'
import CustomButton from '../UI/CustomButton/CustomButton'
import CustomTextField from '../UI/CustomTextField/CustomTextField'
import { useStyles } from './ModalStyles'
import * as actions from '../../store/actions'

const NewMessage = props => {
    const classes = useStyles()
    const {
        user,
        contact,
        templates,
        addNewTemplate,
        deleteTemplate,
        userAction
    } = props

    const handleNewMessage = () => {
        const message = document.querySelector('#message').value

        const data = {
            receiver: parseInt(contact),
            content: message
        }

        userAction(`/api/users/${user.identifier}/conversation`, data, () => {
            document.querySelector('#message').value = ""
        })
    }

    const handleNewTemplate = () => {
        const template = document.querySelector('#message').value

        addNewTemplate(user.identifier, template)
    }

    const handleDeleteTemplate = (template) => {
        deleteTemplate(user.identifier, template)
    }

    const handleSetTemplate = (template) => {
        document.querySelector('#message').value = template
    }

    let messagesTemplates = (
        <Typography variant="body1">
            Your saved templates will apear here...
        </Typography>
    )

    if (templates.length > 0) {
        messagesTemplates = templates.map(template => (
            <Template
                key={template.identifier}
                handleDeleteTemplate={handleDeleteTemplate}
                handleSetTemplate={handleSetTemplate}
                userTemplate={template}
            />
        ))
    }

    return (
        <React.Fragment>
            <Typography
                className={classes.title}
                variant="body1">
                Templates
            </Typography>
            <ul className={classes.templates}>
                {messagesTemplates}
            </ul>
            <Typography
                className={classes.title}
                variant="body1">
                Message
            </Typography>
            <CustomTextField
                id="message"
                name="message"
                placeholder="Message..."
                fullWidth
                multiline
                rows="2"
                classes={{ root: classes.message }}
            />
            <input type="hidden" value={contact} id="contact" />
            <div className={classes.actions}>
                <CustomButton
                    classes={{ root: classes.button }}
                    onClick={handleNewTemplate}
                >
                    Save as Template
                </CustomButton>
                <CustomButton
                    classes={{ root: classes.button }}
                    onClick={handleNewMessage}
                >
                    Send Message
                </CustomButton>
            </div>
        </React.Fragment>
    )
}

const mapStateToProps = state => ({
    user: state.auth.authUser
})

const mapDispatchToProps = dispatch => ({
    addNewTemplate: (userId, template) => dispatch(actions.addNewTemplate(userId, template)),
    deleteTemplate: (userId, template) => dispatch(actions.deleteTemplate(userId, template)),
    userAction: (url, data, next) => dispatch(actions.userAction(url, data, next))
})

export default connect(mapStateToProps, mapDispatchToProps)(NewMessage)