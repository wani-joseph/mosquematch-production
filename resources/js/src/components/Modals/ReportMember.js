import React from 'react'
import { Typography } from '@material-ui/core'
import CustomButton from '../UI/CustomButton/CustomButton'
import CustomTextField from '../UI/CustomTextField/CustomTextField'
import { useStyles } from './ModalStyles'

const ReportMember = props => {
    const classes = useStyles()
    const {
        memberId,
        handleReportMember,
    } = props

    return (
        <React.Fragment>
            <Typography
                className={classes.title}
                variant="body1"
            >
                Are you sure you want to report this member...
            </Typography>
            <CustomTextField
                id="report"
                name="report"
                placeholder="Type your report here..."
                fullWidth
                multiline
                rows="3"
                classes={{ root: classes.message }}
            />
            <div className={classes.actions}>
                <CustomButton
                    classes={{ root: classes.button }}
                >
                    Cancel
                </CustomButton>
                <CustomButton
                    classes={{ root: classes.button }}
                    onClick={() => handleReportMember(memberId)}
                >
                    Report Member
                </CustomButton>
            </div>
        </React.Fragment>
    )
}

export default ReportMember