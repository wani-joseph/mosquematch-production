import React from 'react'
import { Typography } from '@material-ui/core'
import { useStyles } from './SectionSubTitleStyles'

const SectionSubTitle = (props) => {
    const classes = useStyles()
    return (
        <Typography
            {...props}
            className={classes.sectionSubTitle}
            variant="h5"
            color="textPrimary"
            component="h3"
        >
            {props.title}
        </Typography>
    )
}

export default SectionSubTitle