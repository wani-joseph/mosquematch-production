import { makeStyles } from '@material-ui/core/styles';

export const useStyles = makeStyles(theme =>({
    sectionSubTitle: {
        marginTop: theme.spacing(3),
        marginBottom: theme.spacing(1),
    }
}));