import React from 'react'
import { List } from '@material-ui/core'
import FooterLink from '../FooterLink/FooterLink'

const SiteMap = () => {
    return (
        <List>
            <FooterLink
                link="/"
                text="Home"
            />
            <FooterLink
                link="/"
                text="Newest Reciters"
            />
            <FooterLink
                link="/"
                text="Top Recitors"
            />
            <FooterLink
                link="/"
                text="Most Viewed"
            />
            <FooterLink
                link="/"
                text="Recitors"
            />
            <FooterLink
                link="/"
                text="Search"
            />
        </List>
    )
}

export default SiteMap