import React from 'react'
import { connect } from 'react-redux'
import ProfileForm from '../ProfileForm/ProfileForm'
import CustomButton from '../UI/CustomButton/CustomButton'
import ProfileImage from './ProfileImage/ProfileImage'
import Photos from './Photos/Photos'

const UpdateProfile = props => {
    const { page, handleMemberUpdate } = props

    if (page == "Photos") return <Photos />

    if (page == "Basic Information") {
        return (
            <React.Fragment>
                <ProfileImage />
                <ProfileForm
                    page={page}
                    handleSubmit={handleMemberUpdate}
                    submitButton={
                        <CustomButton
                            type="submit"
                        >
                            Save Changes
                        </CustomButton>
                    }
                />
            </React.Fragment>
        )
    }

    return (
        <ProfileForm
            page={page}
            handleSubmit={handleMemberUpdate}
            submitButton={
                <CustomButton
                    type="submit"
                >
                    Save Changes
                </CustomButton>
            }
        />
    )
}

const mapStateToProps = state => ({
    user: state.auth.authUser
})

export default connect(mapStateToProps)(UpdateProfile)