export const styles = theme => ({
    profilePic: {
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'space-between',
        padding: theme.spacing(2),
    },
    profileImage: {
        width: 140,
        height: 140,
        position: 'relative',
    },
    thumb: {
        borderRadius: '50%',
        overflow: 'hidden'
    },
    input: {
        display: 'none',
    },
    upload: {
        position: 'absolute',
        right: 0,
        bottom: 0,
    },
    upload: {
        position: 'absolute',
        right: 0,
        bottom: 0,
    },
    button: {
        height: 50,
        borderRadius: '50%',
    },
})