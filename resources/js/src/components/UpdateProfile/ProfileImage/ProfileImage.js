import React, { Component } from 'react'
import { connect } from 'react-redux'
import Image from '../../UI/Image/Image'
import {
    Button,
    withStyles
} from '@material-ui/core'
import AddIcon from '@material-ui/icons/Add'
import CustomTypography from '../../UI/CustomTypography/CustomTypography'
import { styles } from './ProfileImageStyles'
import * as actions from '../../../store/actions'
import axios from 'axios'

class ProfileImage extends Component {
    constructor() {
        super()
        this.handleChange = this.handleChange.bind(this)
    }

    handleChange(event) {
        const { user, authUser } = this.props
        const fd = new FormData()
        fd.append('profile_image', event.target.files[0], event.target.files[0].name)
        fd.append('_method', 'put')

        axios({
            method: 'post',
            url: `/api/users/${user.identifier}`,
            data: fd,
            headers: {
                'Authorization': `Bearer ${localStorage.getItem('token')}`
            },
            onUploadProgress: progressEvent => {
                // console.log('Upload Progress: ' + Math.round((progressEvent.loaded / progressEvent.total) * 100) + '%')
            }
        })
        .then(res => authUser(res.data.data))
    }

    render() {
        const { user, classes } = this.props
        const { avatar, fname, lname, gender } = user

        let defaultImg = '/images/male-placeholder.jpg'

        if (gender == 'female') defaultImg = '/images/female-placeholder.jpg'
        
        return (
            <div className={classes.profilePic}>
                <CustomTypography
                    bold="true"
                    variant="body1"
                    align="center"
                    gutterBottom
                >
                    Profile Picture
                </CustomTypography>
                <div className={classes.profileImage}>
                    <div className={classes.thumb}>
                        <Image
                            imgSrc={avatar ? `/images/${avatar}` : defaultImg}
                            imgAlt={`/${fname}-${lname}`}
                        />
                    </div>
                    <div className={classes.upload}>
                        <input
                            accept="image/*"
                            id="contained-button-file"
                            multiple
                            type="file"
                            name="avatar"
                            onChange={this.handleChange}
                            className={classes.input}
                        />
                        <label htmlFor="contained-button-file">
                            <Button className={classes.button} variant="contained" component="span">
                                <AddIcon />
                            </Button>
                        </label>
                    </div>
                </div>
            </div>
        )
    }
}

const mapstateToProps = state => ({
    user: state.auth.authUser
})

const mapDispatchToProps = dispatch => ({
    authUser: (user) => dispatch(actions.authUser(user))
})

export default connect(mapstateToProps, mapDispatchToProps)(withStyles(styles)(ProfileImage))