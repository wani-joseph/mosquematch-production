export const styles = theme => ({
  photos: {
    background: theme.palette.background.default,
    padding: theme.spacing(2, 0)
  },
  title: {
    borderBottom: '1px solid rgba(0, 0, 0, .08)',
    borderTop: '1px solid rgba(0, 0, 0, .08)',
    background: theme.palette.common.white,
    padding: theme.spacing(2)
  },
  subTitle: {
    display: 'flex'
  },
  handIcon: {
    display: 'inline-flex',
    width: 30,
    height: 30,
    marginRight: theme.spacing(1),
  },
  dropzoneWrapper: {
    padding: theme.spacing(3, 2),
    background: theme.palette.common.white,
  },
  images: {
    padding: theme.spacing(1),
    background: theme.palette.common.white,
  },
  image: {
    display: 'inline-block',
    maxWidth: '25%',
    padding: theme.spacing(1),
  },
  img: {
    display: 'block',
    maxWidth: '100%',
    height: 'auto',
    borderRadius: 10
  },
})