import React from 'react'
import { connect } from 'react-redux'
import DropZone from '../DropZone/DropZone'
import SvgIcon from '../../svgIcon/svgIcon'
import CustomTypography from '../../UI/CustomTypography/CustomTypography'
import { Grid, withStyles } from '@material-ui/core'
import { styles } from './PhotosStyles'
import {
  newAlbum,
  updateAlbum,
  moveImage,
  deleteImage
} from '../../../store/actions/auth'

const Photos = ({ user, newAlbum, updateAlbum, moveImage, deleteImage, classes }) => {
  const publicAlbum = user.albums.data.filter(album => album.title === 'public')
  const privateAlbum = user.albums.data.filter(album => album.title === 'private')

  return (
    <>
      <div className={classes.photos}>
        <div className={classes.title}>
          <Grid container>
            <Grid item lg={3}>
              <CustomTypography bold="true" variant="h5" component="p">
                Public Photos
              </CustomTypography>
            </Grid>
            <Grid item lg={9}>
              <CustomTypography
                bold="true"
                variant="h5"
                component="p"
                color="textSecondary"
                classes={{ root: classes.subTitle}}
              >
                <span className={classes.handIcon}>
                  <SvgIcon width="100%" height="100%" src="drag-flick" />
                </span>
                Drag and Drop to upload public photos
              </CustomTypography>
            </Grid>
          </Grid>
        </div>
        <DropZone
          buttonText={'Add Public Photo'}
          albumId={publicAlbum.length > 0 ? publicAlbum[0].identifier : null}
          moveToAlbumId={privateAlbum.length > 0 ? privateAlbum[0].identifier : null}
          albumName={'public'}
          moveToAlbumName={'private'}
          moveText={'Move to private'}
          images={publicAlbum.length > 0 ? publicAlbum[0].images.data : publicAlbum}
          newAlbum={newAlbum}
          updateAlbum={updateAlbum}
          moveImage={moveImage}
          deleteImage={deleteImage}
        />
      </div>
      <div className={classes.photos}>
        <div className={classes.title}>
          <Grid container>
            <Grid item lg={3}>
              <CustomTypography bold="true" variant="h5" component="p">
                Private Photos
              </CustomTypography>
            </Grid>
            <Grid item lg={9}>
              <CustomTypography
                bold="true"
                variant="h5"
                component="p"
                color="textSecondary"
                classes={{ root: classes.subTitle}}
              >
                <span className={classes.handIcon}>
                  <SvgIcon width="100%" height="100%" src="drag-flick" />
                </span>
                Drag and Drop to upload private photos
              </CustomTypography>
            </Grid>
          </Grid>
        </div>
        <DropZone
          buttonText={'Add Private Photo'}
          albumId={privateAlbum.length > 0 ? privateAlbum[0].identifier : null}
          moveToAlbumId={publicAlbum.length > 0 ? publicAlbum[0].identifier : null}
          albumName={'private'}
          moveToAlbumName={'public'}
          moveText={'Move to public'}
          images={privateAlbum.length > 0 ? privateAlbum[0].images.data : privateAlbum}
          newAlbum={newAlbum}
          updateAlbum={updateAlbum}
          moveImage={moveImage}
          deleteImage={deleteImage}
        />
      </div>
    </>
  )
}

const mapStateToProps = state => ({
  user: state.auth.authUser
})

const mapDispatchToProps = dispatch => ({
  newAlbum: (userId, albumData, imageData) => dispatch(newAlbum(userId, albumData, imageData)),
  updateAlbum: (userId, albumId, albumData, imageData) => dispatch(updateAlbum(userId, albumId, albumData, imageData)),
  moveImage: (userId, moveToAlbumId, moveToAlbumName, imageId) => dispatch(moveImage(userId, moveToAlbumId, moveToAlbumName, imageId)),
  deleteImage: (albumId, imageId) => dispatch(deleteImage(albumId, imageId))
})

export default connect(mapStateToProps, mapDispatchToProps)(withStyles(styles)(Photos))