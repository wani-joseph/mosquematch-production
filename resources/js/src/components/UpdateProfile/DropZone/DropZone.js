import React, { useState, useMemo, useEffect } from 'react'
import { connect } from 'react-redux'
import { useDropzone } from 'react-dropzone'
import { withStyles } from '@material-ui/core'
import CustomButton from '../../UI/CustomButton/CustomButton'
import CustomTypography from '../../UI/CustomTypography/CustomTypography'
import {
  styles,
  baseStyle,
  activeStyle,
  acceptStyle,
  rejectStyle
} from './DropZoneStyles'
import SettingsIcon from '@material-ui/icons/Settings'

function DropZone(props) {
  const [files, setFiles] = useState([])
  const [openListener, setOpenListener] = useState(false)
  const { buttonText, user, albumId, moveToAlbumId, albumName, moveToAlbumName, images, moveText, newAlbum, updateAlbum, moveImage, deleteImage, classes } = props
  const {
    getRootProps,
    getInputProps,
    isDragActive,
    isDragAccept,
    isDragReject,
    open,
  } = useDropzone({
    // Disable click and keydown behavior
    noClick: true,
    noKeyboard: true,
    accept: 'image/*',
    onDrop: acceptedFiles => {
      setFiles(acceptedFiles.map(file => Object.assign(file, {
        preview: URL.createObjectURL(file)
      })))

      if (albumId) {
        const albumData = new FormData()
        albumData.append('cover_image', acceptedFiles[0], acceptedFiles[0].name)
        albumData.append('_method', 'put')

        const ImageData = new FormData()
        ImageData.append('image', acceptedFiles[0], acceptedFiles[0].name)

        updateAlbum(user.identifier, albumId, albumData, ImageData)
      } else {
        const albumData = new FormData()
        albumData.append('name', albumName)
        albumData.append('cover_image', acceptedFiles[0], acceptedFiles[0].name)

        const ImageData = new FormData()
        ImageData.append('image', acceptedFiles[0], acceptedFiles[0].name)

        newAlbum(user.identifier, albumData, ImageData)
      }
    }
  })

  const handleClick = (identifier) => {
    setOpenListener(openListener == identifier ? false : identifier)
  }

  const handleMoveImage = (imageId) => {
    moveImage(user.identifier, moveToAlbumId, moveToAlbumName, imageId)
    setOpenListener(false)
  }

  const handleDeleteImage = (imageId) => {
    deleteImage(albumId, imageId)
    setOpenListener(false)
  }
  
  const thumbs = files.map(file => (
    <div key={file.name} className={classes.image}>
      <img
        className={classes.img}
        src={file.preview} />
    </div>
  ))

  useEffect(() => () => {
    // Make sure to revoke the data uris to avoid memory leaks
    files.forEach(file => URL.revokeObjectURL(file.preview))
  }, [files])

  const style = useMemo(() => ({
    ...baseStyle,
    ...(isDragActive ? activeStyle : {}),
    ...(isDragAccept ? acceptStyle : {}),
    ...(isDragReject ? rejectStyle : {})
  }), [
    isDragActive,
    isDragReject
  ])

  return (
    <>
      <div className={classes.images}>
        {images.length > 0 ? (
          images.map(image => (
            <div key={image.identifier} className={classes.image}>
              {buttonText == 'Add Private Photo'
                ? <div className={classes.private}></div>
                : null}
              <img
                className={classes.img}
                src={`/images/${image.title}`}
              />
              <div className={classes.settings}>
                <div className={classes.icon}>
                  <SettingsIcon onClick={() => handleClick(image.identifier)} />
                </div>
                {openListener == image.identifier ? (
                  <div className={classes.dropDown}>
                    <button
                      className={classes.button}
                      type="button"
                      onClick={() => handleMoveImage(image.identifier)}
                    >
                      {moveText}
                    </button>
                    <button
                      className={classes.button}
                      type="button"
                      onClick={() => handleDeleteImage(image.identifier)}
                    >
                      Delete
                    </button>
                  </div>
                ) : null}
              </div>
            </div>
          ))
        ) : null}
        {thumbs}
      </div>
      <div className={classes.dropzoneWrapper}>
        <div {...getRootProps({style})}>
          <input {...getInputProps()} />
          <CustomButton classes={{ root: classes.openDialog }} onClick={open}>
            {buttonText}
          </CustomButton>
          <CustomTypography
            classes={{ root: classes.dropZoneText }}
            color="textPrimary"
          >
            Photos need to be 400px witdth and 400px height
          </CustomTypography>
        </div>
      </div>
    </>
  )
}

const mapStateToProps = state => ({
  user: state.auth.authUser
})

export default connect(mapStateToProps)(withStyles(styles)(DropZone))