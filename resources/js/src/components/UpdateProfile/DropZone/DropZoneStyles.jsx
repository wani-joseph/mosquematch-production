export const styles = theme => ({
  images: {
    padding: theme.spacing(1),
    background: theme.palette.common.white,
  },
  image: {
    display: 'inline-block',
    maxWidth: '20%',
    padding: theme.spacing(1),
    position: 'relative',
    textAlign: 'right',
  },
  icon: {
    cursor: 'pointer',
  },
  settings: {
    position: 'absolute',
    right: 0,
    bottom: 0,
    padding: theme.spacing(2),
    color: 'white',
    width: '100%',
  },
  dropDown: {
    position: 'absolute',
    right: 0,
    bottom: -57,
    color: 'black',
    width: 'fit-content',
    margin: theme.spacing(1),
    border: '1px solid rgba(0, 0, 0, .08)',
    padding: theme.spacing(1),
    background: 'white',
    zIndex: 100
  },
  button: {
    padding: 0,
    background: 'transparent',
    border: 'none',
    outline: 'none',
    display: 'block',
    marginBottom: theme.spacing(1),
    cursor: 'pointer',
    '&:last-of-type': {
      marginBottom: 0
    }
  },
  img: {
    display: 'block',
    maxWidth: '100%',
    height: 'auto',
    borderRadius: 10
  },
  dropzoneWrapper: {
    background: theme.palette.common.white,
    padding: theme.spacing(2),
  },
  openDialog: {
    margin: theme.spacing(1),
    borderBottom: 'none',
    borderRadius: 4
  },
  dropZoneText: {
    margin: theme.spacing(1),
  },
  private: {
    width: 'calc(100% - 16px)',
    height: 'calc(100% - 16px)',
    background: theme.palette.primary.main,
    position: 'absolute',
    top: 8,
    left: 8,
    borderRadius: 10,
  }
})

export const baseStyle = {
  flex: 1,
  display: 'flex',
  flexDirection: 'column',
  alignItems: 'center',
  padding: '20px',
  borderWidth: 2,
  borderRadius: 2,
  borderColor: '#eeeeee',
  borderStyle: 'dashed',
  backgroundColor: '#ffffff',
  color: '#bdbdbd',
  outline: 'none',
  transition: 'border .24s ease-in-out'
}

export const activeStyle = {
  borderColor: '#2196f3'
}

export const acceptStyle = {
  borderColor: '#00e676'
}

export const rejectStyle = {
  borderColor: '#ff1744'
}