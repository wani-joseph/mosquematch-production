import { makeStyles } from '@material-ui/core/styles';

export const useStyles = makeStyles(theme =>({
    sectionTitle: {
        marginBottom: theme.spacing(1)
    }
}));