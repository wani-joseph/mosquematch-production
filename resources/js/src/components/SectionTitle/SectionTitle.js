import React from 'react'
import { Typography } from '@material-ui/core'
import { useStyles } from './SectionTitleStyles'

const SectionTitle = (props) => {
    const classes = useStyles()

    return (
        <Typography
            {...props}
            className={classes.sectionTitle}
            variant="h4"
            color="primary"
            component="h2"
            align="center"
        >
            {props.title}
        </Typography>
    )
}

export default SectionTitle