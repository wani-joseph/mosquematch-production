import React, { Component } from 'react'
import Modal from '../../components/UI/Modal/Modal'

const withErrorHandler = (WrappedComponent, axios) => {
    return class extends Component {
        constructor() {
            super()
            this.state = {
                errors: null
            }
            this.errorConfirmedHandler = this.errorConfirmedHandler.bind(this)
        }

        componentDidMount() {
            this.reqInterceptor = axios.interceptors.request.use(req => {
                this.setState({ errors: null })
                return req
            })
            this.resInterceptor = axios.interceptors.response.use(res => res, errors => {
                this.setState({ errors: errors })
                return Promise.reject(errors)
            })
        }

        componentWillUnmount() {
            axios.interceptors.request.eject(this.reqInterceptor);
            axios.interceptors.response.eject(this.resInterceptor);
        }

        errorConfirmedHandler() {
            this.setState(() => ({ errors: null }))
        }

        render() {
            const { errors } = this.state

            return (
                <React.Fragment>
                    <Modal
                        show={errors != null ? true : false}
                        handleClose={this.errorConfirmedHandler}
                    >
                        {errors ? errors.message : null}
                    </Modal>
                    <WrappedComponent {...this.props} />
                </React.Fragment>
            )
        }
    }
}

export default withErrorHandler