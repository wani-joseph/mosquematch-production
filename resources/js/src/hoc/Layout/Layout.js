import React, { Component } from 'react'
import Header from '../../components/Header/Header'
import Logo from '../../components/Logo/Logo'
import Navigation from '../../components/Navigation/Navigation'
import Profile from '../../components/Profile/Profile'
import PageContent from '../../components/PageContent/PageContent'

class Layout extends Component {
    constructor() {
        super()
        this.state = {
            sideDrawer: false
        }
        this.openSideDrawer = this.openSideDrawer.bind(this)
        this.closeSideDrawer = this.closeSideDrawer.bind(this)
    }

    openSideDrawer() {
        this.setState({ sideDrawer: true })
    }

    closeSideDrawer() {
        this.setState({ sideDrawer: false })
    }

    render() {
        const { nomargin } = this.props

        return (
            <React.Fragment>
                <Header
                    spacing="true"
                    siteLogo={<Logo sideDrawer="true" />}
                    mainNav={<Navigation />}
                    account={<Profile />}/>
                <PageContent nomargin={nomargin}>
                    {this.props.children}
                </PageContent>
            </React.Fragment>
        )
    }
}

export default Layout