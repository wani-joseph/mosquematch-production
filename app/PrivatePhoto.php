<?php

namespace App;

use App\User;
use Illuminate\Database\Eloquent\Model;
use App\Transformers\PrivatePhotoTransformer;
use Illuminate\Database\Eloquent\SoftDeletes;

class PrivatePhoto extends Model
{
    use SoftDeletes;

    const PENDING_REQUEST = 'pending';
    const CONFIRMED_REQUEST = 'confirmed';

    public $transformer = PrivatePhotoTransformer::class;
    protected $dates = ['deleted_at'];
    
    protected $fillable = [
        'requester_id',
        'requested_id',
        'status'
    ];

    public function confirmed(){
        return $this->status = PrivatePhoto::CONFIRMED_REQUEST;
    }

    public function requester()
    {
        return $this->belongsTo(User::class);
    }

    public function requested()
    {
        return $this->belongsTo(User::class);
    }
}
