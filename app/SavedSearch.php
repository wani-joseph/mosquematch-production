<?php

namespace App;

use App\User;
use Illuminate\Database\Eloquent\Model;
use App\Transformers\SavedSearchTransformer;
use Illuminate\Database\Eloquent\SoftDeletes;

class SavedSearch extends Model
{
    use SoftDeletes;

    public $transformer = SavedSearchTransformer::class;
    protected $dates = ['deleted_at'];
    
    protected $fillable = [
        'name',
        'url',
        'user_id',
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
