<?php

namespace App\Transformers;

use App\PrivatePhoto;
use App\Transformers\UserTransformer;
use League\Fractal\TransformerAbstract;

class PrivatePhotoTransformer extends TransformerAbstract
{
    /**
     * List of resources to automatically include
     *
     * @var array
     */
    protected $defaultIncludes = [
        'requester', 'requested'
    ];
    
    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = [
        //
    ];
    
    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(PrivatePhoto $privatePhoto)
    {
        return [
            'identifier'        => (int) $privatePhoto->id,
            'requester'         => (int) $privatePhoto->requester_id,
            'requested'         => (int) $privatePhoto->requested_id,
            'status'            => (string) $privatePhoto->status,
            'creationDate'      => (string) $privatePhoto->created_at,
            'modifiedDate'      => (string) $privatePhoto->updated_at,
            'deletedDate'       => isset($privatePhoto->deleted_at) ? (string) $privatePhoto->deleted_at : null,
        ];
    }

    public static function originalAttribute($index)
    {
        $attributes =  [
            'identifier'        => 'id',
            'requester'         => 'requester_id',
            'requested'         => 'requested_id',
            'status'            => 'status',
            'creationDate'      => 'created_at',
            'modifiedDate'      => 'updated_at',
            'deletedDate'       => 'deleted_at',
        ];

        return isset($attributes[$index]) ? $attributes[$index] : null;
    }

    public static function transformedAttribute($index)
    {
        $attributes =  [
            'id'                => 'identifier',
            'requester_id'      => 'requester',
            'requested_id'      => 'requested',
            'status'            => 'status',
            'creationDate'      => 'created_at',
            'modifiedDate'      => 'updated_at',
            'deletedDate'       => 'deleted_at',
        ];

        return isset($attributes[$index]) ? $attributes[$index] : null;
    }

    /**
     * Include Requester
     *
     * @return \League\Fractal\Resource\Item
     */
    public function includeRequester(PrivatePhoto $privatePhoto)
    {
        $requester = $privatePhoto->requester;

        return $this->item($requester, new UserTransformer);
    }

    /**
     * Include Requested
     *
     * @return \League\Fractal\Resource\Item
     */
    public function includeRequested(PrivatePhoto $privatePhoto)
    {
        $requested = $privatePhoto->requested;

        return $this->item($requested, new UserTransformer);
    }
}
