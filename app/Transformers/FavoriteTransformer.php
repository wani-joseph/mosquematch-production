<?php

namespace App\Transformers;

use App\Favorite;
use App\Transformers\UserTransformer;
use League\Fractal\TransformerAbstract;

class FavoriteTransformer extends TransformerAbstract
{
    /**
     * List of resources to automatically include
     *
     * @var array
     */
    protected $defaultIncludes = [
        'follower', 'following'
    ];
    
    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = [
        //
    ];
    
    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(Favorite $favorite)
    {
        return [
            'identifier'        => (int) $favorite->id,
            'status'            => (string) $favorite->status,
            'creationDate'      => (string) $favorite->created_at,
            'modifiedDate'      => (string) $favorite->updated_at,
            'deletedDate'       => isset($favorite->deleted_at) ? (string) $favorite->deleted_at : null,
        ];
    }

    public static function originalAttribute($index)
    {
        $attributes =  [
            'identifier'        => 'id',
            'status'            => 'status',
            'creationDate'      => 'created_at',
            'modifiedDate'      => 'updated_at',
            'deletedDate'       => 'deleted_at',
        ];

        return isset($attributes[$index]) ? $attributes[$index] : null;
    }

    public static function transformedAttribute($index)
    {
        $attributes =  [
            'id'                => 'identifier',
            'status'            => 'status',
            'creationDate'      => 'created_at',
            'modifiedDate'      => 'updated_at',
            'deletedDate'       => 'deleted_at',
        ];

        return isset($attributes[$index]) ? $attributes[$index] : null;
    }

    /**
     * Include Follower
     *
     * @return \League\Fractal\Resource\Item
     */
    public function includeFollower(Favorite $favorite)
    {
        $follower = $favorite->follower;

        return $this->item($follower, new UserTransformer);
    }

    /**
     * Include Following
     *
     * @return \League\Fractal\Resource\Item
     */
    public function includeFollowing(Favorite $favorite)
    {
        $following = $favorite->following;

        return $this->item($following, new UserTransformer);
    }
}
