<?php

namespace App\Transformers;

use App\Template;
use League\Fractal\TransformerAbstract;

class TemplateTransformer extends TransformerAbstract
{
    /**
     * List of resources to automatically include
     *
     * @var array
     */
    protected $defaultIncludes = [
        //
    ];
    
    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = [
        //
    ];
    
    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(Template $template)
    {
        return [
            'identifier'        => (int) $template->id,
            'template'          => (string) $template->text,
            'member'            => (int) $template->user_id,
            'creationDate'      => (string) $template->created_at,
            'modifiedDate'      => (string) $template->updated_at,
            'deletedDate'       => isset($template->deleted_at) ? (string) $template->deleted_at : null,
            'links' => [
                [
                    'rel'   => 'self',
                    'href'  => route('templates.show', $template->id),
                ],
            ]
        ];
    }

    public static function originalAttribute($index)
    {
        $attributes =  [
            'identifier'        => 'id',
            'template'          => 'text',
            'member'            => 'user_id',
            'creationDate'      => 'created_at',
            'modifiedDate'      => 'updated_at',
            'deletedDate'       => 'deleted_at'
        ];

        return isset($attributes[$index]) ? $attributes[$index] : null;
    }

    public static function transformedAttribute($index)
    {
        $attributes =  [
            'id'            => 'identifier',
            'text'          => 'template',
            'user_id'       => 'member',
            'created_at'    => 'creationDate',
            'updated_at'    => 'modifiedDate',
            'deleted_at'    => 'deletedDate'
        ];

        return isset($attributes[$index]) ? $attributes[$index] : null;
    }
}
