<?php

namespace App\Transformers;

use App\Message;
use App\Transformers\UserTransformer;
use League\Fractal\TransformerAbstract;

class MessageTransformer extends TransformerAbstract
{
    /**
     * List of resources to automatically include
     *
     * @var array
     */
    protected $defaultIncludes = [
        'sender', 'receiver'
    ];
    
    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = [
        //
    ];
    
    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(Message $message)
    {
        return [
            'identifier'        => (int) $message->id,
            'content'           => (string) $message->message,
            'isRead'            => (int) $message->is_read,
            'creationDate'      => (string) $message->created_at,
            'modifiedDate'      => (string) $message->updated_at,
            'deletedDate'       => isset($message->deleted_at) ? (string) $message->deleted_at : null,
        ];
    }

    public static function originalAttribute($index)
    {
        $attributes =  [
            'identifier'        => 'id',
            'content'           => 'message',
            'isRead'            => 'is_read',
            'creationDate'      => 'created_at',
            'modifiedDate'      => 'updated_at',
            'deletedDate'       => 'deleted_at',
        ];

        return isset($attributes[$index]) ? $attributes[$index] : null;
    }

    public static function transformedAttribute($index)
    {
        $attributes =  [
            'id'                => 'identifier',
            'content'           => 'message',
            'isRead'            => 'is_read',
            'creationDate'      => 'created_at',
            'modifiedDate'      => 'updated_at',
            'deletedDate'       => 'deleted_at',
        ];

        return isset($attributes[$index]) ? $attributes[$index] : null;
    }

    /**
     * Include Sender
     *
     * @return \League\Fractal\Resource\Item
     */
    public function includeSender(Message $message)
    {
        $sender = $message->sender;

        return $this->item($sender, new UserTransformer);
    }

    /**
     * Include Receiver
     *
     * @return \League\Fractal\Resource\Item
     */
    public function includeReceiver(Message $message)
    {
        $receiver = $message->receiver;

        return $this->item($receiver, new UserTransformer);
    }
}
