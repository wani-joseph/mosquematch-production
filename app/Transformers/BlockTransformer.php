<?php

namespace App\Transformers;

use App\Block;
use App\Transformers\UserTransformer;
use League\Fractal\TransformerAbstract;

class BlockTransformer extends TransformerAbstract
{
    /**
     * List of resources to automatically include
     *
     * @var array
     */
    protected $defaultIncludes = [
        'blocker', 'blocked'
    ];
    
    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = [
        //
    ];
    
    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(Block $block)
    {
        return [
            'identifier'        => (int) $block->id,
            'status'            => (string) $block->status,
            'creationDate'      => (string) $block->created_at,
            'modifiedDate'      => (string) $block->updated_at,
            'deletedDate'       => isset($block->deleted_at) ? (string) $block->deleted_at : null,
        ];
    }

    public static function originalAttribute($index)
    {
        $attributes =  [
            'identifier'        => 'id',
            'status'            => 'status',
            'creationDate'      => 'created_at',
            'modifiedDate'      => 'updated_at',
            'deletedDate'       => 'deleted_at',
        ];

        return isset($attributes[$index]) ? $attributes[$index] : null;
    }

    public static function transformedAttribute($index)
    {
        $attributes =  [
            'id'                => 'identifier',
            'status'            => 'status',
            'creationDate'      => 'created_at',
            'modifiedDate'      => 'updated_at',
            'deletedDate'       => 'deleted_at',
        ];

        return isset($attributes[$index]) ? $attributes[$index] : null;
    }

    /**
     * Include Blocker
     *
     * @return \League\Fractal\Resource\Item
     */
    public function includeBlocker(Block $block)
    {
        $blocker = $block->blocker;

        return $this->item($blocker, new UserTransformer);
    }

    /**
     * Include Blocked
     *
     * @return \League\Fractal\Resource\Item
     */
    public function includeBlocked(Block $block)
    {
        $blocked = $block->blocked;

        return $this->item($blocked, new UserTransformer);
    }
}
