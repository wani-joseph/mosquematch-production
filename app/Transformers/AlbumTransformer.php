<?php

namespace App\Transformers;

use App\Album;
use League\Fractal\TransformerAbstract;

class AlbumTransformer extends TransformerAbstract
{
    /**
     * List of resources to automatically include
     *
     * @var array
     */
    protected $defaultIncludes = [
        'images'
    ];
    
    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = [
        //
    ];
    
    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(Album $album)
    {
        return [
            'identifier'        => (int) $album->id,
            'title'             => (string) $album->name,
            'description'       => (string) $album->description,
            'cover'             => (string) $album->cover_image,
            'user'              => (int) $album->user_id,
            'creationDate'      => (string) $album->created_at,
            'modifiedDate'      => (string) $album->updated_at,
            'deletedDate'       => isset($album->deleted_at) ? (string) $album->deleted_at : null,
        ];
    }

    public static function originalAttribute($index)
    {
        $attributes =  [
            'identifier'        => 'id',
            'title'             => 'name',
            'description'       => 'description',
            'cover'             => 'cover_image',
            'user'              => 'user_id',
            'creationDate'      => 'created_at',
            'modifiedDate'      => 'updated_at',
            'deletedDate'       => 'deleted_at',
        ];

        return isset($attributes[$index]) ? $attributes[$index] : null;
    }

    public static function transformedAttribute($index)
    {
        $attributes =  [
            'id'                => 'identifier',
            'name'              => 'title',
            'description'       => 'description',
            'cover_image'       => 'cover',
            'user_id'           => 'user',
            'creationDate'      => 'created_at',
            'modifiedDate'      => 'updated_at',
            'deletedDate'       => 'deleted_at',
        ];

        return isset($attributes[$index]) ? $attributes[$index] : null;
    }

    /**
     * Include Images
     *
     * @return \League\Fractal\Resource\Collection
     */
    public function includeImages(Album $album)
    {
        $images = $album->images;

        return $this->collection($images, new ImageTransformer);
    }
}
