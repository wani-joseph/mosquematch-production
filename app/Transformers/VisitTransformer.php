<?php

namespace App\Transformers;

use App\Visit;
use App\Transformers\UserTransformer;
use League\Fractal\TransformerAbstract;

class VisitTransformer extends TransformerAbstract
{
    /**
     * List of resources to automatically include
     *
     * @var array
     */
    protected $defaultIncludes = [
        'visitor', 'visited'
    ];
    
    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = [
        //
    ];
    
    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(Visit $visit)
    {
        return [
            'identifier'        => (int) $visit->id,
            'status'            => (string) $visit->status,
            'creationDate'      => (string) $visit->created_at,
            'modifiedDate'      => (string) $visit->updated_at,
            'deletedDate'       => isset($visit->deleted_at) ? (string) $visit->deleted_at : null,
        ];
    }

    public static function originalAttribute($index)
    {
        $attributes =  [
            'identifier'        => 'id',
            'status'            => 'status',
            'creationDate'      => 'created_at',
            'modifiedDate'      => 'updated_at',
            'deletedDate'       => 'deleted_at',
        ];

        return isset($attributes[$index]) ? $attributes[$index] : null;
    }

    public static function transformedAttribute($index)
    {
        $attributes =  [
            'id'                => 'identifier',
            'status'            => 'status',
            'creationDate'      => 'created_at',
            'modifiedDate'      => 'updated_at',
            'deletedDate'       => 'deleted_at',
        ];

        return isset($attributes[$index]) ? $attributes[$index] : null;
    }

    /**
     * Include Visitor
     *
     * @return \League\Fractal\Resource\Item
     */
    public function includeVisitor(Visit $visit)
    {
        $visitor = $visit->visitor;

        return $this->item($visitor, new UserTransformer);
    }

    /**
     * Include Visited
     *
     * @return \League\Fractal\Resource\Item
     */
    public function includeVisited(Visit $visit)
    {
        $visited = $visit->visited;

        return $this->item($visited, new UserTransformer);
    }
}
