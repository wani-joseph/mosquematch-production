<?php

namespace App\Transformers;

use App\Wali;
use League\Fractal\TransformerAbstract;

class WaliTransformer extends TransformerAbstract
{
    /**
     * List of resources to automatically include
     *
     * @var array
     */
    protected $defaultIncludes = [
        'requester', 'requested'
    ];
    
    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = [
        //
    ];
    
    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(Wali $wali)
    {
        return [
            'identifier'        => (int) $wali->id,
            'requester'         => (int) $wali->requester_id,
            'requested'         => (int) $wali->requested_id,
            'status'            => (string) $wali->status,
            'creationDate'      => (string) $wali->created_at,
            'modifiedDate'      => (string) $wali->updated_at,
            'deletedDate'       => isset($wali->deleted_at) ? (string) $wali->deleted_at : null,
        ];
    }

    public static function originalAttribute($index)
    {
        $attributes =  [
            'identifier'        => 'id',
            'requester'         => 'requester_id',
            'requested'         => 'requested_id',
            'status'            => 'status',
            'creationDate'      => 'created_at',
            'modifiedDate'      => 'updated_at',
            'deletedDate'       => 'deleted_at',
        ];

        return isset($attributes[$index]) ? $attributes[$index] : null;
    }

    public static function transformedAttribute($index)
    {
        $attributes =  [
            'id'                => 'identifier',
            'requester_id'      => 'requester',
            'requested_id'      => 'requested',
            'status'            => 'status',
            'creationDate'      => 'created_at',
            'modifiedDate'      => 'updated_at',
            'deletedDate'       => 'deleted_at',
        ];

        return isset($attributes[$index]) ? $attributes[$index] : null;
    }

    /**
     * Include Requester
     *
     * @return \League\Fractal\Resource\Item
     */
    public function includeRequester(Wali $wali)
    {
        $requester = $wali->requester;

        return $this->item($requester, new UserTransformer);
    }

    /**
     * Include Requested
     *
     * @return \League\Fractal\Resource\Item
     */
    public function includeRequested(Wali $wali)
    {
        $requested = $wali->requested;

        return $this->item($requested, new UserTransformer);
    }
}
