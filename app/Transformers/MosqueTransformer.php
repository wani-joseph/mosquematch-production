<?php

namespace App\Transformers;

use App\Mosque;
use League\Fractal\TransformerAbstract;

class MosqueTransformer extends TransformerAbstract
{
    /**
     * List of resources to automatically include
     *
     * @var array
     */
    protected $defaultIncludes = [
        //
    ];
    
    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = [
        //
    ];
    
    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(Mosque $mosque)
    {
        return [
            'identifier'    => (int) $mosque->id,
            'mosqueName'    => (string) $mosque->name,
            'mosqueImam'    => (string) $mosque->imam,
            'coveredRegion' => (string) $mosque->region,
            'city'          => (string) $mosque->city,
            'streetAddress' => (string) $mosque->address,
            'postCode'      => (string) $mosque->post_code,
            'mosqueNumber'  => (string) $mosque->contact,
            'email'         => (string) $mosque->email,
            'website'       => (string) $mosque->website,
            'latitude'      => (string) $mosque->lat,
            'longtitude'    => (string) $mosque->lng,
            'creationDate'  => (string) $mosque->created_at,
            'modifiedDate'  => (string) $mosque->updated_at,
            'deletedDate'   => isset($mosque->deleted_at) ? (string) $mosque->deleted_at : null,
        ];
    }

    public static function originalAttribute($index)
    {
        $attributes =  [
            'identifier'    => 'id',
            'mosqueName'    => 'name',
            'mosqueImam'    => 'imam',
            'coveredRegion' => 'region',
            'city'          => 'city',
            'streetAddress' => 'address',
            'postCode'      => 'post_code',
            'mosqueNumber'  => 'contact',
            'email'         => 'email',
            'website'       => 'website',
            'latitude'      => 'lat',
            'longtitude'    => 'lng',
            'creationDate'  => 'created_at',
            'modifiedDate'  => 'updated_at',
            'deletedDate'   => 'deleted_at',
        ];

        return isset($attributes[$index]) ? $attributes[$index] : null;
    }

    public static function transformedAttribute($index)
    {
        $attributes =  [
            'id'            => 'identifier',
            'name'          => 'mosqueName',
            'imam'          => 'mosqueImam',
            'region'        => 'coveredRegion',
            'city'          => 'city',
            'address'       => 'streetAddress',
            'post_code'     => 'postCode',
            'contact'       => 'mosqueNumber',
            'email'         => 'email',
            'website'       => 'website',
            'lat'           => 'latitude',
            'lng'           => 'longtitude',
            'created_at'    => 'creationDate',
            'updated_at'    => 'modifiedDate',
            'deleted_at'    => 'deletedDate',
        ];

        return isset($attributes[$index]) ? $attributes[$index] : null;
    }
}
