<?php

namespace App\Transformers;

use App\User;
use App\Transformers\AlbumTransformer;
use App\Transformers\MosqueTransformer;
use League\Fractal\TransformerAbstract;
use App\Transformers\TemplateTransformer;
use App\Transformers\SavedSearchTransformer;

class UserTransformer extends TransformerAbstract
{
    /**
     * List of resources to automatically include
     *
     * @var array
     */
    protected $defaultIncludes = [
        'albums', 'mosques', 'savedSearches'
    ];
    
    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = [
        'templates'
    ];
    
    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(User $user)
    {
        return [
            'identifier'    => (int) $user->id,
            'fname'         => (string) $user->firstname,
            'lname'         => (string) $user->lastname,
            'email'         => (string) $user->email,
            'isVerified'    => (int) $user->verified,
            'isAdmin'       => ($user->admin === 'true'),
            'gender'        => (string) $user->gender,
            'location'      => (string) $user->city,
            'dayOfBirth'    => (string) $user->day,
            'monthOfBirth'  => (string) $user->month,
            'yearOfBirth'   => (string) $user->year,
            'countryBirth'  => (string) $user->country_of_birth,
            'tagline'       => (string) $user->tagline,
            'bio'           => (string) $user->about_you,
            'seeking'       => (string) $user->looking,
            'eduLevel'      => (string) $user->education,
            'subject'       => (string) $user->subject,
            'jobTitle'      => (string) $user->job,
            'profession'    => (string) $user->profession,
            'fLang'         => (string) $user->first_language, 
            'sLang'         => (string) $user->second_language,
            'citizenship'   => (string) $user->citizenship,
            'countryOrigin' => (string) $user->country_of_origin,
            'relocate'      => (string) $user->relocate,
            'income'        => (string) $user->income,
            'marriageWithin'=> (string) $user->marriage_within, 
            'martialStatus' => (string) $user->martial_status,
            'children'      => (string) $user->children,
            'likeChildren'  => (string) $user->like_children,
            'arrangements'  => (string) $user->living_arrangement,
            'height'        => (string) $user->height,
            'build'         => (string) $user->build,
            'smoke'         => (string) $user->smoke,
            'drink'         => (string) $user->drink,
            'disabilities'  => (string) $user->disabilities,
            'phonetime'     => (string) $user->daily_phone_time,
            'religious'     => (string) $user->religiousness,
            'sector'        => (string) $user->sector,
            'hijab'         => (string) $user->hijab,
            'beard'         => (string) $user->beard,
            'revert'        => (string) $user->revert, 
            'keepHalal'     => (string) $user->halal,
            'prayer'        => (string) $user->prayer,
            'readQuran'     => (string) $user->read_quran,
            'avatar'        => (string) $user->profile_image,
            'waliName'      => (string) $user->wali_name,
            'waliContact'   => (string) $user->wali_contact,
            'waliEmail'     => (string) $user->wali_email,
            'status'        => (string) $user->status,
            'latitude'      => (string) $user->lat,
            'longtitude'    => (string) $user->lng,
            'creationDate'  => (string) $user->created_at,
            'modifiedDate'  => (string) $user->updated_at,
            'deletedDate'   => isset($user->deleted_at) ? (string) $user->deleted_at : null,
        ];
    }

    public static function originalAttribute($index)
    {
        $attributes =  [
            'identifier'    => 'id',
            'fname'         => 'firstname',
            'lname'         => 'lastname',
            'email'         => 'email',
            'isVerified'    => 'verified',
            'isAdmin'       => 'admin',
            'gender'        => 'gender',
            'location'      => 'city',
            'dayOfBirth'    => 'day',
            'monthOfBirth'  => 'month',
            'yearOfBirth'   => 'year',
            'countryBirth'  => 'country_of_birth',
            'tagline'       => 'tagline',
            'bio'           => 'about_you',
            'seeking'       => 'looking',
            'eduLevel'      => 'education',
            'subject'       => 'subject',
            'jobTitle'      => 'job',
            'profession'    => 'profession',
            'fLang'         => 'first_language', 
            'sLang'         => 'second_language',
            'citizenship'   => 'citizenship',
            'countryOrigin' => 'country_of_origin',
            'relocate'      => 'relocate',
            'income'        => 'income',
            'marriageWithin'=> 'marriage_within', 
            'martialStatus' => 'martial_status',
            'children'      => 'children',
            'likeChildren'  => 'like_children',
            'arrangements'  => 'living_arrangement',
            'height'        => 'height',
            'build'         => 'build',
            'smoke'         => 'smoke',
            'drink'         => 'drink',
            'disabilities'  => 'disabilities',
            'phonetime'     => 'daily_phone_time',
            'religious'     => 'religiousness',
            'sector'        => 'sector',
            'hijab'         => 'hijab',
            'beard'         => 'beard',
            'revert'        => 'revert', 
            'keepHalal'     => 'halal',
            'prayer'        => 'prayer',
            'readQuran'     => 'read_quran',
            'avatar'        => 'profile_image',
            'waliName'      => 'wali_name',
            'waliContact'   => 'wali_contact',
            'waliEmail'     => 'wali_email',
            'status'        => 'status',
            'latitude'      => 'lat',
            'longtitude'    => 'lng',
            'creationDate'  => 'created_at',
            'modifiedDate'  => 'updated_at',
            'deletedDate'   => 'deleted_at',
        ];

        return isset($attributes[$index]) ? $attributes[$index] : null;
    }

    public static function transformedAttribute($index)
    {
        $attributes =  [
            'id'                => 'identifier',
            'firstname'         => 'fname',
            'lastname'          => 'lname',
            'email'             => 'email',
            'verified'          => 'isVerified',
            'admin'             => 'isAdmin',
            'gender'            => 'gender',
            'city'              => 'location',
            'day'               => 'dayOfBirth',
            'month'             => 'monthOfBirth',
            'year'              => 'yearOfBirth',
            'country_of_birth'  => 'countryBirth',
            'tagline'           => 'tagline',
            'about_you'         => 'bio',
            'looking'           => 'seeking',
            'education'         => 'eduLevel',
            'subject'           => 'subject',
            'job'               => 'jobTitle',
            'profession'        => 'profession',
            'first_language'    => 'fLang', 
            'second_language'   => 'sLang',
            'citizenship'       => 'citizenship',
            'country_of_origin' => 'countryOrigin',
            'relocate'          => 'relocate',
            'income'            => 'income',
            'marriage_within'   => 'marriageWithin', 
            'martial_status'    => 'martialStatus',
            'children'          => 'children',
            'like_children'     => 'likeChildren',
            'living_arrangement'=> 'arrangements',
            'height'            => 'height',
            'build'             => 'build',
            'smoke'             => 'smoke',
            'drink'             => 'drink',
            'disabilities'      => 'disabilities',
            'daily_phone_time'  => 'phonetime',
            'religiousness'     => 'religious',
            'sector'            => 'sector',
            'hijab'             => 'hijab',
            'beard'             => 'beard',
            'revert'            => 'revert', 
            'halal'             => 'keepHalal',
            'prayer'            => 'prayer',
            'read_quran'        => 'readQuran',
            'profile_image'     => 'avatar',
            'wali_name'         => 'waliName',
            'wali_contact'      => 'waliContact',
            'wali_email'        => 'waliEmail',
            'status'            => 'status',
            'lat'               => 'latitude',
            'lng'               => 'longtitude',
            'creationDate'      => 'created_at',
            'modifiedDate'      => 'updated_at',
            'deletedDate'       => 'deleted_at',
        ];

        return isset($attributes[$index]) ? $attributes[$index] : null;
    }

    /**
     * Include Albums
     *
     * @return \League\Fractal\Resource\Collection
     */
    public function includeAlbums(User $user)
    {
        $albums = $user->albums;

        return $this->collection($albums, new AlbumTransformer);
    }

    /**
     * Include Mosques
     *
     * @return \League\Fractal\Resource\Collection
     */
    public function includeMosques(User $user)
    {
        $mosques = $user->mosques;

        return $this->collection($mosques, new MosqueTransformer);
    }

    /**
     * Include SavedSearches
     *
     * @return \League\Fractal\Resource\Collection
     */
    public function includeSavedSearches(User $user)
    {
        $savedSearches = $user->savedSearches;

        return $this->collection($savedSearches, new SavedSearchTransformer);
    }

    /**
     * Include Templates
     *
     * @return \League\Fractal\Resource\Collection
     */
    public function includeTemplates(User $user)
    {
        $templates = $user->templates;

        return $this->collection($templates, new TemplateTransformer);
    }
}
