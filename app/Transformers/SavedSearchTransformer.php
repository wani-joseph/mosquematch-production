<?php

namespace App\Transformers;

use App\SavedSearch;
use League\Fractal\TransformerAbstract;

class SavedSearchTransformer extends TransformerAbstract
{
    /**
     * List of resources to automatically include
     *
     * @var array
     */
    protected $defaultIncludes = [
        //
    ];
    
    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = [
        //
    ];
    
    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(SavedSearch $savedSearch)
    {
        return [
            'identifier'        => (int) $savedSearch->id,
            'searchName'        => (string) $savedSearch->name,
            'searchUrl'        => (string) $savedSearch->url,
            'member'            => (int) $savedSearch->user_id,
            'creationDate'      => (string) $savedSearch->created_at,
            'modifiedDate'      => (string) $savedSearch->updated_at,
            'deletedDate'       => isset($savedSearch->deleted_at) ? (string) $savedSearch->deleted_at : null,
        ];
    }

    public static function originalAttribute($index)
    {
        $attributes =  [
            'identifier'        => 'id',
            'searchName'        => 'name',
            'searchUrl'         => 'url',
            'member'            => 'user_id',
            'creationDate'      => 'created_at',
            'modifiedDate'      => 'updated_at',
            'deletedDate'       => 'deleted_at'
        ];

        return isset($attributes[$index]) ? $attributes[$index] : null;
    }

    public static function transformedAttribute($index)
    {
        $attributes =  [
            'id'            => 'identifier',
            'name'          => 'searchName',
            'url'           => 'searchUrl',
            'user_id'       => 'member',
            'created_at'    => 'creationDate',
            'updated_at'    => 'modifiedDate',
            'deleted_at'    => 'deletedDate'
        ];

        return isset($attributes[$index]) ? $attributes[$index] : null;
    }
}
