<?php

namespace App\Transformers;

use App\Report;
use App\Transformers\UserTransformer;
use League\Fractal\TransformerAbstract;

class ReportTransformer extends TransformerAbstract
{
    /**
     * List of resources to automatically include
     *
     * @var array
     */
    protected $defaultIncludes = [
        'reporter', 'reported'
    ];
    
    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = [
        //
    ];
    
    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(Report $report)
    {
        return [
            'identifier'        => (int) $report->id,
            'report'            => (string) $report->message,
            'status'            => (string) $report->status,
            'creationDate'      => (string) $report->created_at,
            'modifiedDate'      => (string) $report->updated_at,
            'deletedDate'       => isset($report->deleted_at) ? (string) $report->deleted_at : null,
        ];
    }

    public static function originalAttribute($index)
    {
        $attributes =  [
            'identifier'        => 'id',
            'report'            => 'message',
            'status'            => 'status',
            'creationDate'      => 'created_at',
            'modifiedDate'      => 'updated_at',
            'deletedDate'       => 'deleted_at',
        ];

        return isset($attributes[$index]) ? $attributes[$index] : null;
    }

    public static function transformedAttribute($index)
    {
        $attributes =  [
            'id'                => 'identifier',
            'message'           => 'report',
            'status'            => 'status',
            'creationDate'      => 'created_at',
            'modifiedDate'      => 'updated_at',
            'deletedDate'       => 'deleted_at',
        ];

        return isset($attributes[$index]) ? $attributes[$index] : null;
    }

    /**
     * Include Reporter
     *
     * @return \League\Fractal\Resource\Item
     */
    public function includeReporter(Report $report)
    {
        $reporter = $report->reporter;

        return $this->item($reporter, new UserTransformer);
    }

    /**
     * Include Reported
     *
     * @return \League\Fractal\Resource\Item
     */
    public function includeReported(Report $report)
    {
        $reported = $report->reported;

        return $this->item($reported, new UserTransformer);
    }
}
