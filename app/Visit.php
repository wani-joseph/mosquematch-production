<?php

namespace App;

use App\User;
use App\Transformers\VisitTransformer;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Visit extends Model
{
    use SoftDeletes;

    const PENDING_VISIT = 'pending';
    const CONFIRMED_VISIT = 'confirmed';

    public $transformer = VisitTransformer::class;
    protected $dates = ['deleted_at'];
    
    protected $fillable = [
        'visitor_id',
        'visited_id',
        'status',
    ];

    public function confirmed(){
        return $this->status = Visit::CONFIRMED_VISIT;
    }

    public function visitor()
    {
        return $this->belongsTo(User::class);
    }

    public function visited()
    {
        return $this->belongsTo(User::class);
    }
}
