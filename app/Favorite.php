<?php

namespace App;

use App\User;
use Illuminate\Database\Eloquent\Model;
use App\Transformers\FavoriteTransformer;
use Illuminate\Database\Eloquent\SoftDeletes;

class Favorite extends Model
{
    use SoftDeletes;

    const PENDING_FAVORITE = 'pending';
    const CONFIRMED_FAVORITE = 'confirmed';

    public $transformer = FavoriteTransformer::class;
    protected $dates = ['deleted_at'];
    
    protected $fillable = [
        'follower_id',
        'following_id',
        'status'
    ];

    public function confirmed(){
        return $this->status = Favorite::CONFIRMED_FAVORITE;
    }

    public function follower()
    {
        return $this->belongsTo(User::class);
    }

    public function following()
    {
        return $this->belongsTo(User::class);
    }
}
