<?php

namespace App\Http\Controllers\User;

use App\User;
use App\Album;
use Illuminate\Http\Request;
use App\Http\Controllers\ApiController;
use Illuminate\Support\Facades\Storage;
use Symfony\Component\HttpKernel\Exception\HttpException;

class UserAlbumController extends ApiController
{
    public function __construct()
    {
        parent::__construct();

        $this->middleware('auth:api');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(User $user)
    {
        $albums = $user->albums;

        return $this->showAll($albums);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, User $user)
    {
        $rules = [
            'name' => 'required',
            'cover_image'=> 'image'
        ];

        $this->validate($request, $rules);

        $data = $request->all();

        $data['user_id'] = $user->id;
        if ($request->hasFile('cover_image')) {
            $data['cover_image'] = $request->cover_image->store('');
        }

        $album = Album::create($data);

        return $this->showOne($album);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user, Album $album)
    {
        $rules = [
            'cover_image' => 'image',
        ];

        $this->validate($request, $rules);

        if ($request->hasFile('cover_image')) {
            Storage::delete($album->cover_image);
            $album->cover_image = $request->cover_image->store('');
        }

        if ($album->isClean()) {
            return $this->errorResponser('You need to specify different field to update', 422);
        }

        $album->save();

        return $this->showOne($album);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user, Album $album)
    {
        $this->checkUser($user, $album);

        $album->delete();
        Storage::delete($album->cover_image);

        return $this->showOne($album);
    }

    protected function checkUser(User $user, Album $album)
    {
        if ($user->id != $album->user_id) {
            throw new HttpException(422, 'The specified user is not the actual creator of this album');
        }
    }
}
