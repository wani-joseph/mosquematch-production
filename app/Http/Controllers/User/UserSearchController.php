<?php

namespace App\Http\Controllers\User;

use App\User;
use App\Block;
use App\Visit;
use App\Favorite;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\ApiController;

class UserSearchController extends ApiController
{
    public function __construct()
    {
        parent::__construct();

        $this->middleware('auth:api');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, User $user)
    {
        $gender = $user->gender === 'male' ? 'female' : 'male';

        if ($request->has('filters')) {

            if ($request->filters == 'Viewed') {

                $members = User::all()->where('gender', $gender)->filter(
                    function($member) use($user) {
                        $viewedList = Visit::all()->where('visitor_id', $user->id);
        
                        if ($viewedList->contains('visited_id', $member->id)) {
                            return $member;
                        }
                    });
        
                return $this->showAll($members);
            }

            if ($request->filters == 'Viewed Me') {

                $members = User::all()->where('gender', $gender)->filter(
                    function($member) use($user) {
                        $viewedList = Visit::all()->where('visited_id', $user->id);
        
                        if ($viewedList->contains('visitor_id', $member->id)) {
                            return $member;
                        }
                    });
        
                return $this->showAll($members);
            }

            if ($request->filters == 'Favorited') {

                $members = User::all()->where('gender', $gender)->filter(
                    function($member) use($user) {
                        $favoritedList = Favorite::all()->where('follower_id', $user->id);
        
                        if ($favoritedList->contains('following_id', $member->id)) {
                            return $member;
                        }
                    });
        
                return $this->showAll($members);
            }

            if ($request->filters == 'Favorited Me') {

                $members = User::all()->where('gender', $gender)->filter(
                    function($member) use($user) {
                        $favoritedList = Favorite::all()->where('following_id', $user->id);
        
                        if ($favoritedList->contains('follower_id', $member->id)) {
                            return $member;
                        }
                    });
        
                return $this->showAll($members);
            }

            
        }

        $members = User::all()->where('gender', $gender)->map(
            function($member) use($user) {
                $blockedList = Block::all()->where('blocker_id', $user->id);

                if (!$blockedList->contains('blocked_id', $member->id)) {
                    return $member;
                }
            });

        return $this->showAll($members);
    }
}
