<?php

namespace App\Http\Controllers\User;

use App\User;
use App\Block;
use Illuminate\Http\Request;
use App\Http\Controllers\ApiController;

class UserBlockedController extends ApiController
{
    public function __construct()
    {
        parent::__construct();

        $this->middleware('auth:api');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(User $user)
    {
        $blocks = Block::all()->where('blocker_id', $user->id);

        return $this->showAll($blocks);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, User $user)
    {
        $rules = [
            'blocked_id' => 'required|integer',
        ];

        $this->validate($request, $rules);

        $data = $request->all();
        $data['blocker_id'] = $user->id;

        if ($user->id == $request->blocked_id) {
            return $this->errorResponser('The blocked must be different from the blocker', 409);
        }

        $block = Block::create($data);

        return $this->showOne($block);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user, $member)
    {
        $block = Block::where('blocker_id', $user->id)
            ->where('blocked_id', $member)->first();

        $block->delete();

        return $this->showOne($block);
    }
}
