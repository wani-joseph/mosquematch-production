<?php

namespace App\Http\Controllers\User;

use App\User;
use App\Message;
use Illuminate\Http\Request;
use App\Http\Controllers\ApiController;

class UserInboxController extends ApiController
{
    public function __construct()
    {
        parent::__construct();

        $this->middleware('auth:api');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(User $user)
    {
        $messages = Message::all()->where('receiver_id', $user->id);

        return $this->showAll($messages);
    }
}
