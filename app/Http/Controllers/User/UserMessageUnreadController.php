<?php

namespace App\Http\Controllers\User;

use App\User;
use App\Message;
use Illuminate\Http\Request;
use App\Http\Controllers\ApiController;

class UserMessageUnreadController extends ApiController
{
    public function __construct()
    {
        parent::__construct();

        $this->middleware('auth:api');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, User $user)
    {
        $rules = [
            'contact' => 'required|integer',
        ];

        $this->validate($request, $rules);

        $messages = Message::where(function($query) use($user, $request) {
            $query->where('sender_id', $request->contact);
            $query->where('receiver_id', $user->id);
            $query->where('is_read', '0');
        })->get();

        return $this->showAll($messages);
    }
}
