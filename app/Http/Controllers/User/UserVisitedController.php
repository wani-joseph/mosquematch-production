<?php

namespace App\Http\Controllers\User;

use App\User;
use App\Visit;
use Illuminate\Http\Request;
use App\Http\Controllers\ApiController;

class UserVisitedController extends ApiController
{
    public function __construct()
    {
        parent::__construct();

        $this->middleware('auth:api');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(User $user)
    {
        $visits = Visit::all()->where('visitor_id', $user->id);

        return $this->showAll($visits);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, User $user)
    {
        $rules = [
            'visited_id' => 'required|integer',
        ];

        $this->validate($request, $rules);

        $data = $request->all();
        $data['visitor_id'] = $user->id;

        $visited = Visit::where('visitor_id', $user->id)
            ->where('visited_id', $request->visited_id)->first();

        if ($visited) {
            return $this->errorResponser('You already visit this member', 409);
        }

        if ($user->id == $request->visited_id) {
            return $this->errorResponser('The visited must be different from the visitor', 409);
        }

        $visit = Visit::create($data);

        return $this->showOne($visit);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user, $member)
    {
        $visit = Visit::where('visitor_id', $user->id)
            ->where('visited_id', $member)->first();

        $visit->delete();

        return $this->showOne($visit);
    }
}
