<?php

namespace App\Http\Controllers\User;

use App\User;
use App\Favorite;
use Illuminate\Http\Request;
use App\Http\Controllers\ApiController;

class UserFollowerController extends ApiController
{
    public function __construct()
    {
        parent::__construct();

        $this->middleware('auth:api');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(User $user)
    {
        $favorites = Favorite::all()->where('following_id', $user->id);

        return $this->showAll($favorites);
    }
}
