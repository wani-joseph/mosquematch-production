<?php

namespace App\Http\Controllers\User;

use App\User;
use App\Report;
use Illuminate\Http\Request;
use App\Http\Controllers\ApiController;

class UserReportedController extends ApiController
{
    public function __construct()
    {
        parent::__construct();

        $this->middleware('auth:api');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(User $user)
    {
        $reported = Report::all()->where('reporter_id', $user->id);

        return $this->showAll($reported);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, User $user)
    {
        $rules = [
            'reported_id' => 'required|integer',
            'message' => 'required'
        ];

        $this->validate($request, $rules);

        $data = $request->all();
        $data['reporter_id'] = $user->id;

        if ($user->id == $request->reported_id) {
            return $this->errorResponser('The reported must be different from the reporter', 409);
        }

        $report = Report::create($data);

        return $this->showOne($report);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user, $member)
    {
        $report = Report::where('reporter_id', $user->id)
            ->where('reported_id', $member)->first();

        $report->delete();

        return $this->showOne($report);
    }
}
