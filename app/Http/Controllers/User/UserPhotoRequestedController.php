<?php

namespace App\Http\Controllers\User;

use App\User;
use App\PrivatePhoto;
use Illuminate\Http\Request;
use App\Http\Controllers\ApiController;

class UserPhotoRequestedController extends ApiController
{
    public function __construct()
    {
        parent::__construct();

        $this->middleware('auth:api');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(User $user)
    {
        $photoRequested = PrivatePhoto::all()->where('requester_id', $user->id);

        return $this->showAll($photoRequested);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, User $user)
    {
        $rules = [
            'requested_id' => 'required|integer',
        ];

        $this->validate($request, $rules);

        $data = $request->all();
        $data['requester_id'] = $user->id;

        $requested = PrivatePhoto::where('requester_id', $user->id)
            ->where('requested_id', $request->requested_id)->first();

        if ($requested) {
            return $this->errorResponser('You already send a request for photo details of this member', 409);
        }

        if ($user->id == $request->requested_id) {
            return $this->errorResponser('The requested must be different from the requester', 409);
        }

        $photo = PrivatePhoto::create($data);

        return $this->showOne($photo);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user, $member)
    {
        $photo = PrivatePhoto::where('requester_id', $user->id)
            ->where('requested_id', $member)->first();

        $photo->delete();

        return $this->showOne($photo);
    }
}
