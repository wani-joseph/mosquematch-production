<?php

namespace App\Http\Controllers\User;

use App\User;
use App\Template;
use Illuminate\Http\Request;
use App\Http\Controllers\ApiController;
use Symfony\Component\HttpKernel\Exception\HttpException;

class UserTemplateController extends ApiController
{
    public function __construct()
    {
        parent::__construct();

        $this->middleware('auth:api');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(User $user)
    {
        $templates = $user->templates;

        return $this->showAll($templates);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, User $user)
    {
        $rules = [
            'text' => 'required',
        ];

        $this->validate($request, $rules);

        $data = $request->all();
        $data['user_id'] = $user->id;

        $template = Template::create($data);

        return $this->showOne($template);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user, Template $template)
    {
        $this->checkUser($user, $template);

        $template->delete();

        return $this->showOne($template);
    }

    protected function checkUser(User $user, Template $template)
    {
        if ($user->id != $template->user_id) {
            throw new HttpException(422, 'The specified user is not the actual creator of this template');
        }
    }
}
