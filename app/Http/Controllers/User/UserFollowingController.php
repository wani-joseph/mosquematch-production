<?php

namespace App\Http\Controllers\User;

use App\User;
use App\Favorite;
use Illuminate\Http\Request;
use App\Http\Controllers\ApiController;

class UserFollowingController extends ApiController
{
    public function __construct()
    {
        parent::__construct();

        $this->middleware('auth:api');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(User $user)
    {
        $favorites = Favorite::all()->where('follower_id', $user->id);

        return $this->showAll($favorites);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, User $user)
    {
        $rules = [
            'following_id' => 'required|integer',
        ];

        $this->validate($request, $rules);

        $data = $request->all();
        $data['follower_id'] = $user->id;

        if ($user->id == $request->following_id) {
            return $this->errorResponser('The following must be different from the follower', 409);
        }

        $favorite = Favorite::create($data);

        return $this->showOne($favorite);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user, $member)
    {
        $favorite = Favorite::where('follower_id', $user->id)
            ->where('following_id', $member)->first();

        $favorite->delete();

        return $this->showOne($favorite);
    }
}
