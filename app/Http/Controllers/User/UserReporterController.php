<?php

namespace App\Http\Controllers\User;

use App\User;
use App\Report;
use Illuminate\Http\Request;
use App\Http\Controllers\ApiController;

class UserReporterController extends ApiController
{
    public function __construct()
    {
        parent::__construct();

        $this->middleware('auth:api');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(User $user)
    {
        $reports = Report::all()->where('reported_id', $user->id);

        return $this->showAll($reports);
    }
}
