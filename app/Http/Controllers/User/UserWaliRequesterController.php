<?php

namespace App\Http\Controllers\User;

use App\User;
use App\Wali;
use Illuminate\Http\Request;
use App\Http\Controllers\ApiController;

class UserWaliRequesterController extends ApiController
{
    public function __construct()
    {
        parent::__construct();

        $this->middleware('auth:api');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(User $user)
    {
        $waliRequesters = Wali::all()->where('requested_id', $user->id);

        return $this->showAll($waliRequesters);
    }
}
