<?php

namespace App\Http\Controllers\User;

use App\User;
use App\Message;
use Illuminate\Http\Request;
use App\Http\Controllers\ApiController;

class UserMessageController extends ApiController
{
    public function __construct()
    {
        parent::__construct();

        $this->middleware('auth:api');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, User $user)
    {
        $rules = [
            'contact' => 'required|integer',
        ];

        $this->validate($request, $rules);

        $messages = Message::where(function($query) use($user, $request) {
            $query->where('sender_id', $user->id);
            $query->where('receiver_id', $request->contact);
        })->orWhere(function($query) use($user, $request) {
            $query->where('sender_id', $request->contact);
            $query->where('receiver_id', $user->id);
        })->get();

        return $this->showAll($messages);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, User $user)
    {
        $rules = [
            'receiver_id' => 'required|integer',
            'message' => 'required',
        ];

        $this->validate($request, $rules);

        $data = $request->all();
        $data['is_read'] = Message::UNREAD_MESSAGE;
        $data['sender_id'] = $user->id;

        if ($user->id == $request->receiver_id) {
            return $this->errorResponser('The receiver must be different from the sender', 409);
        }

        $message = Message::create($data);

        // broadcast(new NewMessage($message));

        return $this->showOne($message);
    }
}
