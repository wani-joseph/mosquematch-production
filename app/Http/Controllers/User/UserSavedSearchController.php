<?php

namespace App\Http\Controllers\User;

use App\User;
use App\SavedSearch;
use Illuminate\Http\Request;
use App\Http\Controllers\ApiController;
use Symfony\Component\HttpKernel\Exception\HttpException;

class UserSavedSearchController extends ApiController
{
    public function __construct()
    {
        parent::__construct();

        $this->middleware('auth:api');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(User $user)
    {
        $savedSearches = $user->savedSearches;

        return $this->showAll($savedSearches);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, User $user)
    {
        $rules = [
            'name' => 'required',
            'url' => 'required',
        ];

        $this->validate($request, $rules);

        $data = $request->all();
        $data['user_id'] = $user->id;

        $savedSearch = SavedSearch::create($data);

        return $this->showOne($savedSearch);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user, SavedSearch $savedSearch)
    {
        $this->checkUser($user, $savedSearch);

        $savedSearch->delete();

        return $this->showOne($savedSearch);
    }

    protected function checkUser(User $user, SavedSearch $savedSearch)
    {
        if ($user->id != $savedSearch->user_id) {
            throw new HttpException(422, 'The specified user is not the actual creator of this saved search');
        }
    }
}
