<?php

namespace App\Http\Controllers\User;

use App\User;
use App\Visit;
use Illuminate\Http\Request;
use App\Http\Controllers\ApiController;

class UserVisitorController extends ApiController
{
    public function __construct()
    {
        parent::__construct();

        $this->middleware('auth:api');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(User $user)
    {
        $visitors = Visit::all()->where('visited_id', $user->id);

        return $this->showAll($visitors);
    }
}
