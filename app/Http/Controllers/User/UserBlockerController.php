<?php

namespace App\Http\Controllers\User;

use App\User;
use App\Block;
use Illuminate\Http\Request;
use App\Http\Controllers\ApiController;

class UserBlockerController extends ApiController
{
    public function __construct()
    {
        parent::__construct();

        $this->middleware('auth:api');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(User $user)
    {
        $blockers = Block::all()->where('blocked_id', $user->id);

        return $this->showAll($blockers);
    }
}
