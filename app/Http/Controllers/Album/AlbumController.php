<?php

namespace App\Http\Controllers\Album;

use App\Album;
use App\Http\Controllers\ApiController;
use Illuminate\Http\Request;

class AlbumController extends ApiController
{
    public function __construct()
    {
        parent::__construct();

        $this->middleware('auth:api');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $albums = Album::all();

        return $this->showAll($albums);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Album  $album
     * @return \Illuminate\Http\Response
     */
    public function show(Album $album)
    {
        return $this->showOne($album);
    }
}
