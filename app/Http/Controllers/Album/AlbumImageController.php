<?php

namespace App\Http\Controllers\Album;

use App\Album;
use App\Image;
use Illuminate\Http\Request;
use App\Http\Controllers\ApiController;
use Illuminate\Support\Facades\Storage;
use Symfony\Component\HttpKernel\Exception\HttpException;

class AlbumImageController extends ApiController
{
    public function __construct()
    {
        parent::__construct();

        $this->middleware('auth:api');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Album $album)
    {
        $images = $album->images;

        return $this->showAll($images);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Album $album)
    {
        $rules = [
            'image' => 'required|image'
        ];

        $this->validate($request, $rules);

        $data = $request->all();
        $data['image'] = $request->image->store('');
        $data['album_id'] = $album->id;

        $image = Image::create($data);

        return $this->showOne($image);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Album  $album
     * @return \Illuminate\Http\Response
     */
    public function destroy(Album $album, Image $image)
    {
        $this->checkAlbum($album, $image);

        $image->delete();
        Storage::delete($image->image);

        return $this->showOne($image);
    }

    protected function checkAlbum(Album $album, Image $image)
    {
        if ($album->id != $image->album_id) {
            throw new HttpException(422, 'The specified album is not the album of this image');
        }
    }
}
