<?php

namespace App\Http\Controllers\PrivatePhoto;

use App\Http\Controllers\ApiController;
use App\PrivatePhoto;
use Illuminate\Http\Request;

class PrivatePhotoController extends ApiController
{
    public function __construct()
    {
        parent::__construct();

        $this->middleware('auth:api');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $privatePhotos = PrivatePhoto::all();

        return $this->showAll($privatePhotos);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\PrivatePhoto  $privatePhoto
     * @return \Illuminate\Http\Response
     */
    public function show(PrivatePhoto $privatePhoto)
    {
        return $this->showOne($privatePhoto);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\PrivatePhoto  $privatePhoto
     * @return \Illuminate\Http\Response
     */
    public function update(PrivatePhoto $privatePhoto)
    {
        $privatePhoto->status = PrivatePhoto::CONFIRMED_REQUEST;
        $privatePhoto->save();
    }
}
