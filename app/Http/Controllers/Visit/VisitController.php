<?php

namespace App\Http\Controllers\Visit;

use App\Http\Controllers\ApiController;
use App\Visit;
use Illuminate\Http\Request;

class VisitController extends ApiController
{
    public function __construct()
    {
        parent::__construct();

        $this->middleware('auth:api');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $visits = Visit::all();

        return $this->showAll($visits);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Visit  $visit
     * @return \Illuminate\Http\Response
     */
    public function show(Visit $visit)
    {
        return $this->showOne($visit);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Visit  $visit
     * @return \Illuminate\Http\Response
     */
    public function update(Visit $visit)
    {
        $visit->status = Visit::CONFIRMED_VISIT;
        $visit->save();
    }
}
