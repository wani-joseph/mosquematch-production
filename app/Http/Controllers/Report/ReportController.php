<?php

namespace App\Http\Controllers\Report;

use App\Http\Controllers\ApiController;
use App\Report;
use Illuminate\Http\Request;

class ReportController extends ApiController
{
    public function __construct()
    {
        parent::__construct();

        $this->middleware('auth:api');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $reports = Report::all();

        return $this->showAll($reports);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Report  $report
     * @return \Illuminate\Http\Response
     */
    public function show(Report $report)
    {
        return $this->showOne($report);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Report  $report
     * @return \Illuminate\Http\Response
     */
    public function update(Report $report)
    {
        $report->status = Report::CONFIRMED_REPORT;
        $report->save();
    }
}
