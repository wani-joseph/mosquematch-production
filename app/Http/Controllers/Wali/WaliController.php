<?php

namespace App\Http\Controllers\Wali;

use App\Http\Controllers\ApiController;
use App\Wali;
use Illuminate\Http\Request;

class WaliController extends ApiController
{
    public function __construct()
    {
        parent::__construct();

        $this->middleware('auth:api');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $walis = Wali::all();

        return $this->showAll($walis);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Wali  $wali
     * @return \Illuminate\Http\Response
     */
    public function show(Wali $wali)
    {
        return $this->showOne($wali);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Wali  $wali
     * @return \Illuminate\Http\Response
     */
    public function update(Wali $wali)
    {
        $wali->status = Wali::CONFIRMED_REQUEST;
        $wali->save();

        return $this->showOne($wali);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Wali  $wali
     * @return \Illuminate\Http\Response
     */
    public function destroy(Wali $wali)
    {
        $wali->delete();

        return $this->showOne($wali);
    }
}
