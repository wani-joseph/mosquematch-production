<?php

namespace App\Http\Controllers\Favorite;

use App\Favorite;
use App\Http\Controllers\ApiController;
use Illuminate\Http\Request;

class FavoriteController extends ApiController
{
    public function __construct()
    {
        parent::__construct();

        $this->middleware('auth:api');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $favorites = Favorite::all();

        return $this->showAll($favorites);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Favorite  $favorite
     * @return \Illuminate\Http\Response
     */
    public function show(Favorite $favorite)
    {
        return $this->showOne($favorite);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Favorite  $favorite
     * @return \Illuminate\Http\Response
     */
    public function update(Favorite $favorite)
    {
        $favorite->status = Favorite::CONFIRMED_FAVORITE;
        $favorite->save();
    }
}
