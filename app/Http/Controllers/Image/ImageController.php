<?php

namespace App\Http\Controllers\Image;

use App\Http\Controllers\ApiController;
use App\Image;
use Illuminate\Http\Request;

class ImageController extends ApiController
{
    public function __construct()
    {
        parent::__construct();

        $this->middleware('auth:api');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $images = Image::all();

        return $this->showAll($images);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Image  $image
     * @return \Illuminate\Http\Response
     */
    public function show(Image $image)
    {
        return $this->showOne($image);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Image  $image
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Image $image)
    {
        $rules = [
            'album_id' => 'integer'
        ];

        $this->validate($request, $rules);

        if ($request->has('album_id')) {
            $image->album_id = $request->album_id;
        }

        $image->save();

        return $this->showOne($image);
    }
}
