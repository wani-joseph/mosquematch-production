<?php

namespace App\Http\Controllers\Template;

use App\Http\Controllers\ApiController;
use App\Template;
use Illuminate\Http\Request;

class TemplateController extends ApiController
{
    public function __construct()
    {
        parent::__construct();

        $this->middleware('auth:api');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $templates = Template::all();

        return $this->showAll($templates);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Template  $template
     * @return \Illuminate\Http\Response
     */
    public function show(Template $template)
    {
        return $this->showOne($template);
    }
}
