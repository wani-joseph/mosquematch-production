<?php

namespace App\Http\Controllers\Mosque;

use App\User;
use App\Mosque;
use Illuminate\Http\Request;
use App\Mail\ConfirmMembership;
use App\Http\Controllers\ApiController;
use Illuminate\Support\Facades\Mail;

class MosqueUserConfirmController extends ApiController
{
    public function __construct()
    {
        parent::__construct();

        $this->middleware('auth:api');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Mosque $mosque, User $user)
    {
        Mail::to($user->email)->send(new ConfirmMembership($user, $mosque));

        return $this->showMessage('Verification link from '. $mosque->imam .' to '. $user->firstname .' has been sent successfully');
    }
}
