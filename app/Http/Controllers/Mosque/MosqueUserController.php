<?php

namespace App\Http\Controllers\Mosque;

use App\User;
use App\Mosque;
use Illuminate\Http\Request;
use App\Mail\MosqueUserCreated;
use Illuminate\Support\Facades\Mail;
use App\Http\Controllers\ApiController;

class MosqueUserController extends ApiController
{
    public function __construct()
    {
        parent::__construct();

        $this->middleware('auth:api');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Mosque $mosque)
    {
        $members = $mosque->members;

        return $this->showAll($members);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Mosque  $mosque
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Mosque $mosque, User $user)
    {
         /**
         * ManyToMany Relationships interaction methods
         * 
         * attach, sync, syncWithoutDetaching
         */
        if ($mosque->members()->find($user->id)) {
            return $this->errorResponser('The specified members is already a member of this mosque', 400);
        }

        $mosque->members()->syncWithoutDetaching($user->id);

        Mail::to($mosque->email)->send(new MosqueUserCreated($user, $mosque));

        return $this->showAll($mosque->members);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Mosque  $mosque
     * @return \Illuminate\Http\Response
     */
    public function destroy(Mosque $mosque, User $user)
    {
        if (!$mosque->members()->find($user->id)) {
            return $this->errorResponser('The specified member is not belong to this mosque', 404);
        }

        $mosque->members()->detach([$user->id]);

        return $this->showAll($mosque->members);
    }
}
