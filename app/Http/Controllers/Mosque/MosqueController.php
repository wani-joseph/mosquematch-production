<?php

namespace App\Http\Controllers\Mosque;

use App\Http\Controllers\ApiController;
use App\Mosque;
use Illuminate\Http\Request;

class MosqueController extends ApiController
{
    public function __construct()
    {
        parent::__construct();

        $this->middleware('auth:api');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $mosques = Mosque::all();

        return $this->showAll($mosques);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'name'      => 'required',
            'imam'      => 'required',
            'region'    => 'required',
            'city'      => 'required',
            'address'   => 'required',
            'post_code' => 'required',
            'contact'   => 'required',
            'email'     => 'required|email|unique:mosques',
        ];

        $this->validate($request, $rules);

        $data = $request->all();

        if ($request->has('website')) {
            $data['website'] = $request->website;
        }

        $mosque = Mosque::create($data);

        return $this->showOne($mosque, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Mosque  $mosque
     * @return \Illuminate\Http\Response
     */
    public function show(Mosque $mosque)
    {
        return $this->showOne($mosque);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Mosque  $mosque
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Mosque $mosque)
    {
        $rules = [
            'email'     => 'email|unique:mosques,email,' . $mosque->id,
        ];

        $this->validate($request, $rules);

        if ($request->has('name')) {
            $mosque->name = $request->name;
        }

        if ($request->has('imam')) {
            $mosque->imam = $request->imam;
        }

        if ($request->has('region')) {
            $mosque->region = $request->region;
        }

        if ($request->has('city')) {
            $mosque->city = $request->city;
        }

        if ($request->has('address')) {
            $mosque->address = $request->address;
        }

        if ($request->has('post_code')) {
            $mosque->post_code = $request->post_code;
        }

        if ($request->has('contact')) {
            $mosque->contact = $request->contact;
        }

        if ($request->has('email') && $mosque->email != $request->email) {
            $mosque->email = $request->email;
        }

        if ($request->has('website')) {
            $mosque->website = $request->website;
        }

        if ($mosque->isClean()) {
            return $this->errorResponser('You need to specify different field to update', 422);
        }

        $mosque->save();

        return $this->showOne($mosque);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Mosque  $mosque
     * @return \Illuminate\Http\Response
     */
    public function destroy(Mosque $mosque)
    {
        $mosque->delete();

        return $this->showOne($mosque);
    }

    public function getSearch(Request $request)
    {
        $search =  $request->search;

        $mosques = '';

        if (trim($request->search)) {
            $mosques = Mosque::where('name','LIKE',"%{$search}%")
                         ->orderBy('created_at','DESC')->limit(15)->get();         
        }

        return $this->showAll($mosques);
    }

    public function getMosquesByLocation(Request $request)
    {
        $search =  $request->search;

        $mosques = '';

        if (trim($request->search)) {
            $mosques = Mosque::where('city','LIKE',"%{$search}%")
                         ->orderBy('created_at','DESC')->limit(15)->get();         
        }

        return $this->showAll($mosques);
    }
}
