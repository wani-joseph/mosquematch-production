<?php

namespace App\Http\Controllers\Message;

use App\Http\Controllers\ApiController;
use App\Message;
use Illuminate\Http\Request;

class MessageController extends ApiController
{
    public function __construct()
    {
        parent::__construct();

        $this->middleware('auth:api');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $messages = Message::all();

        return $this->showAll($messages);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Message  $message
     * @return \Illuminate\Http\Response
     */
    public function show(Message $message)
    {
        return $this->showOne($message);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Message $message)
    {
        $message->is_read = Message::READ_MESSAGE;

        $message->save();

        return $this->showOne($message);
    }
}
