<?php

namespace App\Http\Controllers\Block;

use App\Block;
use App\Http\Controllers\ApiController;
use Illuminate\Http\Request;

class BlockController extends ApiController
{
    public function __construct()
    {
        parent::__construct();

        $this->middleware('auth:api');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $blocks = Block::all();

        return $this->showAll($blocks);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Block  $block
     * @return \Illuminate\Http\Response
     */
    public function show(Block $block)
    {
        return $this->showOne($block);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Block  $block
     * @return \Illuminate\Http\Response
     */
    public function update(Block $block)
    {
        $block->status = Block::CONFIRMED_BLOCK;
        $block->save();
    }
}
