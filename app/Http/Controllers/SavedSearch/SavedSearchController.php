<?php

namespace App\Http\Controllers\SavedSearch;

use App\Http\Controllers\ApiController;
use App\SavedSearch;
use Illuminate\Http\Request;

class SavedSearchController extends ApiController
{
    public function __construct()
    {
        parent::__construct();

        $this->middleware('auth:api');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $savedSearches = SavedSearch::all();

        return $this->showAll($savedSearches);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\SavedSearch  $savedSearch
     * @return \Illuminate\Http\Response
     */
    public function show(SavedSearch $savedSearch)
    {
        return $this->showOne($savedSearch);
    }
}
