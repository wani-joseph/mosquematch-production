<?php

namespace App\Http\Controllers\Auth;

use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\ApiController;

class ApiAuthController extends ApiController
{
    public function __construct()
    {
        parent::__construct();

        $this->middleware('auth:api')->except(['register', 'login']);
    }
    
    public function register(Request $request) {
        $rules = [
            'firstname' => 'required',
            'lastname'  => 'required',
            'email'     => 'required|email|unique:users',
            'password'  => 'required|min:6|confirmed',
        ];

        $this->validate($request, $rules);

        $data = $request->all();
        $data['password'] = bcrypt($request['password']);

        $user = User::create($data);

        $tokenResult = $user->createToken('Personal Access Token');
        $token = $tokenResult->token;
        $token->expires_at = Carbon::now()->addDays(1);
            
        $token->save();

        return $this->showMessage([
            'access_token' => $tokenResult->accessToken,
            'token_type' => 'Bearer',
            'expires_at' => Carbon::parse(
                $tokenResult->token->expires_at
            )->toDateTimeString()
        ]);
    }

    public function login(Request $request)
    {
        $rules = [
            'email'     => 'required|email',
            'password'  => 'required',
            'remember_me' => 'boolean'
        ];

        $this->validate($request, $rules);

        $credentials = request(['email', 'password']);

        if (!auth()->attempt($credentials)) {
            return $this->showMessage(['message' => 'Invalid credentials']);
        }

        $user = $request->user();

        $tokenResult = $user->createToken('Personal Access Token');
        $token = $tokenResult->token;

        if ($request->remember_me) {
            $token->expires_at = Carbon::now()->addWeeks(1);
        } else {
            $token->expires_at = Carbon::now()->addDays(1);
        }
            
        $token->save();
            
        return $this->showMessage([
            'access_token' => $tokenResult->accessToken,
            'token_type' => 'Bearer',
            'expires_at' => Carbon::parse(
                $tokenResult->token->expires_at
            )->toDateTimeString()
        ]);
    }

    public function logout (Request $request) {

        $token = $request->user()->token();
        $token->revoke();

        return $this->showMessage('You have been succesfully logged out!');
    }

    public function authUser()
    {
        $user = auth()->user();

        return $this->showOne($user);
    }
}
