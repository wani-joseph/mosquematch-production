<?php

namespace App;

use App\Transformers\WaliTransformer;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Wali extends Model
{
    use SoftDeletes;

    const PENDING_REQUEST = 'pending';
    const CONFIRMED_REQUEST = 'confirmed';

    public $transformer = WaliTransformer::class;
    protected $dates = ['deleted_at'];
    
    protected $fillable = [
        'requester_id',
        'requested_id',
        'status'
    ];

    public function confirmed(){
        return $this->status = Wali::CONFIRMED_REQUEST;
    }

    public function requester()
    {
        return $this->belongsTo(User::class);
    }

    public function requested()
    {
        return $this->belongsTo(User::class);
    }
}
