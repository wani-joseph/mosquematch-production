<?php

namespace App;

use App\Transformers\ImageTransformer;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Image extends Model
{
    use SoftDeletes;

    public $transformer = ImageTransformer::class;
    protected $dates = ['deleted_at'];
    
    protected $fillable = [
        'image',
        'album_id'
    ];

    public function album()
    {
        return $this->belongsTo(Album::class);
    }
}
