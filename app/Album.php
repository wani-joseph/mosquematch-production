<?php

namespace App;

use App\Transformers\AlbumTransformer;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Album extends Model
{
    use SoftDeletes;

    public $transformer = AlbumTransformer::class;
    protected $dates = ['deleted_at'];
    
    protected $fillable = [
        'name',
        'cover_image',
        'user_id',
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function images(){
        return $this->hasMany(Image::class);
    }
}
