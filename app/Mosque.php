<?php

namespace App;

use App\User;
use App\Transformers\MosqueTransformer;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Mosque extends Model
{
    use SoftDeletes;

    public $transformer = MosqueTransformer::class;
    protected $dates = ['deleted_at'];
    
    protected $fillable = [
        'name',
        'imam',
        'region',
        'city',
        'address',
        'post_code',
        'contact',
        'email',
        'website',
        'lat',
        'lng'
    ];

    public function members()
    {
        return $this->belongsToMany(User::class);
    }
}
