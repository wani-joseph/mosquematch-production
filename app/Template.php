<?php

namespace App;

use App\User;
use Illuminate\Database\Eloquent\Model;
use App\Transformers\TemplateTransformer;
use Illuminate\Database\Eloquent\SoftDeletes;

class Template extends Model
{
    use SoftDeletes;

    public $transformer = TemplateTransformer::class;
    protected $dates = ['deleted_at'];
    
    protected $fillable = [
        'text',
        'user_id',
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
