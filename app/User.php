<?php

namespace App;

use App\Album;
use App\Mosque;
use App\Template;
use App\SavedSearch;
use Illuminate\Support\Str;
use Laravel\Passport\HasApiTokens;
use App\Transformers\UserTransformer;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use HasApiTokens, Notifiable, SoftDeletes;

    const VERIFIED_USER = '1';
    const UNVERIFIED_USER = '0';

    const ADMIN_USER = 'true';
    const REGULAR_USER = 'false';

    const MALE_USER = 'male';
    const FEMALE_USER = 'female';

    const ONLINE_USER = 'online';
    const OFFLINE_USER = 'offline';

    public $transformer = UserTransformer::class;
    protected $dates = ['deleted_at'];

    protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'firstname',
        'lastname',
        'email',
        'password',
        'verified',
        'verification_token',
        'admin',
        'gender',
        'city',
        'day',
        'month',
        'year',
        'country_of_birth',
        'tagline',
        'about_you',
        'looking',
        'education',
        'subject',
        'job',
        'profession',
        'first_language', 
        'second_language',
        'citizenship',
        'country_of_origin',
        'relocate',
        'income',
        'marriage_within', 
        'martial_status',
        'children',
        'like_children',
        'living_arrangement',
        'height',
        'build',
        'smoke',
        'drink',
        'disabilities',
        'daily_phone_time',
        'religiousness',
        'sector',
        'hijab',
        'beard',
        'revert', 
        'halal',
        'prayer',
        'read_quran',
        'profile_image',
        'wali_name',
        'wali_contact',
        'wali_email',
        'status',
        'lat',
        'lng'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
        'verification_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function isVerfied()
    {
        return $this->verified == User::VERIFIED_USER;
    }

    public function isAdmin()
    {
        return $this->admin == User::ADMIN_USER;
    }

    public static function generateVerificationCode()
    {
        return Str::random(40);
    }

    public function setFirstnameAttribute($firstname)
    {
        $this->attributes['firstname'] = strtolower($firstname);
    }

    public function getFirstnameAttribute($firstname)
    {
        return ucfirst($firstname);
    }

    public function setLastnameAttribute($lastname)
    {
        $this->attributes['lastname'] = strtolower($lastname);
    }

    public function albums()
    {
        return $this->hasMany(Album::class);
    }

    public function blocks()
    {
        return $this->hasMany(Block::class);
    }

    public function savedSearches()
    {
        return $this->hasMany(SavedSearch::class);
    }

    public function templates()
    {
        return $this->hasMany(Template::class);
    }

    public function mosques()
    {
        return $this->belongsToMany(Mosque::class);
    }
}
