<?php

namespace App;

use App\User;
use Illuminate\Database\Eloquent\Model;
use App\Transformers\MessageTransformer;
use Illuminate\Database\Eloquent\SoftDeletes;

class Message extends Model
{
    use SoftDeletes;
    
    const READ_MESSAGE = '1';
    const UNREAD_MESSAGE = '0';

    public $transformer = MessageTransformer::class;
    protected $dates = ['deleted_at'];

    protected $fillable = [
        'sender_id',
        'receiver_id',
        'message',
        'is_read',
    ];

    public function isRead()
    {
        return $this->is_read == User::READ_MESSAGE;
    }

    public function sender()
    {
        return $this->belongsTo(User::class);
    }

    public function receiver()
    {
        return $this->belongsTo(User::class);
    }
}
