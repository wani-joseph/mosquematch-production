<?php

namespace App\Mail;

use App\User;
use App\Mosque;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class MosqueUserCreated extends Mailable
{
    use Queueable, SerializesModels;

    public $user;
    public $mosque;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(User $user, Mosque $mosque)
    {
        $this->user = $user;
        $this->mosque = $mosque;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('emails.membership-verification')
                    ->subject('Mosque Membership Verification');
    }
}