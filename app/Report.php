<?php

namespace App;

use App\User;
use App\Transformers\ReportTransformer;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Report extends Model
{
    use SoftDeletes;

    const PENDING_REPORT = 'pending';
    const CONFIRMED_REPORT = 'confirmed';

    public $transformer = ReportTransformer::class;
    protected $dates = ['deleted_at'];
    
    protected $fillable = [
        'reporter_id',
        'reported_id',
        'message',
        'status',
    ];

    public function confirmed(){
        return $this->status = Report::CONFIRMED_REPORT;
    }

    public function reporter()
    {
        return $this->belongsTo(User::class);
    }

    public function reported()
    {
        return $this->belongsTo(User::class);
    }
}
