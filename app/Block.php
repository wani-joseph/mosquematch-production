<?php

namespace App;

use App\User;
use App\Transformers\BlockTransformer;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Block extends Model
{
    use SoftDeletes;

    const PENDING_BLOCK = 'pending';
    const CONFIRMED_BLOCK = 'confirmed';

    public $transformer = BlockTransformer::class;
    protected $dates = ['deleted_at'];
    
    protected $fillable = [
        'blocker_id',
        'blocked_id',
        'status',
    ];

    public function confirmed(){
        return $this->status = Block::CONFIRMED_BLOCK;
    }

    public function blocker()
    {
        return $this->belongsTo(User::class);
    }

    public function blocked()
    {
        return $this->belongsTo(User::class);
    }
}
