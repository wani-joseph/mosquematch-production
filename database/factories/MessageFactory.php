<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\User;
use App\Message;
use Faker\Generator as Faker;

$factory->define(Message::class, function (Faker $faker) {
    $sender = User::all()->random();
    $reciever = User::all()->except($sender->id)->random();
    
    return [
        'sender_id'     => $sender->id,
        'receiver_id'   => $reciever->id,
        'message'       => rtrim($faker->sentence(rand(5, 10)), "."),
        'is_read'       => rand(0, 1),
    ];
});
