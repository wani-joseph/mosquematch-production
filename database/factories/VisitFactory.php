<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\User;
use App\Visit;
use Faker\Generator as Faker;

$factory->define(Visit::class, function (Faker $faker) {
    $visitor = User::all()->random();
    $visited = User::all()->except($visitor->id)->random();
    
    return [
        'visitor_id'  => $visitor->id,
        'visited_id'  => $visited->id,
        'status'      => Visit::PENDING_VISIT,
    ];
});
