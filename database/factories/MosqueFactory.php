<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Mosque;
use Faker\Generator as Faker;

$factory->define(Mosque::class, function (Faker $faker) {
    return [
        'name' => $faker->name . 'Mosque',
        'imam' => $faker->name,
        'region' => $faker->state,
        'city' => $faker->city,
        'address' => $faker->address,
        'post_code' => $faker->postcode,
        'contact' => $faker->e164PhoneNumber,
        'email' => $faker->unique()->safeEmail,
        'website' => $faker->unique()->url,
        'lat' => $faker->latitude($min = -90, $max = 90),
        'lng' => $faker->longitude($min = -180, $max = 180),
    ];
});
