<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\User;
use App\Wali;
use Faker\Generator as Faker;

$factory->define(Wali::class, function (Faker $faker) {
    $requester = User::all()->random();
    $requested = User::all()->except($requester->id)->random();
    
    return [
        'requester_id' => $requester->id,
        'requested_id' => $requested->id,
        'status' => Wali::PENDING_REQUEST,
    ];
});
