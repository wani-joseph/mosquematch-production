<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\User;
use App\Favorite;
use Faker\Generator as Faker;

$factory->define(Favorite::class, function (Faker $faker) {
    $follower = User::all()->random();
    $following = User::all()->except($follower->id)->random();
    
    return [
        'follower_id'   => $follower->id,
        'following_id'  => $following->id,
        'status'        => Favorite::PENDING_FAVORITE,
    ];
});
