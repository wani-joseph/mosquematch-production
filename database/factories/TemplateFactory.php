<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\User;
use App\Template;
use Faker\Generator as Faker;

$factory->define(Template::class, function (Faker $faker) {
    $user = User::all()->random();

    return [
        'text'      => rtrim($faker->sentence(rand(5, 10)), "."),
        'user_id'   => $user->id,
    ];
});
