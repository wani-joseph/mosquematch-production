<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\User;
use App\Album;
use Faker\Generator as Faker;

$factory->define(Album::class, function (Faker $faker) {
    $user = User::all()->random();

    return [
        'name' => $faker->randomElement(['profile picture', 'public', 'private']),
        'cover_image' => $user->gender == User::MALE_USER ? $faker->randomElement(['profile2.jpg','profile3.jpg','profile5.jpg', 'profile8.jpg', 'male-placeholder.jpg']) : $faker->randomElement(['profile.jpg', 'profile1.jpg', 'profile4.jpg', 'profile6.jpg', 'profile7.jpg', 'female-placeholder.jpg']),
        'user_id' => $user->id,
    ];
});
