<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\User;
use App\SavedSearch;
use Faker\Generator as Faker;

$factory->define(SavedSearch::class, function (Faker $faker) {
    $user = User::all()->random();

    return [
        'name' => $faker->name,
        'url' => $faker->url,
        'user_id'   => $user->id,
    ];
});
