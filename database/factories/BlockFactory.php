<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\User;
use App\Block;
use Faker\Generator as Faker;

$factory->define(Block::class, function (Faker $faker) {
    $blocker = User::all()->random();
    $blocked = User::all()->except($blocker->id)->random();
    
    return [
        'blocker_id'   => $blocker->id,
        'blocked_id'  => $blocked->id,
        'status'    => Block::PENDING_BLOCK,
    ];
});
