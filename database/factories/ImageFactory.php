<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Album;
use App\Image;
use Faker\Generator as Faker;

$factory->define(Image::class, function (Faker $faker) {
    $album = Album::all()->random();

    return [
        'image' => $faker->randomElement(['profile2.jpg','profile3.jpg','profile5.jpg', 'profile8.jpg', 'male-placeholder.jpg', 'profile.jpg', 'profile1.jpg', 'profile4.jpg', 'profile6.jpg', 'profile7.jpg', 'female-placeholder.jpg']),
        'album_id' => $album->id,
    ];
});
