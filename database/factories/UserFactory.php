<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\User;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(User::class, function (Faker $faker) {
    return [
        'firstname' => $faker->firstName,
        'lastname' => $faker->lastName,
        'email' => $faker->unique()->safeEmail,
        'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
        'verified' => $verified = $faker->randomElement([User::VERIFIED_USER, User::UNVERIFIED_USER]),
        'verification_token' => $verified == User::VERIFIED_USER ? null : User::generateVerificationCode(),
        'admin' => $faker->randomElement([User::ADMIN_USER, User::REGULAR_USER]),
        'remember_token' => Str::random(10),
        'gender' => $member = $faker->randomElement([User::MALE_USER, User::FEMALE_USER]),
        'city' => $faker->randomElement(['Bath','Birmingham','Bradford','Brighton and Hove','Bristol','Cambridge','Canterbury','Carlisle','Chester','Chichester','Coventry','Derby','Durham','Ely','Exeter','Gloucester','Hereford','Kingston upon Hull','Lancaster','Leeds','Leicester','Lichfield','Lincoln','Liverpool','City of London','Manchester','Newcastle upon Tyne','Norwich','Nottingham','Oxford','Peterborough','Plymouth','Portsmouth','Preston','Ripon','Salford','Salisbury','Sheffield','Southampton','St Albans','Stoke-on-Trent','Sunderland','Truro','Wakefield','Wells','Westminster','Winchester','Wolverhampton','Worcester','York', 'Bangor','Cardiff','Newport','St Davids','Swansea', 'Aberdeen','Dundee','Edinburgh','Glasgow','Inverness','Stirling', 'Armagh','Belfast','Londonderry','Lisburn','Newry']),
        'day' => $faker->randomElement(['1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12', '13', '14', '15', '16', '17', '18', '19', '20', '21', '21', '22', '23', '24', '25', '26', '27', '28', '29', '30', '31']),
        'month' => $faker->randomElement(['1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12']),
        'year' => $faker->randomElement(['1960','1961','1962','1963','1964','1965','1966','1967','1968','1969','1970','1971','1972','1973','1974','1975','1976','1977','1978','1979','1980','1981','1982','1983','1984','1985','1986','1987','1988','1989','1990','1991','1992','1993','1994','1995','1996','1997','1998','1999','2000','2001','2002','2003','2004','2005','2006','2007','2008','2009','2010','2011','2012','2013','2014','2015','2016','2017','2018','2019']),
        'country_of_birth' => $faker->randomElement(['AD', 'AE', 'AF', 'AG', 'AI', 'AL', 'AM', 'AN', 'AO', 'AQ', 'AR', 'AS', 'AT', 'AU', 'AW', 'AX', 'AZ', 'BA', 'BB', 'BD', 'BE', 'BF', 'BG', 'BH', 'HI', 'BJ', 'BL', 'BM', 'BN', 'BO', 'BQ', 'BR', 'BS', 'BT', 'BV', 'BW', 'BY', 'BZ', 'CA', 'CC', 'CD', 'CF', 'CG', 'CH', 'CK', 'CL', 'CM', 'CN', 'CO', 'CR', 'CU', 'CV', 'CW', 'CX', 'CY', 'CZ']),
        'tagline' => rtrim($faker->sentence(rand(5, 10)), "."),
        'about_you' => rtrim($faker->sentence(rand(10, 20)), "."),
        'looking' => $faker->randomElement(['marriage as soon as posible', 'marriage this year', "marriage next year", "marriage"]),
        'education' => $faker->randomElement(['Pre-preparatory','Preparatory or Junior','Senior (Public)','Foundation diploma','Higher diploma','Advanced diploma','International Baccalaureate','Higher National Certificate (HNC)','Higher National Diploma (HND)','Foundation degree','Graduate certificate','Graduate diploma','Professional Graduate Certificate',"Ordinary bachelor's degree","Bachelor's degree with honours","Postgraduate certificate","Postgraduate diploma","Integrated master's degree","Master's degree","Doctorates"]),
        'subject' => $faker->randomElement(["software engineering", "computer science", "information systems"]),
        'job' => $faker->randomElement(['Aircraft Mechanic','Airline Pilot','Airport Security Screener','Flight Attendant','Cashier','Customer Service Representative','Retail Salesperson','Retail Supervisor','Animal Groomer','Auto Mechanic','Cosmetologist','Fitness Trainer','Funeral Director','Hairdressers, Hairstylists, and Cosmetologists','Marriage and Family Therapist','Veterinarian','Correctional Officer','Court Reporter','Firefighter','Fundraiser','Judge','Lawyer','Paralegal and Legal Assistant','Police Officer','Postal Service Worker','Security Guard','Social Worker','Computer Programmer','Computer Systems Analyst','Database Administrator','Software Developer','Web Developer','Consultant','Human Resources Manager','Biomedical Engineer','Cardiovascular Technologist','Dental Hygienist','Dentist','Diagnostic Medical Sonographer','Dietitian/Nutritionist','Doctor','EMTs and Paramedics','Health Educator','Home Health Aide','Licensed Practical Nurse','Medical Assistant','Medical Laboratory Technician','Occupational Therapist','Pharmacist','Pharmacy Technician','Physician Assistant','Physical Therapist','Physical Therapy Assistant','Registered Nurse','Bartender','Chef','Waiter/Waitress','Accountant','Actuary','Bank Teller','Bookkeeping','Accounting','Auditing Clerks','Budget Analyst','Claims Adjuster','Appraiser','Examiner','Investigator','Financial Advisor','Insurance Underwriter','Loan Officer','Chemist','Curator','Guidance Counselor','Hydrologist','Librarian','Special Education Teacher','School Principal','Teacher','Teacher Assistant','Fashion Designer','Interior Designer','Photographer','Architect','Construction Laborer','Electrician','Environmental Engineer','Janitor','Mechanical Engineer','Plumber','Advertising Managers and Promotions Managers','Advertising Sales Agent','Editor','Graphic Designer','Interpreter and Translator','Public Relations Specialist','Social Media Manager','Writer and Editor','Event/Meeting Planner','Purchasing Manager','Receptionist','Secretary / Administrative Assistant','Market Research Analyst']),
        'profession' => $faker->randomElement(['Administrative/Management','Advertising/Marketing/Communications','Construction/Building Trades/Engineering','Creative Arts/Design','Education/Research/Academia','Financial Services','Food and Hospitality Services','Healthcare/Medical Research','Human Resources/Consulting','Information Technology (IT)','Legal Services/Government/Non-Profit','Personal Services','Retail Sales / Customer Service','Transportation']),
        'first_language' => $faker->randomElement(['Acholi','Afrikaans','Albanian','Amharic','Arabic','Ashante','Assyrian','Azerbaijani','Azeri','Bajuni','Basque','Behdini','Belorussian','Bengali','Berber','Bosnian','Bravanese','Bulgarian','Burmese','Cakchiquel','Cambodian','Cantonese','Catalan','Chaldean','Chamorro','Chao-chow','Chavacano','Chuukese','Croatian','Czech','Danish','Dari','Dinka','Diula','Dutch','English','Estonian','Espanol','Fante','Farsi','Finnish','Flemish','French','Fukienese','Fula','Fulani','Fuzhou','Gaddang','Gaelic','Gaelic-irish','Gaelic-scottish','Georgian','German','Gorani','Greek','Gujarati','Haitian Creole','Hakka','Hakka-chinese','Hausa','Hebrew','Hindi','Hmong','Hungarian','Ibanag','Icelandic','Igbo','Ilocano','Indonesian','Inuktitut','Italian','Jakartanese','Japanese','Javanese','Kanjobal','Karen','Karenni','Kashmiri','Kazakh','Kikuyu','Kinyarwanda','Kirundi','Korean','Kosovan','Kotokoli','Krio','Kurdish','Kurmanji','Kyrgyz','Lakota','Laotian','Latvian','Lingala','Lithuanian','Luganda','Maay','Macedonian','Malay','Malayalam','Maltese','Mandarin','Mandingo','Mandinka','Marathi','Marshallese','Mirpuri','Mixteco','Moldavan','Mongolian','Montenegrin','Navajo','Neapolitan','Nepali','Nigerian Pidgin','Norwegian','Oromo','Pahari','Papago','Papiamento','Pashto','Patois','Pidgin English','Polish','Portug creole','Portuguese','Pothwari','Pulaar','Punjabi','Putian','Quichua','Romanian','Russian','Samoan','Serbian','Shanghainese','Shona','Sichuan','Sicilian','Sinhalese','Slovak','Somali','Sorani','Spanish','Sudanese Arabic','Sundanese','Susu','Swahili','Swedish','Sylhetti','Tagalog','Tajik','Tamil','Telugu','Thai','Tibetan','Tigre','Tigrinya','Toishanese','Tongan','Toucouleur','Tshiluba','Turkish','Ukrainian','Urdu','Uyghur','Uzbek','Vietnamese','Visayan','Welsh','Wolof','Yiddish','Yoruba','Yupik']), 
        'second_language' => $faker->randomElement(['Acholi','Afrikaans','Albanian','Amharic','Arabic','Ashante','Assyrian','Azerbaijani','Azeri','Bajuni','Basque','Behdini','Belorussian','Bengali','Berber','Bosnian','Bravanese','Bulgarian','Burmese','Cakchiquel','Cambodian','Cantonese','Catalan','Chaldean','Chamorro','Chao-chow','Chavacano','Chuukese','Croatian','Czech','Danish','Dari','Dinka','Diula','Dutch','English','Estonian','Espanol','Fante','Farsi','Finnish','Flemish','French','Fukienese','Fula','Fulani','Fuzhou','Gaddang','Gaelic','Gaelic-irish','Gaelic-scottish','Georgian','German','Gorani','Greek','Gujarati','Haitian Creole','Hakka','Hakka-chinese','Hausa','Hebrew','Hindi','Hmong','Hungarian','Ibanag','Icelandic','Igbo','Ilocano','Indonesian','Inuktitut','Italian','Jakartanese','Japanese','Javanese','Kanjobal','Karen','Karenni','Kashmiri','Kazakh','Kikuyu','Kinyarwanda','Kirundi','Korean','Kosovan','Kotokoli','Krio','Kurdish','Kurmanji','Kyrgyz','Lakota','Laotian','Latvian','Lingala','Lithuanian','Luganda','Maay','Macedonian','Malay','Malayalam','Maltese','Mandarin','Mandingo','Mandinka','Marathi','Marshallese','Mirpuri','Mixteco','Moldavan','Mongolian','Montenegrin','Navajo','Neapolitan','Nepali','Nigerian Pidgin','Norwegian','Oromo','Pahari','Papago','Papiamento','Pashto','Patois','Pidgin English','Polish','Portug creole','Portuguese','Pothwari','Pulaar','Punjabi','Putian','Quichua','Romanian','Russian','Samoan','Serbian','Shanghainese','Shona','Sichuan','Sicilian','Sinhalese','Slovak','Somali','Sorani','Spanish','Sudanese Arabic','Sundanese','Susu','Swahili','Swedish','Sylhetti','Tagalog','Tajik','Tamil','Telugu','Thai','Tibetan','Tigre','Tigrinya','Toishanese','Tongan','Toucouleur','Tshiluba','Turkish','Ukrainian','Urdu','Uyghur','Uzbek','Vietnamese','Visayan','Welsh','Wolof','Yiddish','Yoruba','Yupik']),
        'citizenship' => 'GB',
        'country_of_origin' => $faker->randomElement(['AD', 'AE', 'AF', 'AG', 'AI', 'AL', 'AM', 'AN', 'AO', 'AQ', 'AR', 'AS', 'AT', 'AU', 'AW', 'AX', 'AZ', 'BA', 'BB', 'BD', 'BE', 'BF', 'BG', 'BH', 'HI', 'BJ', 'BL', 'BM', 'BN', 'BO', 'BQ', 'BR', 'BS', 'BT', 'BV', 'BW', 'BY', 'BZ', 'CA', 'CC', 'CD', 'CF', 'CG', 'CH', 'CK', 'CL', 'CM', 'CN', 'CO', 'CR', 'CU', 'CV', 'CW', 'CX', 'CY', 'CZ']),
        'relocate' => $faker->randomElement(['AD', 'AE', 'AF', 'AG', 'AI', 'AL', 'AM', 'AN', 'AO', 'AQ', 'AR', 'AS', 'AT', 'AU', 'AW', 'AX', 'AZ', 'BA', 'BB', 'BD', 'BE', 'BF', 'BG', 'BH', 'HI', 'BJ', 'BL', 'BM', 'BN', 'BO', 'BQ', 'BR', 'BS', 'BT', 'BV', 'BW', 'BY', 'BZ', 'CA', 'CC', 'CD', 'CF', 'CG', 'CH', 'CK', 'CL', 'CM', 'CN', 'CO', 'CR', 'CU', 'CV', 'CW', 'CX', 'CY', 'CZ']),
        'income' => $faker->randomElement(['Below Average', 'Average', 'Above Average']),
        'marriage_within' => $faker->randomElement(['This year', 'Next year', 'As soon as posible', 'Any Time']), 
        'martial_status' => $faker->randomElement(['Never married', 'Legally married', 'Divorced', 'Widowed', 'Analled']),
        'children' => $faker->randomElement(['Yes', 'No']),
        'like_children' => $faker->randomElement(['Yes I want to have', "No I don't want to have"]),
        'living_arrangement' => $faker->randomElement(['Alone', 'Family', 'Friends', 'Other']),
        'height' => $faker->numberBetween(160, 210) . "cm",
        'build' => $faker->randomElement(['Petite', 'Slim', 'Athletic', 'Medium', 'Muscular', 'Large']),
        'smoke' => $faker->randomElement(['No', 'Yes', 'Sometimes', 'Stopped']),
        'drink' => $faker->randomElement(['No', 'Yes', 'Sometimes', 'Stopped']),
        'disabilities' => $faker->randomElement(['No', 'Yes', 'Speech language', 'Hearing loss', 'Vision loss', 'Physical disability', 'Learning disability', 'Mental illness', 'Chronic disability', 'Autism', 'Ask me']),
        'daily_phone_time' => $faker->randomElement(['Attached to my phone', 'Regular use', 'Not much']),
        'religiousness' => $faker->randomElement(['Very religious', 'Religious', 'Somewhat religious', 'Not religious', 'Prefer not say']),
        'sector' => $faker->randomElement(['Just muslim', 'Sunni', 'Shia', 'Other']),
        'hijab' => $faker->randomElement(['Yes Hijab', 'Yes Niqab', 'No', 'Other']),
        'beard' => $faker->randomElement(['Yes', 'No']),
        'revert' => $faker->randomElement(['Yes', 'No']),
        'halal' => $faker->randomElement(['Always keep halal', 'Usually keep halal', 'I keep halal at home', "Don't keep halal"]),
        'prayer' => $faker->randomElement(['Always', 'Usually', 'Sometimes', 'Never']),
        'read_quran' => $faker->randomElement(['Always', 'Usually', 'Sometimes', 'Never']),
        'profile_image' => $member == User::MALE_USER ? $faker->randomElement(['profile2.jpg','profile3.jpg','profile5.jpg', 'profile8.jpg', 'male-placeholder.jpg']) : $faker->randomElement(['profile.jpg', 'profile1.jpg', 'profile4.jpg', 'profile6.jpg', 'profile7.jpg', 'female-placeholder.jpg']),
        'wali_name' => $faker->name,
        'wali_contact' => $faker->numberBetween(1000000000, 9000000000),
        'wali_email' => $faker->unique()->safeEmail,
        'status' => User::OFFLINE_USER,
        'lat' => $faker->latitude($min = -90, $max = 90),
        'lng' => $faker->longitude($min = -180, $max = 180),
    ];
});
