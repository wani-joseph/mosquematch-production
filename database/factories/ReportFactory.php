<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\User;
use App\Report;
use Faker\Generator as Faker;

$factory->define(Report::class, function (Faker $faker) {
    $reporter = User::all()->random();
    $reported = User::all()->except($reporter->id)->random();
    
    return [
        'reporter_id'   => $reporter->id,
        'reported_id'   => $reported->id,
        'message'       => rtrim($faker->sentence(rand(5, 10)), "."),
        'status'        => Report::PENDING_REPORT,
    ];
});
