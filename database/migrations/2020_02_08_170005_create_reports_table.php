<?php

use App\Report;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReportsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reports', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('reporter_id')->unsigned();
            $table->bigInteger('reported_id')->unsigned();
            $table->text('message');
            $table->string('status')->default(Report::PENDING_REPORT);
            $table->timestamps();
            $table->softDeletes(); // deleted_at

            $table->foreign('reporter_id')->references('id')->on('users');
            $table->foreign('reported_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reports');
    }
}
