<?php

use App\Visit;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVisitsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('visits', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('visitor_id')->unsigned();
            $table->bigInteger('visited_id')->unsigned();
            $table->string('status')->default(Visit::PENDING_VISIT);
            $table->timestamps();
            $table->softDeletes(); // deleted_at

            $table->foreign('visitor_id')->references('id')->on('users');
            $table->foreign('visited_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('visits');
    }
}
