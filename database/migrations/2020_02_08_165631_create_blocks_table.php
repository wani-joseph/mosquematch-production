<?php

use App\Block;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBlocksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('blocks', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('blocker_id')->unsigned();
            $table->bigInteger('blocked_id')->unsigned();
            $table->string('status')->default(Block::PENDING_BLOCK);
            $table->timestamps();
            $table->softDeletes(); // deleted_at

            $table->foreign('blocker_id')->references('id')->on('users');
            $table->foreign('blocked_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('blocks');
    }
}
