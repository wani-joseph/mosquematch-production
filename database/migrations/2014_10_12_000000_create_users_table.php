<?php

use App\User;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('firstname');
            $table->string('lastname');
            $table->string('email')->unique();
            $table->string('password');
            $table->string('verification_token')->nullable();
            $table->string('verified')->default(User::UNVERIFIED_USER);
            $table->string('admin')->default(User::REGULAR_USER);
            $table->string('gender')->nullable();
            $table->string('day')->nullable();
            $table->string('month')->nullable();
            $table->string('year')->nullable();
            $table->string('city')->nullable();
            $table->string('country_of_birth')->nullable();
            $table->string('tagline')->nullable();
            $table->text('about_you')->nullable();
            $table->string('looking')->nullable();
            $table->string('education')->nullable();
            $table->string('subject')->nullable();
            $table->string('job')->nullable();
            $table->string('profession')->nullable();
            $table->string('first_language')->nullable();
            $table->string('second_language')->nullable();
            $table->string('citizenship')->nullable();
            $table->string('country_of_origin')->nullable();
            $table->string('relocate')->nullable();
            $table->string('income')->nullable();
            $table->string('marriage_within')->nullable();
            $table->string('martial_status')->nullable();
            $table->string('children')->nullable();
            $table->string('like_children')->nullable();
            $table->string('living_arrangement')->nullable();
            $table->string('height')->nullable();
            $table->string('build')->nullable();
            $table->string('smoke')->nullable();
            $table->string('drink')->nullable();
            $table->string('disabilities')->nullable();
            $table->string('daily_phone_time')->nullable();
            $table->string('religiousness')->nullable();
            $table->string('sector')->nullable();
            $table->string('hijab')->nullable();
            $table->string('beard')->nullable();
            $table->string('revert')->nullable();
            $table->string('halal')->nullable();
            $table->string('prayer')->nullable();
            $table->string('read_quran')->nullable();
            $table->string('profile_image')->nullable();
            $table->string('wali_name')->nullable();
            $table->string('wali_contact')->nullable();
            $table->string('wali_email')->nullable();
            $table->string('status')->nullable();
            $table->string('lat')->nullable();
            $table->string('lng')->nullable();
            $table->rememberToken();
            $table->timestamps();
            $table->softDeletes(); // deleted_at
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
