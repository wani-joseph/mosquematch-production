<?php

use App\Wali;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWalisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('walis', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('requester_id')->unsigned();
            $table->bigInteger('requested_id')->unsigned();
            $table->string('status')->default(Wali::PENDING_REQUEST);
            $table->timestamps();
            $table->softDeletes(); // deleted_at

            $table->foreign('requester_id')->references('id')->on('users');
            $table->foreign('requested_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('walis');
    }
}
