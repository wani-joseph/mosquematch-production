<?php

use App\PrivatePhoto;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePrivatePhotosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('private_photos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('requester_id')->unsigned();
            $table->bigInteger('requested_id')->unsigned();
            $table->string('status')->default(PrivatePhoto::PENDING_REQUEST);
            $table->timestamps();
            $table->softDeletes(); // deleted_at

            $table->foreign('requester_id')->references('id')->on('users');
            $table->foreign('requested_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('private_photos');
    }
}
