<?php

use App\User;
use App\Wali;
use App\Album;
use App\Block;
use App\Image;
use App\Visit;
use App\Mosque;
use App\Report;
use App\Message;
use App\Favorite;
use App\Template;
use App\SavedSearch;
use App\PrivatePhoto;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS = 0');
        User::truncate();
        Album::truncate();
        Image::truncate();
        Block::truncate();
        Favorite::truncate();
        Message::truncate();
        PrivatePhoto::truncate();
        Report::truncate();
        SavedSearch::truncate();
        Template::truncate();
        Visit::truncate();
        Wali::truncate();
        Mosque::truncate();
        DB::table('mosque_user')->truncate();

        $userQuantities = 50;
        $albumQuantities = 150;
        $imageQuantities = 600;
        $blockQuantities = 100;
        $favoriteQuantities = 100;
        $messageQuantities = 200;
        $privatePhotoQuantities = 200;
        $reportQuantities = 100;
        $savedSearchQuantities = 100;
        $templateQuantities = 100;
        $visitQuantities = 200;
        $waliRequestQuantities = 200;
        $mosqueQuantities = 100;

        factory(User::class, $userQuantities)->create();
        factory(Album::class, $albumQuantities)->create();
        factory(Image::class, $imageQuantities)->create();
        factory(Block::class, $blockQuantities)->create();
        factory(Favorite::class, $favoriteQuantities)->create();
        factory(Message::class, $messageQuantities)->create();
        factory(PrivatePhoto::class, $privatePhotoQuantities)->create();
        factory(Report::class, $reportQuantities)->create();
        factory(SavedSearch::class, $savedSearchQuantities)->create();
        factory(Template::class, $templateQuantities)->create();
        factory(Visit::class, $visitQuantities)->create();
        factory(Wali::class, $waliRequestQuantities)->create();
        factory(Mosque::class, $mosqueQuantities)->create()->each(
            function ($mosque) {
                $users = User::all()->random(mt_rand(1, 5))->pluck('id');

                $mosque->members()->attach($users);
            });
    }
}
